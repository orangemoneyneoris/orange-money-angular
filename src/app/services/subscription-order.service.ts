import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {NEVER, Observable} from "rxjs";
import {JSONResponse} from "../domain/JSONResponse";
import {SubscriptionOrder} from "../domain/SubscriptionOrder";
import {PopupObj} from "../util/PopupObj";
import {CommonsService} from "./commons.service";
import {OrdOrder} from "../domain/OrdOrder";
import {WalletDocument} from "../domain/WalletDocument";
import {UpgradeOrder} from "../domain/UpgradeOrder";
import {TranslateService} from "@ngx-translate/core";
import {catchError, switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SubscriptionOrderService {

  order: SubscriptionOrder;
  orderUpgrade: UpgradeOrder;
  private _contractPreviewed: boolean;
  private _contractGenerated: boolean = false;
  private _contractUploaded: boolean = false;
  private _orderfrozen: boolean = false;
  private _orderValid: boolean;

  private sendDocumentUrl = environment.sendDocumentEndPoint;
  private deleteDocumentUrl = environment.deleteDocumentEndPoint;
  private ordSub: SubscriptionOrder;

  get orderfrozen(): boolean {
    return this._orderfrozen;
  }

  set orderfrozen(value: boolean) {
    this._orderfrozen = value;
  }

  get contractUploaded(): boolean {
    return this._contractUploaded;
  }

  set contractUploaded(value: boolean) {
    this._contractUploaded = value;
  }

  get contractPreviewed(): boolean {
    return this._contractPreviewed;
  }

  set contractPreviewed(value: boolean) {
    this._contractPreviewed = value;
  }

  get contractGenerated(): boolean {
    return this._contractGenerated;
  }

  set contractGenerated(value: boolean) {
    this._contractGenerated = value;
  }

  get orderValid(): boolean {
    return this._orderValid;
  }

  set orderValid(value: boolean) {
    this._orderValid = value;
  }

  constructor(private http: HttpClient,
              private commonsService: CommonsService,
              private translateService: TranslateService) { }

  searchOrder(identificationId: string): Observable<JSONResponse> {
    let searchWalletUrl = environment.searchWalletUrl;
    return this.http.post<JSONResponse>(searchWalletUrl, identificationId);
  }

  getSavedWallet(identificationId: number): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.getSavedOrderEndpoint, identificationId);
  }

  saveWalletDocuments(files: File[], walletID: number, walletDocuments: WalletDocument[], numberDocuments: number): Observable<JSONResponse>{
    const formdata: FormData = new FormData();

    walletDocuments.forEach(document => {
      formdata.append("walletIDS", walletID.toString());
      formdata.append('files', document.document);
      formdata.append("walletDocumentsFiles", document.documentType);
      formdata.append("numberDocuments", numberDocuments.toString());
    });
    return this.http.post<JSONResponse>(this.sendDocumentUrl, formdata);
  }

  deleteWalletDocuments(walletDocuments: WalletDocument[]): Observable<JSONResponse>{
    return this.http.post<JSONResponse>(this.deleteDocumentUrl, walletDocuments);
  }

  cancelOrder(order: OrdOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CANCEL_ORDER, order);
  }

  setSessionOrder(order: SubscriptionOrder){
    this.order = order;
  }

  setSessionOrderUpgrade(order: UpgradeOrder){
    this.orderUpgrade = order;
  }

  isRetailedOrder(phone: string): Observable<JSONResponse> {
    let isRetailedWalletEndpoint = environment.isRetailedWalletEndpoint;
    return this.http.post<JSONResponse>(isRetailedWalletEndpoint, phone);
  }

  saveOrder(order: SubscriptionOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.saveOrderEndPoint, order);
  }

  saveUpgradeOrder(upgradeOrder: UpgradeOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.saveUpgradeOrderEndPoint, upgradeOrder);
  }

  saveWallet(order: SubscriptionOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.saveNewWalletEndPoint, order);
  }

  getSessionOrder(){
    return this.order;
  }

  getSessionOrderUpgrade(){
    return this.orderUpgrade;
  }

  removeSessionOrderUpgrade(){
    this.orderUpgrade = new UpgradeOrder();
  }

  getSavedOrder(orderId: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.getSavedOrderEndpoint, orderId);
  }

  removeSessionOrder(){
    this.order = new SubscriptionOrder();
  }

  getEnableCustomerWallet(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.ENABLE_CUSTOMER_WALLET, null);
  }


  showPopupNotConnected() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.NotConnected.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.NotConnected.message')
    );
  }

  showRetailedWalletWarning() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.RetailedWalletWarning.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.RetailedWalletWarning.message')
    );
  }

  showPopupNotEligible() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.NotEligible.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.NotEligible.message')
    );
  }

  showPopupFormError() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.FormError.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.FormError.message')
    );
  }

  showPopupAgeError(birthDateErrors) {
    this.showPopup(
       this.translateService.instant('showPopup.subscription.AgeError.title'),
      'fas fa-exclamation-triangle',
       this.translateService.instant('showPopup.subscription.AgeError.message')
    );
  }

  showPopupOrderNotFound(orderId) {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.OrderNotFound.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.OrderNotFound.message.linea1') + orderId + this.translateService.instant('showPopup.subscription.OrderNotFound.message.linea2')
  );
  }

  showPopupComboError(error) {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.ComboError.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.ComboError.message') + error.message
    );
  }

  showPopupSaveError(error) {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.SaveError.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.SaveError.message') + error
    );
  }

  cancelError() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.cancelError.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.cancelError.message')
    );
  }

  showPoupSaveComplete() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.SaveComplete.title'),
      'fas fa-info',
      this.translateService.instant('showPopup.subscription.SaveComplete.message')
    );
  }

  showPopupDocumentsError() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.DocumentsError.title'),
    'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.DocumentsError.message')
    );
  }

  showPopupContractError() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.ContractError.title'),
    'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.ContractError.message')
    );
  }

  showPopupNoResponse() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.NoResponse.title'),
    'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.NoResponse.message')
    );
  }

  showPopupExtensionNoValid() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.ExtensionNoValid.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.ExtensionNoValid.message')
    );
  }

  showPopupSizeNoValid() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.SizeNoValid.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.SizeNoValid.message')
    );
  }

  showPopupSubscriptionComplete() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.SubscriptionComplete.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('showPopup.subscription.SubscriptionComplete.message')
    );
  }

  showPopupFrozenOrder() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.FrozenOrder.title'),
    'fas fa-info',
      this.translateService.instant('showPopup.subscription.FrozenOrder.message')
      );
  }

  showPopupEditable() {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.Editable.title'),
      'fas fa-info',
      this.translateService.instant('showPopup.subscription.Editable.message')
    );
  }

  showPopupErrorRGetLang(error: string) {
    this.showPopup(
      this.translateService.instant('showPopup.subscription.ErrorRGetLang.title'),
      'fas fa-exclamation-triangle',
      error
    );
  }

  private showPopup(title: string, icon: string, message: string){
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = title;
    popupObj.popupIconName = icon;
    popupObj.popupMessage = message;
    this.commonsService.openGeneralPopup(popupObj);
  }

  getListDocument(order: OrdOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_WALLET_DOCUMENT_LIST, order);
  }

  showPopupWalletSuspended() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('showPopup.subscription.WalletSuspended.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('showPopup.subscription.WalletSuspended.message');
    this.commonsService.openGeneralPopup(popupObj);
  }

  showPopupClientNotExist() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('showPopup.subscription.ClientNotExist.title'),
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('showPopup.subscription.ClientNotExist.message');
    this.commonsService.openGeneralPopup(popupObj);
  }

  showPopupClientNotUpgrade() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('showPopup.subscription.ClientNotUpgrade.title');
    popupObj.popupIconName = 'fas fa-info';
    popupObj.popupMessage = this.translateService.instant('showPopup.subscription.ClientNotUpgrade.message');
    this.commonsService.openGeneralPopup(popupObj);
  }

   showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('search.message.error.searchingClient.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.message.error.searchingClient.msg');
    this.commonsService.openGeneralPopup(popupObj);
  }

  showPopupIntegrationKO(errorMessage: string) {
    this.showPopup(
      this.translateService.instant('error.title'),
      'fas fa-exclamation-triangle',
      errorMessage);
  }

  showError8500(errorMessage: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('error.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = errorMessage;
    popupObj.enableClose = true;
    popupObj.redirect = true;

    this.commonsService.openGeneralPopup(popupObj);
  }

  showTimeOut(errorMessage: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('error.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = errorMessage;
    this.commonsService.openGeneralPopup(popupObj);
  }

  showPopupRibProblem() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('error.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('new_subscription.error.rib.creation');
    this.commonsService.openGeneralPopup(popupObj);
  }

  showPopupConnectionDBError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('agency.popupConnectionError.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage =  this.translateService.instant('agency.popupConnectionError.message');
    popupObj.redirect=true;
    this.commonsService.openGeneralPopup(popupObj);
  }

  extensionValidate(name: String){
    while(name.includes(".")){
      name=name.substring(name.indexOf(".")+1);
    }
    name = name.toLowerCase();
    if(name=="pdf" || name =="tif" || name=="jpg" || name=='jpeg'){
      return true;
    }else{
      return false;
    }
  }

  showPopupDocumentAttach(documentType: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('showPopup.subscription.documentAttach.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('showPopup.subscription.documentAttach.message1') + " " +
      this.translateService.instant('showPopup.subscription.documentAttach.message2');
    this.commonsService.openGeneralPopup(popupObj);
  }
}
