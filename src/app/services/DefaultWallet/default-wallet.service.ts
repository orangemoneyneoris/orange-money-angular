import { Injectable } from '@angular/core';
import {NEVER, Observable} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {environment} from "../../../environments/environment";
import {DefaultOrders} from "../../domain/DefaultOrders";
import {HttpClient} from "@angular/common/http";
import {OrdOrder} from "../../domain/OrdOrder";

@Injectable({
  providedIn: 'root'
})
export class DefaultWalletService {

  constructor(private http: HttpClient,) { }

  createDefaultWalletOrder(defaultWallet: DefaultOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CLIENT_DEFAULT_WALLET, defaultWallet);
  }

  getDefaultWalletOrder(idOrder: number): Observable<DefaultOrders> {

    let ordOrder = new OrdOrder();
    ordOrder.ordrId = idOrder;
    return this.http.post<DefaultOrders>(environment.GET_DEFAULT_ORDER, ordOrder);
  }

  executeIntegration(defaultOrders: DefaultOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.EXECUTE_INTEGRATION_OTP, defaultOrders);
  }

}
