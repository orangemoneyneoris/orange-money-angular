import { TestBed } from '@angular/core/testing';

import { DefaultWalletService } from './default-wallet.service';

describe('DefaultWalletService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DefaultWalletService = TestBed.get(DefaultWalletService);
    expect(service).toBeTruthy();
  });
});
