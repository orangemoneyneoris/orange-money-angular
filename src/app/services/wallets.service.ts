import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {JSONResponse} from "../domain/JSONResponse";
import {Wallet} from "../domain/Wallet";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class WalletsService {

  wallet: Wallet;

  private searchWalletUrl = environment.searchWalletUrl;
  private defaultWalletStatusUrl = environment.defaultWalletStatusUrl;
  private isRetailedWalletEndpoint = environment.isRetailedWalletEndpoint;

  constructor(private http: HttpClient) { }

  getDefaultWalletStatus(msisdn: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.defaultWalletStatusUrl, msisdn);
  }
}
