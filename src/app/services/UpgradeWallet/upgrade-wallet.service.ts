import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {UpgradeOrder} from "../../domain/UpgradeOrder";
import {Observable} from "rxjs/index";
import {JSONResponse} from "../../domain/JSONResponse";

@Injectable({
  providedIn: 'root'
})
export class UpgradeWalletService {

  constructor(private http: HttpClient) { }

  initUpgradeOrder(upgradeOrder: UpgradeOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.SUBSCRIPTION_INIT_UPGRADE_ORDER, upgradeOrder);
  }

  getOrderById(ordId: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_ORDER_BY_ID, ordId);
  }

  getWalletDocumentById(walletId: number): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_WALLET_DOCUMENT_BY_ID, walletId);
  }

}
