import { TestBed } from '@angular/core/testing';

import { UpgradeWalletService } from './upgrade-wallet.service';

describe('UpgradeWalletService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpgradeWalletService = TestBed.get(UpgradeWalletService);
    expect(service).toBeTruthy();
  });
});
