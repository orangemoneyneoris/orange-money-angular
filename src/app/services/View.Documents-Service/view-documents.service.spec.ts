import { TestBed } from '@angular/core/testing';

import { ViewDocumentsService } from './view-documents.service';

describe('ViewDocumentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewDocumentsService = TestBed.get(ViewDocumentsService);
    expect(service).toBeTruthy();
  });
});
