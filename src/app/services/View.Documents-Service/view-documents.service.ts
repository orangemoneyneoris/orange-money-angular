import { Injectable } from '@angular/core';
import {NEVER, Observable} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {WalletDocument} from "../../domain/WalletDocument";
import {OrdOrder} from "../../domain/OrdOrder";
import {SubscriptionOrder} from "../../domain/SubscriptionOrder";
import {PopupObj} from "../../util/PopupObj";
import {CommonsService} from "../commons.service";
import {TranslateService} from "@ngx-translate/core";
import {UpgradeOrder} from "../../domain/UpgradeOrder";

@Injectable({
  providedIn: 'root'
})
export class ViewDocumentsService {
  order: OrdOrder;




  constructor(
    private http: HttpClient,
    private commonService: CommonsService,
    private translateService: TranslateService) { }

  getListDocument(order: OrdOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_WALLET_DOCUMENT_LIST, order);
  }


  getWalletDocument(idOrder: WalletDocument): Observable<JSONResponse>{

    return this.http.post<JSONResponse>(environment.GET_DOCUMENT_FILE, idOrder);
  }


  ValidationDocument(order: OrdOrder): Observable<JSONResponse> {

    return this.http.post<JSONResponse>(environment.GET_DOCUMENT_VALIDATION, order);
  }

  sendDocuments(ordrId: SubscriptionOrder): Observable<JSONResponse>{
    return this.http.post<JSONResponse>(environment.SEND_DOCUMENTS_SUBSCRIPTION, ordrId);
  }

  sendDocumentsUpgrade(ordrId: UpgradeOrder): Observable<JSONResponse>{
    return this.http.post<JSONResponse>(environment.SEND_DOCUMENTS_ORDER_UPGRADE , ordrId);
  }

  getOrderViewDoc(ordrId: OrdOrder): Observable<JSONResponse>{
    return this.http.post<JSONResponse>(environment.GET_DOCUMENT_ORDER_VIEWDOCUMENT, ordrId);
  }

  showPopupProcessCompleted() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('vistaDocument.subTitle');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('vistaDocument.msg.processCompleted');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupProcessFailed() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('vistaDocument.subTitle');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('vistaDocument.msg.processFailed');
    this.commonService.openGeneralPopup(popupObj);
  }


}

