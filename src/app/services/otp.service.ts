import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {JSONResponse} from '../domain/JSONResponse';
import {environment} from "../../environments/environment";
import {CommonsService} from "./commons.service";
import {PopupObj} from "../util/PopupObj";
import {OtpRequest} from "../domain/OtpRequest";
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class OTPService {

  constructor(private httpClient: HttpClient,
              private commonsService: CommonsService,
              private translateService: TranslateService
  ) {}

  initOtp(orderId): Observable<JSONResponse> {
    let id = orderId as string;
    return this.httpClient.post<JSONResponse>(environment.initOtpStatus, id);
  }

  confirmEnrollment(params: OtpRequest): Observable<JSONResponse> {
    return this.httpClient.post<JSONResponse>(environment.confirmOtp, params);
  }

  getOtpDefaultStatus(msisdn: string): Observable<JSONResponse> {
    return this.httpClient.post<JSONResponse>(environment.getOtpDefaultStatus, msisdn);
  }

  interOpEnrollment(params: OtpRequest): Observable<JSONResponse> {
    return this.httpClient.post<JSONResponse>(environment.interopEnrollment, params);
  }

  resendOtp(orderId): Observable<JSONResponse> {
    return this.httpClient.post<JSONResponse>(environment.resendOtp, orderId);
  }

  interopResendOtp(interopId): Observable<JSONResponse> {
    const id = interopId as string;
    return this.httpClient.post<JSONResponse>(environment.interopResendOtp, id);
  }

  cancelOtp(ordOrderId) {
    const id = ordOrderId as string;
    return this.httpClient.post<JSONResponse>(environment.cancelOtp, id);
  }

  checkOTPTimer(orderId): Observable<JSONResponse> {
    return this.httpClient.post<JSONResponse>(environment.CHECK_TIMER_OTP, orderId);
  }

  private showPopup(title: string, icon: string, message: string){
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = title;
    popupObj.popupIconName = icon;
    popupObj.popupMessage = message;
    this.commonsService.openGeneralPopup(popupObj);
  }

  showPopupOtpAlreadyDone() {
      this.showPopup(
        this.translateService.instant('error.otp.OtpAlreadyDone.title'),
      "fas fa-exclamation-triangle",
      this.translateService.instant('error.otp.OtpAlreadyDone.msg')
      );
  }

  showPopupConnectionError(error) {
      this.showPopup(
        this.translateService.instant('showPopup.otp.ConnectionError.title'),
        'fas fa-exclamation-triangle',
        this.translateService.instant('showPopup.otp.ConnectionError.message') + error
      );
  }

  showPopupOtpCompleted() {
    this.showPopup(
      'Le wallet client a été défini comme étant par défaut avec succès',
      "fas fa-info",
      ''
    );
  }

  showPopupError(error) {
    this.showPopup(
      'Erreur OTP',
      'fas fa-exclamation-triangle',
      error
    );
  }

  showPopupResend() {
    this.showPopup(
      this.translateService.instant('showPopup.otp.PopupResend.title'),
      "fas fa-info",
      this.translateService.instant('showPopup.otp.PopupResend.message')
    )
  }

  showPopupNotFound() {
    this.showPopup(
      this.translateService.instant('showPopup.otp.notFound.title'),
      "fas fa-info",
      this.translateService.instant('showPopup.otp.notFound.message')
    )
  }

  getRefreshDelay() {
    return 60000;
  }

}
