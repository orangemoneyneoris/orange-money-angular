import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {SubscriptionOrder} from "../domain/SubscriptionOrder";
import {CashOrders} from "../domain/CashOrders";
import {UpgradeOrder} from "../domain/UpgradeOrder";

@Injectable({
  providedIn: 'root'
})
export class JasperService {

  constructor(private http: HttpClient) { }

  getPDF(order: SubscriptionOrder): Observable<HttpResponse<Blob>> {

    let headers = new HttpHeaders();

    let uri = environment.generateContractUrl;
    // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
    return this.http.post(uri, order,{
      headers: headers,
      observe: 'response',
      responseType: 'blob'
    });
  }

  getPDFUpgrade(order: UpgradeOrder): Observable<HttpResponse<Blob>> {

    let headers = new HttpHeaders();
    let uri = environment.generateContractUrlUpgrade;
    // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
    return this.http.post(uri, order,{
      headers: headers,
      observe: 'response',
      responseType: 'blob'
    });
  }

  getCashInPDF(order: CashOrders): Observable<HttpResponse<Blob>> {

    let headers = new HttpHeaders();

    let uri = environment.GENERATED_CASH_IN_CONTRACT_URL;
    // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
    return this.http.post(uri, order,{
      headers: headers,
      observe: 'response',
      responseType: 'blob'
    });
  }

  getCashOutPDF(order: CashOrders): Observable<HttpResponse<Blob>> {

    let headers = new HttpHeaders();

    let uri = environment.GENERATED_CASH_OUT_CONTRACT_URL;
    // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
    return this.http.post(uri, order,{
      headers: headers,
      observe: 'response',
      responseType: 'blob'
    });
  }

}
