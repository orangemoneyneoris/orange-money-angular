import { Injectable } from '@angular/core';
import {PROFILES} from "../../util/Constants";

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor() { }

  public get isPOS(): any {
    return PROFILES.POS.VENDEUR;
  }
  public get isPOSOS(): any {
    return PROFILES.POS.VENDEUR_OS;
  }
  public get isChefAgence(): any {
    return PROFILES.POS.CHEF_AGENCE_OS;
  }
  public get isPOS_OM(): any {
    return PROFILES.MONEY.VENDEUR;
  }
  public get isPOSOS_OM(): any {
    return PROFILES.MONEY.VENDEUR_OS;
  }
  public get isChefAgence_OM(): any {
    return PROFILES.MONEY.CHEF_AGENCE_OS;
  }
  public get isSupervisor(): any {
    return PROFILES.MONEY.SUPERVISEUR;
  }
  public get isHeadAgence(): any {
    return PROFILES.MONEY.HEAD_AGENCE;
  }

}
