import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {JSONResponse} from '../domain/JSONResponse';
import {Client} from '../domain/Client';
import {environment} from "../../environments/environment";
import {SubscriptionOrder} from "../domain/SubscriptionOrder";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  private customer: Client;

  constructor(private http: HttpClient) { }

  searchCustomerByPhoneUrl = environment.searchCustomerByPhoneUrl;
  isCustomerEligibleEndpoint = environment.isCustomerEligibleEndpoint;
  isCustomerEligibleMakeDefaultEndpoint = environment.isCustomerEligibleMakeDefaultEndpoint;
  getRibCodeEndpoint = environment.getRibCodeEndpoint;
  getRibCodeInSubscriptionEndpoint = environment.getRibCodeSubscriptionEndpoint;
  createRibCodeEndpoint = environment.createRibCodeEndpoint;

  searchCustomer(msisdn: string, id: string, idType: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.searchCustomerByPhoneUrl, {msisdn, id, idType});
  }

  isCustomerEligible(msisdn: string, id: string, idType: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.isCustomerEligibleEndpoint, {msisdn, id, idType});
  }

  isCustomerEligibleForMakeDefault(msisdn: string, id: string, idType: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.isCustomerEligibleMakeDefaultEndpoint, {msisdn, id, idType});
  }

  getRibCode(msisdn: string, id: string, idType: number): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getRibCodeEndpoint, {msisdn, id, idType});
  }

  getRibCodeInSubscription(order: SubscriptionOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getRibCodeInSubscriptionEndpoint, order);
  }

  createRibCode(order: SubscriptionOrder): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.createRibCodeEndpoint, order);
  }
}
