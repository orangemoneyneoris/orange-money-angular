import { Injectable } from '@angular/core';
import {JSONResponse} from "../../domain/JSONResponse";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {User} from "../../domain/User";
import {ADuser} from "../../domain/ADuser";
import {NgxPermissionsService} from "ngx-permissions";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient,
              private permissionsService: NgxPermissionsService) { }

  login(username: string, password: string): Observable<JSONResponse> {

    let user = new User();
    user.username = username;
    user.password = password;

    return this.http.post<JSONResponse>(environment.LOGIN, user);
  }

  loadMenu(): Observable<string>{

   
    return this.http.get(environment.portalMenu,{withCredentials:true,responseType: "text"});
  }

  loadPermissions(){
    this.executeLoadPermissions(
      localStorage.getItem('groupId'),
      localStorage.getItem('entEntityId'))
      .subscribe(
        response => {

          let JSONResponse = response as JSONResponse;

          if (JSONResponse.success) {

            const permissions = JSONResponse.permissions;
            if (permissions != null) {
              this.permissionsService.loadPermissions(permissions);
            }
          }
        },
        error => {
          console.debug('Error a cargar los permisos');
        });
  }

  executeLoadPermissions(groupId: string, entEntityId: string): Observable<JSONResponse> {

    let user = new ADuser();
    user.entEntityId = entEntityId;
    user.groupId = groupId;

    return this.http.post<JSONResponse>(environment.LOAD_PERMISSIONS, user);
  }

}
