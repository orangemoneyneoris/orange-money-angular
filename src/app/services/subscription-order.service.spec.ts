import { TestBed } from '@angular/core/testing';

import { SubscriptionOrderService } from './subscription-order.service';

describe('SubscriptionOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubscriptionOrderService = TestBed.get(SubscriptionOrderService);
    expect(service).toBeTruthy();
  });
});
