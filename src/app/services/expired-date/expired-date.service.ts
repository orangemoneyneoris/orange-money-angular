import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {ClientUpdateDocument} from "../../domain/ClientUpdateDocument";
import {PopupObj} from "../../util/PopupObj";
import {CommonsService} from "../commons.service";
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class ExpiredDateService {

  constructor(private http: HttpClient,private commonsService: CommonsService,private translateService: TranslateService) { }

  sendUpdateClientOrder = environment.saveUpdateClient;
  getClientUpdateOrd = environment.getClientUpdateOrd;
  resendDocumentClient = environment.resendDocumentClient;

  getClientUpdateOrder(orderId: string): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getClientUpdateOrd, orderId);
  }

  saveDocuments(files: File[], orderId: string, clientUpdateDocuments: ClientUpdateDocument[], motif:number, msisdn:string): Observable<JSONResponse>{
    const formdata: FormData = new FormData();

    clientUpdateDocuments.forEach(document => {
      formdata.append('files', document.document);
      formdata.append("clientUpdateFiles", document.documentType);
    });
    formdata.append("orderId", orderId);
    formdata.append("motif", motif.toString());
    formdata.append("msisdn", msisdn);
    return this.http.post<JSONResponse>(this.sendUpdateClientOrder, formdata);
  }

  resendDocuments(orderId: string, motif:number): Observable<JSONResponse> {
    const formdata: FormData = new FormData();

    formdata.append("orderId", orderId);
    formdata.append("motif", motif.toString())
    return this.http.post<JSONResponse>(this.resendDocumentClient, formdata);
  }

  private showPopup(title: string, icon: string, message: string){
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = title;
    popupObj.popupIconName = icon;
    popupObj.popupMessage = message;
    this.commonsService.openGeneralPopup(popupObj);
  }
  showPopupDocumentsComplete() {
    this.showPopup(
      this.translateService.instant('expiredClient.popups.title'),
      'fas fa-exclamation-triangle',
      this.translateService.instant('expiredClient.popups.message')
    );
  }

}
