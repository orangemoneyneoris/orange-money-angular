import { TestBed } from '@angular/core/testing';

import { ExpiredDateService } from './expired-date.service';

describe('ExpiredDateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpiredDateService = TestBed.get(ExpiredDateService);
    expect(service).toBeTruthy();
  });
});
