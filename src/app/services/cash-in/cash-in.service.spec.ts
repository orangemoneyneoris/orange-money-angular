import { TestBed } from '@angular/core/testing';

import { CashInService } from './cash-in.service';

describe('CashInService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashInService = TestBed.get(CashInService);
    expect(service).toBeTruthy();
  });
});
