import { Injectable } from '@angular/core';
import {CashOrders} from "../../domain/CashOrders";
import {Observable} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {HttpClient} from "@angular/common/http";
import {PopupObj} from "../../util/PopupObj";
import {CommonsService} from "../commons.service";
import {OrdOrder} from "../../domain/OrdOrder";
import {environment} from "../../../environments/environment";
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class CashInService {

  order: CashOrders;

  constructor(private http: HttpClient,
              private commonService: CommonsService,
              private cashInService: CashInService,
              private translateService: TranslateService) {
  }

  createOrder(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_IN_CREATE_ORDER_OPERATION, cashOrder);
  }

  executeCashIn(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_IN_EXECUTE_OPERATION, cashOrder);
  }

  checkCallBack(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_IN_REFRESH_OPERATION, cashOrder);
  }

  cashInVerify(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_IN_VERIFY, cashOrder);
  }

  // TODO remove, only for testing
  callBackConfirmation(orderId: number): Observable<any> {
    return this.http.post<JSONResponse>(environment.CASHIN_CALLBACK, {
      'txnid': orderId,
      'txnstatus': 200
    });
  }

  // TODO config file
  getRefreshDelay() {
    return 30000;
  }

  getVerifyDelay(): Observable<number> {
    return this.http.post<number>(environment.GET_VERIFY_TIME, null);
  }

  annulCashOrder(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.ANNUL_CASH_ORDER, cashOrder);
  }

  rejectCashOrder(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.REJECT_CASH_ORDER, cashOrder);
  }

  getSessionOrder() {
    return this.order;
  }

  setSessionOrder(order: CashOrders) {
    this.order = order;
  }

  // TODO remove mock. Get logged user msisdn. (dealer)
  getUserId() {
    return localStorage.getItem('dealer');
  }

  removeSessionOrder() {
    this.order = new CashOrders();
  }

  showPopupNotEligible(popupObj) {
    this.cashInService.showPopupNotEligible(popupObj);
  }

  getCustomerDetailInfo(ordrId: number): Observable<CashOrders> {
    let ordOrder = new OrdOrder();
    ordOrder.ordrId = ordrId;

    return this.http.post<CashOrders>(environment.CASH_IN_GET_CASH_ORDER, ordOrder);
  }

  showPopupPosIntegrationFail() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.PosIntFail');
    this.commonService.openGeneralPopup(popupObj);
  }
  showPopupCashInComplete() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.succeed');
    popupObj.popupIconName = 'fas fa-check-double';
    popupObj.popupMessage = '';
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupKnowError(message: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = message;
    this.commonService.openGeneralPopup(popupObj);
  }

  showCashInFailed() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.failed');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupCashInInProgress() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.inProgress');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.inProgress');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupVerifyLimitReached() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.verifyLimit');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.verifyLimit');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.connectionError');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.connectionError');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupContractError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.connectionError');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.contractError');
    this.commonService.openGeneralPopup(popupObj);
  }

  showFormFieldsWarning() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.formWarning');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.formWarning');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupCallbackTimeout() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.timeOut');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashin.popup.message.timeOut');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupVerifyFailed(triesLeft) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashin.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = triesLeft + this.translateService.instant('cashin.popup.message.verifyTries');
    this.commonService.openGeneralPopup(popupObj);
  }

}
