import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {PopupObj} from '../util/PopupObj';
import * as moment from 'moment';
import {JSONResponse} from "../domain/JSONResponse";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class CommonsService {

  private generalPopup = new Subject<PopupObj>();
  private mixer = new Subject<String>();
  private mixerCash = new Subject<String>();

  private cashPopup = new Subject<PopupObj>();

  constructor(
    private http: HttpClient,
    private translateService: TranslateService,
    private router: Router) {}

  openGeneralPopup(popupObj: PopupObj) {
    this.generalPopup.next(popupObj);
  }

  openCashPopup(popupObj: PopupObj) {
    this.cashPopup.next(popupObj);
  }

  closePopup() {
    this.generalPopup.next();
  }

  closeCashPopup() {
    this.cashPopup.next(null);
  }

  popups(): Observable<PopupObj> {
    return this.generalPopup.asObservable();
  }

  popupsCash(): Observable<PopupObj> {
    return this.cashPopup.asObservable();
  }

  mixers(): Observable<String> {
    return this.mixer.asObservable();
  }

  openMixer(message: String){
    this.mixer.next(message);
  }

  closeMixer(){
    this.mixer.next(undefined);
  }

  mixersCash(): Observable<String> {
    return this.mixerCash.asObservable();
  }

  openMixerCash(message: String){
    this.mixerCash.next(message);
  }

  closeMixerCash(){
    this.mixerCash.next(undefined);
  }

  getComboValue(comboValues:Array<Object>, id: number, valueFieldName: string): string{

    let index = comboValues.findIndex(element => {
      return element['value'] == id;
    });
    if (index >= 0) {
      return comboValues[index][valueFieldName];
    } else {
      return undefined;
    }
  }

  getDate(year: string, month: string, day:string){
    return year + "-" + month + "-" + day;
  }

  getDateFromDatePicker(datePicker) {
    return new Date(Date.UTC(datePicker['year'], datePicker['month'] - 1, datePicker['day'])).toISOString();
  }

  /*
  getdatePickerFormat(date: string){
    let formattedDate = {year:0, month:0, day:0};
    let items = date.split("-", 3);
    formattedDate.year = parseInt(items[0]);
    formattedDate.month = parseInt(items[1]);
    formattedDate.day = parseInt(items[2]);
    return formattedDate;
  }
  */

  /*
  getdatePickerFormat(date: string){
    date = new Date(date);
    let formattedDate = {year:0, month:0, day:0};
    formattedDate.year = date.getFullYear();
    formattedDate.month = date.getMonth() + 1;
    formattedDate.day = date.getDate();
    return formattedDate;
  }
  */

  getdatePickerFormat(date: Date){
    let fecha= date.toString().split("T");
    let fechaSplit = fecha[0].split("-");
    let formattedDate = {year:0, month:0, day:0};
    formattedDate.year = parseInt(fechaSplit[0]);
    formattedDate.month = parseInt(fechaSplit[1]);
    formattedDate.day = parseInt(fechaSplit[2]);
    /*if(formattedDate.day==31 && formattedDate.month==12){
        formattedDate.year+=1;
        formattedDate.month=1;
        formattedDate.day=1;
    }else if(formattedDate.day==31){
        formattedDate.month+=1;
        formattedDate.day=1;
    }else if(formattedDate.day==30 && (formattedDate.month==4 || formattedDate.month==6 || formattedDate.month==9 || formattedDate.month==11)){
      formattedDate.month+=1;
      formattedDate.day=1;
    }else if(this.leapYear(formattedDate.year) && formattedDate.month==2 && formattedDate.day==29){
      formattedDate.month+=1;
      formattedDate.day=1;
    }else if(!this.leapYear(formattedDate.year) && formattedDate.month==2 && formattedDate.day==28) {
      formattedDate.month+=1;
      formattedDate.day=1;
    }else{
      formattedDate.day+=1;
    }*/
    return formattedDate;
  }

  leapYear(year)
  {
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
  }

  today(){
    let today: moment.Moment = moment();

    let month: string = today.month().toString();
    if (month.toString().length===1){
      month = '0'+month;
    }

    let day: string = today.day().toString();
    if (day.length===1){
      day = '0'+day;
    }

    const year: string = today.year().toString();

    return this.getDate(year, month, day);
  }

  getAuthToken() {
    return localStorage.getItem('jwt_token');
  }

  showSessionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('error.SessionError.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('error.SessionError.msg');
    this.openGeneralPopup(popupObj);
  }

  showTokenInvalid() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('error.TokenInvalid.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('error.TokenInvalid.msg');
    this.openGeneralPopup(popupObj);
  }

  handlerResponse(response: JSONResponse) {
    if (response.tokenInvalid) {
      this.closeMixer();

      this.showSessionError();

      setTimeout(() => {
          this.router.navigate(['/login']);
        },
        1500);
    }else{

    }
  }

  handlerSessionExpired(response: JSONResponse) {
    if (response.tokenInvalid) {
      this.closeMixer();

      this.showSessionError();

      setTimeout(() => {
          this.router.navigate(['/login']);
        },
        1500);
    }
  }

  redirectToLogin() {
    this.closeMixer();
    this.closePopup();
    this.closeCashPopup();
    this.closeMixerCash();
    if(localStorage.getItem("lastRequest")!=null && localStorage.getItem("group")!=null) {
      this.showSessionError();
    }else{
      this.showTokenInvalid();
    }
    this.cleanAuthenticationToken();


    localStorage.clear();

    setTimeout(() => {
        this.router.navigate(['/login']);
      },
      1500);
  }

  cleanAuthenticationToken() {
    localStorage.clear();
  }

  showPopupGeneralError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('error.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('error.general');
    this.openGeneralPopup(popupObj);
  }

}
