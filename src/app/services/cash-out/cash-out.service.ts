import {Injectable} from '@angular/core';
import {CashOrders} from "../../domain/CashOrders";
import {Observable, of} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {CommonsService} from "../commons.service";
import {PopupObj} from "../../util/PopupObj";
import {TranslateService} from "@ngx-translate/core";
import {OrdOrder} from "../../domain/OrdOrder";

@Injectable({
  providedIn: 'root'
})
export class CashOutService {

  private order: CashOrders;

  constructor(private http: HttpClient,
              private commonService: CommonsService,
              private translateService: TranslateService) {}


  createOrder(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_OUT_CREATE_ORDER_OPERATION, cashOrder);
  }

  cashOutInit(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_OUT_EXECUTE_OPERATION, cashOrder);
  }

  checkCallBack(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_OUT_REFRESH_OPERATION, cashOrder);
  }

  cashOutVerify(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CASH_OUT_VERIFY, cashOrder);
  }

  annulCashOrder(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.ANNUL_CASH_ORDER, cashOrder);
  }

  rejectCashOrder(cashOrder: CashOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.REJECT_CASH_ORDER, cashOrder);
  }

  // TODO remove, only for testing
  callBackConfirmation(orderId: number): Observable<any> {
    return this.http.post<JSONResponse>(environment.CASHOUT_CALLBACK, {'txnid': orderId, 'txnstatus': 200});
  }

  getCustomerDetailInfo(ordrId: number): Observable<CashOrders> {
    let ordOrder = new OrdOrder();
    ordOrder.ordrId = ordrId;

    return this.http.post<CashOrders>(environment.CASH_IN_GET_CASH_ORDER, ordOrder);
  }

  // TODO config file
  getRefreshDelay() {
    return 30000;
  }

  getVerifyDelay(): Observable<number> {
    return this.http.post<number>(environment.GET_VERIFY_TIME, null);
  }

  getSessionOrder(){
    return this.order;
  }

  setSessionOrder(order: CashOrders) {
    this.order = order;
  }

  // TODO remove mock. Get logged user msisdn. (dealer)
  getUserId() {
    return localStorage.getItem('dealer');
  }

  removeSessionOrder() {
    this.order = new CashOrders();
  }

  showPopupCashOUTComplete() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.succeed');
    popupObj.popupIconName = 'fas fa-check-double';
    popupObj.popupMessage = '';
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupCashOutFailed() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashout.popup.message.failed');

    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupCashOutInProgress() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.inProgress');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashout.popup.message.inProgress');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupPosIntegrationFail() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashout.popup.message.PosIntFail');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupCallbackTimeout() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.timeOut');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashout.popup.message.timeOut');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupVerifyLimitReached() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.verifyLimit');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashout.popup.message.verifyLimit');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupConnectionError() {
      const popupObj: PopupObj = new PopupObj();
      popupObj.title = this.translateService.instant('cashout.popup.title.connectionError');
      popupObj.popupIconName = 'fas fa-exclamation-triangle';
      popupObj.popupMessage = this.translateService.instant('cashout.popup.message.connectionError');
      this.commonService.openGeneralPopup(popupObj);
  }

  showPopupContractError() {
      const popupObj: PopupObj = new PopupObj();
      popupObj.title = this.translateService.instant('cashout.popup.title.connectionError');
      popupObj.popupIconName = 'fas fa-exclamation-triangle';
      popupObj.popupMessage = this.translateService.instant('cashout.popup.message.contractError');
      this.commonService.openGeneralPopup(popupObj);
  }

  showFormFieldsWarning() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.formWarning');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('cashout.popup.message.formWarning');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupKnowError(message: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = message;
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupVerifyFailed(triesLeft) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('cashout.popup.title.failed');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = triesLeft + this.translateService.instant('cashout.popup.message.verifyTries');

    this.commonService.openGeneralPopup(popupObj);
  }


}
