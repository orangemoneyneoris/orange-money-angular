import { TestBed } from '@angular/core/testing';

import { CashOutService } from './cash-out.service';

describe('CashOutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashOutService = TestBed.get(CashOutService);
    expect(service).toBeTruthy();
  });
});
