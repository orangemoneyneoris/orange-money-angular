import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {CommonsService} from "../commons.service";
import {PopupObj} from "../../util/PopupObj";
import {BalanceOrders} from "../../domain/BalanceOrders";
import {ChangePINOrders} from "../../domain/ChangePINOrders";
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class AgencyService {

  constructor(private http: HttpClient,
              private commonService: CommonsService,
              private translateService: TranslateService) { }

  getPinPad(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.AGENCY_GET_PIN_PAD, null);
  }

  createBalanceOrder(balanceOrder: BalanceOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.BALANCE_CREATE_ORDER, balanceOrder);
  }

  getBalance(balanceOrder: BalanceOrders): Observable<JSONResponse>{
    return this.http.post<JSONResponse>(environment.GET_AGENCY_BALANCE, balanceOrder);
  }

  createChangePinOrder(changePinOrder: ChangePINOrders): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.CHANGE_PIN_CREATE_ORDER, changePinOrder);
  }

  changePin(changePinOrder: ChangePINOrders): Observable<JSONResponse>{
    return this.http.post<JSONResponse>(environment.CHANGE_PIN_AGENCY, changePinOrder);
  }

  showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('agency.popupConnectionError.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage =  this.translateService.instant('agency.popupConnectionError.message');
    this.commonService.openGeneralPopup(popupObj);
  }

  showFormPinEmpty(){
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('agency.popup.title.mandatory');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('agency.popup.title.mandatoryPin');
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupErrorService(message : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('agency.popup.title.error');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = message;
    this.commonService.openGeneralPopup(popupObj);
  }

  showPopupSuccessService(message : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.popupIconName = 'fas fa-check-double';
    popupObj.title = this.translateService.instant('agency.popup.title.succeed');
    popupObj.popupMessage = message;
    this.commonService.openGeneralPopup(popupObj);
  }
}
