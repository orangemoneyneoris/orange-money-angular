import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {OrdOrder} from "../../domain/OrdOrder";
import {SubscriptionOrder} from "../../domain/SubscriptionOrder";
import {OrdType} from "../../domain/OrdType";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CustomerDetailService {

  constructor(private http: HttpClient) {}

  getCustomerDetailInfo(ordrId: number, typeId: number): Observable<SubscriptionOrder> {
    let ordOrder = new OrdOrder();
    ordOrder.ordrId = ordrId;

    let ordType = new OrdType();
    ordType.typeId = typeId;
    ordOrder.ordType = ordType;

    return this.http.post<SubscriptionOrder>(environment.GET_CUSTOMER_DETAIL_INFO_URL, ordOrder);
  }
}
