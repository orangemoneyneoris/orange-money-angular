import { Injectable } from '@angular/core';
import {DatesFilterList} from "../../domain/DatesFilterList";
import {Observable} from "rxjs";
import {JSONResponse} from "../../domain/JSONResponse";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ListTransactionsCashService {

  constructor(private http: HttpClient, private route: Router) {}

  getOrdOrderList(dates: DatesFilterList): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_CASH_LIST_URL, dates);
  }

  private modals: any[] = [];

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    this.modals = this.modals.filter(x => x.id !== id);
  }

  open(id: string) {
    // open modal specified by id
    const modal = this.modals.find(x => x.id === id);
    modal.open();
  }

  close(id: string) {
    // close modal specified by id
    const modal = this.modals.find(x => x.id === id);
    modal.close();
  }
}
