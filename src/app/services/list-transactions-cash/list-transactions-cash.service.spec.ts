import { TestBed } from '@angular/core/testing';

import { ListTransactionsCashService } from './list-transactions-cash.service';

describe('ListTransactionsCashService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListTransactionsCashService = TestBed.get(ListTransactionsCashService);
    expect(service).toBeTruthy();
  });
});
