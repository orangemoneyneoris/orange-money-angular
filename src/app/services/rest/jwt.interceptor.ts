import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { of } from 'rxjs';
import { CommonsService } from '../commons.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { Router } from '@angular/router';
import { throwError as observableThrowError, Observable, BehaviorSubject, empty, EMPTY, throwError, from, EmptyError } from 'rxjs';
import { take, filter, catchError, switchMap, finalize, map } from 'rxjs/operators';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';


@Injectable()
export class RestInterceptor implements HttpInterceptor {
  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);


  constructor(
    private commonsService: CommonsService,
    private permissionsService: NgxPermissionsService, private oauthService: OAuthService,
    private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    /* if (this.permissionsService.getPermissions() <= 0) {
       this.router.navigate(['/login']);
     } else {*/

      console.log('intercepted request url:', request.url);
      this.isRefreshingToken = false;

      if (request.url.includes(environment.portalHome)) {
        return next.handle(request);

      }
      if (request.url.includes('oauth')) {

      request = request.clone({
         withCredentials: true,
        headers: request.headers
        .set('Cache-Control', 'no-cache')
        .set('Pragma', 'no-cache')
        .set('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT')
        .set('If-Modified-Since', '0')




      });

      return next.handle(request);

    } else {
      const token = this.oauthService.getAccessToken();
      if (token) {
        request = request.clone({
          setHeaders: {
            Authorization: 'Bearer ' + `${token}`
            // lastRequest: localStorage.getItem('lastRequest'),
           // dealer: localStorage.getItem('dealer')
          }

        });
      }
    }

      return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          const JSONResponse = JSON.stringify(event.body);
          let splitted = JSONResponse.split(',', 3);
          if (splitted.some(x => x === '"errorMessage":"FORBIDDEN"') && !request.url.toString().includes('enableCustomerWallet')) {
            this.commonsService.closeMixer();
            this.commonsService.closePopup();
            this.commonsService.redirectToLogin();
          } else {

            const fecha = this.getNowDate();

            console.debug('last request: ' + fecha);
            localStorage.setItem('lastRequest', fecha);
          }
        }
        return event;
      }),
      catchError((err: any) => {
        console.debug('Error: ', err);

        if (err instanceof HttpErrorResponse) {
            console.debug('Error: ' + err.name);
            console.debug('Error from: ' + err.url);



            switch ((err as HttpErrorResponse).status) {
              case 401:
                return this.handle401Error(request, next);
              default:

                return this.handleOtherErrorCodes(err);
            }
          }

        console.debug('Error not http error response ?');
        this.commonsService.closeMixer();
        this.commonsService.closePopup();
        this.commonsService.showPopupGeneralError();

        return of(err);


      }));
    /*}*/
  }


  handleOtherErrorCodes(error) {

    // if (!err.url.includes("enableCustomerWallet")) { // not used in menucomponenent
    //   this.commonsService.showPopupGeneralError();
    // }

    // keep same behaviour as before ADFS"
    const fecha = this.getNowDate();
    console.debug('last request: ' + fecha);
    localStorage.setItem('lastRequest', fecha);

    //
    this.commonsService.closeMixer();
    this.commonsService.closePopup();
    this.commonsService.showPopupGeneralError();

    return of(error);


  }


  addAuthHeader(request) {
    const token = this.oauthService.getAccessToken();
    if (token) {

      return request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + `${token}`,
        }
      });
    }
    return request;
  }
  handle401Error(req: HttpRequest<any>, next: HttpHandler) {


            if (this.isRefreshingToken) {
              // If refreshTokenInProgress is true, we will wait until token is silently refreshed
              // which means the new token is ready and we can retry the request
              return this.oauthService.events.pipe(
                filter(result => result.type === 'token_refreshed'),
                take(1),
                switchMap(() => next.handle(this.addAuthHeader(req)))
              );  
            } else {
              this.isRefreshingToken = true;

              return from(this.oauthService.refreshToken()).pipe(
                switchMap(() => next.handle(this.addAuthHeader(req))),
                catchError(error => {

                console.error('error refreshing', error);
                this.commonsService.closeMixer();
                this.commonsService.closePopup();
                console.log('login again ..');

                this.redirectToLoginAdfs();

                return of(error);
                }),

                finalize(() => this.isRefreshingToken = false)
              );
            }



  }


  redirectToLoginAdfs() {
    const currentLocation = window.location.origin + window.location.pathname;
    if (!currentLocation.includes(environment.DOMAIN_ORANGE_ADFS)) {
      this.oauthService.logOut();
      localStorage.clear();
      this.commonsService.closeMixer();
      this.commonsService.closePopup();
      this.commonsService.showSessionError();

      setTimeout(() => {
        this.router.navigate(['/login']);
      },
      1500);


    } else {
      this.oauthService.logOut();
      localStorage.clear();
      // this.oauthService.logOut();

      this.commonsService.closeMixer();
      this.commonsService.closePopup();
      this.commonsService.showSessionError();

      setTimeout(() => {
        this.oauthService.initCodeFlow(this.router.url);
      },
      1500);


     // this.oauthService.initLoginFlow(encodeURIComponent( this.router.url));

    }
  }
  logoutUser() {

    alert('should logout');
  }
  getNowDate() {
    // return string
    let returnDate = '';
    // get datetime now
    let today = new Date();
    // split
    let dd = today.getDate();
    let mm = today.getMonth() + 1; // because January is 0!
    let yyyy = today.getFullYear();

    let hh = today.getHours();
    let MM = today.getMinutes();
    let ss = today.getSeconds();

    // Interpolation date
    if (dd < 10) {
      returnDate += `0${dd}/`;
    } else {
      returnDate += `${dd}/`;
    }

    if (mm < 10) {
      returnDate += `0${mm}/`;
    } else {
      returnDate += `${mm}/`;
    }
    returnDate += yyyy + ' ';

    if (hh < 10) {
      returnDate += `0${hh}:`;
    } else {
      returnDate += `${hh}:`;
    }

    if (MM < 10) {
      returnDate += `0${MM}:`;
    } else {
      returnDate += `${MM}:`;
    }

    if (ss < 10) {
      returnDate += `0${ss}`;
    } else {
      returnDate += `${ss}`;
    }

    return returnDate;
  }


}
