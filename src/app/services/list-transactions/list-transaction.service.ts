import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {JSONResponse} from '../../domain/JSONResponse';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {DatesFilterList} from "../../domain/DatesFilterList";

@Injectable({
  providedIn: 'root'
})
export class ListTransactionService {

  constructor(private http: HttpClient, private route: Router) {}


  getOrdOrderList(dates: DatesFilterList): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_ORD_ORDER_LIST_URL, dates);
  }

  getOrdParentOrderById(ordOrderId: number): Observable<number> {
    return this.http.post<number>(environment.GET_ORD_PARENT_ORDER_BY_ID, ordOrderId);
  }

  redirect(typeId: number, ordrId: number, ordCategory?: string) {

    switch (typeId) {
      case 1: {
        if (ordCategory && ordCategory.toLowerCase() === 'make default') {
          this.route.navigate(['/defaultWallet/' + ordrId]);
        } else {
          if (ordCategory && ordCategory.toLowerCase() === 'upgrade') {
            this.route.navigate(['/upgrade-subscription/' + ordrId]);

          }else {
            let orderId=ordrId;
            this.route.navigate(['/subscription/' + orderId]);
            //this.route.navigate(['/viewDocs/' + ordrId]);
          }
        }
        break;
      }
      case 2: {
       if (ordCategory && ordCategory.toLowerCase() === 'cash in') {
         this.route.navigate(['/cashin/' + ordrId]);



        } else if (ordCategory && ordCategory.toLowerCase() === 'cash out') {
          this.route.navigate(['/cashout/' + ordrId]);
        }
       break;
      }
      case 4:{
        if (ordCategory && ordCategory.toLowerCase() === 'mise a jour client') {
          this.route.navigate(['/expired-date/' + ordrId]);
        }else {
          this.route.navigate(['/viewDocs/' + ordrId]);
        }
        break;
      }
      default: {
        this.route.navigate(['/listTransaction']);
        break;
      }
    }
  }

  detail(typeId: number, ordrId: number, currentPage: number) {
    this.route.navigate(['detail', ordrId, typeId, currentPage]);
  }

  goHistoryScreen(ordrId: number) {
    this.route.navigate(['listTransactionHistory', ordrId]);
  }

}
