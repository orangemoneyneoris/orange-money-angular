import { TestBed } from '@angular/core/testing';

import { ListTransactionService } from './list-transaction.service';

describe('ListTransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListTransactionService = TestBed.get(ListTransactionService);
    expect(service).toBeTruthy();
  });
});
