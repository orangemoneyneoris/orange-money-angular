import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {JSONResponse} from "../../domain/JSONResponse";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {Pin} from "../../domain/Pin";
import {OrdOrder} from "../../domain/OrdOrder";

@Injectable({
  providedIn: 'root'
})
export class ListTransactionsClientService {

  constructor(private http: HttpClient, private route: Router) {}

  getOrdOrderList(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(environment.GET_ORD_ORDER_CLIENT_LIST_URL, null);
  }

  getTransactionHitoryList(ordOrderId: number): Observable<JSONResponse> {
    let ordOrder = new OrdOrder();
    ordOrder.ordrId = ordOrderId;

    return this.http.post<JSONResponse>(environment.GET_TRANSACTIONS_HISTORY_LIST, ordOrder);
  }

  redirect(typeId: number, ordrId: number) {
    switch(typeId) {
      case 1: {
        this.route.navigate(['/subscription/' + ordrId]);
        break;
      }
      case 2: {
        this.route.navigate(['/cashin/' + ordrId]);
        break;
      }
      default: {
        this.route.navigate(['/listTransaction']);
        break;
      }
    }
  }

  detail(typeId: number, ordrId: number) {
    this.route.navigate(['detail', ordrId, typeId]);
  }

  redirectRemote(transactionID: string, amount: string, serviceType: string,
                 mobileNumber: string, entryType: string, statusTransaction: string) {

    if(serviceType.toLowerCase() === 'cash in') {
      this.route.navigate(['/cashin/', transactionID, amount, serviceType
        , mobileNumber, entryType, statusTransaction]);
    }else{
      this.route.navigate(['/cashout/', transactionID, amount, serviceType
        , mobileNumber, entryType, statusTransaction]);
    }
  }

}
