import { TestBed } from '@angular/core/testing';

import { ListTransactionsClientService } from './list-transactions-client.service';

describe('ListTransactionsClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListTransactionsClientService = TestBed.get(ListTransactionsClientService);
    expect(service).toBeTruthy();
  });
});
