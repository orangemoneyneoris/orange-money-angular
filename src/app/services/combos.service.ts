import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {JSONResponse} from "../domain/JSONResponse";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {EntCountry} from "../domain/EntCountry";

@Injectable({
  providedIn: 'root'
})
export class CombosService {

  countries = undefined;
  provinces = undefined;
  cities = undefined;
  postalCodes  = undefined;
  streets = undefined;
  identityTypes = undefined;
  walletOpeningReasons: any[] = undefined;
  professions = undefined;
  nationalities = undefined;
  salarys=undefined;

  private getAllCountriesEndpoint = environment.getAllCountriesEndpoint;
  private getidentityTypesEndpoint = environment.getidentityTypesEndpoint;
  private getOpeningReasonsEndpoint = environment.getOpeningReasonsEndpoint;
  private getProfessionsEndpoint = environment.getProfessionsEndpoint;
  private getNationalitiesEndpoint = environment.getNationalitiesEndpoint;
  private getSalaryEndpoint = environment.getSalaryEndpoint;
  private getReasonExpiredClient = environment.getReasonsExpiredClient;

  constructor(private http: HttpClient) {}

  getAllCountries(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getAllCountriesEndpoint, '');
  }

  getIdentityTypes(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getidentityTypesEndpoint, '');
  }

  getOpeningReasonsList(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getOpeningReasonsEndpoint, '');
  }

  getProfessionsList(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getProfessionsEndpoint, '');
  }

  getNationalities(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getNationalitiesEndpoint, '');
  }

  getSalaryList(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getSalaryEndpoint, '');
  }

  getReasonsExpiredClient(): Observable<JSONResponse> {
    return this.http.post<JSONResponse>(this.getReasonExpiredClient, '');
  }
}
