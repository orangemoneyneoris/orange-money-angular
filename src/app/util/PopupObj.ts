export class PopupObj {
  title: string;
  popupIconName: string;
  popupMessage: string;
  progressBarStatus: string;
  enableClose: boolean;
  redirect: boolean = false;
}
