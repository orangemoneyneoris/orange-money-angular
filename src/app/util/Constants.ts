export class PopupObj {
  title: string;
  popupIconName: string;
  popupMessage: string;
  progressBarStatus: string;
  enableClose: boolean;
}

export const NEW_SUBSCRIPTION = {

  ACCOUNT_LEVELS : [
    {viewValue: '', value: ''},
    {viewValue: 'N2', value: 'N2'},
    {viewValue: 'N3', value: 'N3'},
  ],

  UPGRADE_WALLET_COLLABORATOR : [
    {viewValue: '', value: ''},
    {viewValue: 'N2 Collaborator', value: 'N2P'},
    {viewValue: 'N3 Collaborator', value: 'N3P'},
  ],

  GENDERS: [
    {viewValue: '', value: ''},
    {viewValue: 'Mr.', value: '1'},
    {viewValue: 'Mme', value: '2'}
  ],

  STATUS: {
    IN_PROGRESS: 'in progress',
    FROZEN: 'frozen',
    CANCELLED: 'cancelled',
    PROCESSED: 'processed'
  },

  LEVELN1: 'N1',
  LEVELN2: 'N2',
  LEVELN3: 'N3',
  LEVELN1P: 'N1P',
  LEVELN2P: 'N2P',
  LEVELN3P: 'N3P'

};

export const CASHOUT = {

  CALLBACK_STATUS: {
    OK: '200',
    TIMEOUT: '203'
  },
  NON_EDITABLE_MODE:{
    POS_KO: 18,
    CB: 9
  }

};

export const CASHIN = {
  CALLBACK_STATUS: {
    OK: '200',
    TIMEOUT: '203'
  },
  NON_EDITABLE_MODE:{
    POS_KO: 18,
    CB: 9
  }
};

export const PROFILES = {
    POS: {
      VENDEUR: '1',
      VENDEUR_OS: '2',
      CHEF_AGENCE_OS: '46'
    },
    MONEY: {
      VENDEUR: '77',
      VENDEUR_OS: '78',
      CHEF_AGENCE_OS: '79',
      SUPERVISEUR: '80',
      HEAD_AGENCE: '81',
      REPORT_CENTRAL: '93'
  }
};

export const FUNCTIONALITIES = {
  CASH: {
    IN: '1',
    OUT: '2'
  },
  CUSTOMER: {
    SUBSCRIPTION: '77',
    UPGRADE: '78',
    MAKE_DEFAULT: '79',
    SEARCH: '80'
  }
};
