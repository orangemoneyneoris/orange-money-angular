/** A hero's name can't match the given regular expression */
import {AbstractControl, ValidatorFn, FormGroup} from '@angular/forms';
import * as moment from 'moment';

export function under18(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    if (!control.value || control.value === null || control.value.day==undefined || (control.value.year!=undefined && control.value.year.toString().length<4)) {
      return null;
    }
    let birthDate: moment.Moment = moment([control.value.year, control.value.month, control.value.day]);
    let today: moment.Moment = moment();
    let years: number = today.diff(birthDate, 'years');

    return !isDate18orMoreYearsOld(control.value.day, control.value.month, control.value.year)
      ? {under18: {value: years}} : null;
  };
}

export function expirationDate(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    if (!control.value || control.value === null || control.value.day==undefined || (control.value.year!=undefined && control.value.year.toString().length<4)) {
      
      return null;
    }
    let expirationDate: moment.Moment = moment([control.value.year, control.value.month, control.value.day]);
    let today: moment.Moment = moment();
    let days: number = today.diff(expirationDate, 'days')+29;
    return !isDateOld(days) ? {expirationDate: {value: days}} : null;
  };
}

export function startWith0607(): ValidatorFn{
  return (control: AbstractControl): {[key: string]: any} | null => {
    if (!control.value || control.value === null) {
      return null;
    }
    let msisdn:string = control.value;
    return (!msisdn.startsWith('06') && !msisdn.startsWith('07')) ? {startWith0607: {value: true}} : null;
  };
}

function isDateOld(days) {
  if(days>0){
    return null;
  }else{
    return true;
  }
}

function isDate18orMoreYearsOld(day, month, year) {
  return new Date(year+18, month-1, day) <= new Date();
}


export function dateIsValid(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    if (!control.value || control.value === null) {
      return null;
    }
    let dateFormat = 'YYYY-MM-DD';
    let dateAsTxt;
    if(typeof control.value === 'string'){
      let date= control.value.split("-");
      if(date[0]!=undefined && date[0].length==2 && date[1]!=undefined && date[1].length==2 && date[2]!=undefined &&
      parseInt(date[2])>=1000) {
        dateAsTxt = date[2] + "-" + date[1] + "-" + date[0];
      }else{
        return {dateInvalid: {value: control.value}};
      }
    }else{
      if(control.value.year>=1000){
        dateAsTxt = control.value.year + "-" + control.value.month + "-" + control.value.day;
      }else{
        return {dateInvalid: {value: control.value}};
      }
    }

    let momentDate: moment.Moment = moment(dateAsTxt, dateFormat);

    return momentDate.isValid() ? null : {dateInvalid: {value: control.value}};
  };
}

export function NationalityOverIdType(idTypeControl: AbstractControl, nationalityControl: AbstractControl): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {



    let identityType = idTypeControl.value;
    let nationality = nationalityControl.value;

    let error = identityType==1 && nationality!=1 && nationality !="" || nationality==1 && identityType!=1 && identityType!="";
    return (error) ? {NationalityOverIdType: {value: control.value}} : null;
  };
}

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    let pinConfirm = matchingControl.value != null ? matchingControl.value.toString() : null;
    let size =0;

    if(pinConfirm != null)
      size = pinConfirm.length;

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (size >=4 && control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
