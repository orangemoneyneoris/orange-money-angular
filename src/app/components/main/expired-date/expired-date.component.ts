import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CommonsService} from "../../../services/commons.service";
import {CombosService} from "../../../services/combos.service";
import {ReasonsExpiredClient} from "../../../domain/ReasonsExpiredClient";
import {ActivatedRoute, Router} from "@angular/router";
import {Client} from "../../../domain/Client";
import {Wallet} from "../../../domain/Wallet";
import * as FileSaver from 'file-saver';
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {ExpiredDateService} from "../../../services/expired-date/expired-date.service";
import {ClientUpdateDocument} from "../../../domain/ClientUpdateDocument";
import {OrdOrder} from "../../../domain/OrdOrder";

@Component({
  selector: 'app-expired-date',
  templateUrl: './expired-date.component.html',
  styleUrls: ['./expired-date.component.css']
})
export class ExpiredDateComponent implements OnInit, OnDestroy {

  expiredDateForm: FormGroup;
  motifTypes: ReasonsExpiredClient[] = new Array<ReasonsExpiredClient>();
  levelWallet:string ;
  client:Client = new Client();
  wallet:Wallet = new Wallet();
  private removeSession: boolean = false;
  isUpgradeOrder: boolean = false;
  validationError: boolean = false;
  clientUpdateDocument: ClientUpdateDocument[] = new Array<ClientUpdateDocument>();
  order: OrdOrder = new OrdOrder();
  orderCreated:boolean = false;
  orderClosed: boolean = false;
  showErrorDocuments: boolean = false;
  showErrorCRM: boolean = false;

  constructor(private sCommons: CommonsService,
              private sCombos: CombosService,
              private thisRoute: ActivatedRoute,
              private sOrder: SubscriptionOrderService,
              private router: Router,
              private expiredServ: ExpiredDateService) { }

  ngOnInit() {

    this.initForm();

    let ordrId = parseInt(this.thisRoute.snapshot.paramMap.get('ordrId'));
    if(ordrId){
      this.order.ordrId=ordrId;
      this.sCommons.openMixer("Chargement. S'il vous plaît, attendez...");
      this.expiredServ.getClientUpdateOrder(ordrId.toString()).subscribe(response => {
        if (response.success) {
          this.orderCreated = true;
          this.clientUpdateDocument = response.successResponse as ClientUpdateDocument[];
          this.motifTypes.push(this.clientUpdateDocument[0].reasonExpiredClient);
          this.expiredDateForm.controls['motif'].patchValue(this.clientUpdateDocument[0].reasonExpiredClient.reasonId);
          this.expiredDateForm.controls['motif'].disable();
          let allOk = true;
          this.clientUpdateDocument.forEach(doc => {
            if (!doc.gidOK) {
              allOk = false;
            }
          });

          if((this.clientUpdateDocument[0].order != null
            && (this.clientUpdateDocument[0].order.ordStatus.statusId === 3 ||
              this.clientUpdateDocument[0].order.ordStatus.statusId === 4 ||
              this.clientUpdateDocument[0].order.ordStatus.statusId === 5)) || allOk) {
            this.orderClosed = true;
          }
        } else {
          this.sCommons.closeMixer();
          this.sCommons.showPopupGeneralError();
        }
      });
      this.sCommons.closeMixer();
    }else {
      this.order.ordrId=null;
      let sessionCustomer = this.sOrder.getSessionOrder() != null ?
        this.sOrder.getSessionOrder().client : window.history.state.client as Client;
      if (sessionCustomer && sessionCustomer.accountLevel != null && sessionCustomer.msisdn != null) {
        this.client = sessionCustomer;
      } else {
        let sessionCustomer2 = this.sOrder.getSessionOrderUpgrade() != null ?
          this.sOrder.getSessionOrderUpgrade().client : window.history.state.client as Client;
        if (sessionCustomer2 && sessionCustomer2.accountLevel != null && sessionCustomer2.msisdn != null) {
          this.client = sessionCustomer2;
          this.isUpgradeOrder = true;
        } else {
          this.returnBack();
        }
      }
      this.levelWallet = this.client.accountLevel;
      this.order.msisdn = this.client.msisdn;
      this.getReasonsExpiredClient();
    }
  }

  private initForm() {
    this.expiredDateForm = new FormGroup({
      motif: new FormControl('', [Validators.required]),
      idObverseDoc: new FormControl({value: '', disabled:true}),
      idObverseDocHidden: new FormControl(''),
      idReverseDoc: new FormControl({value: '', disabled:true}),
      idReverseDocHidden: new FormControl(''),
      addressDoc: new FormControl({value: '', disabled:true}),
      addressDocHidden: new FormControl(''),
      otherDoc: new FormControl({value: '', disabled:true}),
      otherDocHidden: new FormControl('')
    });
  }

  getFormControl(controlName: string) {
    return this.expiredDateForm.get(controlName) as FormControl;
  }

  enableDocs(){
    this.disableAllDocs();
    this.expiredDateForm.controls['otherDoc'].enable();
    this.expiredDateForm.controls['otherDocHidden'].enable();
    if(this.expiredDateForm.controls['motif'].value==='1') {//Pièce d'identité expirée
      this.expiredDateForm.controls['idObverseDoc'].setValidators([Validators.required]);
      this.expiredDateForm.controls['idObverseDoc'].enable();
      this.expiredDateForm.controls['idObverseDocHidden'].enable();
      this.expiredDateForm.controls['idReverseDoc'].setValidators([Validators.required]);
      this.expiredDateForm.controls['idReverseDoc'].enable();
      this.expiredDateForm.controls['idReverseDocHidden'].enable();

    }else if(this.expiredDateForm.controls['motif'].value==='2'){//Pièce d'identité perdue
      this.expiredDateForm.controls['idObverseDoc'].enable();
      this.expiredDateForm.controls['idObverseDocHidden'].enable();
      this.expiredDateForm.controls['idReverseDoc'].enable();
      this.expiredDateForm.controls['idReverseDocHidden'].enable();
/*      this.expiredDateForm.controls['addressDoc'].enable();
      this.expiredDateForm.controls['addressDocHidden'].enable();
      this.expiredDateForm.controls['otherDoc'].enable();
      this.expiredDateForm.controls['otherDocHidden'].enable();*/

    }else if(this.expiredDateForm.controls['motif'].value==='3') {//Changement d'adresse
      this.expiredDateForm.controls['addressDoc'].setValidators([Validators.required]);
      this.expiredDateForm.controls['addressDoc'].enable();
      this.expiredDateForm.controls['addressDocHidden'].enable();

    }else if(this.expiredDateForm.controls['motif'].value==='4') {//Autre
      /*this.expiredDateForm.controls['otherDoc'].setValidators([Validators.required]);*/
      this.expiredDateForm.controls['idObverseDoc'].enable();
      this.expiredDateForm.controls['idObverseDocHidden'].enable();
      this.expiredDateForm.controls['idReverseDoc'].enable();
      this.expiredDateForm.controls['idReverseDocHidden'].enable();
      this.expiredDateForm.controls['addressDoc'].enable();
      this.expiredDateForm.controls['addressDocHidden'].enable();
    }
  }

  disableAllDocs(){
    this.expiredDateForm.controls['idObverseDoc'].setValidators([]);
    this.resetFile('idObverseDoc');
    this.expiredDateForm.controls['idObverseDoc'].disable();
    this.expiredDateForm.controls['idObverseDocHidden'].disable();

    this.expiredDateForm.controls['idReverseDoc'].setValidators([]);
    this.resetFile('idReverseDoc');
    this.expiredDateForm.controls['idReverseDoc'].disable();
    this.expiredDateForm.controls['idReverseDocHidden'].disable();

    this.expiredDateForm.controls['addressDoc'].setValidators([]);
    this.resetFile('addressDoc');
    this.expiredDateForm.controls['addressDoc'].disable();
    this.expiredDateForm.controls['addressDocHidden'].disable();

    this.expiredDateForm.controls['otherDoc'].setValidators([]);
    this.resetFile('otherDoc');
    /*this.expiredDateForm.controls['otherDoc'].disable();
    this.expiredDateForm.controls['otherDocHidden'].disable();*/
  }

  resetFile(controlName: string) {
    const control = this.expiredDateForm.controls[controlName];
    if (!control.disabled) {
      control.reset();
      this.expiredDateForm.controls[controlName+'Hidden'].reset();
    }
    let indexOf = this.getDocumentIndex(controlName);
    this.clientUpdateDocument.splice(indexOf, 1);
  }

  private getDocumentIndex(documentType: string): number {
    let document = this.clientUpdateDocument.find(function (document) {
      return document.documentType === documentType;
    });
    return this.clientUpdateDocument.indexOf(document);
  }

  attachFile2Wallet(controlName: string, $event: any) {
    this.sCommons.openMixer("Joindre un document...");
    let documents = $event.target.files;
    let file: File = documents[0];
    let fileName = file.name;
    let sizeValid : boolean = file.size/1024/1024 <=10;
    let extensionValid : boolean = this.extensionValidate(fileName);
    if(extensionValid && sizeValid){
      this.expiredDateForm.controls[controlName].setValue(fileName);
      this.saveDocumentOnWallet(controlName, file, fileName);
      this.sCommons.closeMixer();
    }else if(!extensionValid){
      this.sCommons.closeMixer();
      this.sOrder.showPopupExtensionNoValid();
    }else{
      this.sCommons.closeMixer();
      this.sOrder.showPopupExtensionNoValid();
    }
  }

  extensionValidate(name: String){
    while(name.includes(".")){
      name=name.substring(name.indexOf(".")+1);
    }
    name = name.toLowerCase();
    if(name=="pdf" || name =="tif" || name=="jpg" || name=='jpeg'){
      return true;
    }else{
      return false;
    }
  }

  private saveDocumentOnWallet(controlName: string, file: File, fileName) {
    let clientUpdateDoc: ClientUpdateDocument = new ClientUpdateDocument();
    clientUpdateDoc.document = file;
    clientUpdateDoc.documentType = controlName;
    clientUpdateDoc.documentName = fileName;

    this.clientUpdateDocument.push(clientUpdateDoc);
  }

  getReasonsExpiredClient(){
    this.sCommons.openMixer("Chargement. S'il vous plaît, attendez...");
    this.sCombos.getReasonsExpiredClient().subscribe(ajaxResponse => {
    if (ajaxResponse.success) {
      let reasons:ReasonsExpiredClient[] = Array<ReasonsExpiredClient>();
      reasons = ajaxResponse.successResponse as ReasonsExpiredClient[];
      this.motifTypes.push({reasonId:null,reasonName:"", reasonDesc:""});
      reasons.forEach(motif => {
        if (motif.reasonId === 3) {
          if (this.levelWallet === 'N3') {
            this.motifTypes.push(motif);
          }
        } else {
          this.motifTypes.push(motif);
        }
      });
      this.sCommons.closeMixer();
    } else {
      if(ajaxResponse.errorMessage!="FORBIDDEN"){
        this.sCommons.closeMixer();
        this.sCommons.closePopup();
      }
    }

    }, error1 => {
      this.sCommons.closeMixer();
      this.sCommons.closePopup();

    });
  }

  sendOrder(){
    let validationOk: boolean = false;
    if(this.expiredDateForm.controls['motif'].value==='2') {
      if(this.expiredDateForm.controls['idObverseDoc'].value
        || this.expiredDateForm.controls['idReverseDoc'].value
        || this.expiredDateForm.controls['otherDoc'].value){
        validationOk = true;
      }
    }else{
      validationOk=true;
    }
    if(this.expiredDateForm.valid && validationOk){
      this.validationError = false;

      this.sCommons.openMixer("Chargement. S'il vous plaît, attendez...");

      let files: File[] = new Array<File>();
      let clientUpdateFiles: ClientUpdateDocument[] = new Array<ClientUpdateDocument>();
      this.clientUpdateDocument.forEach(document => {
        if(document.document["name"]!= undefined) {
          files.push(document.document);
          clientUpdateFiles.push(document);
        }
      });

      if (clientUpdateFiles.length!=0) {
        let orderId = '';
        let msisdn = this.client.msisdn ? this.client.msisdn:'';
        let motif = this.expiredDateForm.controls['motif'].value;

        if(this.order.ordrId)
          orderId= this.order.ordrId.toString();

        this.expiredServ.saveDocuments(files, orderId, clientUpdateFiles, motif, msisdn).subscribe(ajaxResponse => {

          if (ajaxResponse.success) {
            if(ajaxResponse.successResponse.toString() === 'OK') {
              this.showSuccesSentPopup();
              this.router.navigate(['/listTransaction']);
            }else{
              this.order = ajaxResponse.successResponse as OrdOrder;
              if(this.order) {
                this.orderCreated = true;
                this.expiredDateForm.controls['motif'].disable();
              }
              this.showErrorDocuments=true;
            }
            this.sCommons.closeMixer();
          } else {
            if (ajaxResponse.errorMessage != "FORBIDDEN") {

              this.order = ajaxResponse.successResponse as OrdOrder;
              if(this.order.ordrId) {
                this.orderCreated = true;
                this.expiredDateForm.controls['motif'].disable();
                if(ajaxResponse.errorMessage === 'KODOC') {
                  this.showErrorDocuments = true;
                  this.sCommons.closeMixer();
                }else {
                  this.showSuccesSentPopup();
                  this.router.navigate(['/listTransaction']);
                }
              }else {

                this.sCommons.closeMixer();
                this.sCommons.closePopup();
              }
            }
          }

        }, error1 => {
          this.sCommons.closeMixer();
          this.sCommons.closePopup();

        });
      }

    }else{
      this.validationError = true;
    }
  }

  private showSuccesSentPopup() {
    this.sCommons.closeMixer();
    this.expiredServ.showPopupDocumentsComplete();
  }

  returnBack(){
    this.removeSession=false;
    if(this.isUpgradeOrder) {
      this.router.navigate(['/upgrade-subscription']);
    }else if(this.orderCreated || this.orderClosed){
      this.router.navigate(['/listTransaction']);
    }else {
      this.router.navigate(['/resercher']);
    }
  }

  cancel(){
    this.removeSession=true;
      this.router.navigate(['/resercher']);
  }

  ngOnDestroy(): void {
    if(this.removeSession) {
      this.sOrder.removeSessionOrderUpgrade();
      this.sOrder.removeSessionOrder();
    }
  }

  downloadFile(item:ClientUpdateDocument){
    const img: string = item.document.toString();
    const bytes: string = atob(img);
    const byteNumbers = new Array(bytes.length);
    for (let i = 0; i < bytes.length; i++) {
      byteNumbers[i] = bytes.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob: Blob = new Blob([byteArray], { type: 'application/pdf' });
    FileSaver.saveAs(blob, item.documentName);
  }

  reSendOrder(){
    this.sCommons.openMixer("Chargement. S'il vous plaît, attendez...");
    let orderId = '';
    let motif = this.expiredDateForm.controls['motif'].value;
    if(this.order.ordrId)
      orderId= this.order.ordrId.toString();

    this.expiredServ.resendDocuments(orderId, motif).subscribe(ajaxResponse => {

      if (ajaxResponse.success) {
        if(ajaxResponse.successResponse.toString() === 'OK') {
          this.showSuccesSentPopup();
          this.router.navigate(['/listTransaction']);
        }else{
          this.showErrorDocuments=true;
        }
        this.sCommons.closeMixer();
      } else {
        if (ajaxResponse.errorMessage != "FORBIDDEN") {
          //this.showErrorCRM = true;
          this.order = ajaxResponse.successResponse as OrdOrder;
          if(this.order.ordrId) {
            if(ajaxResponse.errorMessage === 'KODOC')
              this.showErrorDocuments = true;
            else {
              this.showSuccesSentPopup();
              this.router.navigate(['/listTransaction']);
            }
          }
          this.sCommons.closeMixer();
        }
      }

    }, error1 => {
      this.sCommons.closeMixer();

    });
  }

}
