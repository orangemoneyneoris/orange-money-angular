import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpiredDateComponent } from './expired-date.component';

describe('ExpiredDateComponent', () => {
  let component: ExpiredDateComponent;
  let fixture: ComponentFixture<ExpiredDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpiredDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpiredDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
