import {Component, Input, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {Client} from '../../../domain/Client';
import {Wallet} from "../../../domain/Wallet";
import {Router} from "@angular/router";
import {ClientsService} from "../../../services/clients.service";
import {UpgradeOrder} from "../../../domain/UpgradeOrder";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {NgbModal, NgbModalOptions, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {PopupObj} from "../../../util/PopupObj";
import {NEW_SUBSCRIPTION} from "../../../util/Constants";

@Component({
  selector: 'app-customer-card',
  templateUrl: './customer-card.component.html',
  styleUrls: ['./customer-card.component.css']
})
export class CustomerCardComponent implements OnInit, OnDestroy  {

  @Input() client: Client;
  @Input() wallet: Wallet;
  @Input() defaultWalletError: Boolean;
  @ViewChild('validateDate') validateDate: ElementRef;

  LEVELN1 = NEW_SUBSCRIPTION.LEVELN1;
  LEVELN2 = NEW_SUBSCRIPTION.LEVELN2;
  LEVELN3 = NEW_SUBSCRIPTION.LEVELN3;
  LEVELN1P = NEW_SUBSCRIPTION.LEVELN1P;
  LEVELN2P = NEW_SUBSCRIPTION.LEVELN2P;
  LEVELN3P = NEW_SUBSCRIPTION.LEVELN3P;

  private validateModalRef: NgbModalRef;
  popupObj: PopupObj = new PopupObj();
  expiredDate:boolean = false;

  private removeSession: boolean = true;

  constructor( private route: Router,
               private sOrder: SubscriptionOrderService,
               private clientService: ClientsService,
               private modalService: NgbModal,
               private router: Router) { }

  ngOnInit() {
    console.log(this.wallet.accountLevel)
    //this.checkExpiredDate();
  }


  checkExpiredDate(){

    if(this.sOrder.order.client.expirationDate != null) {
      const date:Date = new Date();
      const expDate: Date = new Date(this.formatDate(this.sOrder.order.client.expirationDate));
      if (expDate < date) {
        this.expiredDate = true;
        this.sOrder.setSessionOrder(this.sOrder.order);
        this.openValidateDateModal();
      }
    }
  }

  openValidateDateModal() {
    const ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      windowClass: "sizeValidateModal",
      keyboard: false,
    };
    this.validateModalRef = this.modalService.open(this.validateDate, ngbModalOptions);
    this.popupObj.popupMessage='La pièce d\'identité du client a été expirée. Merci de procéder à la mise à jour des données client.';
    this.popupObj.title='Orange Money Indique';
    this.popupObj.popupIconName='fas fa-exclamation-triangle';
  }

  closeValidateDateModal() {
    if(this.validateModalRef!=null) {
      this.validateModalRef.close();
    }
  }

  closeRedirect() {
    this.closeValidateDateModal();
    this.removeSession=false;
    this.router.navigate(['/expired-date']);
  }

  redirectExpired() {
    this.removeSession = false;
    this.route.navigate(['/expired-date']);
  }

  get n2Upgradable() {
    return this.wallet !== undefined && (this.wallet.accountLevel === this.LEVELN1 || this.wallet.accountLevel === this.LEVELN1P);
  }

  get n3Upgradable() {
    return this.n2Upgradable || (this.wallet !== undefined && (this.wallet.accountLevel === this.LEVELN2 ||
      this.wallet.accountLevel === this.LEVELN2P));
  }

  get name(){
    if (this.client !== undefined && this.client.name !== undefined) {
      return this.client.name;
    }
  }

  get identificationId(){
    if (this.client !== undefined && this.client.identificationId !== undefined) {
      return this.client.identificationId;
    }
  }

  get accountLevel(){
    if (this.wallet !== undefined && this.wallet.accountLevel !== undefined && this.wallet.accountLevel !== this.LEVELN1P
      && this.wallet.accountLevel !== this.LEVELN2P && this.wallet.accountLevel !== this.LEVELN3P) {
      return this.wallet.accountLevel;
    } else if (this.wallet !== undefined && this.wallet.accountLevel !== undefined) {
      if (this.wallet.accountLevel === this.LEVELN1P){
        return 'N1 Collaborateur';
      } else if (this.wallet.accountLevel === this.LEVELN2P){
        return 'N2 Collaborateur';
      } else {
        return 'N3 Collaborateur';
      }
    }
  }

  get surname(){
    if (this.client !== undefined && this.client.surname !== undefined) {
      return this.client.surname;
    }
  }

  get phone(){
    if (this.client !== undefined && this.client.msisdn !== undefined) {
      return this.client.msisdn;
    }
  }

  get accountBalance(){
    if (this.wallet !== undefined && this.wallet.accountBalance !== undefined) {
      return this.wallet.accountBalance;
    }
  }

  get walletDefault(): boolean{
    if (this.wallet !== undefined && this.wallet.defaultAccount !== undefined) {
      return (this.wallet.defaultAccount != null ? this.wallet.defaultAccount : true);
    }else{
      return true;
    }
  }

  get buttondefaultwall (): boolean {
    return this.walletDefault || this.isWalletSuspended;
  }

  navigateCashIn(){
    this.removeSession=false;
  }

  return() {
    localStorage.setItem('showSearchPanel', '1');
  }

  navigateCash(cashin: boolean) {
    if (!cashin) {
      this.removeSession=false;
      this.route.navigate(['/cashout'], {
        state: {client: this.client}});
    }

  }

  navigateDefault() {

    this.removeSession=false;
      this.route.navigate(['/defaultWallet'], {
        state: {client: this.client,
                wallet: this.wallet}});

  }

  navigateUpgrade(level: string) {
    let upgradeOrder = new UpgradeOrder();
    upgradeOrder.wallet = this.wallet;
    upgradeOrder.client = this.client;

    this.removeSession=false;

    this.route.navigate(['/upgrade-subscription'], {
      state: {
        level: level,
        upgradeOrder: upgradeOrder,
      }
    });

  }

  get accountStatus(){
    if (this.wallet !== undefined && this.wallet.statusAccount !== undefined) {
      return this.wallet.statusAccount;
    }
  }

  get birthday(){
    if (this.client !== undefined && this.client.birthDate !== undefined) {

      return this.client.birthDate;
    }
  }

  get sex(){
    if (this.client !== undefined && this.client.gender !== undefined) {
      return this.client.gender.name;
    }
  }

  get nationalite(){
    if (this.client !== undefined && this.client.nationality !== undefined) {
      return this.client.nationality.name;
    }
  }

  get post(){
    if (this.client !== undefined && this.client.address !== undefined) {
      let address = (this.client.address.address != null ? this.client.address.address :"")
      address = address + " "+ (this.client.address.entVille != null ? this.client.address.entVille :"");
      address = address + " "+ (this.client.address.entCodePostal != null ? this.client.address.entCodePostal :"");
      address = address + " "+(this.client.address.entCountry != null ? this.client.address.entCountry.name :"");
      return address;
    }
  }

  get typeId(){
    if (this.client !== undefined && this.client.identityType !== undefined) {
      return this.client.identityType.name;
    }
  }

  get numRib(){
    if (this.client !== undefined && this.client.numRib !== undefined) {
      return this.client.numRib;
    }
  }

  get enrolStatus(){
    if (this.wallet !== undefined
      && this.wallet.enrolementWallet
      && this.wallet.enrolementWallet !== undefined) {
      if(this.wallet.enrolementWallet.indexOf("error")== -1){
        return this.wallet.enrolementWallet;
      }

    }
  }

  get subsDate(){
    if (this.client !== undefined && this.client.expirationDate !== undefined) {
      return this.client.expirationDate;
    }
  }

  redirect(){

  }

  get isWalletSuspended() {
    if (this.wallet !== undefined && this.wallet.statusAccount !== undefined) {
      return this.wallet.statusAccount !== 'Actif';
    }
  }

  ngOnDestroy(): void {
    if(this.removeSession) {
      this.sOrder.removeSessionOrderUpgrade();
      this.sOrder.removeSessionOrder();
    }
  }

  private formatDate(date: Date){
    let format: string = date.toString().split('+')[0];
    format=format+'Z';
    return format;
  }
}
