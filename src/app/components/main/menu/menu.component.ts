import { Compiler, Component, ElementRef, Input, OnInit, Pipe, ViewChild } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { Router } from '@angular/router';
import { PROFILES } from '../../../util/Constants';
import { FUNCTIONALITIES } from '../../../util/Constants';
import { PermissionsService } from '../../../services/permissions/permissions.service';
import { SubscriptionOrderService } from '../../../services/subscription-order.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';
import { LoginService } from 'src/app/services/login/login.service';
import { DomSanitizer } from '@angular/platform-browser';

 
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @Input() overMenu: boolean;
  @Input() closeMenu: boolean;

  @ViewChild('iframe') iframe:  ElementRef;

  menuDisabled = false;
  portalMenu:any ="";

  constructor(private compiler: Compiler, private sanitizer: DomSanitizer,

              private ngxPermissionsService: NgxPermissionsService, private loginService: LoginService,
              private sOrder: SubscriptionOrderService, private oauthService: OAuthService,

              private router: Router) { }

              ngOnInit() {

                const currentLocation = window.location.origin + window.location.pathname;
                if (currentLocation.includes(environment.DOMAIN_ORANGE_ADFS)) {
            
                  this.loginService.loadMenu().subscribe(
                    response => {
                      if (response.search('.cd-main-header') < 0) {
                        alert('Veuillez vous connecter au portail. ');
                        window.location.href = environment.portalHome;
            
                      } else {

                        this.portalMenu=response;
                        //this.portalMenu =this.sanitizer.bypassSecurityTrustHtml(response);
                          console.log('menu downloaded');
                           let  iFrameDoc = this.iframe.nativeElement.contentDocument || this.iframe.nativeElement.contentWindow;
                          iFrameDoc.write(this.portalMenu);
                          iFrameDoc.close();
                          var bt = this.iframe.nativeElement.contentDocument.createElement("base");
                          bt.setAttribute('target', '_parent');
                          this.iframe.nativeElement.contentDocument.getElementsByTagName("head")[0].appendChild(bt);
                          
                          this.setIframeHeight('framePortal', 114);
                          

            
                      }
                    }, error => {
                      console.log('error getting menu',error);
                      alert('Veuillez vous connecter au portail. ');
                      window.location.href =environment.portalHome;
            
                  }
                );
            
            
                  this.menuDisabled = true;
                  window.addEventListener('message', this.handleDocHeightMsg, false);

                }

    // this.sOrder.getEnableCustomerWallet().subscribe(res => {
    //     if (res.success && (res.successResponse as String).toLowerCase() == "n") {
    //       this.menuDisabled = true;
    //     } else {
    //       this.menuDisabled = false;
    //     }
    //   });


  }

  logOut() {
    this.compiler.clearCache();
    this.ngxPermissionsService.flushPermissions();
    localStorage.clear();

    this.router.navigate(['/login']);
  }

  get getUserName(): any {
    return localStorage.getItem('usernameInfo');
  }

  get getGroup(): any {
    return localStorage.getItem('group');
  }

  public get isPOS(): any {
    return PROFILES.POS.VENDEUR;
  }
  public get isPOSOS(): any {
    return PROFILES.POS.VENDEUR_OS;
  }
  public get isChefAgence(): any {
    return PROFILES.POS.CHEF_AGENCE_OS;
  }
  public get isPOS_OM(): any {
    return PROFILES.MONEY.VENDEUR;
  }
  public get isPOSOS_OM(): any {
    return PROFILES.MONEY.VENDEUR_OS;
  }
  public get isChefAgence_OM(): any {
    return PROFILES.MONEY.CHEF_AGENCE_OS;
  }
  public get isSupervisor(): any {
    return PROFILES.MONEY.SUPERVISEUR;
  }
  public get isHeadAgence(): any {
    return PROFILES.MONEY.HEAD_AGENCE;
  }
  public get isReportingCentral(): any {
    return PROFILES.MONEY.REPORT_CENTRAL;
  }

  public get isDefaultWalletEnabled(): any {
    return localStorage.getItem('isDefaultWalletEnabled') === 'Y';
  }

  get allowCashin(): any {
    return FUNCTIONALITIES.CASH.IN;
  }

  redirect(url: string) {
    this.router.navigateByUrl('/DummyComponent', { skipLocationChange: true }).then(() =>
      this.router.navigate([url]));
  }


 isJsonString(str:string){
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
    handleDocHeightMsg = (e): void  => {
    console.log('MessageEvent:' + e.data);
    //  const data =  JSON.parse(e.data) ;

       if (e && e.data && this.isJsonString(e.data))   {
        
        var data = JSON.parse(e.data);

        if (data && data.docHeight)
           this.setIframeHeight('framePortal', data['docHeight']);


      }
    }
  
  
  



  setIframeHeight(id, ht) {
    const initsize = 114;
    const ifrm = document.getElementById(id);

    ifrm.style.visibility = 'hidden';
    ifrm.style.height = ht + 'px';
    ifrm.style.visibility = 'visible';
    // ifrm.style.overflow = 'hidden';
    // ifrm.style.overflowX = 'hidden';
    // ifrm.style.overflowY = 'hidden';

    const pos = document.getElementById('frameContainer');
    // pos.style.overflowY = 'hidden'; // hide vertical

    if ( ht > initsize) {
      if (pos != null) {
        pos.style.position = 'absolute' ;
       // pos.style.top = '0px';
      }
    } else {
      if (pos != null) {
        pos.style.position = 'relative' ;
      }
    }
  }
}
