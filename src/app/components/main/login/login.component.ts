import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from '../../../services/login/login.service';
import {JSONResponse} from '../../../domain/JSONResponse';
import {NgxPermissionsService} from 'ngx-permissions';
import {ADuser} from '../../../domain/ADuser';
import {PROFILES} from '../../../util/Constants';
import { OAuthService } from 'angular-oauth2-oidc';
import { filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CommonsService } from 'src/app/services/commons.service';
import { Subscription } from 'rxjs';
import jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  userNotFound = false;
  isEnabled = true;
  year = (new Date()).getFullYear();
  eventSubscription: Subscription | undefined;

  constructor(
    private oauthService: OAuthService,
    private commonsService: CommonsService,
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,

    private activatedRoute: ActivatedRoute,
    private permissionsService: NgxPermissionsService) {

      const currentLocation = window.location.origin + window.location.pathname;

      this.oauthService.configure({
        tokenEndpoint: environment.TOKEN_ENDPOINT,
        clientId: environment.CLIENT_ID,
        dummyClientSecret: environment.CLIENT_SECRET,
        showDebugInformation: true,

        disablePKCE:true,
        requireHttps:false,

        useHttpBasicAuth: false,
        scope: '',
        oidc: false
     });
      this.oauthService.setStorage(sessionStorage);
  }

  ngOnInit() {
    this.permissionsService.flushPermissions();
    localStorage.clear();
    //  Set -1 to avoid display the menu in login page
    const permissions = ['-1'];
    this.permissionsService.loadPermissions(permissions);

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    const ordrId = this.activatedRoute.snapshot.paramMap.get('orderID');
    const msisdnPOS = this.activatedRoute.snapshot.paramMap.get('msisdnPOS');

    console.log('The URL ordrId is: ' + ordrId);
    console.log('The URL msisdnPOS is: ' + msisdnPOS);

    if (!ordrId !== null) {
      localStorage.setItem('ordrIdPOS', ordrId);
      localStorage.setItem('msisdnPOS', msisdnPOS);
    }
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    const ordrId = this.activatedRoute.snapshot.paramMap.get('orderID');
    const msisdnPOS = this.activatedRoute.snapshot.paramMap.get('msisdnPOS');

    console.log('The URL ordrId is: ' + ordrId);
    console.log('The URL msisdnPOS is: ' + msisdnPOS);

    if (ordrId !== null) {
      localStorage.setItem('ordrIdPOS', ordrId);
    }

    this.loading = true;
    this.userNotFound = false;
    this.isEnabled = false;
    this.permissionsService.flushPermissions();

    this.oauthService.fetchTokenUsingPasswordFlow(  this.loginForm.get('username').value , this.loginForm.get('password').value).then((resp) => {
      console.log('got token');
    }).then(() => {
      console.log('logged in');
      const  user :any= jwt_decode(this.oauthService.getAccessToken());
      console.log(user);
      const permissions = user.authorities;
      if (permissions != null) {
        this.permissionsService.loadPermissions(permissions);
      }
      localStorage.setItem('username', user.username);
      localStorage.setItem('usernameInfo', user.usernameInfo);
      localStorage.setItem('group', user.group);
      localStorage.setItem('groupId', user.groupId);
      localStorage.setItem('entEntityId', user.entEntityId);
      localStorage.setItem('isDefaultWalletEnabled', user.isDefaultWalletEnabled);
      localStorage.setItem('dealer', user.dealer);

      if (permissions && permissions.includes( PROFILES.MONEY.SUPERVISEUR)) {
        this.router.navigate(['/listTransaction']);
      } else  if (permissions && permissions.includes(PROFILES.MONEY.REPORT_CENTRAL)) {
          this.router.navigate(['/listTransactionCash']);
        } else {
          let ordrId = this.activatedRoute.snapshot.paramMap.get('orderID');
          if (ordrId != null) {
            this.router.navigate(['/subscription']);
          } else {
            console.log("The user in component login is: " + localStorage.getItem("username"));
            console.log("The token in component login is: " + localStorage.getItem("jwt_token"));
            console.log("The lastRequest in component login is: " + localStorage.getItem("lastRequest"));
            this.router.navigate(['/resercher']);
          }
        }

        this.eventSubscription = this.oauthService.events.subscribe(e => {
        // tslint:disable-next-line:no-console
        console.debug('oauth/oidc event', e);
      });


     //    this.router.navigateByUrl('/home');
    }).catch((err) => {

      if (err.status === 400) {
        this.userNotFound = true;
      } else {
        this.commonsService.showPopupGeneralError();
      }
      this.loading = false;
      this.isEnabled = true;
      console.log('Error al hacer login' + err);
    });


  }

  get f() { return this.loginForm.controls; }

  hideMessage() {
    this.userNotFound = false;
  }



  ngOnDestroy(): void {
  if(this.eventSubscription)
   this.eventSubscription.unsubscribe();
}

}
