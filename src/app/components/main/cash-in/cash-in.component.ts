import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Client} from "../../../domain/Client";
import {CommonsService} from "../../../services/commons.service";
import {CashOrders} from "../../../domain/CashOrders";
import {IdentificationType} from "../../../domain/IdentificationType";
import {ActivatedRoute, Router} from "@angular/router";
import {CashInService} from "../../../services/cash-in/cash-in.service";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {NEVER, Subscription, timer} from "rxjs";
import {NgbModal, NgbModalOptions, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {CombosService} from "../../../services/combos.service";
import {PinPad} from "../../../domain/PinPad";
import {JasperService} from "../../../services/jasper.service";
import {environment} from "../../../../environments/environment";
import {catchError, switchMap, takeWhile} from "rxjs/operators";
import {JSONResponse} from "../../../domain/JSONResponse";
import {CASHIN} from "../../../util/Constants";
import {TranslateService} from "@ngx-translate/core";
import {startWith0607} from "../../../util/custom-validators";

let required = Validators.required;

@Component({
  selector: 'app-cash-in',
  templateUrl: './cash-in.component.html',
  styleUrls: ['./cash-in.component.css']
})
export class CashInComponent implements OnInit, OnDestroy {

  @ViewChild('mixerCash') mixerModalCash: ElementRef;

  cashinForm: FormGroup;
  identityTypes: IdentificationType[] = [];

  // TODO remove, only for testing
  isLocalEnv = environment.local;

  pin = '';
  pinPad: PinPad = new PinPad();
  pinFocus =true;

  enableCloseBtn: boolean = false;
  orderIsClosed: boolean = false;
  withExternalID: boolean = false;
  enableVerifyBtn: boolean = false;
  notRemoteOrder: boolean = true;
  orderCancel: boolean =false;
  private mixerSubscriptionCash: Subscription;
  private mixerModalRefCash: NgbModalRef;
  private enableRefresh: boolean = false;
  private cashOrder: CashOrders = new CashOrders();
  private client: Client;

  private alive: boolean = true;

  constructor(private cashInService: CashInService,
              private commonService: CommonsService,
              private subscriptionOrderService: SubscriptionOrderService,
              private activatedRoute: ActivatedRoute,
              private route: Router,
              private modalService: NgbModal,
              private combosService: CombosService,
              private sJasper: JasperService,
              private translateService: TranslateService) {
  }


  ngOnInit() {
    this.initForm();
    this.subscribe2Mixers();
    this.enableCloseBtn = false;

    let amount = this.activatedRoute.snapshot.paramMap.get('amount');
    if(amount != null){
      this.initRemoteScreenParameters();
    } else {
      this.initScreenParameters();

      let ordrId = this.activatedRoute.snapshot.paramMap.get('ordrId');
      if (ordrId != null) {
        this.loadOrderFromDatabase(ordrId);
      } else {
        this.fillInFormWithSessionCustomer();
      }
    }
  }

  getFormControl(controlName: string) {
    return this.cashinForm.get(controlName) as FormControl;
  }

  setPinPad(pinpad: PinPad) {
    if (pinpad) {
      this.pinPad = pinpad;
    }
  }

  eventClickButton(res){
    //Event to get the button clicked on the pinPad component
    if (res !== null || res !== undefined) {
      let button = res;

      if (button == "r") {
        this.pin = this.pin.substring(0, this.pin.length - 1);
      } else {
        this.pin = this.pin + button;
      }

      this.cashinForm.get('pin').setValue(this.pin);
    }
  }

  pinInputSelected(display:boolean){
    this.pinFocus = display;
  }

  refresh(showPopup: boolean = true) {
    this.cashInService.checkCallBack(this.cashOrder)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(response => {
      if (response != null && response.success && response.successResponse) {
        // callback received, cashin completed
        this.handleCallbackResponse(response);
      }else if(response.code && response.code ==400){
          this.commonService.closeMixerCash();
          this.cashInService.showPopupPosIntegrationFail();
          this.enableRefresh = false;
          this.orderIsClosed = false;
      } else if (showPopup){
        // callback pending
        this.cashInService.showPopupCashInInProgress();
      }
    }, () => {
      if (showPopup) {
        this.cashInService.showPopupConnectionError();
      }
    });

  }

  save(){
    this.cashInService.checkCallBack(this.cashOrder)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(response => {
      if (response != null && response.success && response.successResponse) {
        this.handleCallbackResponse(response);
      } else {
        this.navigateToOrderList();
      }
    }, checkCallBackErr => {
      this.navigateToOrderList();
    });
  }

  verify() {
    this.enableVerifyBtn = false;
    this.cashInService.cashInVerify(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {

      if (res.success) {
        if (res.errorMessage) {
          // Verify Limit reached
          this.cashInService.showPopupVerifyLimitReached();
          this.enableCloseButton();
        } else {
          // Cashin OK
          this.commonService.closeMixerCash();
          this.cashInService.showPopupCashInComplete();
          this.enableCloseButton();
        }
      } else {
        if (res.successResponse) {
          this.cashInService.showPopupVerifyFailed(res.successResponse);
        } else {
          this.cashInService.showCashInFailed();
        }
      }
    }, verifyErr => {
      this.cashInService.showCashInFailed();
    });
    this.enableVerifyTimer();
  }


  close(){
    //update order status --> Annulé
    this.cashInService.annulCashOrder(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      if(res){
        this.commonService.closeMixerCash();
        this.navigateToOrderList();
      }else{
        this.commonService.closeMixerCash();
        this.cashInService.showPopupConnectionError();
      }
    });

  }

  cancel(){
    //update order status --> Rejeté
    this.cashInService.rejectCashOrder(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      if(res){
        this.commonService.closeMixerCash();
        this.navigateToOrderList();
      }else{
        this.commonService.closeMixerCash();
        this.cashInService.showPopupConnectionError();
      }
    });
  }

  generateContract() {
    this.commonService.openMixer(this.translateService.instant('cashin.popup.pendingContract'));

    this.sJasper.getCashInPDF(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe( pdfResponse => {
      this.subscriptionOrderService.contractGenerated = true;
      this.commonService.closeMixer();
      this.serveFile2User(pdfResponse);
    }, err => {
      this.commonService.closeMixer();
      this.subscriptionOrderService.contractGenerated = false;
      this.cashInService.showPopupContractError();
    });

  }

  execute()  {
    this.pinFocus = true;
    this.disableFields(false);
    if (this.isFormValid()) {
      this.commonService.openMixer(this.translateService.instant('cashin.popup.pending'));
      this.initCashOrder();

      this.cashInService.createOrder(this.cashOrder).pipe(
        takeWhile( () => this.alive),
        catchError((createOrderErr) => {
          this.commonService.closeMixer();
          this.cashInService.showPopupConnectionError();
          return NEVER;
        }),
        switchMap(createOrderRes => {
          return this.handleCreateOrder(createOrderRes);
        })
      ).subscribe((cashInRes) => {
        this.handleCashinInit(cashInRes);
      } ,cashInErr => {
        this.initCashPopup();
      });
    } else{
      if(this.cashOrder.ordOrder != null
        && (this.cashOrder.ordOrder.ordSubStatus.statusId === CASHIN.NON_EDITABLE_MODE.POS_KO
          || this.cashOrder.ordOrder.ordSubStatus.statusId === CASHIN.NON_EDITABLE_MODE.CB)) {
        this.disableFields(true);
        this.cashinForm.controls['pin'].enable();
      }
      this.setFormAsTouched(true);
    }
  }

  resetForm() {
    this.cashinForm.reset();
    this.cashInService.removeSessionOrder();
    this.disableFields(false);
    this.pin = '';
    this.pinFocus = true;
    this.orderIsClosed = false;
    this.cashOrder = new CashOrders();
    this.enableCloseBtn = false;
  }

  back() {
    if(!this.notRemoteOrder) {
      this.route.navigate(['/listTransactionTable']);
    } else {
      if (this.cashOrder != null && this.cashOrder !== undefined
        && this.cashOrder.ordOrder != null) {
        this.route.navigate(['/listTransaction']);
      } else {
        this.route.navigate(['/resercher']);
      }
    }
  }

  ngOnDestroy(): void {
    this.enableRefresh = false;
    this.alive = false;
    this.cashInService.removeSessionOrder();
    this.subscriptionOrderService.removeSessionOrder();
    if(localStorage.getItem("lastRequest")!=null) {
      this.commonService.closeMixer();
      this.commonService.closeMixerCash();
      this.commonService.closePopup();
      this.commonService.closeCashPopup();
    }
  }

  // TODO remove, only for testing
  callbackConfirmation() {
    // this.cashinForm.get('phone').value
    this.cashInService.callBackConfirmation(this.cashOrder.ordOrder.ordrId).subscribe(res => {
      alert('callbackconfirmation ' + res.callBackCode + ' ' + res.callBackMessage);
    });
  }

  // TODO remove mock, only for testing
  fillForm(){
    this.cashinForm.get('phone').setValue('0666666666');
    this.cashinForm.get('amount').setValue('100');
    this.cashinForm.get('clientID').setValue('F0000');
    this.cashinForm.get('pin').setValue('0000');
    this.pin = '0000';

  }

  private initAutoRefresh() {
    this.enableRefresh = true;
   /* const refreshTime = this.cashInService.getRefreshDelay();
    timer(refreshTime, refreshTime).pipe(
      takeWhile(() => this.enableRefresh)
    ).subscribe((val) => {
      console.log('Checking callback ', val);
      this.refresh(false);
    });*/

  }

  private initForm() {
    this.cashinForm = new FormGroup({
      pin: new FormControl('', [required]),
      phone: new FormControl('', [required, Validators.pattern('[0-9]*'), Validators.minLength(10), startWith0607()]),
      identityType: new FormControl('', [required]),
      clientID: new FormControl('', [required]),
      amount: new FormControl('', [required, Validators.pattern('^[1-9]\\d*(\\.\\d+)?$')]),
    });
  }

  private initScreenParameters(){
    this.loadIdentificationTypeList();
  }

  private initRemoteScreenParameters(){
    let mobileNumber = this.activatedRoute.snapshot.paramMap.get('mobileNumber');
    let amount = this.activatedRoute.snapshot.paramMap.get('amount');

    this.cashinForm.get('amount').setValue(amount);
    this.cashinForm.get('phone').setValue(mobileNumber);

    this.orderIsClosed = true;
    this.notRemoteOrder = false;

    this.cashinForm.disable();
  }

  private loadIdentificationTypeList() {
    this.identityTypes = [];

    this.commonService.openMixer("Consultation en cours...");

    this.combosService.getIdentityTypes()
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(identificationTypeList => {
        if (identificationTypeList != null
          && identificationTypeList.success
          && identificationTypeList.successResponse !=null) {

          this.identityTypes = identificationTypeList.successResponse as IdentificationType[];
          let initType : IdentificationType = new IdentificationType();
          this.identityTypes.unshift(initType);

          this.commonService.closeMixer();
        }

        // Error on call
      }, error1 => {
        this.cashInService.showPopupConnectionError();
      });
  }

  private openMixer(MixerModal){

    const ngbModalOptions: NgbModalOptions = {
      centered: true,

      //To avoid user closing the Modal
      keyboard: false,
      backdrop : 'static'
    };

    this.mixerModalRefCash = this.modalService.open(MixerModal, ngbModalOptions);
  }

  private enableVerifyTimer() {
    this.cashInService.getVerifyDelay()
      .subscribe(response => {

        let timeout = response;

        setTimeout(() => {
          this.enableVerifyBtn = true;
        }, timeout);

      }, () => {
        const timeout = 60000;
        setTimeout(() => {
          this.enableVerifyBtn = true;
        }, timeout);
      });

  }

  private handleCallbackResponse(callbackResponse: JSONResponse) {
    this.cashOrder = callbackResponse.successResponse as CashOrders;
    this.cashInService.setSessionOrder(this.cashOrder);
    this.enableRefresh = false;
    this.orderIsClosed = true;
    if (this.cashOrder.externalIDCB === CASHIN.CALLBACK_STATUS.OK) {
      this.withExternalID = true;
      this.commonService.closeMixerCash();
      this.cashInService.showPopupCashInComplete();
    } else if (this.cashOrder.externalIDCB === CASHIN.CALLBACK_STATUS.TIMEOUT) {
      this.enableCloseButton();
      this.cashInService.showPopupCallbackTimeout();
    } else {
      this.enableCloseButton();
      this.cashInService.showCashInFailed();
    }
  }

  private enableCloseButton() {
    this.enableCloseBtn = true;
    this.orderIsClosed = true;
    this.enableRefresh = false;
    this.enableVerifyBtn = false;
  }

  private navigateToOrderList() {
    this.commonService.closeMixerCash();
    this.route.navigate(['listTransaction']);
  }

  private loadOrderFromDatabase(ordrId) {
    this.commonService.openMixer("Chargement des informations, veuillez patienter....");

    this.cashInService.getCustomerDetailInfo(ordrId)
      .subscribe(cashorder => {
        if (cashorder != null) {
          this.cashOrder = cashorder;
          this.client = cashorder.client;
          this.cashInService.setSessionOrder(cashorder);

          if (this.client) {

            if (this.client.identificationId )
              this.cashinForm.get('clientID').setValue(this.client.identificationId);

            if(this.client.identityType && this.client.identityType.identificationTypeID)
              this.cashinForm.get('identityType').setValue(this.client.identityType.identificationTypeID);

            if(this.client.msisdn)
              this.cashinForm.get('phone').setValue(this.client.msisdn);  //setValue(this.client.msisdn);
          }
          if (this.cashOrder.amount) {
            this.cashinForm.get('amount').setValue(this.cashOrder.amount);
          }
          //this.fillInFormWithSessionCustomer();

          if(this.cashOrder.ordOrder != null
            && (this.cashOrder.ordOrder.ordStatus.statusId === 3 || this.cashOrder.ordOrder.ordStatus.statusId === 5 || this.cashOrder.ordOrder.ordStatus.statusId === 4)) {
            this.orderIsClosed = true;
            if(this.cashOrder.ordOrder.ordStatus.statusId === 5 || this.cashOrder.ordOrder.ordStatus.statusId === 4){
              this.orderCancel = true;
            }
            else{
              this.orderCancel = false;

            }


            this.disableFields(true);

            if(this.cashOrder.externalID != null) {
              this.withExternalID = true;
            }
          }

          if(this.cashOrder.ordOrder != null
            && (this.cashOrder.ordOrder.ordSubStatus.statusId === CASHIN.NON_EDITABLE_MODE.POS_KO)) {
            this.disableFields(true);
            this.cashinForm.controls['pin'].enable();
          }

          this.commonService.closeMixer();
        }else{
          this.commonService.closeMixer();

          this.cashInService.showPopupConnectionError();
        }

        // Error on call
      }, () => {
        this.commonService.closeMixer();

        this.cashInService.showPopupConnectionError();
      });
  }

  private fillInFormWithSessionCustomer() {
    let sessionCustomer = this.subscriptionOrderService.getSessionOrder() != null ?
      this.subscriptionOrderService.getSessionOrder().client : window.history.state.client as Client;

    if (!sessionCustomer) {
      this.client = this.cashInService.getSessionOrder()!= null ? this.cashInService.getSessionOrder().client : null;
    } else {
      this.client = sessionCustomer;
    }
    if(this.client) {
      if(this.client.msisdn)
        this.cashinForm.get('phone').setValue(this.client.msisdn);  //setValue(this.client.msisdn);

      if (this.client.identificationId )
        this.cashinForm.get('clientID').setValue(this.client.identificationId);

      if(this.client.identityType && this.client.identityType.identificationTypeID)
        this.cashinForm.get('identityType').setValue(this.client.identityType.identificationTypeID);

      if (this.cashOrder.amount) {
        this.cashinForm.get('amount').setValue(this.cashOrder.amount);
      }

      this.cashOrder.client = this.client;
      this.disableFields(false);
    }
  }

  private disableFields(disable: boolean) {
    for (let key in this.cashinForm.controls){
      if (this.cashinForm.controls.hasOwnProperty(key)) {
        disable ? this.cashinForm.controls[key].disable() : this.cashinForm.controls[key].enable();
      }
    }
    disable ? this.cashinForm.controls['pin'].disable() : this.cashinForm.controls['pin'].enable();
  }

  private subscribe2Mixers() {
    this.mixerSubscriptionCash = this.commonService.mixersCash()
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(message => {
      if (message !== undefined) {

        if (this.mixerModalRefCash !== undefined) {
          this.mixerModalRefCash.close();
          this.mixerModalRefCash = undefined;
        }

        this.openMixer(this.mixerModalCash);

      } else {
        if (this.mixerModalRefCash !== undefined) {
          this.mixerModalRefCash.close();
          this.mixerModalRefCash = undefined;
        }
      }
    });
  }

  private serveFile2User(response) {
    let newBlob = new Blob([response.body], {type: "application/pdf"});

    // IE compatibility
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(newBlob);
      return;
    }

    const data = window.URL.createObjectURL(newBlob);
    let filename = response.headers.get("filename");
    let link = this.createLinkAndClick(data, filename);
    this.removeLink(data, link);
  }


  private removeLink(data, link) {
    setTimeout(function () {
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
  }

  private createLinkAndClick(data, filename) {
    let link = document.createElement('a');
    link.href = data;
    link.download = filename;
    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
    return link;
  }

  private isFormValid() {
    return this.pin && this.pin !== '' && this.cashinForm.valid;
  }

  private initCashOrder() {
    if(!this.cashOrder.client){
      let client = new Client();

      client.identificationId = this.cashinForm.controls['clientID'].value;
      client.identityType.identificationTypeID = this.cashinForm.controls['identityType'].value;

      this.cashOrder.client = client;
    }
    this.cashOrder.client.msisdn = this.cashinForm.controls['phone'].value;
    this.cashOrder.amount = this.cashinForm.controls['amount'].value;
    this.cashOrder.client.identificationId = this.cashinForm.controls['clientID'].value;
    this.cashOrder.msisdn = this.cashInService.getUserId();
    this.cashOrder.pin = this.pin;
    this.cashOrder.pinCode = this.pinPad.privateKey;
  }

  private handleCashinInit(initResponse: JSONResponse) {
    if (initResponse && initResponse.code) {
      switch(initResponse.code) {
        case 200:
          // cashin OK -> close order
          this.commonService.closeMixer();
          this.enableCloseButton();
          this.cashInService.showPopupCashInComplete();
          this.cashOrder = initResponse.successResponse as CashOrders;
          this.cashInService.setSessionOrder(this.cashOrder);
          this.withExternalID = true;
          break;
        case 400:
          //POS integration fail
          this.cashInService.showPopupPosIntegrationFail();
          this.commonService.closeMixer();
          this.enableRefresh = false;
          this.disableFields(true);
          break;
        case 500:
          // cashin KO unknown error -> wait for callback notification
          console.log('Cashin KO unknown error message: ', initResponse.errorMessage);
          this.initCashPopup();
          break;
        default:
          // cashin KO known error -> enable edit form
          console.log('Cashin KO known error message: ', initResponse.errorMessage);
          this.commonService.closeMixer();
          this.cashInService.showPopupKnowError(initResponse.errorMessage);
          this.disableFields(false);
          break;
      }
    } else {
      // cashin KO unknown error -> wait for callback notification
      console.log('Cashin KO message: ', initResponse.errorMessage);
      this.initCashPopup();
    }

  }

  // initCashIn OK, Timeout or unknown error
  private initCashPopup() {
    this.commonService.closeMixer();
    this.commonService.openMixerCash(this.translateService.instant('cashin.modal.title'));
    this.initAutoRefresh();
    this.enableVerifyTimer();
  }

  private handleCreateOrder(orderResponse: JSONResponse) {
    if (orderResponse != null && orderResponse.success) {
      this.cashOrder = orderResponse.successResponse as CashOrders;
      this.cashInService.setSessionOrder(this.cashOrder);
      this.disableFields(true);
      return this.cashInService.executeCashIn(this.cashOrder);
    } else {
      this.commonService.closePopup();
      this.commonService.closeMixer();
      this.cashInService.showPopupKnowError(orderResponse.errorMessage);
      return NEVER;
    }
  }

  private setFormAsTouched(touched: boolean) {
    for (const key in this.cashinForm.controls) {
      if (this.cashinForm.controls.hasOwnProperty(key)) {
        if (touched) {
          this.cashinForm.controls[key].markAsTouched();
        } else {
          this.cashinForm.controls[key].markAsUntouched();
        }
      }
    }
  }

}
