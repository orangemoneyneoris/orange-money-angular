import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Client} from '../../../domain/Client';
import {ClientsService} from '../../../services/clients.service';
import {CommonsService} from '../../../services/commons.service';
import {PopupObj} from '../../../util/PopupObj';
import {NEVER, Observable} from 'rxjs';
import {NavigationStart, Router} from "@angular/router";
import {catchError, filter, switchMap} from "rxjs/operators";
import {Wallet} from "../../../domain/Wallet";
import {WalletsService} from "../../../services/wallets.service";
import {IdentificationType} from "../../../domain/IdentificationType";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {SubscriptionOrder} from "../../../domain/SubscriptionOrder";
import {CombosService} from "../../../services/combos.service";
import {TranslateService} from "@ngx-translate/core";
import {JSONResponse} from "../../../domain/JSONResponse";
import {startWith0607} from "../../../util/custom-validators";
import {CustomerCardComponent} from "../customer-card/customer-card.component";

@Component({
  selector: 'ch-client',
  templateUrl: './search-client.component.html',
  styleUrls: ['./search-client.component.css']
})
export class SearchClientComponent implements OnInit {

  searched: boolean = false;
  navigationEvent: Observable<NavigationStart>; //Watch an event on a routing navigation start
  @ViewChild(CustomerCardComponent) hijo: CustomerCardComponent;

  searchClientForm = new FormGroup({
    msisdn: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), startWith0607()]),
    identificationId: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z0-9]*')]),
    idType: new FormControl({value: '', disabled:true}, [Validators.required]),
  });

  clientIsDefault = "Reg Ongoing" ;
  isDefaultOk = "Enrollment Done";
  noRegisterCodeDefault = "INTEROP_40082";
  defaultWalletError = false;
  errorTechnical = false;
  activeErrors = false;

  constructor(private clientService: ClientsService,
              private walletService: WalletsService,
              private sOrder: SubscriptionOrderService,
              private commonsService: CommonsService,
              private sCombos: CombosService,
              private router: Router,
              private translateService: TranslateService) {

    this.navigationEvent = router.events.pipe(filter(evt =>
      evt instanceof NavigationStart)) as Observable<NavigationStart>;
  }

  ngOnInit() {
    console.log("The user in component searchClient is: " + localStorage.getItem("username"));
    console.log("The token in component searchClient is: " + localStorage.getItem("jwt_token"));
    console.log("The lastRequest in component searchClient is: " + localStorage.getItem("lastRequest"));
    this.navigationEvent.subscribe(evt => {
      if (evt.url === '/resercher') {
        this.searched = false;
        this.sOrder.removeSessionOrder();
        this.getIdentityTypes();
        this.initializeObjects();
      }
    });

    this.getIdentityTypes();
    this.initializeObjects();
  }

  getSearched() {
    if(localStorage.getItem('showSearchPanel') === '1') {
      localStorage.setItem('showSearchPanel', null);
      this.searched = false;
      this.resetForm();
      return false;
    } else {
      return this.searched;
    }
  }

  getFormControl(controlName: string) {
    return this.searchClientForm.get(controlName) as FormControl;
  }

  getClient() {
    return this.sOrder.order.client;
  }

  getWallet() {
    return this.sOrder.order.wallet;
  }

  get idTypes(){
    return this.sCombos.identityTypes;
  }

  private getIdentityTypes() {
      this.sCombos.identityTypes = [];
      this.commonsService.openMixer('Demander au serveur la liste des types d\'identité. S\'il vous plaît, attendez...');

    this.sCombos.getIdentityTypes().subscribe(ajaxResponse => {
      if (ajaxResponse.success) {
        let idTypes = [];
        const response = ajaxResponse.successResponse as IdentificationType[];
        idTypes.push({viewValue: "", value: ""})
        for (let valueKey in response) {
          idTypes.push({viewValue: response[valueKey].name, value: response[valueKey].identificationTypeID});
        }
        this.sCombos.identityTypes = idTypes;
        this.commonsService.closeMixer();
        this.searchClientForm.controls['idType'].enable();

      } else {
        if(ajaxResponse.errorMessage!="FORBIDDEN"){
          this.commonsService.closeMixer();
          this.commonsService.closePopup();
          this.errorGettingComboFromServer(ajaxResponse.errorMessage);
        }
      }

    }, error1 => {
      this.commonsService.closeMixer();
      this.commonsService.closePopup();
      this.errorGettingComboFromServer(error1);
    });
  }

  private errorGettingComboFromServer(error) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('search.message.error.identificationType.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.message.error.identificationType.msg')+" " + error.message;
    this.commonsService.openGeneralPopup(popupObj);
  }

  private initializeObjects() {
      let order = new SubscriptionOrder();
      let client = new Client();
      let wallet = new Wallet();
      order.client = client;
      order.wallet = wallet;
      this.sOrder.setSessionOrder(order);
  }

  processForm() {

    this.activeErrors = true;
    if (!this.searchClientForm.valid) {
      this.setFormAsTouched(true);
    } else {
      // Get Form fields
      const msisdn2Search: string = this.searchClientForm.get('msisdn').value;
      const id: string = this.searchClientForm.get('identificationId').value;
      const idType: string = this.searchClientForm.get('idType').value;
      /*this.searchClient(msisdn2Search, id, idType);*/
      this.searchClientNew(msisdn2Search, id, idType);

      this.activeErrors = false;
    }
  }


  private searchClientNew(msisdn2Search: string, id: string, idType: string) {

    this.commonsService.openMixer('Merci de patienter, la recherche du client est en cours…');

    //  Call to mock search customer
    this.clientService.searchCustomer(msisdn2Search, id, idType).pipe(
      catchError((customerSearchedErr) => {

        this.commonsService.closeMixer();
        this.sOrder.removeSessionOrder();

        this.showPopupConnectionError();

        return NEVER;
      }),
      switchMap(customerSearched => {
        //  Call to mock search wallet
        if (customerSearched != null && customerSearched.success && customerSearched.successResponse!=null) {
          let client = customerSearched.successResponse as Client;
          this.searchClientForm.get('msisdn').setValue(client.msisdn);
        }else{
          this.errorTechnical=true;
        }
        return this.handleCustomerSearched(customerSearched);
      }),
      catchError((defaultWalletResErr) => {
        this.commonsService.closeMixer();
        this.sOrder.removeSessionOrder();

        return this.handleWalletDefault(defaultWalletResErr);
      }),
      switchMap(defaultWalletRes => {
        return this.handleWalletDefault(defaultWalletRes);
      })
    ).subscribe((ribResponse) => {
      this.handleRibResponse(ribResponse);
      this.hijo.checkExpiredDate();
    } , ribResponseErr => {
      this.commonsService.closeMixer();
      this.showPopupConnectionError();
    });

  }

  private handleCustomerSearched(customer: JSONResponse) {
    this.searched = true;
    if(customer == null ) { //  Error no controlado
      this.searched = false;

      this.commonsService.closeMixer();
      this.sOrder.removeSessionOrder();

      this.showPopupConnectionError();
    }else {
      if (customer.success) {
        if (customer.errorMessage === 'Id error') {
          this.searched = false;
          this.commonsService.closeMixer();
          this.sOrder.removeSessionOrder();

          this.showPopupErrorID();

          return NEVER;

        } else {
          let client = customer.successResponse as Client;
          this.sOrder.order.client = client;
          console.log("Customer found: " + this.sOrder.order.client.clientID);

          let walletSearched = this.setWalletInfoInClient(client);

          this.sOrder.order.wallet = walletSearched as Wallet;

          return this.walletService.getDefaultWalletStatus(this.sOrder.order.client.msisdn);
        }
      } else if(customer.errorMessage!=undefined && customer.code!=null){
        this.showPopupError(customer.errorMessage);

        this.commonsService.closeMixer();
        this.sOrder.removeSessionOrder();

        return NEVER;
      }
      else{
        //  Cliente no encontrado
        this.errorTechnical=false;

        this.commonsService.closePopup();
        this.commonsService.closeMixer();
        this.sOrder.removeSessionOrder();
        if(this.getClient()===undefined && this.getSearched()) {
          this.commonsService.closeMixer();
          this.showPopupClientNoExist();
        }
        return NEVER;
      }
    }
  }

  private setWalletInfoInClient (client: Client): Wallet {
    let walletSearched = new Wallet();
    walletSearched.statusAccount = client.statusAccount;
    walletSearched.accountBalance = client.accountBalance;
    walletSearched.accountLevel = client.accountLevel;
    walletSearched.defaultAccount = client.defaultAccount;
    walletSearched.enrolementWallet = client.enrolementWallet;
    walletSearched.sponsorCode = client.sponsorCode;
    walletSearched.expirationDate = client.expirationDate;
    walletSearched.walletOpeningReasons = client.walletOpeningReasons;
    walletSearched.walletDocuments = client.walletDocuments;

    return walletSearched;
  }

  private handleWalletDefault(isDefault: JSONResponse){
    if (isDefault != null && isDefault.success) {
      this.defaultWalletError = false;
      let walletOrder = this.sOrder.order.wallet;
      let status = isDefault.successResponse;
      // code = 78 + regOngoing - Button  make default enable and field en cours
      if(status.toString().toUpperCase() == this.clientIsDefault.toUpperCase()){
        walletOrder.defaultAccount = false;
        walletOrder.enrolementWallet="En cours";
        //code 00000 o 9  - Wallet is already default - Button make default disable
      }else if(status.toString().toUpperCase() == this.isDefaultOk.toUpperCase()){
        walletOrder.defaultAccount = true;
        walletOrder.enrolementWallet="Oui";
        //code 82 - Button make default enable
      }else if(status.toString().toUpperCase() == this.noRegisterCodeDefault.toUpperCase()){
        walletOrder.defaultAccount = false;
        walletOrder.enrolementWallet="Non";
      }
      this.sOrder.order.wallet = walletOrder;
      this.searched = true;

      return this.clientService.getRibCode(
        this.sOrder.order.client.msisdn,
        this.sOrder.order.client.identificationId,
        this.sOrder.order.client.identityType.identificationTypeID);
    } else {
      if(isDefault.errorMessage !== 'client not found') {
        this.searched = true;
        this.commonsService.closePopup();
        this.commonsService.closeMixer();
        if (isDefault.errorMessage) {
          this.defaultWalletError = true;
          this.sOrder.order.wallet.enrolementWallet = isDefault.errorMessage;
        } else {
          this.sOrder.order.wallet.enrolementWallet = "Error in default service";
        }
      }else{
        this.searched = true;
        this.sOrder.removeSessionOrder();
        this.commonsService.closePopup();
        this.commonsService.closeMixer();
      }

      return this.clientService.getRibCode(
        this.sOrder.order.client.msisdn,
        this.sOrder.order.client.identificationId,
        this.sOrder.order.client.identityType.identificationTypeID);;
    }
  }

  private handleRibResponse(response: JSONResponse) {
    if (response != null && response.success) {
      let rib = response.successResponse;

      this.sOrder.order.client.numRib = rib.toString();

      this.commonsService.closePopup();
      this.commonsService.closeMixer();

    }else{
      this.sOrder.order.client.numRib = '';

      this.commonsService.closePopup();
      this.commonsService.closeMixer();
    }
  }

  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('search.message.error.searchingClient.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.message.error.searchingClient.msg');
    this.commonsService.openGeneralPopup(popupObj);
  }

  private showPopupError(error : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = error;
    this.commonsService.openGeneralPopup(popupObj);
  }

  private showPopupErrorID() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title =  this.translateService.instant('search.message.form.mandatoryInformationID.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.message.form.mandatoryInformationID.msg');
    this.commonsService.openGeneralPopup(popupObj);
  }

  resetForm() {
    this.searchClientForm.reset();
    this.searched = false;
    this.sOrder.removeSessionOrder();
    this.activeErrors=false;
  }

  gotoCreateNewWallet() {
    let newCustomer: Client = new Client();
    newCustomer.identificationId = this.searchClientForm.get('identificationId').value;

    let idType: IdentificationType = new IdentificationType();
    const id = this.searchClientForm.get('idType').value;
    idType.identificationTypeID = parseInt(id);
    idType.name = this.commonsService.getComboValue(this.sCombos.identityTypes, id, 'viewValue');

    newCustomer.identityType = idType;
    newCustomer.msisdn = this.searchClientForm.get('msisdn').value;

    this.sOrder.orderfrozen=false;
    this.sOrder.contractUploaded=false;
    this.sOrder.contractGenerated=false;
    this.sOrder.contractPreviewed=undefined;
    this.sOrder.orderValid=undefined;

    this.sOrder.order = new SubscriptionOrder();
    this.sOrder.order.client = newCustomer;
    this.sOrder.order.wallet = new Wallet();
    this.router.navigate(["/subscription"]);
  }

  private setFormAsTouched(touched: boolean) {
    for (const key in this.searchClientForm.controls) {
      if (this.searchClientForm.controls.hasOwnProperty(key)) {
        if (touched) {
          this.searchClientForm.controls[key].markAsTouched();
        } else {
          this.searchClientForm.controls[key].markAsUntouched();
        }
      }
    }
  }

   return() {
     this.router.navigateByUrl('/DummyComponent', {skipLocationChange: true}).then(()=>
       this.router.navigate(["/resercher"]));
  }

  private showPopupClientNoExist() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('search.label.client.notExits.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.client.notExist');
    this.commonsService.openGeneralPopup(popupObj);
  }

}
