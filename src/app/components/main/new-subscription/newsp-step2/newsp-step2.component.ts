import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {JasperService} from "../../../../services/jasper.service";
import {CommonsService} from "../../../../services/commons.service";
import {WalletDocument} from "../../../../domain/WalletDocument";
import {SubscriptionOrderService} from "../../../../services/subscription-order.service";
import {SubscriptionOrder} from "../../../../domain/SubscriptionOrder";
import {NEVER} from "rxjs/index";
import {catchError, switchMap, takeWhile} from "rxjs/internal/operators";
import {ViewDocumentsService} from "../../../../services/View.Documents-Service/view-documents.service";
import {OrdSubStatus2} from "../../../../domain/OrdSubStatus2";
import {TranslateService} from "@ngx-translate/core";
import {ClientsService} from "../../../../services/clients.service";
import {JSONResponse} from "../../../../domain/JSONResponse";
import {PopupObj} from "../../../../util/PopupObj";
import {OrdOrder} from "../../../../domain/OrdOrder";
import {NewSubscriptionComponent} from "../new-subscription.component";
import {NEW_SUBSCRIPTION} from "../../../../util/Constants";

@Component({
  selector: 'app-newsp-step2',
  templateUrl: './newsp-step2.component.html',
  styleUrls: ['./newsp-step2.component.css']
})
export class NewspStep2Component implements OnInit, OnDestroy {

  @Output() step2Go = new EventEmitter<string>();

  LEVELN3 = NEW_SUBSCRIPTION.LEVELN3;

  showOtpModal: boolean = false;
  refId: string = '';

  private subsOrder: SubscriptionOrder;
  private alive: boolean = true;
  private removeSession: boolean = true;
  documentsSave: boolean = true;
  private listTypeDocuments: string[] = new Array<string>();
  private errorAttach: boolean = false;


  documentationForm = new FormGroup({
    defaultAccount: new FormControl(true),
    idObverseDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
    idObverseDocHidden: new FormControl({value: '', disabled:true}),
    idReverseDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
    idReverseDocHidden: new FormControl({value: '', disabled:true}),
    addressDoc: new FormControl({value: '', disabled:true}),
    addressDocHidden: new FormControl({value: '', disabled:true}),
    contractDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
    contractDocHidden: new FormControl({value: '', disabled:true}),
    otherDoc: new FormControl({value: '', disabled:true}),
    otherDocHidden: new FormControl({value: '', disabled:true})
  });

  constructor(private sCommons: CommonsService,
              private sOrder: SubscriptionOrderService,
              private sJasper: JasperService,
              private router: Router,
              private serviceDoc: ViewDocumentsService,
              private translate: TranslateService,
              private clientService: ClientsService,
              private parentNewSubscription: NewSubscriptionComponent) {}


  ngOnInit() {
    this.sCommons.closeMixer();

    //if(localStorage.getItem('ordrIdPOS')!=null){
     // localStorage.setItem('ordrIdPOS', null);

      this.sOrder.getSavedOrder(this.sOrder.order.ordOrder.ordrId.toString()).pipe(
        catchError((responseSearchErr) => {
          return NEVER;
        }),
        switchMap(res => {
          if(res.success && res.successResponse!=null) {
            this.sOrder.setSessionOrder(res.successResponse as SubscriptionOrder);
            this.subsOrder=res.successResponse as SubscriptionOrder;
          }
          return this.parentNewSubscription.handleCustomerSearched(res);
        })
      ).subscribe((defaultWalletRes) => {
        if (defaultWalletRes != null
          && defaultWalletRes.success
          && defaultWalletRes.successResponse != null) {
          this.subsOrder.wallet.walletDocuments = defaultWalletRes.successResponse as WalletDocument[];
        }
        this.synchronizeForm();
        this.setDefault()
      } , defaultWalletErr => {
        //error
        return NEVER;
      });
    //}

    let sessionOrder = this.sOrder.getSessionOrder();

    if (sessionOrder !== undefined) {
      this.subsOrder = sessionOrder;

        if(this.subsOrder.ordOrder.ordStatus.statusId==2){
             this.synchronizeForm();
          if (this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['contractDoc'].value!=null) {
            //this.documentationForm.controls['contractDoc'].enable();
            //this.documentationForm.controls['contractDocHidden'].enable();
            this.documentationForm.controls['idObverseDoc'].enable();
            this.documentationForm.controls['idObverseDocHidden'].enable();
            this.documentationForm.controls['idReverseDoc'].enable();
            this.documentationForm.controls['idReverseDocHidden'].enable();

            if(this.subsOrder && this.subsOrder.wallet.accountLevel === this.LEVELN3) {
              this.documentationForm.controls['addressDoc'].enable();
              this.documentationForm.controls['addressDocHidden'].enable();
            }
            this.documentationForm.controls['otherDoc'].enable();
            this.documentationForm.controls['otherDocHidden'].enable();
        }
        if(this.contractPreviewed==true){
          this.documentationForm.controls["defaultAccount"].disable();
        }
        if(this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
          && this.documentationForm.controls['idReverseDoc'].value!="" && this.documentationForm.controls['addressDoc'].value!=""
          && this.subsOrder.wallet.accountLevel.toLowerCase()== this.LEVELN3.toLowerCase()){
          this.sOrder.contractUploaded = true;
          this.sOrder.orderfrozen = true;
          this.disableAllFields();
        }else if(this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
          && this.documentationForm.controls['idReverseDoc'].value!="" && this.subsOrder.wallet.accountLevel.toLowerCase()!=this.LEVELN3.toLowerCase()){
          this.sOrder.contractUploaded = true;
          this.sOrder.orderfrozen = true;
          this.disableAllFields();
        }
      }
      else{
        this.removeSession=false;
        this.router.navigate(["wallet-preview"]);
      }

    } else {
      this.subsOrder = new SubscriptionOrder();
    }

    if (this.sOrder.orderfrozen) {
      this.sOrder.orderfrozen=true;
      this.sOrder.contractGenerated=true;
      this.sOrder.contractUploaded=true;
      this.disableAllFields();
    }

    this.checkAccountLevel();

    if(this.subsOrder.wallet.defaultAccount!=null){
      this.documentationForm.controls['defaultAccount'].setValue(this.subsOrder.wallet.defaultAccount);
    }
    else{
      this.setDefault();
    }

    if(localStorage.getItem('isDefaultWalletEnabled') != 'Y'){
      this.documentationForm.controls['defaultAccount'].disable();
      this.documentationForm.controls['defaultAccount'].setValue(false);
    }

  }

  gotoPrevious() {
    this.saveOrder(false, false);

    this.removeSession=false;
    this.step2Go.emit('step1');
  }

  cancelOrder() {
    this.sOrder.contractPreviewed = false;
    this.sOrder.orderValid = false;
    this.sOrder.contractGenerated = false;
    this.sOrder.setSessionOrder(new SubscriptionOrder());
    // noinspection JSIgnoredPromiseFromCall
    this.sOrder.cancelOrder(this.subsOrder.ordOrder).subscribe(res => {
      if(res.success){
        this.router.navigate(['/listTransaction']);
      }else{
        if(localStorage.getItem("lastRequest")!=null) {
          this.sOrder.cancelError();
        }
      }
    });
  }

  getFormControl(controlName: string) {
    return this.documentationForm.get(controlName) as FormControl;
  }

  get isN3Level(){
    return this.subsOrder && this.subsOrder.wallet.accountLevel !== this.LEVELN3 ? "grayedIcon" : "";
  }

  get contractGenerated(){
    return this.sOrder.contractGenerated;
  }

  get contractUploaded(){
    return this.sOrder.contractUploaded;
  }

  get orderFrozen(){
    return this.sOrder.orderfrozen;
  }

  get orderId() {
    return this.subsOrder && this.subsOrder.ordOrder.ordrId;
  }

  get msisdn() {
    return this.subsOrder && this.subsOrder.client.msisdn;
  }

  get walletId() {
    return this.subsOrder && this.subsOrder.wallet ? this.subsOrder.wallet.walletID : null;
  }

  previewContract() {
    // noinspection JSIgnoredPromiseFromCall
    this.removeSession=false;
    this.router.navigate(["wallet-preview"]);
  }

  generateContract() {

    this.sCommons.openMixer("Générer le contrat. S'il vous plaît, attendez...");

    if(localStorage.getItem('isDefaultWalletEnabled') != 'Y'){
      this.subsOrder.wallet.defaultAccount=false;
    }
    this.clientService.getRibCodeInSubscription(this.subsOrder).pipe(
      takeWhile( () => this.alive),
      catchError((ribResponseErr) => {
        this.sCommons.closeMixer();
        this.sOrder.contractGenerated = false;
        this.sOrder.showPopupContractError();
        return NEVER;
      }),
      switchMap(ribResponse => {
        this.documentationForm.controls['contractDoc'].enable();
        this.documentationForm.controls['contractDocHidden'].enable();
        return this.handleRibResponse(ribResponse);
      })
    ).subscribe((contractResponse) => {
      this.handleContractGenerated(contractResponse);
    } , contractResponseErr => {
      this.sCommons.closeMixer();
      this.sOrder.contractGenerated = false;
      this.sOrder.showPopupContractError();
    });
  }

  get contractPreviewed(){
    return this.sOrder.contractPreviewed;
  }

  sendContract() {
    this.sCommons.openMixer(this.translate.instant('new_subscription.waitting'));

    this.sOrder.saveOrder(this.subsOrder).pipe(
      takeWhile(() => this.alive),
      catchError(saveOrderResErr => {
          this.sCommons.closeMixer();
          this.sOrder.showPopupNoResponse();
          return NEVER;
        }
      ),
      switchMap(saveOrderRes => {
        //this.handleResponseDocument();
        if (saveOrderRes) {
          if (saveOrderRes.success) {
            this.sCommons.closeMixer();
            this.subsOrder.ordOrder =saveOrderRes.successResponse as OrdOrder;
            if (saveOrderRes.errorMessage != null && saveOrderRes.errorMessage.startsWith("KO")) {

              this.showError8500(saveOrderRes.errorMessage.replace("KO - ", ""));
              setTimeout(() => {
                  this.sCommons.closePopup();
                  this.router.navigate(['/listTransaction']);
                },
                3000);
            } else {
              if (this.subsOrder.wallet.defaultAccount && saveOrderRes.code==82000) {
                this.showOtpModal = true;
              } else {
                //  ThirdSubscription ->  OK
                this.showPopSuccesUpgradePopUp(saveOrderRes.errorMessage.replace("OK - ", ""));
                setTimeout(() => {
                    this.sCommons.closePopup();
                    this.router.navigate(['/listTransaction']);
                  },
                  3000);
              }
            }
            return NEVER;
          } else {
            if(saveOrderRes.successResponse!=null){
              let listDocuments: string[] = saveOrderRes.successResponse as string[];
              if(listDocuments!=null && listDocuments.length>0){
                this.enableAllFields();
                for(let i = 0; i<listDocuments.length;i++){
                  this.resetFile(listDocuments[i])
                }
              }
            }
            this.sCommons.closeMixer();
            this.sOrder.showPopupIntegrationKO(saveOrderRes.errorMessage);
            return NEVER;
          }
        }else{
          this.sCommons.closeMixer();
          this.sOrder.showPopupNoResponse();
          return NEVER;
        }

      })).subscribe(secondResponse => {
      //segunda respuesta
      console.log('Creado orden en de document');
    }, walletDocumentsErr => {
      this.sCommons.closeMixer();
      this.sOrder.showPopupConnectionDBError();
      setTimeout(() => {
          this.sCommons.closePopup();
          this.router.navigate(['/listTransaction']);
        },
        3000);
    });


  }


  /*handleResponseDocument(){
    this.serviceDoc.ValidationDocument(this.subsOrder.ordOrder).subscribe(response=>{
      console.log("Send Document");
    });
  }*/

  resetFile(controlName: string) {
    const control = this.documentationForm.controls[controlName];
    if (!control.disabled || this.errorAttach) {
      if(this.listTypeDocuments.indexOf(controlName)>=0){
        this.listTypeDocuments.splice(this.listTypeDocuments.indexOf(controlName),1);
      }
      control.reset();
      this.documentationForm.controls[controlName+'Hidden'].reset();
      let indexOf = this.getDocumentIndex(controlName);

      if(indexOf>=0) {
        this.subsOrder.wallet.walletDocuments.splice(indexOf, 1);
      }
      this.documentationForm.controls[controlName].setValue("");

      this.sOrder.setSessionOrder(this.subsOrder);
    }
  }

  attachFile2Wallet(controlName: string, $event: any) {
    this.sCommons.openMixer("Joindre un document...");
    let documents = $event.target.files;
    let file: File = documents[0];
    let fileName = file.name;
    let sizeValid : boolean = file.size/1024/1024 <=10;
    let extensionValid : boolean = this.sOrder.extensionValidate(fileName);
    if(extensionValid && sizeValid){
      this.documentationForm.controls[controlName].setValue(fileName);
      this.listTypeDocuments.push(controlName);
      this.saveDocumentOnWallet(controlName, file, fileName);
      this.sCommons.closeMixer();
    }else if(!extensionValid){
      this.sCommons.closeMixer();
      this.sOrder.showPopupExtensionNoValid();
    }else{
      this.sCommons.closeMixer();
      this.sOrder.showPopupSizeNoValid();
    }
  }

  setDefault() {
    this.subsOrder.wallet.defaultAccount = this.documentationForm.controls['defaultAccount'].value;
  }

  unfreezeOrder() {
    if(this.documentationForm.controls['idObverseDoc'].value !=null && this.documentationForm.controls['idReverseDoc'].value!=null){
      this.listTypeDocuments = new Array<string>();
      this.sOrder.contractUploaded = false;
      this.sOrder.contractGenerated = false;
      this.sOrder.contractPreviewed = false;
      this.sOrder.orderfrozen = false;
      this.documentationForm.controls['contractDoc'].reset();
      this.documentationForm.controls['contractDocHidden'].reset();
      let indexOf = this.getDocumentIndex('contractDoc');
      if(indexOf>=0) {
        this.subsOrder.wallet.walletDocuments.splice(indexOf, 1);
      }
      this.documentationForm.controls['contractDoc'].setValue("");
      this.sOrder.setSessionOrder(this.subsOrder);
      let subStatus2: OrdSubStatus2 = new OrdSubStatus2();
      subStatus2.status2Id=4;
      subStatus2.name="step1_in_progress"
      this.sOrder.order.ordOrder.ordSubStatus2= subStatus2;

      //this.enableAllFields();
      //this.documentationForm.controls['contractDoc'].disable();
      //this.documentationForm.controls['contractDocHidden'].disable();

      this.sOrder.showPopupEditable();
      setTimeout(() => {
        this.sCommons.closePopup();
      }, 2000);
    }
  }

  saveOrder(showPopups: boolean, showFrozenPopup: boolean){
    this.documentsSave=false;
    const order = this.sOrder.order;

    let subscriptionOrder: SubscriptionOrder;
    if (this.sOrder.order === undefined) {
      subscriptionOrder = new SubscriptionOrder();
      this.sOrder.order = subscriptionOrder;
    } else {
      subscriptionOrder = this.sOrder.order;
    }

    subscriptionOrder.ordOrder = order.ordOrder;
    subscriptionOrder.client = this.sOrder.order.client;
    subscriptionOrder.wallet = this.sOrder.order.wallet;
    subscriptionOrder.id = this.sOrder.order.id;

    if (showPopups){
      this.sCommons.openMixer("Saving order. Please, wait...");
    }

    this.sOrder.saveWallet(subscriptionOrder).pipe(
      takeWhile(() => this.alive),
      catchError(saveWalletErr => {
          if(localStorage.getItem("lastRequest")!=null) {
            this.sCommons.closeMixer();
            this.sOrder.showPopupSaveError(saveWalletErr);
          }
          return NEVER;
        }
      ),
      switchMap(ajaxResponse => {
        if (ajaxResponse !== undefined && ajaxResponse.success) {
          let response = ajaxResponse.successResponse as SubscriptionOrder;

          if(!this.orderFrozen && this.subsOrder.wallet.walletDocuments.length) {
            let walletDocuments: WalletDocument[] = new Array<WalletDocument>();
            this.subsOrder.wallet.walletDocuments.forEach(document => {
              if(document.walletDocumentID!= undefined) {
                walletDocuments.push(document);
              }
            });
            this.sOrder.deleteWalletDocuments(walletDocuments).subscribe(res => {
              console.log("Documentos borrados")
            });
          }

          let files: File[] = new Array<File>();
          let walletFiles: WalletDocument[] = new Array<WalletDocument>();
          this.subsOrder.wallet.walletDocuments.forEach(document => {
            if(document.document["name"]!= undefined) {
              files.push(document.document);
              walletFiles.push(document)
            }
          });

          if (walletFiles.length!=0) {
            this.errorAttach=false;
            return this.sOrder.saveWalletDocuments(files, subscriptionOrder.wallet.walletID, walletFiles, this.listTypeDocuments.length);
          } else {
            this.errorAttach=true;
            this.checkDocumentsAttach(walletFiles);
            this.sCommons.closeMixer();
            this.router.navigate(['/listTransaction']);
          }
        } else {
          let error = ajaxResponse !== undefined ? ajaxResponse.errorMessage : "";
          if(localStorage.getItem("lastRequest")!=null) {
            this.sCommons.closeMixer();
            this.sOrder.showPopupSaveError(error);
          }
        }
        return NEVER;
      })).subscribe(walletDocumentsResponse => {

        if (showPopups) {
          this.sCommons.closeMixer();
          this.router.navigate(['/listTransaction']);
        } else {
          if (walletDocumentsResponse.success) {
            this.subsOrder.wallet.walletDocuments = walletDocumentsResponse.successResponse as WalletDocument[];
            if(showFrozenPopup){
              this.sOrder.showPopupFrozenOrder();
              this.documentsSave=true;
            }
          }else if(walletDocumentsResponse.successResponse!=null){
            this.checkDocumentsAttachBackend(walletDocumentsResponse.successResponse as string[]);
          }else{
            this.errorAttach=true;
            this.sOrder.showPopupDocumentAttach(walletDocumentsResponse.errorMessage);
            this.resetFile(walletDocumentsResponse.errorMessage);
            this.documentationForm.controls['contractDoc'].enable();
            this.documentationForm.controls['contractDocHidden'].enable();
            this.documentationForm.controls['idObverseDoc'].enable();
            this.documentationForm.controls['idObverseDocHidden'].enable();
            this.documentationForm.controls['idReverseDoc'].enable();
            this.documentationForm.controls['idReverseDocHidden'].enable();
            if(this.subsOrder && this.subsOrder.wallet.accountLevel === this.LEVELN3){
              this.documentationForm.controls['addressDoc'].enable();
              this.documentationForm.controls['addressDocHidden'].enable();
            }
            this.documentationForm.controls['otherDoc'].enable();
            this.documentationForm.controls['otherDocHidden'].enable();
          }
      }
    }, walletDocumentsErr => {
      this.sCommons.closeMixer();
      this.sOrder.showPopupNotConnected();
    });
  }

  closeOtpModal() {
    this.showOtpModal = false;
    this.router.navigate(['listTransaction']);
  }

  ngOnDestroy(): void {
    if(this.removeSession){
      this.sOrder.removeSessionOrder();
      this.sOrder.contractUploaded = false;
      this.sOrder.contractGenerated = false;
      this.sOrder.contractPreviewed = false;
      this.sOrder.orderfrozen = false;
    }
    this.alive = false;
  }

  private listenClosePopup() {
    this.sCommons.popups()
      .pipe(takeWhile(() => this.alive))
      .subscribe(res => {
        this.router.navigate(['listTransaction']);
      });
  }

  // noinspection JSMethodCanBeStatic
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with ${reason}`;
    }
  }

  private serveFile2User(response) {
    let newBlob = new Blob([response.body], {type: "application/pdf"});

    // IE compatibility
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(newBlob);
      return;
    }

    const data = window.URL.createObjectURL(newBlob);

    let filename = response.headers.get("filename");
    let link = this.createLinkAndClick(data, filename);
    this.removeLink(data, link);
  }

  private removeLink(data, link) {
    setTimeout(function () {
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
  }

  // noinspection JSMethodCanBeStatic
  private createLinkAndClick(data, filename) {
    let link = document.createElement('a');
    link.href = data;
    link.download = filename;
    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
    return link;
  }

  private saveDocumentOnWallet(controlName: string, file: File, fileName) {
    let walletDocument: WalletDocument = new WalletDocument();
    walletDocument.document = file;
    walletDocument.documentType = controlName;
    walletDocument.documentName = fileName;
    let alreadyExists = this.subsOrder.wallet.walletDocuments.find(document => {
      return document.documentType === walletDocument.documentType
    });
    if (alreadyExists !== undefined) {
      this.resetFile(walletDocument.documentType);
    }

    this.subsOrder.wallet.walletDocuments.push(walletDocument);
    this.sOrder.setSessionOrder(this.subsOrder);

    this.synchronizeForm();

    if(this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
      && this.documentationForm.controls['idReverseDoc'].value!="" && this.documentationForm.controls['addressDoc'].value!=""
      && this.subsOrder.wallet.accountLevel.toLowerCase()== this.LEVELN3.toLowerCase()) {
      this.sOrder.contractUploaded = true;
      this.sOrder.orderfrozen = true;
      this.disableAllFields();
      this.saveOrder(false, true);
    }else if(this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
      && this.documentationForm.controls['idReverseDoc'].value!="" && this.subsOrder.wallet.accountLevel.toLowerCase()!= this.LEVELN3.toLowerCase()){
      this.sOrder.contractUploaded = true;
      this.sOrder.orderfrozen = true;
      this.disableAllFields();
      this.saveOrder(false, true);
    }
  }

  private disableAllFields() {
    for (let controlsKey in this.documentationForm.controls) {
      this.documentationForm.controls[controlsKey].disable();
    }
  }

  private enableAllFields() {
    for (let controlsKey in this.documentationForm.controls) {
      this.documentationForm.controls[controlsKey].enable();
    }
    if(localStorage.getItem('isDefaultWalletEnabled') != 'Y'){
      this.documentationForm.controls["defaultAccount"].disable();
      this.documentationForm.controls['defaultAccount'].setValue(false);
    }
  }

  private synchronizeForm() {
    if(!this.contractGenerated){
      this.documentationForm.controls['contractDoc'].reset();
      this.documentationForm.controls['contractDocHidden'].reset();
      let indexOf = this.getDocumentIndex('contractDoc');
      if(indexOf>=0) {
        this.subsOrder.wallet.walletDocuments.splice(indexOf, 1);
      }
      this.documentationForm.controls['contractDoc'].setValue("");
      this.sOrder.setSessionOrder(this.subsOrder);
    }
    if(this.subsOrder.wallet.walletDocuments!=undefined) {
      this.subsOrder.wallet.walletDocuments.forEach((document) => {
        this.documentationForm.controls[document.documentType].setValue(document.documentName);
      });
    }
  }

  private checkAccountLevel() {
    if (this.subsOrder.wallet.accountLevel !== this.LEVELN3) {

      if(this.subsOrder.wallet.walletDocuments!=undefined) {
        let indexOf = this.getDocumentIndex('addressDoc');

        if (indexOf >= 0) {
          this.subsOrder.wallet.walletDocuments.splice(indexOf, 1);
          this.sOrder.setSessionOrder(this.subsOrder);
        }

      }
      let control = this.documentationForm.controls['addressDoc'];
      control.disable();
      control.reset();

      let hiddenControl = this.documentationForm.controls['addressDocHidden'];
      hiddenControl.disable();
      hiddenControl.reset();

      this.documentationForm.controls['addressDoc'].clearValidators();

    } else {

      this.documentationForm.controls['addressDoc'].setValidators([Validators.required]);
    }
  }

  private getDocumentIndex(documentType: string): number {
    let document = this.subsOrder.wallet.walletDocuments.find(function (document) {
      return document.documentType === documentType;
    });
    return this.subsOrder.wallet.walletDocuments.indexOf(document);
  }

  private showSuccesSentPopup() {
    this.sCommons.closeMixer();
    this.sOrder.showPopupSubscriptionComplete();
    this.listenClosePopup();
  }

  private showTimeOut(errorMessage: string) {
    this.sCommons.closeMixer();
    this.sOrder.showTimeOut(errorMessage);
    this.listenClosePopup();
  }

  showError8500(errorMessage: string) {
    this.sOrder.showError8500(errorMessage);
    this.listenClosePopup();
  }

  private handleRibResponse(response: JSONResponse) {
    if (response != null && response.success) {
      this.subsOrder.ribCode = response.successResponse.toString();

      this.sJasper.getPDF(this.subsOrder)
        .pipe(takeWhile(() => this.alive))
        .subscribe(response => {
          this.sCommons.closeMixer();
          this.serveFile2User(response);
          this.sOrder.contractGenerated = true;
          this.documentationForm.controls['contractDoc'].enable();
          this.documentationForm.controls['contractDocHidden'].enable();
          this.documentationForm.controls['idObverseDoc'].enable();
          this.documentationForm.controls['idObverseDocHidden'].enable();
          this.documentationForm.controls['idReverseDoc'].enable();
          this.documentationForm.controls['idReverseDocHidden'].enable();
          if(this.subsOrder && this.subsOrder.wallet.accountLevel === this.LEVELN3){
            this.documentationForm.controls['addressDoc'].enable();
            this.documentationForm.controls['addressDocHidden'].enable();
          }
          this.documentationForm.controls['otherDoc'].enable();
          this.documentationForm.controls['otherDocHidden'].enable();

        },() => {
          this.sCommons.closeMixer();
          this.sOrder.contractGenerated = false;
          this.sOrder.showPopupContractError();
        });

    }else{
      this.sOrder.order.client.numRib = 'RIB code not found';

      this.sCommons.closePopup();
      this.sCommons.closeMixer();

      this.showPopupErrorRIB();

      return NEVER;
    }
  }

  private handleContractGenerated(response: JSONResponse) {
    if (response != null && response.success) {
      this.sJasper.getPDF(this.subsOrder)
        .pipe(takeWhile(() => this.alive))
        .subscribe(response => {
          this.sCommons.closeMixer();
          this.serveFile2User(response);
          this.sOrder.contractGenerated = true;
          this.documentationForm.controls['contractDoc'].enable();
          this.documentationForm.controls['contractDocHidden'].enable();
          this.documentationForm.controls['idObverseDoc'].enable();
          this.documentationForm.controls['idObverseDocHidden'].enable();
          this.documentationForm.controls['idReverseDoc'].enable();
          this.documentationForm.controls['idReverseDocHidden'].enable();
          if(this.subsOrder && this.subsOrder.wallet.accountLevel === this.LEVELN3){
            this.documentationForm.controls['addressDoc'].enable();
            this.documentationForm.controls['addressDocHidden'].enable();
          }
          this.documentationForm.controls['otherDoc'].enable();
          this.documentationForm.controls['otherDocHidden'].enable();

        },() => {
          this.sCommons.closeMixer();
          this.sOrder.contractGenerated = false;
          this.sOrder.showPopupContractError();
        });

    }else{
      this.sOrder.order.client.numRib = 'RIB code not found';

      this.sCommons.closePopup();
      this.sCommons.closeMixer();

      this.showPopupErrorRIB();

      return NEVER;
    }
  }

  private showPopupErrorRIB() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = 'Code RIB introuvable, poursuivez le processus plus tard.';
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopSuccesUpgradePopUp(message: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-info';
    popupObj.popupMessage = message;
    popupObj.redirect=true;
    this.sCommons.openGeneralPopup(popupObj);
  }

  private checkDocumentsAttach(documents: WalletDocument[]){
    let exist: boolean;
    for(let i = 0; i<this.listTypeDocuments.length;i++){
      exist=false;
      for(let j = 0; j<documents.length;j++){
        if(this.listTypeDocuments[i]==documents[j].documentType){
          exist=true;
        }
      }
      if(!exist){
        let documentType: string;
        if(this.listTypeDocuments[i]=='idObverseDoc'){
          documentType=this.translate.instant('new_subscription.field.docObserve');
        }else if(this.listTypeDocuments[i]=='idReverseDoc'){
          documentType=this.translate.instant('new_subscription.field.docReverse');
        }else if(this.listTypeDocuments[i]=='addressDoc'){
          documentType=this.translate.instant('new_subscription.field.docJustification');
        }else if(this.listTypeDocuments[i]=='contractDoc'){
          documentType=this.translate.instant('new_subscription.field.boxSigned');
        }else if(this.listTypeDocuments[i]=='otherDoc'){
          documentType=this.translate.instant('new_subscription.field.docOther');
        }
        this.sOrder.showPopupDocumentAttach(documentType);
        this.resetFile(this.listTypeDocuments[i]);
      }
    }
  }

  private checkDocumentsAttachBackend(documents: string[]){
    /*let exist: boolean;
    for(let i = 0; i<this.listTypeDocuments.length;i++){
      exist=false;
      for(let j = 0; j<documents.length;j++){
        if(this.listTypeDocuments[i]==documents[j]){
          exist=true;
        }
      }
      if(!exist) {
        let documentType: string;
        if (this.listTypeDocuments[i] == 'idObverseDoc') {
          documentType = this.translate.instant('new_subscription.field.docObserve');
        } else if (this.listTypeDocuments[i] == 'idReverseDoc') {
          documentType = this.translate.instant('new_subscription.field.docReverse');
        } else if (this.listTypeDocuments[i] == 'addressDoc') {
          documentType = this.translate.instant('new_subscription.field.docJustification');
        } else if (this.listTypeDocuments[i] == 'contractDoc') {
          documentType = this.translate.instant('new_subscription.field.boxSigned');
        } else if (this.listTypeDocuments[i] == 'otherDoc') {
          documentType = this.translate.instant('new_subscription.field.docOther');
        }
        this.sOrder.showPopupDocumentAttach(documentType);
        this.resetFile(this.listTypeDocuments[i]);
      }
    }*/
    this.errorAttach=true;
    this.sOrder.showPopupDocumentAttach("");
    this.resetFile('contractDoc');
  }
}
