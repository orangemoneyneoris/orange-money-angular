import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewspStep2Component } from './newsp-step2.component';

describe('NewspStep2Component', () => {
  let component: NewspStep2Component;
  let fixture: ComponentFixture<NewspStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewspStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewspStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
