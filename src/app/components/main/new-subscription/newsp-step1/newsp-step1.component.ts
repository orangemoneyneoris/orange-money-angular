import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output,
  ViewChild
} from '@angular/core';
import {ComboOption} from '../../../html/util/utils';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientsService} from '../../../../services/clients.service';
import {CommonsService} from '../../../../services/commons.service';
import {dateIsValid, expirationDate, NationalityOverIdType, under18, startWith0607} from '../../../../util/custom-validators';
import {OpeningReason} from '../../../../domain/OpeningReason';
import {IdentificationType} from '../../../../domain/IdentificationType';
import {Profession} from '../../../../domain/Profession';
import {Nationality} from '../../../../domain/Nationality';
import {Gender} from '../../../../domain/Gender';
import {SubscriptionOrder} from '../../../../domain/SubscriptionOrder';
import {SubscriptionOrderService} from '../../../../services/subscription-order.service';
import {Client} from '../../../../domain/Client';
import {Wallet} from '../../../../domain/Wallet';
import {CombosService} from '../../../../services/combos.service';
import {ActivatedRoute, Router} from '@angular/router';
import {WalletDocument} from '../../../../domain/WalletDocument';
import {switchMap} from 'rxjs/operators';
import {forkJoin, NEVER, Observable, of} from 'rxjs';
import {NEW_SUBSCRIPTION} from '../../../../util/Constants';
import {catchError, takeWhile, tap} from 'rxjs/internal/operators';
import {JSONResponse} from '../../../../domain/JSONResponse';
import {environment} from '../../../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {EntCountry} from '../../../../domain/EntCountry';
import {Salary} from '../../../../domain/Salary';
import {PopupObj} from "../../../../util/PopupObj";
import {NgbInputDatepicker} from "@ng-bootstrap/ng-bootstrap";

let required = Validators.required;

@Component({
  selector: 'app-newsp-step1',
  templateUrl: './newsp-step1.component.html',
  styleUrls: ['./newsp-step1.component.css']
})
export class NewspStep1Component implements OnInit, OnDestroy {

  @ViewChild('expirationDate') nameRef: NgbInputDatepicker;

  @Output()
  step2Go = new EventEmitter<string>();

  subscriptionForm = new FormGroup({
    msisdn: new FormControl('', [required, Validators.pattern('[0-9]*'), Validators.minLength(10), startWith0607()]),
    nationality: new FormControl({value: '', disabled:true}), //Validators are set aside
    identityType: new FormControl(''), //Validators are set aside
    identificationId: new FormControl('', [required, Validators.pattern('^[A-Za-z0-9]*')]),
    expirationDate: new FormControl({value: '', disabled: true}, [required, expirationDate(), dateIsValid()]),
    gender: new FormControl({value: '', disabled:true}, [required]),
    name: new FormControl({value: '', disabled:true}, [required]),
    surname: new FormControl({value: '', disabled:true}, [required]),
    birthDate: new FormControl({value: '', disabled:true}, [required, under18(), dateIsValid()]),
    accountLevel: new FormControl({value:'', disabled: true}, [required]),
    entCountry: new FormControl({value:'', disabled: true}, [required]),
    entVille: new FormControl({value:'', disabled: true}, [required]),
    entCodePostal: new FormControl({value:'', disabled: true}, [required, Validators.pattern('[0-9]*')]),
    address: new FormControl({value: '', disabled:true}, [required]),
    email: new FormControl({value: '', disabled:true}, [Validators.email]),
    sponsorCode: new FormControl({value: '', disabled:true}),
    profession: new FormControl({value: '', disabled:true}, [required]),
    walletOpeningReasons: new FormControl({value: '', disabled: true}, [required]),
    salary: new FormControl({value: '', disabled:true}, [required])

  });

  // TODO remove, only for testing
  isLocalEnv = environment.local;

  ACCOUNT_LEVELS = NEW_SUBSCRIPTION.ACCOUNT_LEVELS;
  GENDERS: ComboOption[] = NEW_SUBSCRIPTION.GENDERS;

  customerIsEligible: boolean = false;
  customerHasBeenCheckedAndFailed = false;
  private order: SubscriptionOrder;
  private orderInitialized: boolean = false;
  private orderId: string = null;
  private firstTime: boolean = true;
  private existOrder: boolean = false;
  fechaActual: Date = new Date();
  controlValue: boolean = true;
  focusOut: boolean = false;
  disabledCalendar: boolean = true;
  disabledIfPOSIntegration: boolean = false;

  private removeSession: boolean = true;

  private alive: boolean = true;

  private ribCode: string;

  private isPOSorder='0';

  msisdnPOS;

  constructor(private clientService: ClientsService,
              private sOrder: SubscriptionOrderService,
              private sCommons: CommonsService,
              private sCombos: CombosService,
              private route: ActivatedRoute,
              private cd: ChangeDetectorRef,
              private translate: TranslateService,
              private router: Router) {

  }

  ngOnInit() {
    this.loadCombosValue();

    this.route.paramMap.pipe(
      takeWhile(() => this.alive),
      switchMap( params => {
        let orderId: string = params.get("orderId");
        if (orderId!=null && orderId !=="") {
          this.orderId = orderId;
        }else{
          //  POS integration
          let orderIDPOS: string = localStorage.getItem('ordrIdPOS');
          let msisdnPOS: string = localStorage.getItem('msisdnPOS');
          if (orderIDPOS != null && orderIDPOS != 'null') {
            this.orderId = orderIDPOS;
            this.msisdnPOS = msisdnPOS;
            localStorage.setItem('ordrIdPOS', null);
            localStorage.setItem('msisdnPOS', null);
            this.isPOSorder = '1';
          }
        }
        if(params.get("step")!=null){
          this.existOrder=true;
        }
        return this.switchInitOrder();
      })).subscribe(res => {
        console.log('ngOnInit OK ', res);
    }, err => {
      console.log('ngOnInit KO ', err);
    });
    if(this.sOrder!=undefined && this.sOrder.order!=undefined && this.sOrder.order.ordOrder!=undefined
      && this.sOrder.order.ordOrder.ordSubStatus!=undefined && this.sOrder.order.ordOrder.ordSubStatus2.status2Id==1){
      this.subscriptionForm.controls['msisdn'].disable();
      this.subscriptionForm.controls['identificationId'].disable();
      this.subscriptionForm.controls['identityType'].disable();
      this.controlValue = false;
    }

    if(this.sOrder!=undefined && this.sOrder.order!=undefined
      && this.sOrder.order.ordOrder!=undefined && this.sOrder.order.ordOrder.ordStatus!=undefined
      && this.sOrder.order.ordOrder.ordStatus.statusId !== 3
      && this.sOrder.order.ordOrder.ordStatus.statusId !== 4
      && !this.sOrder.contractGenerated){
      this.enableControls();
    }

    if(this.sOrder.orderfrozen){
      this.controlValue=false;
    }
  }

  get countries(){
    return this.sCombos.countries;
  }

  get identityTypes(){
    return this.sCombos.identityTypes;
  }

  get walletOpeningReasons(){
    return this.sCombos.walletOpeningReasons;
  }

  get professions(){
    return this.sCombos.professions;
  }

  get salarys(){
    return this.sCombos.salarys;
  }

  get nationalities(){
    return this.sCombos.nationalities;
  }

  checkAge() {
    let birthDateErrors = this.subscriptionForm.controls['birthDate'].errors;
    if (birthDateErrors !== null && birthDateErrors.under18 !== null && birthDateErrors.under18 !== undefined &&
      this.subscriptionForm.controls['birthDate'].value.toString().length>14) {
      this.sOrder.showPopupAgeError(birthDateErrors);
    }
  }

  cancel() {
    this.route.paramMap.subscribe(params => {
      let orderId:string = params.get("orderId");
      if(orderId == null || orderId==""){
        orderId = params.get("step");
      }
      if(orderId!=null && orderId!="") {
        this.sOrder.cancelOrder(this.sOrder.order.ordOrder).subscribe(res => {
          if(res.success){
            this.router.navigate(['/listTransaction']);
          }else{
            if(localStorage.getItem("lastRequest")!=null) {
              this.sOrder.cancelError();
            }
          }
        });
      }else {
        this.router.navigate(['/resercher']);
      }
    });
  }

  gotoStep2() {
    this.createRibCode();
  }

  getFormControl(controlName: string) {
    if(this.existOrder && !this.sOrder.orderfrozen){
      this.controlValue=true;
    }
    return this.subscriptionForm.get(controlName) as FormControl;
  }

  onChangeField() {
    this.checkEligibility().pipe(
      takeWhile(() => this.alive)
    ).subscribe();
  }

  updateIdType() {

    console.log('MAM: ' + this.order);

    if(this.order && this.order.client) {
      let identificationType: IdentificationType = new IdentificationType();
      const id = parseInt(this.subscriptionForm.controls['identityType'].value);
      identificationType.identificationTypeID = id;
      identificationType.name = this.sCommons.getComboValue(this.sCombos.identityTypes, id, 'viewValue');
      this.order.client.identityType = identificationType;
    }
  }

  updateContry(){
    if(this.order.client && this.order.client.address) {
      let country: EntCountry = new EntCountry();
      const id = parseInt(this.subscriptionForm.controls['entCountry'].value);
      country.id = id;
      country.name = this.sCommons.getComboValue(this.sCombos.countries, id, 'viewValue');
      this.order.client.address.entCountry = country;
    }
  }

  updateProfession() {
    if(this.order.client) {
      let profession: Profession = new Profession();
      const id = parseInt(this.subscriptionForm.controls['profession'].value);
      profession.professionID = id;
      profession.name = this.sCommons.getComboValue(this.sCombos.professions, id, 'viewValue');
      this.order.client.profession = profession;
    }
  }

  updateSalary() {
    if(this.order.client) {
      let salary: Salary = new Salary();
      const id = parseInt(this.subscriptionForm.controls['salary'].value);
      salary.salaryID = id;
      salary.name = this.sCommons.getComboValue(this.sCombos.salarys, id, 'viewValue');
      this.order.client.salary = salary;
    }
  }

  updateNationality() {
    if(this.order.client) {
      let nationality: Nationality = new Nationality();
      const id = parseInt(this.subscriptionForm.controls['nationality'].value);
      if (id === 1) {
        this.subscriptionForm.controls['identityType'].setValue('1');
        this.subscriptionForm.controls['entCountry'].setValue('1');
      }
      this.subscriptionForm.controls['identityType'].updateValueAndValidity();
      this.subscriptionForm.controls['entCountry'].updateValueAndValidity();
      this.subscriptionForm.controls['nationality'].updateValueAndValidity();
      nationality.nationalityID = id;
      nationality.name = this.sCommons.getComboValue(this.sCombos.nationalities, id, 'viewValue');
      this.order.client.nationality = nationality;
    }
  }

  updateGender() {
    if(this.order.client) {
      let gender: Gender = new Gender();
      const id = parseInt(this.subscriptionForm.controls['gender'].value);
      gender.genderId = id;
      gender.name = this.sCommons.getComboValue(this.GENDERS, id, 'viewValue');
      this.order.client.gender = gender;
    }
  }


  // TODO split save and checkEligibility
  saveOrder(showPopups: boolean, nextStep?: boolean) {
    this.synchronizeForm('form2Object');

    const order = this.sOrder.order;
    const ordOrder = order.ordOrder;
    let subscriptionOrder: SubscriptionOrder;

    if (this.sOrder.order === undefined) {
      subscriptionOrder = new SubscriptionOrder();
      this.sOrder.order = subscriptionOrder;
    } else {
      subscriptionOrder = this.sOrder.order;
    }
    if(!this.sOrder.order.client.identityType.name) {
      this.updateIdType();
    }
    subscriptionOrder.ordOrder = ordOrder;
    subscriptionOrder.client = this.sOrder.order.client;
    subscriptionOrder.wallet = this.sOrder.order.wallet;
    if(subscriptionOrder.ordOrder.posOrderID!=null){
      subscriptionOrder.wallet.defaultAccount=true;
    }
    subscriptionOrder.id = this.sOrder.order.id;
    subscriptionOrder.correlatorID = this.ribCode;
    this.sOrder.saveWallet(subscriptionOrder).subscribe(ajaxResponse => {
        if (ajaxResponse !== undefined && ajaxResponse.success) {
          let response = ajaxResponse.successResponse as SubscriptionOrder;

          this.setDatabaseIDs(response);

          if (showPopups) {
            this.router.navigate(['/listTransaction']);
          }
          if (nextStep) {
            this.removeSession=false;
            this.sCommons.closeMixer();
            this.sCommons.openMixer(this.translate.instant('new_subscription.waitting'));

            let orderIDPOS = localStorage.getItem('ordrIdPOS');
            let msisdnPOS = localStorage.getItem('msisdnPOS');
            if(orderIDPOS && orderIDPOS!='null' && orderIDPOS!=null){
              localStorage.setItem('ordrIdPOS', orderIDPOS);
              localStorage.setItem('msisdnPOS', msisdnPOS);
            }

            this.step2Go.emit('step2');
          }

        } else {
          let error = ajaxResponse !==undefined ? ajaxResponse.errorMessage : "";
          if(localStorage.getItem("lastRequest")!=null) {
            this.sCommons.closeMixer();
            this.sOrder.showPopupSaveError(error);
          }
        }
      }, error => {
        if(localStorage.getItem("lastRequest")!=null) {
          this.sCommons.closeMixer();
          this.sOrder.showPopupSaveError(error);
        }
      });

  }

  ngOnDestroy(): void {
    if(this.removeSession){
      this.sOrder.removeSessionOrder();
    }
    this.alive = false;
    if(localStorage.getItem("lastRequest")!=null) {
      this.sCommons.closePopup();
    }
  }

  private switchInitOrder(): Observable<boolean> {
    if (this.orderId) {
      return this.initSavedOrder();
    }
    return this.initSessionOrder();
  }

  private loadCombosValue() {
    const combosData = [
      { data: "identityTypes",
        service: this.sCombos.getIdentityTypes(),
        field: "identificationTypeID"},
      { data: "professions",
        service: this.sCombos.getProfessionsList(),
        field: "professionID"},
      { data: "walletOpeningReasons",
        service: this.sCombos.getOpeningReasonsList(),
        field: "openingReasonID"},
      { data: "nationalities",
        service: this.sCombos.getNationalities(),
        field: "nationalityID"},
      { data: "countries",
        service: this.sCombos.getAllCountries(),
        field: "id"},
      { data: "salarys",
        service: this.sCombos.getSalaryList(),
        field: "salaryID"},
    ];

    this.sCommons.openMixer("Chargement. S'il vous plaît, attendez...");
    forkJoin(combosData.map(n => n.service))
      .pipe(takeWhile(() => this.alive))
      .subscribe(getCombosDataRes => {
        if(localStorage.getItem("lastRequest")!=null) {
          // Fill each combo with service response data
          getCombosDataRes.forEach((serviceRes, index) => {
            this.getComboElements(
              combosData[index].data, serviceRes,
              combosData[index].field, "name"
            );
          });
          this.checkRequiredCombos();
          this.sCommons.closeMixer();
        }
      }, err => {
        this.sCommons.closeMixer();
        this.sOrder.showPopupNotConnected();
      });
  }


  private initOrder(subscriptionOrder) {
    this.sOrder.setSessionOrder(subscriptionOrder);
    this.order = new SubscriptionOrder();
    this.order.id = subscriptionOrder.id;
    this.order.wallet = subscriptionOrder.wallet;
    this.order.client = subscriptionOrder.client;
    this.order.ordOrder = subscriptionOrder.ordOrder;
    this.order.client.address = subscriptionOrder.client.address;
    //this.order.correlatorID = subscriptionOrder.correlatorID;
    this.order.wallet.walletDocuments = new Array<WalletDocument>();
    this.subscriptionForm.controls['gender'].setValue(subscriptionOrder.client.gender.genderId);
    this.sOrder.setSessionOrder(this.order);
    this.orderInitialized = true;
  }

  private initSavedOrder(): Observable<boolean> {
    let orderId: string = this.orderId;

    return this.sOrder.getSavedOrder(orderId)
      .pipe(
        takeWhile(() => this.alive),
        catchError(savedOrderErr => {
          this.sOrder.showPopupNotConnected();
          return NEVER;
        }),
        switchMap(savedOrderRes => {
          if (savedOrderRes.success) {
            let subscriptionOrder = savedOrderRes.successResponse as SubscriptionOrder;

            if (this.msisdnPOS != null && this.msisdnPOS != 'null') {
              subscriptionOrder.client.msisdn = this.msisdnPOS;
            }

            this.initOrder(subscriptionOrder);
            if(this.isPOSorder=="1"){
              let country: EntCountry = new EntCountry();
              country.id=1;
              country.name="MAROC";
              country.desc="00089";
              this.order.client.address.entCountry=country;
            }
            this.synchronizeForm("object2Form");
            return this.checkEligibility();
          } else {
            // getSavedOrder KO
            this.sOrder.showPopupOrderNotFound(orderId);
            this.resetOrder();
            this.orderInitialized = true;

            this.sCommons.closeMixer();

            return NEVER;
          }
        })
      );
  }

  private initSessionOrder(): Observable<boolean> {
    this.orderInitialized = true;


    if (!this.sOrder.getSessionOrder()) {
      this.resetOrder();
    } else {

      if (this.sOrder.getSessionOrder().ordOrder.ordrId==undefined) {
        this.sOrder.orderfrozen=false;
        this.sOrder.contractGenerated=false;
        this.sOrder.contractPreviewed=undefined;
        this.sOrder.orderValid=undefined;
      }


      this.order = this.sOrder.getSessionOrder();
    }
    this.synchronizeForm("object2Form");
    if(this.sOrder.order.ordOrder.ordrId  != undefined) {
      this.firstTime=false;
      this.customerIsEligible=true;
      if(this.sOrder.order.ordOrder.ordSubStatus==undefined && this.sOrder.order.ordOrder.ordSubStatus2.status2Id!=2) {
        this.enableControls();
      }
      return of(true);
    }
    return this.checkEligibility();

  }

  private resetOrder() {
    this.order = new SubscriptionOrder();
    this.order.wallet = new Wallet();
    this.order.client = new Client();

    this.sOrder.orderfrozen=false;
    this.sOrder.contractGenerated=false;
    this.sOrder.contractPreviewed=undefined;
    this.sOrder.orderValid=undefined;

    this.sOrder.setSessionOrder(this.order);
  }

  private checkRequiredCombos(){
    let nationalitiesLoaded = this.sCombos.nationalities !== undefined;
    let identityTypesLoaded = this.sCombos.identityTypes !== undefined;
    if (nationalitiesLoaded && identityTypesLoaded) {

      this.setNationalityAndIdTypeValidators();
      //this.setDefaultIdType();
      this.updateIdType();
      this.setDefaultNationality();
    }
  }

  private getComboElements(comboName: string, ajaxResponse, valueAttr: string, viewValueAttr: string){

    if (this.sCombos[comboName] === undefined) {
      this.sCommons.closeMixer();
      this.sCommons.openMixer("Asking server for " + comboName + " list. Please, wait...");

        if (ajaxResponse.success) {
          let list = [];
          const successResponse = ajaxResponse.successResponse;

          if(comboName !== 'identityType' && comboName !== 'walletOpeningReasons') {
            list.push({
              viewValue: "",
              value: ""
            });
          }

          for (let valueKey in successResponse as Object) {
            list.push({
              viewValue: successResponse[valueKey][viewValueAttr],
              value: successResponse[valueKey][valueAttr]
            });
          }
          this.sCombos[comboName] = list;
          this.sCommons.closeMixer();

        } else {
          this.sCommons.closeMixer();
          this.sCommons.closePopup();
          this.sOrder.showPopupComboError(ajaxResponse.errorMessage);
        }
    }
  }


  private setDefaultNationality() {
    const nationality = this.subscriptionForm.controls['nationality'];
    const idType = this.subscriptionForm.controls['identityType'];
    this.subscriptionForm.controls['entCountry'].setValue('1');
    if (nationality.value === '' && idType.value === '1') {
      nationality.setValue('1');
      this.updateNationality();
    }
  }

  private setDefaultIdType() {
    const idType = this.subscriptionForm.controls['identityType'];
    if (idType.value === '') {
      idType.setValue('1');
      this.updateIdType();
    }
  }

  private setNationalityAndIdTypeValidators() {
    let idTypeControl = this.subscriptionForm.controls['identityType'];
    let nationalityControl = this.subscriptionForm.controls['nationality'];
    let NationalityOverIdTypeValidator = NationalityOverIdType(idTypeControl, nationalityControl);
    nationalityControl.setValidators([Validators.required, NationalityOverIdTypeValidator]);
    idTypeControl.setValidators([Validators.required, NationalityOverIdTypeValidator]);
  }

  // TODO use this
  private checkRequiredFilled(msisdn, identificationId, identityType) {
    let idTypeValued = identityType !== '' && identityType !== null;
    let idValued = identificationId !== '' && identificationId !== null;
    let msisdnValued = msisdn !== '' && msisdn !== null;
    return idTypeValued && idValued && msisdnValued;
  }

  private checkEligibility(): Observable<boolean> {
    this.customerIsEligible = false;
    const msisdn = this.getFormControl('msisdn').value;
    const identificationId = this.getFormControl('identificationId').value;
    const identityType = this.getFormControl('identityType').value;
    let allAreFilledIn = this.checkRequiredFilled(msisdn, identificationId, identityType);


    if (allAreFilledIn) {
     // this.subscriptionForm.controls['nationality'].setValue("");
      return this.checkIsRetailedWallet(msisdn).pipe(
        catchError(retailedErr => {
          this.sOrder.showPopupNotConnected();
          return NEVER;
        }),
        switchMap(retailedResponse => {
          this.sCommons.closeMixer();
          if (retailedResponse && retailedResponse.code!=500) {

              if (retailedResponse.success) { //Is retailed and can't be used for new subscriptions
                this.sOrder.showRetailedWalletWarning();
                this.customerHasBeenCheckedAndFailed = false;
                this.subscriptionForm.reset();
                this.resetControls(); // TODO meter en el subscribe final con false
                return NEVER;
              } else {
                return this.checkIsEligible(msisdn, identificationId, identityType);
              }
          }
          else{
              this.sOrder.showPopupErrorRGetLang(retailedResponse.errorMessage);
          }
          return NEVER;
        }),
        catchError(isEligibleErr => {
          this.sCommons.closeMixer();
          this.sOrder.showPopupNotConnected();
          this.customerHasBeenCheckedAndFailed = true;
          this.resetControls();
          return NEVER;
        }),
        switchMap(isEligibleResponse => {
          this.handleIsEligible(isEligibleResponse);
          return of(this.customerIsEligible);
        })
      );
    }
    return of(false);
  }

  private checkIsRetailedWallet(msisdn): Observable<JSONResponse> {
    this.sCommons.openMixer(this.translate.instant('new_subscription.mixer.checkingRetailed'));
    return this.sOrder.isRetailedOrder(msisdn);
  }

  private checkIsEligible(msisdn, identificationId, idType): Observable<JSONResponse> {
    this.sCommons.openMixer("Vérification de l'éligibilité pour ce client...");
    return this.clientService.isCustomerEligible(msisdn, identificationId, idType);
  }

  private createRibCode() {
    this.sCommons.openMixer("Order de sauvegarde. S'il vous plaît, attendez...");
    this.synchronizeForm('form2Object');
    const order = this.sOrder.order;
    const ordOrder = order.ordOrder;

    let subscriptionOrder: SubscriptionOrder;
    if (this.sOrder.order === undefined) {
      subscriptionOrder = new SubscriptionOrder();
      this.sOrder.order = subscriptionOrder;
    } else {
      subscriptionOrder = this.sOrder.order;
      this.controlValue = false;
    }

    subscriptionOrder.ordOrder = ordOrder;
    subscriptionOrder.client = this.sOrder.order.client;
    subscriptionOrder.wallet = this.sOrder.order.wallet;
    subscriptionOrder.id = this.sOrder.order.id;
    subscriptionOrder.correlatorID = this.ribCode;

    this.clientService.createRibCode(subscriptionOrder)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(response => {
      if (response && response.success) {

        let subscriptionOrder = response.successResponse as SubscriptionOrder;

        this.ribCode = subscriptionOrder.correlatorID;
        console.debug("correlatorID recibido: " + this.ribCode);

        this.customerHasBeenCheckedAndFailed = false;
        this.enableControls();
        this.sCommons.closeMixer();

        this.setFormAsTouched(true);

        if ((this.customerIsEligible && (this.subscriptionForm.valid || this.controlValue==false)) || this.sOrder.orderfrozen) {
          this.saveOrder(false, true);
          // this.step2Go.emit('step2');
        } else {
          this.sOrder.showPopupFormError();
        }
      } else {
        this.sCommons.closeMixer();
        this.sOrder.showPopupRibProblem();
       /* this.subscriptionForm.reset();
        this.resetControls();*/
      }
    }, () => {
      this.sCommons.closeMixer();
      this.sOrder.showPopupRibProblem();
    /*  this.subscriptionForm.reset();
      this.resetControls();*/
    });

  }

  private handleIsEligible(response: JSONResponse) {
    this.customerIsEligible = response && response.success && !!response.successResponse;
    if (this.customerIsEligible) {
      this.customerHasBeenCheckedAndFailed = false;

      this.enableControls();
     /* if (this.sOrder.orderfrozen) {
        this.disableControls();
      } else {

        this.enableControls();
      }*/

      //  Call to mock search customer.
      const msisdn = this.getFormControl('msisdn').value;
      const id = this.getFormControl('identificationId').value;
      const idType = this.getFormControl('identityType').value;
      let allAreFilledIn = this.checkRequiredFilled(msisdn, id, idType);

      //  POS integration
      if (allAreFilledIn && this.isPOSorder === '0') {
        this.clientService.searchCustomer(msisdn, id, idType).pipe(
          switchMap(customerSearched => {
            //  Call to mock search wallet
            this.handleCustomerSearched(customerSearched);
            this.subscriptionForm.controls['entCountry'].setValue('1');
            this.updateContry();
            return NEVER;
          }),

        ).subscribe();
      }else{
        this.disabledIfPOSIntegration = true;
        this.sCommons.closeMixer();
        return NEVER;
      }

    } else if (response!=null && response.errorMessage!=undefined && response.code!=null){
      this.showPopupError(response.errorMessage);
      this.resetControls();
      this.sCommons.closeMixer();
      return NEVER;

    }else {
      this.sCommons.closeMixer();
      this.sOrder.showPopupNotEligible();
      this.customerHasBeenCheckedAndFailed = true;
      this.resetControls();

      return NEVER;
    }

  }

  private setFormAsTouched(touched: boolean) {
    for (const key in this.subscriptionForm.controls) {
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in#Iterating_over_own_properties_only
      if (this.subscriptionForm.controls.hasOwnProperty(key)) {
        if (touched) {
          this.subscriptionForm.controls[key].markAsTouched();
        } else {
          this.subscriptionForm.controls[key].markAsUntouched();
        }
      }
    }
  }

  private enableControls() {
      for (let key in this.subscriptionForm.controls) {
        if (this.subscriptionForm.controls.hasOwnProperty(key)) {
          this.subscriptionForm.controls[key].enable();
        }
    }
    if (this.firstTime) {
      this.focusName();
    }
    this.disabledCalendar= false;
  }

  private focusName() {
    this.firstTime = false;
    if(this.subscriptionForm.controls!=null) {
      this.nameRef.open();
      this.cd.detectChanges();
    }
  }

  private resetControls() {

    for (let key in this.subscriptionForm.controls){
      if (this.subscriptionForm.controls.hasOwnProperty(key)) {
        this.subscriptionForm.controls[key].disable();
      }
    }

    this.disabledCalendar=true;
    this.subscriptionForm.controls['msisdn'].enable();
    this.subscriptionForm.controls['identificationId'].enable();
    this.subscriptionForm.controls['identityType'].enable();

    this.subscriptionForm.controls['identityType'].setValue('1');
    this.subscriptionForm.controls['nationality'].setValue('1');
    this.subscriptionForm.controls['entCountry'].setValue('1');
  }

  private synchronizeForm(synchronizingDirection: string) {

    let toForm = synchronizingDirection === 'object2Form';
    this.synchronizeField(this.order.client, 'name', toForm, false);
    this.synchronizeField(this.order.client, 'surname', toForm, false);
    this.synchronizeField(this.order.client, 'identificationId', toForm, false);
    this.synchronizeField(this.order.client, 'msisdn', toForm, false);
    this.synchronizeCombo(this.order.client, 'identityType', 'identificationTypeID', toForm);
    this.synchronizeCombo(this.order.client, 'nationality', 'nationalityID', toForm);
    this.synchronizeCombo(this.order.client, 'gender', 'genderId', toForm);
    this.synchronizeCombo(this.order.client, 'profession', 'professionID', toForm);

    if(this.order.client != undefined) {
      this.synchronizeField(this.order.client.address, 'address', toForm, false);
      this.synchronizeCombo(this.order.client.address, 'entCountry', 'id', toForm);
      this.synchronizeField(this.order.client.address, 'entVille', toForm, false);
      this.synchronizeField(this.order.client.address, 'entCodePostal', toForm, false);
    }
    this.synchronizeField(this.order.client as Client, 'birthDate', toForm, true);
    this.synchronizeField(this.order.client, 'email', toForm, false);
    this.synchronizeCombo(this.order.client, 'salary', 'salaryID', toForm);

    this.synchronizeField(this.order.wallet, 'accountLevel', toForm, false);
    this.synchronizeField(this.order.wallet, 'expirationDate', toForm, true);
    this.synchronizeField(this.order.wallet, 'sponsorCode', toForm, false);
    this.synchronizeOpeningReasons(toForm);
    if (!toForm) {
      this.updateNationality();
      this.sOrder.setSessionOrder(this.order);
    }
  }

  private synchronizeOpeningReasons(toForm) {

    if (!toForm) {
        let openingReasons: OpeningReason[] = [];
        let selectedReasons = this.subscriptionForm.controls['walletOpeningReasons'].value;
        selectedReasons.forEach(walletOpeningReason => {
          this.addOpeningReason(walletOpeningReason, openingReasons);
        });
        this.order.wallet.walletOpeningReasons = openingReasons;


    } else {


      if(this.order.wallet!= null && this.order.wallet.walletOpeningReasons != null && this.order.wallet.walletOpeningReasons !== undefined) {
        let selectedReasons: String[] = [];
        this.order.wallet.walletOpeningReasons.forEach(openingReason => {
          const reasonID = openingReason.openingReasonID.toString();
          selectedReasons.push(reasonID);
        });
        this.subscriptionForm.controls['walletOpeningReasons'].setValue(selectedReasons);
      }
    }
  }

// noinspection JSMethodCanBeStatic
  private addOpeningReason(walletOpeningReason, openingReasons: OpeningReason[]) {
    let reasonID = parseInt(walletOpeningReason);
    let openingReason: OpeningReason = new OpeningReason();
    openingReason.openingReasonID = reasonID;
    openingReason.name = this.getReasonDescription(reasonID);
    openingReasons.push(openingReason);
  }

  private getReasonDescription(reasonID: number): string{
    let reason = this.sCombos.walletOpeningReasons.find(function (reason) {
      return parseInt(reason.value) === reasonID
    });
    return reason.viewValue;
  }

  private synchronizeField(obj: Object, attr: string, toForm: boolean, dateType: boolean){
    if (toForm) {
      try {
        let value = obj[attr];
        if (value !== undefined && value !== null) {

          if (dateType) {
            // let date = new Date(value);
            this.subscriptionForm.controls[attr].setValue(this.sCommons.getdatePickerFormat(value));
          } else {
            this.subscriptionForm.controls[attr].setValue(value);
          }
        }
      }
      catch (err) {
        this.subscriptionForm.controls[attr].setValue("");
      }

    } else {
      if (dateType) {
        let value = this.subscriptionForm.controls[attr].value;
        let date = this.sCommons.getDateFromDatePicker(value);
        if (date !== undefined && date !== null) {
          obj[attr] = date;
        }
      } else {
        let value = this.subscriptionForm.controls[attr].value;
        if (value !== undefined && value !== null) {
          obj[attr] = value;
        }
      }
    }
  }

  private synchronizeCombo(obj: Object, attr: string, subAttr: string, toForm: boolean){
    if (toForm) {
      try {
        let value = obj[attr][subAttr];

        if (value !== undefined && value !== null) {
          this.subscriptionForm.controls[attr].setValue(value);
        }
      }
      catch (err) {
        this.subscriptionForm.controls[attr].setValue("");
      }



    } else {
      let value = this.subscriptionForm.controls[attr].value;
      if (value !== undefined && value !== null) {
        obj[attr][subAttr] = value;
      }
    }
  }

  private disableControls(){
    for (let controlsKey in this.subscriptionForm.controls) {
      this.subscriptionForm.controls[controlsKey].disable();
    }
    this.disabledCalendar=true;
  }


  private setDatabaseIDs(response) {
    this.sOrder.order.id = response.id;
    this.sOrder.order.wallet.walletID = response.wallet.walletID;
    this.sOrder.order.client.clientID = response.client.clientID;
    this.sOrder.order.client.address.id = response.client.address.id;
    this.sOrder.order.ordOrder.ordrId = response.ordOrder.ordrId;
    this.sOrder.order.wallet.walletOpeningReasons = response.wallet.walletOpeningReasons;
    this.sOrder.order.ordOrder=response.ordOrder;
  }

  resetForm() {
    if(this.subscriptionForm.controls['identityType'].value==1){
      this.subscriptionForm.get('nationality').setValue(1);
      this.updateNationality();
    }else{
      this.subscriptionForm.get('nationality').setValue('');
    }
    this.subscriptionForm.get('gender').setValue('');
    this.subscriptionForm.get('name').setValue('');
    this.subscriptionForm.get('surname').setValue('');
    this.subscriptionForm.get('entCountry').setValue('');
    this.subscriptionForm.get('entVille').setValue('');
    this.subscriptionForm.get('entCodePostal').setValue('');
    this.subscriptionForm.get('address').setValue('');
    this.subscriptionForm.get('email').setValue('');
    this.subscriptionForm.get('sponsorCode').setValue('');
    this.subscriptionForm.get('profession').setValue('');
    this.subscriptionForm.get('salary').setValue('');
    this.subscriptionForm.get('birthDate').setValue('');

    this.subscriptionForm.get('accountLevel').setValue('');
    this.subscriptionForm.get('expirationDate').setValue('');
    this.subscriptionForm.get('sponsorCode').setValue('');
  }

  private handleCustomerSearched(customer: JSONResponse) {
    this.sOrder.contractUploaded = false; //  Nos aseguramos de obligar a generar el contrato tras consultar un cliente

    if (customer != null && customer.success) {
      if (customer.errorMessage === 'Id error') {

        this.sCommons.closeMixer();
        this.sOrder.removeSessionOrder();
        this.resetForm();
        this.order.client = new Client();
        this.order.wallet = new Wallet();
        this.showPopupErrorID();
        return NEVER;

      } else {
        let client = customer.successResponse as Client;

        this.order = new SubscriptionOrder();
        this.order.client = client;

        this.order.wallet = new Wallet();
        this.order.wallet.expirationDate=client.expirationDate;
        this.order.wallet.accountLevel=client.accountLevel;
        this.order.wallet.sponsorCode=client.sponsorCode;
        this.order.wallet.walletOpeningReasons=client.walletOpeningReasons;
        this.synchronizeForm('object2Form');
        console.log('Cliente encontrado!');

        if(client.statusAccount !== 'Actif'){
          this.sCommons.closeMixer();
          this.sOrder.showPopupWalletSuspended();
          return NEVER;
        }
      }
    }else if(customer != null && customer.errorMessage!=undefined && customer.code!=null){
      this.showPopupError(customer.errorMessage);

      this.sCommons.closeMixer();
      this.sOrder.removeSessionOrder();
      this.resetForm();
      this.order.client = new Client();
      this.order.wallet = new Wallet();

      return NEVER;
    }
    else {
      this.sCommons.closePopup();
      this.sCommons.closeMixer();
      console.log('Cliente NO encontrado');

      this.resetForm();
      this.order.client = new Client();
      this.order.wallet = new Wallet();

      return NEVER;
    }
  }
  private showPopupError(error : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = error;
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopupErrorID() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title =this.translate.instant('search.message.form.mandatoryInformationID.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translate.instant('search.message.form.mandatoryInformationID.msg');
    this.sCommons.openGeneralPopup(popupObj);
  }

  upperCase(){
    this.subscriptionForm.controls['name'].setValue((this.subscriptionForm.controls['name'].value).toUpperCase());
    this.subscriptionForm.controls['surname'].setValue((this.subscriptionForm.controls['surname'].value).toUpperCase());
  }

  isFocusOut(){
    this.focusOut=true;
  }

  isFocus(){
    this.focusOut=false;
  }

}
