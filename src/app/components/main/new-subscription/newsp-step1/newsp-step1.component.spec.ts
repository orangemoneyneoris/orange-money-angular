import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewspStep1Component } from './newsp-step1.component';

describe('NewspStep1Component', () => {
  let component: NewspStep1Component;
  let fixture: ComponentFixture<NewspStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewspStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewspStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
