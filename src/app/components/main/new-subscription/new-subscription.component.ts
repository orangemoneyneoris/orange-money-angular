import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Route, Router} from "@angular/router";
import {WalletsService} from "../../../services/wallets.service";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {SubscriptionOrder} from "../../../domain/SubscriptionOrder";
import {catchError, switchMap} from "rxjs/operators";
import {NEVER} from "rxjs";
import {JSONResponse} from "../../../domain/JSONResponse";
import {WalletDocument} from "../../../domain/WalletDocument";
import {connectableObservableDescriptor} from "rxjs/internal/observable/ConnectableObservable";

@Component({
  selector: 'app-new-subscription',
  templateUrl: './new-subscription.component.html',
  styleUrls: ['./new-subscription.component.css']
})
export class NewSubscriptionComponent implements OnInit {

  step2Go: string;
  private dataSource: WalletDocument[];
  private ordSub: SubscriptionOrder;

  constructor(private thisRoute: ActivatedRoute,
              private router: Router,
              private sOrder: SubscriptionOrderService,
              private sWallet: WalletsService) {}

  ngOnInit() {
    this.setStep2Go();
  }

  public setStep2Go() {

 /*   this.thisRoute.paramMap.subscribe(params => {

      let step = params.get('step');

      if (step === null || step === undefined) {
        this.step2Go = 'step1';

      } else {

        if (this.sOrder.getSessionOrder() === undefined) {
          this.sOrder.getSavedOrder(parseInt(step)).subscribe(res =>{
            if(res.success && res.successResponse!=null) {
              this.sOrder.setSessionOrder(res.successResponse as SubscriptionOrder)
              this.step2Go = 'step2';
            }
          })
        } else {
          this.step2Go = step;
          this.gotoStep(step);
        }
      }
    });       */

   let step= this.thisRoute.snapshot.paramMap.get('step');


    if (step === null || step === undefined) {
      this.step2Go = 'step1';

    } else {
      let session=this.sOrder.getSessionOrder();
      if (session === undefined
        || session.client==undefined
        || session.ordOrder.ordStatus==undefined) {

        this.sOrder.getSavedOrder(step).pipe(
          catchError((responseSearchErr) => {
            //error

            return NEVER;
          }),
          switchMap(res => {
            //  primera respuesta

            if(res.success && res.successResponse!=null) {
              this.sOrder.setSessionOrder(res.successResponse as SubscriptionOrder);
              this.ordSub=res.successResponse as SubscriptionOrder;
            }


            return this.handleCustomerSearched(res);
          })
        ).subscribe((defaultWalletRes) => {
          this.handleWalletDefault(defaultWalletRes);
        } , defaultWalletErr => {
          //error
          return NEVER;
        });


      }
      else {
        this.step2Go = 'step2';
        this.gotoStep(this.step2Go);
      }

    }


  }
 public handleCustomerSearched(res: JSONResponse){
    if(res!=null && res.success){
      let orderS= res.successResponse as SubscriptionOrder;
      let wallet= orderS.wallet;


      return this.sOrder.getListDocument(orderS.ordOrder);
    }
    else{
      return NEVER;
    }

  }

  gotoStep(step2Go: string) {
    this.step2Go = step2Go;
  }

  private handleWalletDefault(response: JSONResponse) {
    if (response != null
      && response.success
      && response.successResponse != null) {
      this.dataSource = response.successResponse as WalletDocument[];
      this.ordSub.wallet.walletDocuments=this.dataSource;

      var file = this.ordSub.wallet.walletDocuments.find(
        function (doc) {
          return doc.documentType==="contractDoc";
        }
      );

      if(file!=undefined){
        this.sOrder.contractGenerated= true;
      }
      else{
        this.sOrder.contractGenerated= false;
      }


    }
    this.step2Go = 'step2';
    this.sOrder.setSessionOrder(this.ordSub);



  }
}
