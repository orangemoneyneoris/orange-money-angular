import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletPreviewComponent } from './wallet-preview.component';

describe('WalletPreviewComponent', () => {
  let component: WalletPreviewComponent;
  let fixture: ComponentFixture<WalletPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
