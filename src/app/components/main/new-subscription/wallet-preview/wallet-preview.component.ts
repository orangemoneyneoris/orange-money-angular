import {Component, OnInit, OnDestroy} from '@angular/core';
import {Wallet} from "../../../../domain/Wallet";
import {Client} from "../../../../domain/Client";
import {ClientsService} from "../../../../services/clients.service";
import {WalletsService} from "../../../../services/wallets.service";
import {CommonsService} from "../../../../services/commons.service";
import {PopupObj} from "../../../../util/PopupObj";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {SubscriptionOrderService} from "../../../../services/subscription-order.service";
import {SubscriptionOrder} from "../../../../domain/SubscriptionOrder";
import {WalletDocument} from "../../../../domain/WalletDocument";
import {TranslateService} from "@ngx-translate/core";
import {ViewDocumentsService} from "../../../../services/View.Documents-Service/view-documents.service";
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-wallet-preview',
  templateUrl: './wallet-preview.component.html',
  styleUrls: ['./wallet-preview.component.css']
})
export class WalletPreviewComponent implements OnInit, OnDestroy {

  order: SubscriptionOrder = new SubscriptionOrder();
  wallet: Wallet;
  client: Client;
  checked: boolean = false;
  disabledClass = "";
  buttonClass = "btn-primary";
  statusSeccion: boolean= true;
  documents: boolean = true;

  private removeSession: boolean = true;

  previewForm = new FormGroup({
    previewed: new FormControl('')
  });

  constructor(private sClient: ClientsService,
              private sWallet: WalletsService,
              private sOrder: SubscriptionOrderService,
              private commons: CommonsService,
              private router: Router,
              private translateService: TranslateService,
              private service: ViewDocumentsService) {}

  ngOnInit() {
    if(!this.initializeWallet()){
      this.removeSession=true;
      this.router.navigate(["/listTransaction"]);
    }
    this.enableDisableButton();
    if(localStorage.getItem('isDefaultWalletEnabled') != 'Y'){
      this.order.wallet.defaultAccount=false;
    }
    console.log(this.order.wallet)
  }

  private enableDisableButton() {
    if (!this.checked) {
      this.previewForm.controls['previewed'].disable();
      this.disabledClass = "disabled";
    }
  }

  previewPushed(){
    let previewed = this.previewForm.controls['previewed'].value;
    this.buttonClass = previewed ? "btn-primary": "btn-secondary";
  }

  goBack(){

    if(this.statusSeccion){
      this.removeSession=false;
      this.sOrder.contractPreviewed = !!this.previewForm.controls['previewed'].value;
      if (!this.sOrder.contractPreviewed) {
        this.sOrder.contractGenerated = false;
      }
      let ordId=this.sOrder.order.ordOrder.ordrId;
      this.router.navigate(['subscription', ordId]);
    }
    else{

      this.removeSession=true;
      this.router.navigate(['/listTransaction']);
    }


  }

  private initializeWallet(): boolean {
    let order = this.sOrder.getSessionOrder();
    let status=2;

    if (order !== undefined) {
      if(order.ordOrder.ordStatus && order.ordOrder.ordStatus.statusId!=status){
        this.statusSeccion=false;
      }
      console.log(order.wallet)
      this.order = order;
      this.wallet = order.wallet;
      this.client = order.client;
      this.checked = true;
      return true;

    } else {
      let popupObj: PopupObj = new PopupObj();
      popupObj.title =  "Le portefeuille n'est pas chargé";
      popupObj.popupIconName = 'fas fa-exclamation-triangle';
      popupObj.popupMessage = "L'aperçu du contrat ne doit être affiché qu'avec un portefeuille déjà chargé, mais ce n'est pas le cas";
      this.commons.openGeneralPopup(popupObj);
      return false;
    }
  }

  getDocumentName(documentType: string): string {
    this.documents=this.wallet.walletDocuments.length>0;
    const walletDocuments = this.wallet.walletDocuments;
    let document;
    document = new WalletDocument();

    if(walletDocuments!=undefined){
      document = walletDocuments.find(item => {
        return item.documentType === documentType;
      });
      if (document !== undefined) {
        return document.documentName;
      }
    }
    else{
      document.documentName="";
      return document.documentName;
    }
    

  }

  private downloadFile(documentType: string){
    const walletDocuments = this.wallet.walletDocuments;
    let document;
    document = new WalletDocument();

    if(walletDocuments!=undefined){
      document = walletDocuments.find(item => {
        return item.documentType === documentType;
      });
      if (document !== undefined) {
        this.commons.openMixer("Déchargement...");
        this.service.getWalletDocument(document)
          .subscribe(response =>{

            if (response != null
              && response.success
              && response.successResponse != null) {

              const img: string = response.successResponse.toString();
              const bytes: string = atob(img);
              const byteNumbers = new Array(bytes.length);
              for (let i = 0; i < bytes.length; i++) {
                byteNumbers[i] = bytes.charCodeAt(i);
              }
              const byteArray = new Uint8Array(byteNumbers);
              const blob: Blob = new Blob([byteArray], { type: 'application/pdf' });
              FileSaver.saveAs(blob, document.documentName+".pdf");
              this.commons.closeMixer();

            }
            else {
              this.commons.closeMixer();

            }


          });
      }

    }
  }

  ngOnDestroy(): void {
    if(this.removeSession){
      this.sOrder.removeSessionOrder();
    }
  }
}
