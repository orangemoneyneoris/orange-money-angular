import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradespStep1Component } from './upgradesp-step1.component';

describe('UpgradespStep1Component', () => {
  let component: UpgradespStep1Component;
  let fixture: ComponentFixture<UpgradespStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradespStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradespStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
