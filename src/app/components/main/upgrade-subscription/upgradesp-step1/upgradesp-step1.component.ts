import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output,
  ViewChild
} from '@angular/core';
import {ComboOption} from '../../../html/util/utils';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientsService} from '../../../../services/clients.service';
import {CommonsService} from '../../../../services/commons.service';
import {
  dateIsValid,
  expirationDate,
  NationalityOverIdType,
  startWith0607,
  under18
} from "../../../../util/custom-validators";
import {OpeningReason} from "../../../../domain/OpeningReason";
import {IdentificationType} from "../../../../domain/IdentificationType";
import {Profession} from "../../../../domain/Profession";
import {Nationality} from "../../../../domain/Nationality";
import {Gender} from "../../../../domain/Gender";
import {SubscriptionOrderService} from "../../../../services/subscription-order.service";
import {Client} from "../../../../domain/Client";
import {CombosService} from "../../../../services/combos.service";
import {ActivatedRoute, Router} from "@angular/router";
import {forkJoin, NEVER, Observable, of} from "rxjs";
import {JSONResponse} from "../../../../domain/JSONResponse";
import {catchError, switchMap, takeWhile, tap} from "rxjs/internal/operators";
import {TranslateService} from "@ngx-translate/core";
import {NEW_SUBSCRIPTION} from "../../../../util/Constants";
import {UpgradeOrder} from "../../../../domain/UpgradeOrder";
import {UpgradeWalletService} from "../../../../services/UpgradeWallet/upgrade-wallet.service";
import {Salary} from "../../../../domain/Salary";
import {PopupObj} from "../../../../util/PopupObj";
import {Wallet} from "../../../../domain/Wallet";
import {WalletDocument} from "../../../../domain/WalletDocument";
import {EntCountry} from "../../../../domain/EntCountry";
import {NgbInputDatepicker, NgbModal, NgbModalOptions, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {ValidationErrorsComponent} from "../../../html/validation-errors/validation-errors.component";

let required = Validators.required;

@Component({
  selector: 'app-upgradesp-step1',
  templateUrl: './upgradesp-step1.component.html',
  styleUrls: ['./upgradesp-step1.component.css']
})
export class UpgradespStep1Component implements OnInit, OnDestroy {

  @ViewChild('expirationDate') nameRef: NgbInputDatepicker;

  @Input()
  updLevel: string;

  @Input()
  upgradeOrder: UpgradeOrder;

  @Output()
  step2Go = new EventEmitter<string>();

  @ViewChild('validateDate') validateDate: ElementRef;

  LEVELN1 = NEW_SUBSCRIPTION.LEVELN1;
  LEVELN2 = NEW_SUBSCRIPTION.LEVELN2;
  LEVELN3 = NEW_SUBSCRIPTION.LEVELN3;
  LEVELN1P = NEW_SUBSCRIPTION.LEVELN1P;
  LEVELN2P = NEW_SUBSCRIPTION.LEVELN2P;
  LEVELN3P = NEW_SUBSCRIPTION.LEVELN3P;

  private validateModalRef: NgbModalRef;
  popupObj: PopupObj = new PopupObj();
  clientLevel:string;
  expiredDate:boolean= false;

  subscriptionForm = new FormGroup({
    msisdn: new FormControl({value: '', disabled: this.isFieldMsidnDisabled()}, [required, Validators.pattern('[0-9]*'), Validators.minLength(10), startWith0607()]),
    nationality: new FormControl({value: '', disabled: true}, [required]), //Validators are set aside
    identityType: new FormControl({value: '', disabled: this.isFieldDisabled()}), //Validators are set aside
    identificationId: new FormControl({value: '', disabled: this.isFieldDisabled()}, [required, Validators.pattern('^[A-Za-z0-9]*')]),
    expirationDate: new FormControl({value: '', disabled: true}, [required, expirationDate(), dateIsValid()]),
    gender: new FormControl({value: '', disabled: true}, [required]),
    name: new FormControl({value: '', disabled: true}, [required]),
    surname: new FormControl({value: '', disabled: true}, [required]),
    birthDate: new FormControl({value: '', disabled: true}, [required, under18(), dateIsValid()]),
    accountLevel: new FormControl({value: '', disabled: true}, [required]),
    entCountry: new FormControl({value:'', disabled: true}, [required]),
    entVille: new FormControl({value:'', disabled: true}, [required]),
    entCodePostal: new FormControl({value:'', disabled: true}, [required]),
    address: new FormControl({value:'', disabled: true}, [required]),
    email: new FormControl({value: '', disabled: true}, [Validators.email]),
    sponsorCode: new FormControl({value: '', disabled: true}),
    profession: new FormControl({value: '', disabled: true}, [required]),
    walletOpeningReasons: new FormControl({value: '', disabled: true}, [required]),
    salary: new FormControl({value: '', disabled:true}, [required]),
    numRib: new FormControl({value: '', disabled:true})
  });


  ACCOUNT_LEVELS = NEW_SUBSCRIPTION.ACCOUNT_LEVELS;
  UPGRADE_WALLET_COLLABORATOR = NEW_SUBSCRIPTION.UPGRADE_WALLET_COLLABORATOR;
  GENDERS: ComboOption[] = NEW_SUBSCRIPTION.GENDERS;

  customerIsEligible: boolean = false;
  customerHasBeenCheckedAndFailed = false;
  private orderInitialized: boolean = false;
  private orderId: number = null;
  private firstTime: boolean = true;
  fechaActual: Date = new Date();
  private removeSession: boolean = true;
  focusOut: boolean = false;
  disabledCalendar: boolean = true;
  errorTechnicalSearch: boolean = false;

  private alive: boolean = true;
  private ribNum;

  constructor(private upgradeService: UpgradeWalletService,
              private activatedRoute: ActivatedRoute,
              private clientService: ClientsService,
              private sOrder: SubscriptionOrderService,
              private sCommons: CommonsService,
              private sCombos: CombosService,
              private route: ActivatedRoute,
              private cd: ChangeDetectorRef,
              private translate: TranslateService,
              private router: Router,
              private modalService: NgbModal) {}

  ngOnInit() {
    let ordrId = this.activatedRoute.snapshot.paramMap.get('ordrId');
    if (ordrId != null && this.sOrder.getSessionOrderUpgrade() == undefined) {

      this.upgradeService.getOrderById(ordrId).subscribe(res => {
        if (res && res.success) {
          this.upgradeOrder = res.successResponse as UpgradeOrder;
          this.updLevel = this.upgradeOrder.wallet.accountLevel.toLowerCase();
          this.upgradeOrder.client.accountLevel = this.upgradeOrder.lastLevelWallet;
          this.upgradeOrder.client.numRib = this.upgradeOrder.ribCode;
          this.loadForm();
          this.checkDisability(true);
        } else {
          this.sOrder.showPopupOrderNotFound(ordrId);
        }
      });
    }else {

      if(this.upgradeOrder==undefined && this.sOrder.getSessionOrderUpgrade()!=undefined){
        this.upgradeOrder=this.sOrder.getSessionOrderUpgrade();
        if(this.upgradeOrder.lastLevelWallet!=undefined){
          this.upgradeOrder.client.accountLevel=this.upgradeOrder.lastLevelWallet;
        }
        if(this.upgradeOrder.ribCode!=undefined){
          this.upgradeOrder.client.numRib=this.upgradeOrder.ribCode;
        }
        
      }
      this.loadForm();

    }
  }

  get countries(){
    return this.sCombos.countries;
  }

  get identityTypes(){
    return this.sCombos.identityTypes;
  }

  get walletOpeningReasons(){
    return this.sCombos.walletOpeningReasons;
  }

  get professions(){
    return this.sCombos.professions;
  }

  get salarys(){
    return this.sCombos.salarys;
  }

  get nationalities(){
    return this.sCombos.nationalities;
  }

  get numRib(){
    if(this.upgradeOrder!= undefined && this.upgradeOrder.client!= undefined) {
      return this.upgradeOrder.client.numRib;
    }
  }

  cancel() {
    if(this.activatedRoute.snapshot.paramMap.get('ordrId')!=null) {
      this.sOrder.cancelOrder(this.upgradeOrder.ordOrder).subscribe(res => {
        if(res.success){
          this.router.navigate(['/listTransaction']);
        }else{
          if(localStorage.getItem("lastRequest")!=null) {
            this.sOrder.cancelError();
          }
        }
      });
    }else {
      this.router.navigate(['/resercher']);
    }
  }

  checkAge() {
    let birthDateErrors = this.subscriptionForm.controls['birthDate'].errors;
    if (birthDateErrors !== null && birthDateErrors.under18 !== null && birthDateErrors.under18 !== undefined &&
      this.subscriptionForm.controls['birthDate'].value.toString().length>14) {
      this.sOrder.showPopupAgeError(birthDateErrors);
    }
  }


  gotoStep2() {
    this.sCommons.openMixer("Order de sauvegarde. S'il vous plaît, attendez...");
    this.setFormAsTouched(true);
    if (this.subscriptionForm.valid || this.sOrder.orderfrozen) {
      this.saveOrder(false, true);
      // this.step2Go.emit('step2');
    } else {
      this.sCommons.closeMixer();
      this.sOrder.showPopupFormError();
    }
  }

  checkFieldsRequired(){
    if(this.getFormControl('profession').value!="" && this.getFormControl('expirationDate').value!="" &&
      this.getFormControl('gender').value !="" && this.getFormControl('name').value !="" &&
      this.getFormControl('surname').value !="" && this.getFormControl('birthDate').value !="" &&
      this.getFormControl('accountLevel').value !="" && this.getFormControl('entCountry').value !="" &&
      this.getFormControl('entVille').value !="" && this.getFormControl('entCodePostal').value !="" &&
      this.getFormControl('address').value !="" && this.getFormControl('walletOpeningReasons').value !="" &&
      this.getFormControl('salary').value !="" && this.getFormControl('nationality').value !="") {

      return true;
    }else{
      return false;
    }
  }

  getFormControl(controlName: string) {
    return this.subscriptionForm.get(controlName) as FormControl;
  }

  onChangeField() {
    this.checkEligibility().pipe(
      takeWhile(() => this.alive)
    ).subscribe(isElegible => {
      const msisdn = this.getFormControl('msisdn').value;
      const id = this.getFormControl('identificationId').value;
      const idType = this.getFormControl('identityType').value;
      let allAreFilledIn = this.checkRequiredFilled(msisdn, id, idType)
      if (allAreFilledIn && isElegible) {
        this.clientService.searchCustomer(msisdn, id, idType).pipe(

          catchError((responseSearchErr) => {
            this.sCommons.closeMixer();
            this.sOrder.showPopupNotConnected();

            this.resetFields();

            return NEVER;
          }),
          switchMap(customerSearched => {
            //  Call to mock search wallet
            return this.handleCustomerSearched(customerSearched);
          }),
          catchError((eligibilityResponse) => {
            this.sCommons.closeMixer();

            return NEVER;
          })
        ).subscribe((ribResponse) => {
          this.updateCountry();
          this.handleRibResponse(ribResponse);

          if(this.upgradeOrder.client.expirationDate != null) {
            let date = new Date();
            let expDate = new Date(this.upgradeOrder.client.expirationDate);

            if (expDate < date) {
              this.expiredDate = true;
              this.openValidateDateModal();
            }
          }
        }, ribResponseErr => {
          this.sCommons.closeMixer();
          this.sOrder.showPopupNotConnected();
        });
      }

    });

    this.checkDisability(false);
  }

  private handleRibResponse(response: JSONResponse) {
    if (response != null && response.success) {
      let rib = response.successResponse;

      this.ribNum = rib.toString();
      this.upgradeOrder.client.numRib = rib.toString();

      this.sOrder.setSessionOrderUpgrade(this.upgradeOrder);
      this.sCommons.closePopup();
      this.sCommons.closeMixer();

    }else{
      this.ribNum = '';
      this.upgradeOrder.client.numRib = '';

      this.sOrder.setSessionOrderUpgrade(this.upgradeOrder);
      this.sCommons.closePopup();
      this.sCommons.closeMixer();
    }

    this.checkDisability(false);
  }

  updateIdType() {
    let identificationType: IdentificationType = new IdentificationType();
    const id = parseInt(this.subscriptionForm.controls['identityType'].value);
    identificationType.identificationTypeID = id;
    identificationType.name = this.sCommons.getComboValue(this.sCombos.identityTypes, id, 'viewValue');
    if(this.upgradeOrder!=undefined && this.upgradeOrder.client!=undefined) {
      this.upgradeOrder.client.identityType = identificationType;
    }
  }

  updateCountry(){
    if(this.upgradeOrder.client && this.upgradeOrder.client.address) {
      let country: EntCountry = new EntCountry();
      const id = parseInt(this.subscriptionForm.controls['entCountry'].value);
      country.id = id;
      country.name = this.sCommons.getComboValue(this.sCombos.countries, id, 'viewValue');
      this.upgradeOrder.client.address.entCountry = country;
    }
  }

  updateProfession() {
    let profession: Profession = new Profession();
    const id = parseInt(this.subscriptionForm.controls['profession'].value);
    profession.professionID = id;
    profession.name = this.sCommons.getComboValue(this.sCombos.professions, id, 'viewValue');
    if(this.upgradeOrder!=undefined) {
      this.upgradeOrder.client.profession = profession;
    }
  }

  updateSalary() {
    let salary: Salary = new Salary();
    const id = parseInt(this.subscriptionForm.controls['salary'].value);
    salary.salaryID = id;
    salary.name = this.sCommons.getComboValue(this.sCombos.salarys, id, 'viewValue');
    this.upgradeOrder.client.salary = salary;
  }

  updateNationality() {
    let nationality: Nationality = new Nationality();
    const id = parseInt(this.subscriptionForm.controls['nationality'].value);
    if(id === 1) {
      this.subscriptionForm.controls['identityType'].setValue('1');
      this.subscriptionForm.controls['entCountry'].setValue('1');
    }
    this.subscriptionForm.controls['identityType'].updateValueAndValidity();
    this.subscriptionForm.controls['entCountry'].updateValueAndValidity();
    this.subscriptionForm.controls['nationality'].updateValueAndValidity();
    nationality.nationalityID = id;
    nationality.name = this.sCommons.getComboValue(this.sCombos.nationalities, id, 'viewValue');
    if(this.upgradeOrder!=undefined && this.upgradeOrder.client!=undefined) {
      this.upgradeOrder.client.nationality = nationality;
    }
  }

  updateGender() {
    let gender: Gender = new Gender();
    const id = parseInt(this.subscriptionForm.controls['gender'].value);
    gender.genderId = id;
    gender.name = this.sCommons.getComboValue(this.GENDERS, id, 'viewValue');
    this.upgradeOrder.client.gender = gender;
  }


  // TODO split save and checkEligibility
  saveOrder(showPopups: boolean, nextStep?: boolean) {
    if( this.checkFieldsRequired()) {
      this.synchronizeForm('form2Object');

      if(this.ribNum) {
        this.upgradeOrder.client.numRib = this.ribNum;
      }
      this.upgradeService.initUpgradeOrder(this.upgradeOrder).subscribe(initUpgradeResponse => {

        if (initUpgradeResponse != undefined && initUpgradeResponse.success) {
          let response = initUpgradeResponse.successResponse as UpgradeOrder;
          this.setDatabaseIDs(response);
          this.sCommons.closeMixer();
          if (showPopups) {
            this.router.navigate(['/listTransaction']);
          }
          if (nextStep) {
            if(this.upgradeOrder.client.identityType.desc==undefined){
              this.updateIdType();
            }
            if(this.upgradeOrder.client.nationality.desc==undefined){
              this.updateNationality()
            }
            this.sOrder.setSessionOrderUpgrade(this.upgradeOrder);
            this.removeSession = false;
            this.step2Go.emit('step2');
          }

        } else {
          let error = initUpgradeResponse != undefined ? initUpgradeResponse.errorMessage : "";
          if(localStorage.getItem("lastRequest")!=null) {
            this.sCommons.closeMixer();
            this.sOrder.showPopupSaveError(error);
          }
        }
      }, error => {
        if(localStorage.getItem("lastRequest")!=null) {
          this.sCommons.closeMixer();
          this.sOrder.showPopupSaveError(error);
        }
      });
    }else{
      this.sCommons.closeMixer();
      this.sOrder.showPopupFormError()
    }
  }

  private loadForm(){
    if(this.upgradeOrder != undefined && this.upgradeOrder.client!=undefined) {
      this.loadCombosValue()
        .pipe(
          takeWhile(() => this.alive),
          catchError(err => {
            this.sOrder.showPopupNotConnected();
            return NEVER;
          }),
          switchMap(loadCombosRes => {
            const client = this.upgradeOrder.client;
            return this.checkIsEligible(client.msisdn, client.identificationId, client.identityType.name)
          }))
        .subscribe(isEligibleRes => {
          if(isEligibleRes && isEligibleRes.success) {
            this.synchronizeForm('object2Form');
            this.customerIsEligible=true;
            this.customerHasBeenCheckedAndFailed=false;
            this.checkDisability(false);
          } else {
            this.sCommons.closeMixer();
            this.sOrder.showPopupNotEligible();
            this.customerIsEligible=false;
            this.customerHasBeenCheckedAndFailed=true;
            let msisdn= this.upgradeOrder.client.msisdn;
            let identificationId = this.upgradeOrder.client.identificationId;
            this.resetFields();
            this.subscriptionForm.controls["msisdn"].setValue(msisdn);
            this.subscriptionForm.controls["identificationId"].setValue(identificationId);
          }
        }, isEligibleErr => {
          this.sOrder.showPopupNotConnected();
        });
    }else{
      this.loadCombosValue2()
    }
  }

  ngOnDestroy(): void {
    if(this.removeSession){
      this.sOrder.removeSessionOrderUpgrade();
      this.sOrder.removeSessionOrder();
    }
    this.alive = false;
    if(localStorage.getItem("lastRequest")!=null) {
      this.sCommons.closePopup();
    }
  }

  /*private switchInitOrder(): Observable<boolean> {
    if (this.orderId) {
      return this.initSavedOrder();
    }
    return this.initSessionOrder();
  }*/

  private loadCombosValue(): Observable<any> {
    const combosData = [
      { data: "identityTypes",
        service: this.sCombos.getIdentityTypes(),
        field: "identificationTypeID"},
      { data: "professions",
        service: this.sCombos.getProfessionsList(),
        field: "professionID"},
      { data: "walletOpeningReasons",
        service: this.sCombos.getOpeningReasonsList(),
        field: "openingReasonID"},
      { data: "nationalities",
        service: this.sCombos.getNationalities(),
        field: "nationalityID"},
      { data: "countries",
        service: this.sCombos.getAllCountries(),
        field: "id"},
      { data: "salarys",
        service: this.sCombos.getSalaryList(),
        field: "salaryID"},
    ];

    //this.sCommons.openMixer("Loading. Please, wait...");
    return forkJoin(combosData.map(n => n.service))
      .pipe(
        tap(getCombosDataRes => {
          // Fill each combo with service response data
          getCombosDataRes.forEach( (serviceRes, index) => {
            this.getComboElements(
              combosData[index].data, serviceRes,
              combosData[index].field, "name"
            );
          });
          this.checkRequiredCombos();
          this.sCommons.closeMixer();
        })
      );
  }

  private checkRequiredCombos(){
    let nationalitiesLoaded = this.sCombos.nationalities !== undefined;
    let identityTypesLoaded = this.sCombos.identityTypes !== undefined;
    if (nationalitiesLoaded && identityTypesLoaded) {

      this.setNationalityAndIdTypeValidators();
      this.setDefaultIdType();
      this.setDefaultNationality();
    }
  }

  private getComboElements(comboName: string, ajaxResponse, valueAttr: string, viewValueAttr: string){

    if (this.sCombos[comboName] === undefined) {
      this.sCommons.closeMixer();
      this.sCommons.openMixer("Asking server for " + comboName + " list. Please, wait...");

      if (ajaxResponse.success) {
        let list = [];
        list.push({viewValue: "", value: ""});
        const successResponse = ajaxResponse.successResponse;
        for (let valueKey in successResponse as Object) {
          list.push({
            viewValue: successResponse[valueKey][viewValueAttr],
            value: successResponse[valueKey][valueAttr]
          });
        }
        this.sCombos[comboName] = list;
        this.sCommons.closeMixer();

      } else {
          this.sCommons.closeMixer();
          this.sCommons.closePopup();
          this.sOrder.showPopupComboError(ajaxResponse.errorMessage);
      }
    }
  }


  private setDefaultNationality() {
    const nationality = this.subscriptionForm.controls['nationality'];
    const idType = this.subscriptionForm.controls['identityType'];
    if (nationality.value === '' && idType.value === '1') {
      nationality.setValue('1');
      this.updateNationality();
    }
  }

  private setDefaultIdType() {
    const idType = this.subscriptionForm.controls['identityType'];
    if (idType.value === '') {
      idType.setValue('1');
      this.updateIdType();
    }
  }

  private setNationalityAndIdTypeValidators() {
    let idTypeControl = this.subscriptionForm.controls['identityType'];
    let nationalityControl = this.subscriptionForm.controls['nationality'];
    let NationalityOverIdTypeValidator = NationalityOverIdType(idTypeControl, nationalityControl);
    nationalityControl.setValidators([Validators.required, NationalityOverIdTypeValidator]);
    idTypeControl.setValidators([Validators.required, NationalityOverIdTypeValidator]);
  }

  // TODO use this
  private checkRequiredFilled(msisdn, identificationId, identityType) {
    let idTypeValued = identityType !== '' && identityType !== null;
    let idValued = identificationId !== '' && identificationId !== null;
    let msisdnValued = msisdn !== '' && msisdn !== null;
    return idTypeValued && idValued && msisdnValued;
  }

  private checkEligibility(): Observable<boolean> {
    this.customerIsEligible = false;
    const msisdn = this.getFormControl('msisdn').value;
    const identificationId = this.getFormControl('identificationId').value;
    const identityType = this.getFormControl('identityType').value;
    let allAreFilledIn = this.checkRequiredFilled(msisdn, identificationId, identityType);

    if (allAreFilledIn) {

      return this.checkIsEligible(msisdn, identificationId, identityType).pipe(
        switchMap(isEligibleResponse => {
          this.handleIsEligible(isEligibleResponse);
          this.sCommons.closeMixer();
          return of(this.customerIsEligible);
        }),
        catchError(isEligibleErr => {
          this.sCommons.closeMixer();
          this.sOrder.showPopupNotConnected();
          this.customerHasBeenCheckedAndFailed = true;
          this.resetFields();
          return NEVER;
        })
      );
    }
    return of(false);
  }

  private checkIsEligible(msisdn, identificationId, idType): Observable<JSONResponse> {
    this.sCommons.openMixer("Vérification de l'éligibilité pour ce client KYC...");

    return this.clientService.isCustomerEligibleForMakeDefault(msisdn, identificationId, idType);
  }

  private handleCustomerSearched(customer: JSONResponse) {
    if(!this.isFieldDisabled()){
      for (let key in this.subscriptionForm.controls){
        if (this.subscriptionForm.controls.hasOwnProperty(key) &&  key!="msisdn") {
          this.subscriptionForm.controls[key].enable();
        }
      }
      this.disabledCalendar=false;
    }else{
      this.subscriptionForm.controls["entCountry"].enable();
      this.subscriptionForm.controls["entVille"].enable();
      this.subscriptionForm.controls["entCodePostal"].enable();
      this.subscriptionForm.controls["address"].enable();
    }
    if (customer != null && customer.success) {
      let client = customer.successResponse as Client;
      if(client!= null && (client.accountLevel==this.LEVELN3 || client.accountLevel==this.LEVELN3P)){
        this.sOrder.showPopupClientNotUpgrade();
        this.resetFields();
      }else if (customer.errorMessage === 'Id error') {
        this.sCommons.closePopup();
        this.sCommons.closeMixer();
        this.resetFields();
        this.showPopupErrorID();

      } else {
        this.upgradeOrder = new UpgradeOrder();
        this.upgradeOrder.client = client;

        this.upgradeOrder.wallet = new Wallet();
        this.upgradeOrder.wallet.expirationDate=client.expirationDate;
        this.clientLevel = client.accountLevel;
        if(client.accountLevel==this.LEVELN2){
          this.upgradeOrder.wallet.accountLevel=this.LEVELN3;
        }else if (client.accountLevel==this.LEVELN1){
          this.upgradeOrder.wallet.accountLevel=this.LEVELN2;
        }else if (client.accountLevel==this.LEVELN2P){
          this.upgradeOrder.wallet.accountLevel=this.LEVELN3P;
        }else{
          this.upgradeOrder.wallet.accountLevel=this.LEVELN2P;
        }
        this.upgradeOrder.wallet.sponsorCode=client.sponsorCode;
        this.upgradeOrder.wallet.walletOpeningReasons=client.walletOpeningReasons;
        this.upgradeOrder.wallet.walletDocuments = new Array<WalletDocument>();
        this.sOrder.setSessionOrderUpgrade(this.upgradeOrder);
        this.synchronizeForm('object2Form');
        this.checkDisability(false);
        console.log('Cliente encontrado!');

        if(client.statusAccount !== 'Actif'){
          this.sCommons.closeMixer();
          this.sOrder.showPopupWalletSuspended();
          return NEVER;
        }else{
          return this.clientService.getRibCode(
            this.upgradeOrder.client.msisdn,
            this.upgradeOrder.client.identificationId,
            this.upgradeOrder.client.identityType.identificationTypeID);
        }
      }
    } else if(customer != null && customer.errorMessage!=undefined && customer.code!=undefined){
      this.errorTechnicalSearch=true;
      this.showPopupError(customer.errorMessage);

      this.sCommons.closeMixer();
      this.sOrder.removeSessionOrder();

      this.resetFields();

      return NEVER;
    }else if(customer==null){
      this.sCommons.closePopup();
      this.sCommons.closeMixer();

      this.showPopupConnectionError();
      this.resetFields();

      return NEVER;
    }else {
      this.sCommons.closePopup();
      this.sCommons.closeMixer();

      this.sOrder.showPopupClientNotExist();
      console.log('Cliente NO encontrado');
      this.resetFields();

      return NEVER;
    }
  }

  private resetFields(){
    this.subscriptionForm.reset();
    this.upgradeOrder=undefined;
    for (let field in this.subscriptionForm.controls) {
      if (field != "msisdn" && field != "identityType" && field != "identificationId") {
        this.subscriptionForm.controls[field].disable();
      }
      this.disabledCalendar=true;
      this.subscriptionForm.controls['identityType'].setValue('1');
      this.subscriptionForm.controls['nationality'].setValue('1');
      this.subscriptionForm.controls['entCountry'].setValue('1');
    }
  }

  private handleIsEligible(response: JSONResponse) {
    this.sCommons.closeMixer();
    this.customerIsEligible = response && response.success && !!response.successResponse;
    if (this.customerIsEligible) {
      this.customerHasBeenCheckedAndFailed = false;

      /*
      if (this.sOrder.orderfrozen) {
        this.disableControls();
      } else {
        this.enableControls();
      }
      */
    } else if (response!=null && response.errorMessage!=undefined && response.code!=null){
      this.showPopupError(response.errorMessage);
      this.resetFields();

    }else {
      if(this.errorTechnicalSearch) {
        this.sOrder.showPopupNotEligible();
        this.errorTechnicalSearch=false;
      }

      this.customerHasBeenCheckedAndFailed = true;
      let msisdn= this.subscriptionForm.controls["msisdn"].value;
      let identificationId = this.subscriptionForm.controls["identificationId"].value;
      this.resetFields();
      this.subscriptionForm.controls["msisdn"].setValue(msisdn);
      this.subscriptionForm.controls["identificationId"].setValue(identificationId);
    }

  }

  private setFormAsTouched(touched: boolean) {
    for (const key in this.subscriptionForm.controls) {
      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in#Iterating_over_own_properties_only
      if (this.subscriptionForm.controls.hasOwnProperty(key)) {
        if (touched) {
          this.subscriptionForm.controls[key].markAsTouched();
        } else {
          this.subscriptionForm.controls[key].markAsUntouched();
        }
      }
    }
  }

  private enableControls() {
    for (let key in this.subscriptionForm.controls){
      if (this.subscriptionForm.controls.hasOwnProperty(key)) {
        this.subscriptionForm.controls[key].enable();
      }
    }
    this.disabledCalendar=false;
    if (this.firstTime) {
      this.focusName();
    }
  }

  private focusName() {
    this.firstTime = false;
    if(this.subscriptionForm.controls.value!=null) {
      this.nameRef.open();
      this.cd.detectChanges();
    }
  }

  private synchronizeForm(synchronizingDirection: string) {
    if(this.upgradeOrder.client.address.entCountry.id==0){
      this.upgradeOrder.client.address.entCountry.id=1;
      this.upgradeOrder.client.address.entCountry.name="Maroc";

    }
    let toForm = synchronizingDirection === 'object2Form';
    this.synchronizeField(this.upgradeOrder.client, 'name', toForm, false);
    this.synchronizeField(this.upgradeOrder.client, 'surname', toForm, false);
    this.synchronizeField(this.upgradeOrder.client, 'identificationId', toForm, false);
    this.synchronizeField(this.upgradeOrder.client, 'msisdn', toForm, false);
    this.synchronizeCombo(this.upgradeOrder.client, 'identityType', 'identificationTypeID', toForm);

    this.synchronizeCombo(this.upgradeOrder.client, 'nationality', 'nationalityID', toForm);
    this.synchronizeCombo(this.upgradeOrder.client, 'gender', 'genderId', toForm);
    this.synchronizeCombo(this.upgradeOrder.client, 'profession', 'professionID', toForm);
    this.synchronizeField(this.upgradeOrder.client.address, 'address', toForm, false);
    this.synchronizeCombo(this.upgradeOrder.client.address, 'entCountry', 'id', toForm);
    this.synchronizeField(this.upgradeOrder.client.address, 'entVille', toForm, false);
    this.synchronizeField(this.upgradeOrder.client.address, 'entCodePostal', toForm, false);
    this.synchronizeField(this.upgradeOrder.client, 'birthDate', toForm, true);
    this.synchronizeField(this.upgradeOrder.client, 'email', toForm, false);
    this.synchronizeCombo(this.upgradeOrder.client, 'salary', 'salaryID', toForm);
    this.synchronizeField(this.upgradeOrder.wallet, 'accountLevel', toForm, false);
    this.synchronizeField(this.upgradeOrder.wallet, 'expirationDate', toForm, true);
    this.synchronizeField(this.upgradeOrder.wallet, 'sponsorCode', toForm, false);
    this.synchronizeField(this.upgradeOrder.client, 'numRib', toForm, false);
    this.upperCase();
    this.synchronizeOpeningReasons(toForm);
    if (!toForm) {
      this.sOrder.setSessionOrderUpgrade(this.upgradeOrder);
    }
    if (this.updLevel != null){
      this.subscriptionForm.controls['accountLevel'].setValue(this.updLevel.toUpperCase());
    }
    this.sCommons.closeMixer();
  }

  private synchronizeOpeningReasons(toForm) {
    if (!toForm) {
      let openingReasons: OpeningReason[] = [];
      let selectedReasons = this.subscriptionForm.controls['walletOpeningReasons'].value;
      selectedReasons.forEach(walletOpeningReason => {
        this.addOpeningReason(walletOpeningReason, openingReasons);
      });

      this.upgradeOrder.wallet.walletOpeningReasons = openingReasons;

    } else {
      let selectedReasons: String[] = [];
      if(this.upgradeOrder.wallet.walletOpeningReasons != null && this.upgradeOrder.wallet.walletOpeningReasons !== undefined) {
        this.upgradeOrder.wallet.walletOpeningReasons.forEach(openingReason => {
          const reasonID = openingReason.openingReasonID.toString();
          selectedReasons.push(reasonID);
        });
        this.subscriptionForm.controls['walletOpeningReasons'].setValue(selectedReasons);
      }
      else{
        this.subscriptionForm.controls['walletOpeningReasons'].setValue("");
      }


    }

  }

// noinspection JSMethodCanBeStatic
  private addOpeningReason(walletOpeningReason, openingReasons: OpeningReason[]) {
    let reasonID = parseInt(walletOpeningReason);
    let openingReason: OpeningReason = new OpeningReason();
    openingReason.openingReasonID = reasonID;
    openingReason.name = this.getReasonDescription(reasonID);
    openingReasons.push(openingReason);
  }

  private getReasonDescription(reasonID: number): string{
    let reason = this.sCombos.walletOpeningReasons.find(function (reason) {
      return parseInt(reason.value) === reasonID
    });
    return reason.viewValue;
  }

  private synchronizeField(obj: object, attr: string, toForm: boolean, dateType: boolean){
    if (toForm) {

      try {
        let value = obj[attr];
        if (value !== undefined && value !== null) {

          if (dateType) {
            // let date = new Date(value);
            this.subscriptionForm.controls[attr].setValue(this.sCommons.getdatePickerFormat(value));
          } else {
            this.subscriptionForm.controls[attr].setValue(value);
          }
        }
      }
      catch(err) {
        this.subscriptionForm.controls[attr].setValue("");
      }

    } else {
      if (dateType) {
        let value = this.subscriptionForm.controls[attr].value;
        let date = this.sCommons.getDateFromDatePicker(value);
        if (date !== undefined && date !== null) {
          obj[attr] = date;
        }
      } else {
        let value = this.subscriptionForm.controls[attr].value;
        if (value !== undefined && value !== null) {
          obj[attr] = value;
        }
      }
    }
  }

  private synchronizeCombo(obj: object, attr: string, subAttr: string, toForm: boolean){
    if (toForm) {

      try {
        let value = obj[attr][subAttr];

        if (value !== undefined && value !== null) {
          this.subscriptionForm.controls[attr].setValue(""+value);
        }
      }
      catch(err) {
        this.subscriptionForm.controls[attr].setValue("");
      }


    } else {
      try {
      let value = this.subscriptionForm.controls[attr].value;
        if (value !== undefined && value !== null) {
          obj[attr][subAttr] = value;
        }
      }
      catch(err) {
        this.subscriptionForm.controls[attr].setValue("");
      }

    }
  }


  private setDatabaseIDs(response) {
    this.sOrder.orderUpgrade.id = response.id;
    this.sOrder.orderUpgrade.wallet.walletID = response.wallet.walletID;
    this.sOrder.orderUpgrade.client.clientID = response.client.clientID;
    this.sOrder.orderUpgrade.client.address.id = response.client.address.id;
    this.sOrder.orderUpgrade.ordOrder.ordrId = response.ordOrder.ordrId;
    this.sOrder.orderUpgrade.wallet.walletOpeningReasons = response.wallet.walletOpeningReasons;
  }

  private checkDisability(checkExpDate: boolean){
    if(this.subscriptionForm.controls["expirationDate"].value!=null && checkExpDate){
      this.subscriptionForm.controls["expirationDate"].updateValueAndValidity()
      if(this.subscriptionForm.controls["expirationDate"].invalid && this.subscriptionForm.controls["expirationDate"].value != ""){
        this.showPopupErrorDateExpiration();
      }
    }
    if(this.upgradeOrder!=null && this.upgradeOrder.client !=null && this.upgradeOrder.client.accountLevel !=null){
      if(this.upgradeOrder.client.accountLevel.toLowerCase()===this.LEVELN2.toLowerCase() || this.upgradeOrder.client.accountLevel.toLowerCase()===this.LEVELN2P.toLowerCase()) {
        for (let field in this.subscriptionForm.controls) {
          if(field != "entCountry" && field!="entVille" && field!="entCodePostal" && field!="address" && (field!="expirationDate" || this.subscriptionForm.controls["expirationDate"].value!=null) && field!="walletOpeningReasons" && field!="salary" && field!="profession") {
            this.subscriptionForm.controls[field].disable();
            this.disabledCalendar=true;
          }else if(field == "entCountry" || field=="entVille" || field=="entCodePostal" || field=="address" || (field=="expirationDate" && this.subscriptionForm.controls["expirationDate"].value==null) || field=="walletOpeningReasons" || field=="salary" || field=="profession"){
            this.subscriptionForm.controls[field].enable();
          }
        }
        this.sOrder.orderfrozen=false;

      }else {
        for (let field in this.subscriptionForm.controls) {
          this.subscriptionForm.controls[field].enable();
          this.disabledCalendar=false;
        }
        this.subscriptionForm.controls["numRib"].disable();
        if (this.upgradeOrder !== undefined && this.upgradeOrder.client.accountLevel.toLowerCase() ===  this.LEVELN1.toLowerCase() || this.upgradeOrder.client.accountLevel.toLowerCase() === this.LEVELN1P.toLowerCase()) {
          this.subscriptionForm.controls["msisdn"].disable();
        }
      }
    }
    if(this.subscriptionForm.controls['entCountry'].value==0) {
      this.subscriptionForm.controls['entCountry'].setValue('1');
    }

    if(this.sOrder.orderfrozen){
      for (let field in this.subscriptionForm.controls) {
        this.subscriptionForm.controls[field].disable();
      }
    }
  }

  private getRibCode() {
    this.sCommons.openMixer('Merci de patienter, la recherche du client est en cours…');

    this.clientService.getRibCode( this.sOrder.order.client.msisdn,
      this.sOrder.order.client.identificationId,
      this.sOrder.order.client.identityType.identificationTypeID).subscribe(
      response => {
        if (response != null && response.success) {
          let rib = response.successResponse;

          this.sOrder.order.client.numRib = rib.toString();

          this.sCommons.closePopup();
          this.sCommons.closeMixer();

        } else {
          this.sOrder.order.client.numRib = 'RIB code not found';

          this.sCommons.closePopup();
          this.sCommons.closeMixer();

          this.showPopupErrorRIB();
        }

      }, responseErr => {
        this.sCommons.closeMixer();

        this.showPopupErrorRIB();
      });
  }

  private isFieldDisabled(): boolean {
    return this.updLevel === this.LEVELN3.toLowerCase() || this.updLevel === this.LEVELN3P.toLowerCase();
  }

  private isFieldMsidnDisabled(): boolean{
    return this.upgradeOrder !== undefined || this.updLevel === this.LEVELN3.toLowerCase() || this.updLevel === this.LEVELN3P.toLowerCase();
  }

  private loadCombosValue2() {
    const combosData = [
      { data: "identityTypes",
        service: this.sCombos.getIdentityTypes(),
        field: "identificationTypeID"},
      { data: "professions",
        service: this.sCombos.getProfessionsList(),
        field: "professionID"},
      { data: "walletOpeningReasons",
        service: this.sCombos.getOpeningReasonsList(),
        field: "openingReasonID"},
      { data: "nationalities",
        service: this.sCombos.getNationalities(),
        field: "nationalityID"},
      { data: "countries",
        service: this.sCombos.getAllCountries(),
        field: "id"},
      { data: "salarys",
        service: this.sCombos.getSalaryList(),
        field: "salaryID"},
    ];

    this.sCommons.openMixer("Chargement. S'il vous plaît, attendez...");
    forkJoin(combosData.map(n => n.service))
      .pipe(takeWhile(() => this.alive))
      .subscribe(getCombosDataRes => {
        if(localStorage.getItem("lastRequest")!=null) {
          // Fill each combo with service response data
          getCombosDataRes.forEach((serviceRes, index) => {
            this.getComboElements(
              combosData[index].data, serviceRes,
              combosData[index].field, "name"
            );
          });
          this.checkRequiredCombos();
          this.sCommons.closeMixer();
        }
      }, err => {
        this.sOrder.showPopupNotConnected();
      });
  }


  private showPopupErrorRIB() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = 'RIB not found';
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopupError(error : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = error;
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopupErrorID() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translate.instant('search.message.form.mandatoryInformationID.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translate.instant('search.message.form.mandatoryInformationID.msg');
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopupErrorDateExpiration() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translate.instant('Orange Money indique');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translate.instant('form.error.expiration');
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translate.instant('search.message.error.searchingClient.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translate.instant('search.message.error.searchingClient.msg');
    this.sCommons.openGeneralPopup(popupObj);
  }

  upperCase(){
    this.subscriptionForm.controls['name'].setValue((this.subscriptionForm.controls['name'].value).toUpperCase());
    this.subscriptionForm.controls['surname'].setValue((this.subscriptionForm.controls['surname'].value).toUpperCase());
  }

  isFocusOut(){
    this.focusOut=true;
  }

  isFocus(){
    this.focusOut=false;
  }

  openValidateDateModal() {
    const ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      windowClass: "sizeValidateModal"
    };
    this.validateModalRef = this.modalService.open(this.validateDate, ngbModalOptions);
    this.popupObj.popupMessage='La pièce d\'identité du client a été expirée. Merci de procéder à la mise à jour des données client.';
    this.popupObj.title='Orange Money Indique';
    this.popupObj.popupIconName='fas fa-exclamation-triangle';
  }

  closeValidateDateModal() {
    if(this.validateModalRef!=null) {
      this.validateModalRef.close();
    }
  }

  closeRedirect() {
    this.closeValidateDateModal();
    this.removeSession=false;
    this.router.navigate(['/expired-date']);
  }
}
