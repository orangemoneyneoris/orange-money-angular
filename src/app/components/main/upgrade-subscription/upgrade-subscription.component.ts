import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Route, Router} from "@angular/router";
import {WalletsService} from "../../../services/wallets.service";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {UpgradeOrder} from "../../../domain/UpgradeOrder";
import {UpgradeWalletService} from "../../../services/UpgradeWallet/upgrade-wallet.service";

@Component({
  selector: 'app-upgrade-subscription',
  templateUrl: './upgrade-subscription.component.html',
  styleUrls: ['./upgrade-subscription.component.css']
})
export class UpgradeSubscriptionComponent implements OnInit {

  step2Go: string;
  upgradeLevel: string;
  upgradeOrder: UpgradeOrder;

  private alive: boolean = true;

  constructor(private thisRoute: ActivatedRoute,
              private router: Router,
              private upgradeService: UpgradeWalletService,
              private sOrder: SubscriptionOrderService,
              private activatedRoute: ActivatedRoute,
              private sWallet: WalletsService) {}

  ngOnInit() {
    this.upgradeOrder = window.history.state.upgradeOrder as UpgradeOrder;
    this.upgradeLevel = window.history.state.level;

    let ordrId = this.activatedRoute.snapshot.paramMap.get('ordrId');
    if(ordrId==null) {
      this.gotoStep('step1');
    }else{
      this.gotoStep('step2');
    }
  }

  gotoStep(step2Go: string) {
    this.step2Go = step2Go;
  }
}
