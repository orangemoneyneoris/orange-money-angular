import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from "@angular/router";
import {JasperService} from "../../../../services/jasper.service";
import {CommonsService} from "../../../../services/commons.service";
import {WalletDocument} from "../../../../domain/WalletDocument";
import {SubscriptionOrderService} from "../../../../services/subscription-order.service";
import {catchError, switchMap, takeWhile} from "rxjs/internal/operators";
import {NEVER} from "rxjs/index";
import {UpgradeOrder} from "../../../../domain/UpgradeOrder";
import {UpgradeWalletService} from "../../../../services/UpgradeWallet/upgrade-wallet.service";
import {TranslateService} from "@ngx-translate/core";
import {ViewDocumentsService} from "../../../../services/View.Documents-Service/view-documents.service";
import {PopupObj} from "../../../../util/PopupObj";
import {OrdOrder} from "../../../../domain/OrdOrder";
import {OrdSubStatus2} from "../../../../domain/OrdSubStatus2";
import {NEW_SUBSCRIPTION} from "../../../../util/Constants";

@Component({
  selector: 'app-upgradesp-step2',
  templateUrl: './upgradesp-step2.component.html',
  styleUrls: ['./upgradesp-step2.component.css']
})
export class UpgradespStep2Component implements OnInit, OnDestroy {

  @Input() updLevel: string;
  @Input() upgradeOrder: UpgradeOrder;

  @Output() step2Go = new EventEmitter<string>();

  LEVELN1 = NEW_SUBSCRIPTION.LEVELN1;
  LEVELN2 = NEW_SUBSCRIPTION.LEVELN2;
  LEVELN3 = NEW_SUBSCRIPTION.LEVELN3;
  LEVELN1P = NEW_SUBSCRIPTION.LEVELN1P;
  LEVELN2P = NEW_SUBSCRIPTION.LEVELN2P;
  LEVELN3P = NEW_SUBSCRIPTION.LEVELN3P;

  refId: string = '';
  documentationForm= new FormGroup({
  idObverseDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
  idObverseDocHidden: new FormControl({value: '', disabled:true}),
  idReverseDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
  idReverseDocHidden: new FormControl({value: '', disabled:true}),
  addressDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
  addressDocHidden: new FormControl({value: '', disabled:true}),
  contractDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
  contractDocHidden: new FormControl({value: '', disabled:true}),
  otherDoc: new FormControl({value: '', disabled:true}),
  otherDocHidden: new FormControl({value: '', disabled:true})
});

  private updOrder: UpgradeOrder;
  private alive: boolean = true;
  formCharge: boolean = false;
  private removeSession: boolean = true;
  contractsUploaded: boolean = false;
  documentsSave: boolean = true;
  ordrId: string;
  private listTypeDocuments: string[] = new Array<string>();
  private errorAttach: boolean = false;

  constructor(private sCommons: CommonsService,
              private upgradeService: UpgradeWalletService,
              private sOrder: SubscriptionOrderService,
              private activatedRoute: ActivatedRoute,
              private sJasper: JasperService,
              private translate: TranslateService,
              private router: Router,
              private serviceDoc: ViewDocumentsService) {}

  ngOnInit() {
    this.ordrId = this.activatedRoute.snapshot.paramMap.get('ordrId');
    if ( this.ordrId != null && (this.sOrder.getSessionOrderUpgrade()==undefined || this.sOrder.getSessionOrderUpgrade().ordOrder.ordrId==undefined)) {
      this.upgradeService.getOrderById( this.ordrId).subscribe(res => {
        if (res && res.success) {
          this.upgradeOrder = res.successResponse as UpgradeOrder;
          this.upgradeOrder.client.accountLevel=this.upgradeOrder.lastLevelWallet;
          if(this.upgradeOrder.ordOrder.ordStatus.statusId==2) {
            this.upgradeService.getWalletDocumentById(this.upgradeOrder.wallet.walletID).subscribe(response => {
              if (response && response.success) {
                let walletDocuments = response.successResponse as WalletDocument[];
                if (walletDocuments != undefined) {
                  this.upgradeOrder.wallet.walletDocuments = walletDocuments;
                } else {
                  this.upgradeOrder.wallet.walletDocuments = new Array<WalletDocument>();
                }
                this.sOrder.orderUpgrade = this.upgradeOrder;
                this.updLevel = this.upgradeOrder.wallet.accountLevel.toLocaleLowerCase();
              } else {
                this.sCommons.closeMixer();
                this.sOrder.showPopupOrderNotFound( this.ordrId);
              }
              if (this.updOrder && this.updOrder.lastLevelWallet != this.LEVELN1 && this.updOrder.lastLevelWallet != this.LEVELN1P) {
                this.initForm();
              }
              this.formCharge = true;

              let sessionOrder = this.upgradeOrder;

              if (sessionOrder !== undefined) {

                this.updOrder = sessionOrder;
                this.synchronizeForm();
                if (this.updOrder && (this.updOrder.lastLevelWallet == this.LEVELN1 || this.updOrder.lastLevelWallet == this.LEVELN1P) && this.documentationForm.controls['contractDoc'].value != "" &&
                  this.documentationForm.controls['contractDoc'].value != null) {
                  this.enableFieldsN1();
                } else if (this.updOrder.lastLevelWallet != this.LEVELN1 && this.updOrder.lastLevelWallet != this.LEVELN1P) {
                  this.documentationForm.controls['addressDoc'].enable();
                  this.documentationForm.controls['addressDocHidden'].enable();
                }
              } else {

                this.updOrder = new UpgradeOrder();
              }


              if (this.sOrder.orderfrozen) {
                this.disableAllFields();
              }
              this.checkAccountLevel();

              if (this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
                && this.documentationForm.controls['idReverseDoc'].value!="" && this.upgradeOrder.wallet.accountLevel.toLowerCase()==this.LEVELN2.toLowerCase()) {


                this.sOrder.contractUploaded = true;
                this.sOrder.orderfrozen = true;
                this.disableAllFields();
              }

              if (this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
                && this.documentationForm.controls['idReverseDoc'].value!="" && this.documentationForm.controls['addressDoc'].value!=""
                && (this.upgradeOrder.wallet.accountLevel.toLowerCase()==this.LEVELN3.toLowerCase() ||
                  this.upgradeOrder.wallet.accountLevel.toLowerCase()==this.LEVELN3P.toLowerCase())) {

                this.sOrder.contractUploaded = true;
                this.sOrder.orderfrozen = true;
                this.disableAllFields();
              }

              if( this.upgradeOrder.client.accountLevel==null) {
                this.showPopupConnectionError();
                setTimeout(() => {
                    this.sCommons.closePopup();
                    this.router.navigate(['/listTransaction']);
                  },
                  3000);
              }
            });
          }else{
            this.sOrder.setSessionOrderUpgrade(this.upgradeOrder);
            this.removeSession=false;
            this.router.navigate(["wallet-upgrade-preview"],{
              state: {
                level: this.updLevel,
              }
            });
          }

        }
      });
    }
    else {
      if(this.upgradeOrder==undefined){
        this.upgradeOrder=this.sOrder.getSessionOrderUpgrade();
        this.updLevel=this.upgradeOrder.wallet.accountLevel.toLocaleLowerCase();
      }
      if(this.updOrder && this.updOrder.lastLevelWallet!=this.LEVELN1 && this.updOrder.lastLevelWallet!=this.LEVELN1P){
        this.initForm();
      }
      this.formCharge=true;
      let sessionOrder = this.sOrder.getSessionOrderUpgrade();
      if (sessionOrder !== undefined) {
        this.updOrder = sessionOrder;
        if(this.updOrder.wallet.walletDocuments == undefined) {
          this.updOrder.wallet.walletDocuments = new Array<WalletDocument>();
        }
        this.synchronizeForm();
        if(this.updOrder && (this.updOrder.client.accountLevel == this.LEVELN1 || this.updOrder.client.accountLevel==this.LEVELN1P) && this.documentationForm.controls['contractDoc'].value!="" &&
          this.documentationForm.controls['contractDoc'].value!=null) {
          this.enableFieldsN1();
        } else if(this.updOrder.client.accountLevel!=this.LEVELN1 && this.updOrder.client.accountLevel!=this.LEVELN1P) {
          this.documentationForm.controls['addressDoc'].enable();
          this.documentationForm.controls['addressDocHidden'].enable();
        }
      } else {
        this.updOrder = new UpgradeOrder();
      }

      if (this.sOrder.orderfrozen) {
        this.disableAllFields();
      }

      this.checkAccountLevel();
    }
  }

  gotoPrevious() {
    this.removeSession=false;
    this.step2Go.emit('step1');
  }

  cancelOrder() {
    this.sOrder.contractPreviewed = false;
    this.sOrder.orderValid = false;
    this.sOrder.contractGenerated = false;
    this.sOrder.setSessionOrderUpgrade(new UpgradeOrder());
    // noinspection JSIgnoredPromiseFromCall
    this.sOrder.cancelOrder(this.upgradeOrder.ordOrder).subscribe(res => {
      if(res.success){
        this.router.navigate(['/listTransaction']);
      }else{
        if(localStorage.getItem("lastRequest")!=null) {
          this.sOrder.cancelError();
        }
      }
    });
  }

  getFormControl(controlName: string) {
      return this.documentationForm.get(controlName) as FormControl;
  }

  get isN3Level(){
    return this.updOrder && (this.updOrder.wallet.accountLevel === this.LEVELN3 || this.updOrder.wallet.accountLevel === this.LEVELN3P) ? "grayedIcon" : "";
  }

  get contractGenerated(){
    return this.sOrder.contractGenerated;
  }

  get contractUploaded(){
    return this.sOrder.contractUploaded;
  }

  get orderFrozen(){
    return this.sOrder.orderfrozen;
  }

  get orderId() {
    return this.updOrder && this.updOrder.ordOrder.ordrId;
  }

  get msisdn() {
    return this.updOrder && this.updOrder.client.msisdn;
  }

  get walletId() {
    return this.updOrder && this.updOrder.wallet ? this.updOrder.wallet.walletID : null;
  }

  previewContract() {
      // noinspection JSIgnoredPromiseFromCall
      this.removeSession=false;
      this.router.navigate(["wallet-upgrade-preview"],{
        state: {
          level: this.updLevel,
        }
      });
  }

  generateContract() {

    this.sCommons.openMixer("Génération du contrat en cours...");
    if(this.updOrder.ribCode==null && this.updOrder.client.numRib!=null){
      this.updOrder.ribCode=this.updOrder.client.numRib;
    }
    this.sJasper.getPDFUpgrade(this.updOrder)
      .pipe(takeWhile(() => this.alive))
      .subscribe(response => {
        this.sCommons.closeMixer();
        this.serveFile2User(response);
        this.sOrder.contractGenerated = true;
        this.enableFieldsN1();
      },() => {
        this.sCommons.closeMixer();
        this.sOrder.contractGenerated = false;
        this.sOrder.showPopupContractError();
      });

  }

  get contractPreviewed(){
    return this.sOrder.contractPreviewed;
  }

  sendContract() {
    // TODO limit number of calls before disabling 'Send Contract' button
    this.sCommons.openMixer(this.translate.instant('new_subscription.waitting'));

    this.sOrder.saveUpgradeOrder(this.updOrder).pipe(
      takeWhile(() => this.alive),
      catchError(saveOrderResErr => {
          //error
          this.sCommons.closeMixer();
          this.sOrder.showPopupNoResponse();
          return NEVER;
        }
      ),
      switchMap(saveOrderRes => {
        //this.handleResponseDocument();
        if (saveOrderRes) {
          if (saveOrderRes.success) {
            this.sCommons.closeMixer();
            this.updOrder.ordOrder =saveOrderRes.successResponse as OrdOrder;
            if (saveOrderRes.errorMessage != null && saveOrderRes.errorMessage.startsWith("KO")) {

              this.showError8500(saveOrderRes.errorMessage.replace("KO - ", ""));
              setTimeout(() => {
                  this.sCommons.closePopup();
                  this.router.navigate(['/listTransaction']);
                },
                3000);
            } else {

              this.showPopSuccesUpgradePopUp(saveOrderRes.errorMessage.replace("OK - ", ""));

              setTimeout(() => {
                  this.sCommons.closePopup();
                  this.router.navigate(['/listTransaction']);
                },
                3000);
              //  ThirdSunscription ->  OK
            }
            return NEVER;
          } else {
            if(saveOrderRes.successResponse!=null){
              let listDocuments: string[] = saveOrderRes.successResponse as string[];
              if(listDocuments!=null && listDocuments.length>0){
                this.enableAllFields();
                for(let i = 0; i<listDocuments.length;i++){
                  this.resetFile(listDocuments[i])
                }
              }
            }
            this.sCommons.closeMixer();
            this.sOrder.showPopupIntegrationKO(saveOrderRes.errorMessage);
            return NEVER;
          }
        }else{
          this.sCommons.closeMixer();
          this.sOrder.showPopupNoResponse();
          return NEVER;
        }

      })).subscribe(secondResponse => {
      //segunda respuesta
      console.log('Creado orden en de document');
    }, walletDocumentsErr => {
      this.sCommons.closeMixer();
      this.sOrder.showPopupConnectionDBError();
      setTimeout(() => {
          this.sCommons.closePopup();
          this.router.navigate(['/listTransaction']);
        },
        3000);
    });
  }

  /*handleResponseDocument(){
    this.serviceDoc.ValidationDocument(this.updOrder.ordOrder).subscribe(response=>{
      console.log("Send Document");
    });
  }*/


  showError8500(errorMessage: string) {
    this.sOrder.showError8500(errorMessage);
    this.listenClosePopup();
  }

  resetFile(controlName: string) {
    const control = this.documentationForm.controls[controlName];
    if (!control.disabled || this.errorAttach) {
      if(this.listTypeDocuments.indexOf(controlName)>=0) {
        this.listTypeDocuments.splice(this.listTypeDocuments.indexOf(controlName), 1);
      }
      control.reset();
      this.documentationForm.controls[controlName+'Hidden'].reset();
      let indexOf = this.getDocumentIndex(controlName);
      if(indexOf>=0) {
        this.updOrder.wallet.walletDocuments.splice(indexOf, 1);
      }
      this.documentationForm.controls[controlName].setValue("");

      this.sOrder.setSessionOrderUpgrade(this.updOrder);
    }
  }

  attachFile2Wallet(controlName: string, $event: any) {
    this.sCommons.openMixer("Joindre un document...");
    this.contractsUploaded=true;
    let documents = $event.target.files;
    let file: File = documents[0];
    let fileName = file.name;
    let sizeValid : boolean = file.size/1024/1024 <=10;
    let extensionValid : boolean = this.sOrder.extensionValidate(fileName);
    if(extensionValid && sizeValid){
      this.documentationForm.controls[controlName].setValue(fileName);
      this.listTypeDocuments.push(controlName);
      this.saveDocumentOnWallet(controlName, file, fileName);
      this.sCommons.closeMixer();
    }else if(!extensionValid){
      this.sCommons.closeMixer();
      this.sOrder.showPopupExtensionNoValid();
    }else{
      this.sCommons.closeMixer();
      this.sOrder.showPopupSizeNoValid();
    }
  }

  unfreezeOrder() {
    this.listTypeDocuments = new Array<string>();
    this.sOrder.contractUploaded = false;
    this.sOrder.contractGenerated = false;
    this.sOrder.contractPreviewed = false;
    this.sOrder.orderfrozen = false;
    //this.enableAllFields();
    this.documentationForm.controls['contractDoc'].reset();
    this.documentationForm.controls['contractDocHidden'].reset();
    let indexOf = this.getDocumentIndex('contractDoc');
    this.updOrder.wallet.walletDocuments.splice(indexOf, 1);
    this.sOrder.setSessionOrder(this.updOrder);
    let subStatus2: OrdSubStatus2 = new OrdSubStatus2();
    subStatus2.status2Id=4;
    subStatus2.name="step1_in_progress"
    this.sOrder.order.ordOrder.ordSubStatus2= subStatus2;
    /*if(this.updOrder && this.updOrder.lastLevelWallet==this.LEVELN1){
      this.enableFieldsN1();
    }else{
      this.documentationForm.controls['contractDoc'].disable();
      this.documentationForm.controls['contractDocHidden'].disable();
    }*/
    //this.documentationForm.controls['contractDoc'].disable();
    //this.documentationForm.controls['contractDocHidden'].disable();

    this.checkAccountLevel();

    this.sOrder.showPopupEditable();
    setTimeout(() => {
      this.sCommons.closePopup();
    }, 2000);
  }

  saveOrder(showPopups: boolean, showFrozenPopup: boolean){
    this.documentsSave=false;
    const order = this.sOrder.orderUpgrade;

    let upgradeOrder: UpgradeOrder;
    if (this.sOrder.orderUpgrade === undefined) {
      upgradeOrder = new UpgradeOrder();
      this.sOrder.orderUpgrade = upgradeOrder;
    } else {

      upgradeOrder = this.sOrder.orderUpgrade;
    }
    upgradeOrder.ordOrder = order.ordOrder;
    upgradeOrder.client = this.sOrder.orderUpgrade.client;
    upgradeOrder.wallet = this.sOrder.orderUpgrade.wallet;
    upgradeOrder.id = this.sOrder.orderUpgrade.id;

    if (showPopups){
      this.sCommons.openMixer("Saving order. Please, wait...");
    }

    this.upgradeService.initUpgradeOrder(upgradeOrder).pipe(
      takeWhile(() => this.alive),
      catchError(saveWalletErr => {
          if(localStorage.getItem("lastRequest")!=null) {
            this.sCommons.closeMixer();
            this.sOrder.showPopupSaveError(saveWalletErr);
          }
          return NEVER;
        }
      ),
      switchMap(ajaxResponse => {
        if (ajaxResponse !== undefined && ajaxResponse.success) {
          let response = ajaxResponse.successResponse as UpgradeOrder;

          if(!this.orderFrozen && this.updOrder.wallet.walletDocuments.length) {
            let walletDocuments: WalletDocument[] = new Array<WalletDocument>();
            this.updOrder.wallet.walletDocuments.forEach(document => {
              if(document.walletDocumentID!= undefined) {
                walletDocuments.push(document);
              }
          });
            this.sOrder.deleteWalletDocuments(walletDocuments).subscribe(res => {
              console.log("Documentos borrados")
            });
          }
          let files: File[] = new Array<File>();
          let walletFiles: WalletDocument[] = new Array<WalletDocument>();
          this.updOrder.wallet.walletDocuments.forEach(document => {
            if(document.document["name"]!= undefined) {
              files.push(document.document);
              walletFiles.push(document)
            }
          });
          if (walletFiles.length!=0) {
            this.errorAttach=false;
            return this.sOrder.saveWalletDocuments(files, upgradeOrder.wallet.walletID, walletFiles, this.listTypeDocuments.length);
          } else {
            this.errorAttach=true;
            this.checkDocumentsAttach(walletFiles);
            this.sCommons.closeMixer();
            this.router.navigate(['/listTransaction']);
          }
        } else {
          let error = ajaxResponse !== undefined ? ajaxResponse.errorMessage : "";
          if(localStorage.getItem("lastRequest")!=null) {
            this.sCommons.closeMixer();
            this.sOrder.showPopupSaveError(error);
          }
        }
        return NEVER;
      })).subscribe(walletDocumentsResponse => {
        if (showPopups) {
          this.sCommons.closeMixer();
          this.sOrder.showPoupSaveComplete();
          this.removeSession=true;
          this.router.navigate(['/listTransaction']);
        }else {
          if (walletDocumentsResponse.success) {
            this.updOrder.wallet.walletDocuments = walletDocumentsResponse.successResponse as WalletDocument[];
            if(showFrozenPopup){
              this.sOrder.showPopupFrozenOrder();
              setTimeout(() => {
                this.documentsSave=true;
                this.sCommons.closePopup();
              }, 2000);
            }
          }else if(walletDocumentsResponse.successResponse!=null){
            this.checkDocumentsAttachBackend(walletDocumentsResponse.successResponse as string[]);
          }else{
            this.errorAttach=true;
            this.sOrder.showPopupDocumentAttach(walletDocumentsResponse.errorMessage);
            this.resetFile(walletDocumentsResponse.errorMessage);
            this.enableFieldsN1();
          }


        }

    }, walletDocumentsErr => {
      this.sCommons.closeMixer();
      this.sOrder.showPopupNotConnected();
    });
  }

  ngOnDestroy(): void {
    if(this.removeSession){
      this.sOrder.removeSessionOrderUpgrade();
      this.sOrder.removeSessionOrder();
      this.sOrder.contractUploaded = false;
      this.sOrder.contractGenerated = false;
      this.sOrder.contractPreviewed = false;
      this.sOrder.orderfrozen = false;
    }
    this.alive = false;
    if(localStorage.getItem("lastRequest")!=null) {
      this.sCommons.closePopup();
    }
  }

  private initForm() {

    this.documentationForm = new FormGroup({
      idObverseDoc: new FormControl({value: '', disabled:this.isFieldDisabled()}, [Validators.required]),
      idObverseDocHidden: new FormControl(),
      idReverseDoc: new FormControl({value: '', disabled:this.isFieldDisabled()}, [Validators.required]),
      idReverseDocHidden: new FormControl(),
      addressDoc: new FormControl(''),
      addressDocHidden: new FormControl(),
      contractDoc: new FormControl({value: '', disabled:true}, [Validators.required]),
      contractDocHidden: new FormControl(),
      otherDoc: new FormControl({value: '', disabled:this.isFieldDisabled()}),
      otherDocHidden: new FormControl()
    });

  }
  private isFieldDisabled(): boolean {

    if(this.upgradeOrder.client.accountLevel == null && this.activatedRoute.snapshot.paramMap.get('ordrId') !=null){
      this.upgradeOrder.client.accountLevel = this.upgradeOrder.lastLevelWallet;
    }
    return (this.upgradeOrder.client.accountLevel!=null && this.upgradeOrder.client.accountLevel.toLowerCase()===this.LEVELN2.toLowerCase());
  }

  private listenClosePopup() {
    this.sCommons.popups()
      .pipe(takeWhile(() => this.alive))
      .subscribe(res => {
        this.router.navigate(['listTransaction']);
      });
  }

  // noinspection JSMethodCanBeStatic
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with ${reason}`;
    }
  }

  private serveFile2User(response) {
    let newBlob = new Blob([response.body], {type: "application/pdf"});

    // IE compatibility
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(newBlob);
      return;
    }

    const data = window.URL.createObjectURL(newBlob);

    let filename = response.headers.get("filename");
    let link = this.createLinkAndClick(data, filename);
    this.removeLink(data, link);
  }

  private removeLink(data, link) {
    setTimeout(function () {
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
  }

  // noinspection JSMethodCanBeStatic
  private createLinkAndClick(data, filename) {
    let link = document.createElement('a');
    link.href = data;
    link.download = filename;
    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
    return link;
  }

  private saveDocumentOnWallet(controlName: string, file: File, fileName) {
    let walletDocument: WalletDocument = new WalletDocument();
    walletDocument.document = file;
    walletDocument.documentType = controlName;
    walletDocument.documentName = fileName;

    let alreadyExists = this.updOrder.wallet.walletDocuments.find(document => {
      return document.documentType === walletDocument.documentType
    });
    if (alreadyExists !== undefined) {
      this.resetFile(walletDocument.documentType);
    }

    this.updOrder.wallet.walletDocuments.push(walletDocument);
    this.sOrder.setSessionOrderUpgrade(this.updOrder);

    this.synchronizeForm();

    if (this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
      && this.documentationForm.controls['idReverseDoc'].value!="" && this.upgradeOrder.wallet.accountLevel.toLowerCase()==this.LEVELN2.toLowerCase()) {

      this.sOrder.contractUploaded = true;
      this.sOrder.orderfrozen = true;
      this.disableAllFields();
      this.saveOrder(false, true);
    }

    if (this.documentationForm.controls['contractDoc'].value!="" && this.documentationForm.controls['idObverseDoc'].value !=""
      && this.documentationForm.controls['idReverseDoc'].value!="" && this.documentationForm.controls['addressDoc'].value!=""
      && (this.upgradeOrder.wallet.accountLevel.toLowerCase()==this.LEVELN3.toLowerCase() ||
        this.upgradeOrder.wallet.accountLevel.toLowerCase()==this.LEVELN3P.toLowerCase())) {

      this.sOrder.contractUploaded = true;
      this.sOrder.orderfrozen = true;
      this.disableAllFields();

      this.saveOrder(false, true);
    }

    if((this.upgradeOrder.client.accountLevel.toLowerCase()==this.LEVELN2.toLowerCase() ||
      this.upgradeOrder.client.accountLevel.toLowerCase()==this.LEVELN2P.toLowerCase()) &&
      this.documentationForm.controls['addressDoc'].value!=""){
      this.saveOrder(false, true);
    }

  }

  private disableAllFields() {
    for (let controlsKey in this.documentationForm.controls) {
      this.documentationForm.controls[controlsKey].disable();
    }
  }

  private enableAllFields() {
    for (let controlsKey in this.documentationForm.controls) {
      this.documentationForm.controls[controlsKey].enable();
    }
  }

  private synchronizeForm() {
    if(this.updOrder.wallet.walletDocuments!=undefined) {
      this.updOrder.wallet.walletDocuments.forEach((document) => {
        this.documentationForm.controls[document.documentType].setValue(document.documentName);
      });
    }
  }

  private checkAccountLevel() {

    if (this.updOrder.wallet.accountLevel !== this.LEVELN3 && this.updOrder.wallet.accountLevel !== this.LEVELN3P) {
      if(this.updOrder.wallet.walletDocuments!=undefined){
        let indexOf = this.getDocumentIndex('addressDoc');

        if (indexOf>=0){
          this.updOrder.wallet.walletDocuments.splice(indexOf, 1);
          this.sOrder.setSessionOrderUpgrade(this.updOrder);
        }
      }

      let control = this.documentationForm.controls['addressDoc'];
      control.disable();
      control.reset();

      let hiddenControl = this.documentationForm.controls['addressDocHidden'];
      hiddenControl.disable();
      hiddenControl.reset();

      this.documentationForm.controls['addressDoc'].clearValidators();

    } else {

      this.documentationForm.controls['addressDoc'].setValidators([Validators.required]);
    }
  }

  private getDocumentIndex(documentType: string): number {
    let document = this.updOrder.wallet.walletDocuments.find(function (document) {
      return document.documentType === documentType;
    });
    return this.updOrder.wallet.walletDocuments.indexOf(document);
  }

  private showSuccesSentPopup() {
    this.sCommons.closeMixer();
    this.sOrder.showPopupSubscriptionComplete();
    this.listenClosePopup();
  }

  private showPopSuccesUpgradePopUp(message: string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-info';
    popupObj.popupMessage = message;
    popupObj.redirect = true;
    this.sCommons.openGeneralPopup(popupObj);
  }

  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Connection Error';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = 'Impossible de se connecter au serveur en essayant de récupérer les informations du client';
    popupObj.redirect = true;
    this.sCommons.openGeneralPopup(popupObj);
  }

  private enableFieldsN1(){
    if(this.sOrder.contractGenerated){
      this.documentationForm.controls['contractDoc'].enable();
      this.documentationForm.controls['contractDocHidden'].enable();
    }
    this.documentationForm.controls['idObverseDoc'].enable();
    this.documentationForm.controls['idObverseDocHidden'].enable();
    this.documentationForm.controls['idReverseDoc'].enable();
    this.documentationForm.controls['idReverseDocHidden'].enable();
    if(this.updOrder && (this.updOrder.wallet.accountLevel === this.LEVELN3 || this.updOrder.wallet.accountLevel === this.LEVELN3P)) {
      this.documentationForm.controls['addressDoc'].enable();
      this.documentationForm.controls['addressDocHidden'].enable();
    }
    this.documentationForm.controls['otherDoc'].enable();
    this.documentationForm.controls['otherDocHidden'].enable();
  }


  private checkDocumentsAttach(documents: WalletDocument[]){
    let exist: boolean;
    for(let i = 0; i<this.listTypeDocuments.length;i++){
      exist=false;
      for(let j = 0; j<documents.length;j++){
        if(this.listTypeDocuments[i]==documents[j].documentType){
          exist=true;
        }
      }
      if(!exist){
        let documentType: string;
        if(this.listTypeDocuments[i]=='idObverseDoc'){
          documentType=this.translate.instant('new_subscription.field.docObserve');
        }else if(this.listTypeDocuments[i]=='idReverseDoc'){
          documentType=this.translate.instant('new_subscription.field.docReverse');
        }else if(this.listTypeDocuments[i]=='addressDoc'){
          documentType=this.translate.instant('new_subscription.field.docJustification');
        }else if(this.listTypeDocuments[i]=='contractDoc'){
          documentType=this.translate.instant('new_subscription.field.boxSigned');
        }else if(this.listTypeDocuments[i]=='otherDoc'){
          documentType=this.translate.instant('new_subscription.field.docOther');
        }
        this.sOrder.showPopupDocumentAttach(documentType);
        this.resetFile(this.listTypeDocuments[i]);
      }
    }
  }

  private checkDocumentsAttachBackend(documents: string[]){
    /*let exist: boolean;
    for(let i = 0; i<this.listTypeDocuments.length;i++){
      exist=false;
      for(let j = 0; j<documents.length;j++){
        if(this.listTypeDocuments[i]==documents[j]){
          exist=true;
        }
      }
      if(!exist){
        let documentType: string;
        if(this.listTypeDocuments[i]=='idObverseDoc'){
          documentType=this.translate.instant('new_subscription.field.docObserve');
        }else if(this.listTypeDocuments[i]=='idReverseDoc'){
          documentType=this.translate.instant('new_subscription.field.docReverse');
        }else if(this.listTypeDocuments[i]=='addressDoc'){
          documentType=this.translate.instant('new_subscription.field.docJustification');
        }else if(this.listTypeDocuments[i]=='contractDoc'){
          documentType=this.translate.instant('new_subscription.field.boxSigned');
        }else if(this.listTypeDocuments[i]=='otherDoc'){
          documentType=this.translate.instant('new_subscription.field.docOther');
        }
        this.sOrder.showPopupDocumentAttach(documentType);
        this.resetFile(this.listTypeDocuments[i]);
      }
    }*/
    this.errorAttach=true;
    this.sOrder.showPopupDocumentAttach("");
    this.resetFile('contractDoc');
  }
}
