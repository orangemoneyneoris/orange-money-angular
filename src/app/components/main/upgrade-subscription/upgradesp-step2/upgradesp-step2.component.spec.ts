import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradespStep2Component } from './upgradesp-step2.component';

describe('UpgradespStep2Component', () => {
  let component: UpgradespStep2Component;
  let fixture: ComponentFixture<UpgradespStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradespStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradespStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
