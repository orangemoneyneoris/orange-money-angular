import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletUpgradePreviewComponent } from './wallet-upgrade-preview.component';

describe('WalletUpgradePreviewComponent', () => {
  let component: WalletUpgradePreviewComponent;
  let fixture: ComponentFixture<WalletUpgradePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletUpgradePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletUpgradePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
