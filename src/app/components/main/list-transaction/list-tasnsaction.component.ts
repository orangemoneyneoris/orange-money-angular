import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {JSONResponse} from "../../../domain/JSONResponse";
import {ListTransactionService} from "../../../services/list-transactions/list-transaction.service";
import {PopupObj} from "../../../util/PopupObj";
import {CommonsService} from "../../../services/commons.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {OrdOrderList} from "../../../domain/OrdOrderList";

import {ExportexcelService} from "../../../services/exportexcel/exportexcel.service";
import {FormControl, FormGroup} from "@angular/forms";

import {DatesFilterList} from "../../../domain/DatesFilterList";

@Component({
  selector: 'app-list-tasnsaction',
  templateUrl: './list-tasnsaction.component.html',
  styleUrls: ['./list-tasnsaction.component.css']
})
export class ListTasnsactionComponent implements OnInit{
  dataSource;
  ListaTotal;
  visible=true;
  valor="";

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });
  private limite: number;

  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }



  constructor(private service: ListTransactionService,
              private route: Router,
              private actRoute: ActivatedRoute,
              private commonsService: CommonsService,
              private translateService: TranslateService,
              private excelService: ExportexcelService) {}

  displayedColumns: string[] = ['numContract','entEntity', 'msisdn', 'ordType', 'ordCategory', 'ordSubCategory', 'ordStatus', 'ordSubStatus', 'ordSubStatus2', 'updatedDate', 'actions'];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  evento: JSONResponse;
  currentPage: number = 0;

  private LIMIT_MONTH = 3;

  private dates : DatesFilterList;

  ngOnInit() {
    this.dates= new DatesFilterList();
    let dateNow = new Date();

    let page = this.actRoute.snapshot.paramMap.get('currentPage');
    this.currentPage = +page;

    dateNow.setMonth(dateNow.getMonth()+1);
    if(dateNow.getMonth()==0){
      this.dates.finishDate=dateNow.getFullYear()+"/"+12+"/"+dateNow.getDate();
    }else{
      this.dates.finishDate=dateNow.getFullYear()+"/"+dateNow.getMonth()+"/"+dateNow.getDate();
    }

    dateNow.setMonth(dateNow.getMonth() - this.LIMIT_MONTH);
    if(dateNow.getMonth()==0){
      let year = dateNow.getFullYear() - 1;

      this.dates.startDate = year + "/" + 12 + "/" + dateNow.getDate();
    }else{
      this.dates.startDate = dateNow.getFullYear() + "/" + dateNow.getMonth() + "/" + dateNow.getDate();
    }

    this.commonsService.openMixer("Consultation en cours...");
    this.listaListransaction();
  }

  edit(typeId: number, orderId: number, orderCategory?: string){
    this.service.redirect(typeId, orderId, orderCategory);
  }

  view(typeId: number, orderId: number){
    if(typeId == 4){
      this.service.getOrdParentOrderById(orderId).subscribe(response => {
        this.service.detail(1, response, this.paginator.pageIndex);
      });
    }else{
      this.service.detail(typeId, orderId, this.paginator.pageIndex);
    }
  }

  goHistoryScreen(orderId: number){
    this.service.goHistoryScreen(orderId);
  }


  getdatePickerFormatF(date: string){
    let formattedDate = {year:"0", month:"0", day:"0"};
    let items = date.split("/", 3);
    if(window.navigator.userAgent.toLowerCase().indexOf('trident') > -1){
      formattedDate.year = items[2].substr(0,5);
      if(items[1].substring(1,2) == "0"){
        formattedDate.month = items[1].substring(2,3);
      }else{
        formattedDate.month = items[1];
      }
      if(items[0].substring(1,2) == "0"){
        formattedDate.day = items[0].substring(2,3);
      }else{
        formattedDate.day = items[0];
      }
    }else {
      formattedDate.year = items[2].substr(0, 4);
      formattedDate.month = items[1];
      formattedDate.day = items[0];
    }
    console.log(formattedDate)
    return formattedDate;
  }


  getDateRange(value) {
    this.commonsService.openMixer("Consultation en cours...");
    this.resetListArray();
    // getting date from calendar
    if(value.fromDate==null){
      value.fromDate="";
    }
    if(value.toDate==null){
      value.toDate="";
    }
    if(value.fromDate=="" && value.toDate==""){
      let dateNow = new Date();
      dateNow.setMonth(dateNow.getMonth()+1);
      if(dateNow.getMonth()==0){
        this.dates.finishDate=dateNow.getFullYear()+"/"+12+"/"+dateNow.getDate();
      }else{
        this.dates.finishDate=dateNow.getFullYear()+"/"+dateNow.getMonth()+"/"+dateNow.getDate();
      }

      dateNow.setMonth(dateNow.getMonth() - this.LIMIT_MONTH);
      if(dateNow.getMonth()==0){
        let year = dateNow.getFullYear() - 1;

        this.dates.startDate = year + "/" + 12 + "/" + dateNow.getDate();
      }else{
        this.dates.startDate = dateNow.getFullYear() + "/" + dateNow.getMonth() + "/" + dateNow.getDate();
      }
    }else if(value.fromDate!="" && value.toDate!="") {
      let fecha;
      let startDate = value.fromDate.toLocaleString();
      let finishDate = value.toDate.toLocaleString();
      fecha = this.getdatePickerFormatF(startDate);
      this.dates.startDate = fecha.year + "/" + fecha.month + "/" + fecha.day;

      fecha = this.getdatePickerFormatF(finishDate);
      this.dates.finishDate = fecha.year + "/" + fecha.month + "/" + fecha.day;
    }else if(value.fromDate=="" && value.toDate!=""){
      let fecha;
      let dateNow = new Date(value.toDate);
      dateNow.setMonth(dateNow.getMonth()-2);

      if(dateNow.getMonth()==0){
        let year = dateNow.getFullYear() - 1;

        this.dates.startDate = year + "/" + 12 + "/" + dateNow.getDate();
      }else{
        this.dates.startDate = dateNow.getFullYear() + "/" + dateNow.getMonth() + "/" + dateNow.getDate();
      }

      let finishDate = value.toDate.toLocaleString();
      fecha = this.getdatePickerFormatF(finishDate);
      this.dates.finishDate = fecha.year + "/" + fecha.month + "/" + fecha.day;
    }else if(value.fromDate!="" && value.toDate==""){
      let fecha;
      let dateNow = new Date(value.fromDate);
      dateNow.setMonth(dateNow.getMonth()+4);

      if(dateNow.getMonth()==0){
        let year = dateNow.getFullYear() - 1;

        this.dates.finishDate = year + "/" + 12 + "/" + dateNow.getDate();
      }else{
        this.dates.finishDate = dateNow.getFullYear() + "/" + dateNow.getMonth() + "/" + dateNow.getDate();
      }

      let startDate = value.fromDate.toLocaleString();
      fecha = this.getdatePickerFormatF(startDate);
      this.dates.startDate = fecha.year + "/" + fecha.month + "/" + fecha.day;
    }
    this.listaListransaction();


  }

  applyFilter(filterValue: string) {
    if(filterValue==""){
      this.dataSource.filter="";
    }
    this.dataSource.filter= filterValue.trim().toLowerCase();
    if(this.dataSource.filteredData.length!=0){
      this.visible=true;
    }
    else{
      this.visible=false;
    }
  }

  resetListArray(){

    this.dataSource =new MatTableDataSource<OrdOrderList>(this.ListaTotal as OrdOrderList[]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  resetList() {
    this.commonsService.openMixer("Consultation en cours...");
    this.resetListArray();

    this.filterForm.get('fromDate').setValue('');
    this.filterForm.get('toDate').setValue('');
    this.valor = "";
    let dateNow = new Date();
    dateNow.setMonth(dateNow.getMonth()+1);
    if(dateNow.getMonth()==0){
      this.dates.finishDate=dateNow.getFullYear()+"/"+12+"/"+dateNow.getDate();
    }else{
      this.dates.finishDate=dateNow.getFullYear()+"/"+dateNow.getMonth()+"/"+dateNow.getDate();
    }
    dateNow.setMonth(dateNow.getMonth() - this.LIMIT_MONTH);
    if(dateNow.getMonth()==0){
      let year = dateNow.getFullYear() - 1;

      this.dates.startDate = year + "/" + 12 + "/" + dateNow.getDate();
    }else{
      this.dates.startDate = dateNow.getFullYear() + "/" + dateNow.getMonth() + "/" + dateNow.getDate();
    }

    this.listaListransaction();

  }

  listaListransaction(){
    this.service.getOrdOrderList(this.dates)
      .subscribe(response => {

        if (response != null
          && response.success
          && response.successResponse != null) {

          this.ListaTotal =response.successResponse as OrdOrderList[];

          this.resetListArray();

        } else {
          if(localStorage.getItem("lastRequest")!=null) {
            this.commonsService.closeMixer();
            this.showPopupConnectionError();
          }
        }

        this.commonsService.closeMixer();

        // Error on call
      }, error1 => {
        if(localStorage.getItem("lastRequest")!=null) {
          this.commonsService.closeMixer();
          this.showPopupConnectionError();
        }
      });
  }



  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Connection Error';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('listTransations.message.error.list');
    this.commonsService.openGeneralPopup(popupObj);
  }

  executeExport():void{
    this.excelService.exportAsExcelFile(this.dataSource.filteredData, 'Listado');
  }


}

