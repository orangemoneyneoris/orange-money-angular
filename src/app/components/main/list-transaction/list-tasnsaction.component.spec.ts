import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTasnsactionComponent } from './list-tasnsaction.component';

describe('ListTasnsactionComponent', () => {
  let component: ListTasnsactionComponent;
  let fixture: ComponentFixture<ListTasnsactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTasnsactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTasnsactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
