import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaDocumentsComponent } from './vista-documents.component';

describe('VistaDocumentsComponent', () => {
  let component: VistaDocumentsComponent;
  let fixture: ComponentFixture<VistaDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
