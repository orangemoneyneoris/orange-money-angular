import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {WalletDocument} from "../../../domain/WalletDocument";
import {ViewDocumentsService} from "../../../services/View.Documents-Service/view-documents.service";
import {CommonsService} from "../../../services/commons.service";
import * as FileSaver from 'file-saver';
import {OrdOrder} from "../../../domain/OrdOrder";
import {SubscriptionOrder} from "../../../domain/SubscriptionOrder";
import {catchError, switchMap, takeWhile} from "rxjs/operators";
import {NEVER} from "rxjs";

@Component({
  selector: 'app-vista-documents',
  templateUrl: './vista-documents.component.html',
  styleUrls: ['./vista-documents.component.css']
})

export class VistaDocumentsComponent implements OnInit {
  dataSource;
  private ordrId: any;
  private ordOrde: OrdOrder;
  private subscriptionOrder: SubscriptionOrder;
  checked: boolean = false;
  disabledBack: boolean=false;
  disabledIntgr: boolean=true;
  numContract: string;
  private alive: boolean = true;
  constructor(
      private activatedRoute: ActivatedRoute,
      private service: ViewDocumentsService,
      private commonsService: CommonsService,
      private router: Router
  ) { }

  ngOnInit() {
    this.ordrId = parseInt(this.activatedRoute.snapshot.paramMap.get('ordrId'));
    //this.numContract = this.activatedRoute.snapshot.paramMap.get('numContract');
   this.ordOrde =new OrdOrder();
    if(this.ordrId != null){
            this.ordOrde.ordrId=this.ordrId;

      this.inicializar(this.ordOrde);


    }else{
      alert('Crear un pop-up para decir que ha habido un error');
    }

  }

  inicializar(order: OrdOrder){

   /* this.service.getListDocument(order)
      .subscribe(response =>{

        if (response != null
          && response.success
          && response.successResponse != null) {
          this.dataSource = response.successResponse as WalletDocument[];
          this.verificar();

        } else {
          this.commonsService.closeMixer();

        }

        // Error on call
      }, error1 => {
        this.commonsService.closeMixer();

      });   */


    this.service.getOrderViewDoc(order).pipe(
      takeWhile(() => this.alive),
      catchError(saveWalletErr => {
          //error
          return NEVER;
        }
      ),
      switchMap(ajaxResponse => {
        if (ajaxResponse !== undefined && ajaxResponse.success) {
          //primera respuesta
          this.ordOrde = ajaxResponse.successResponse as OrdOrder;
          this.numContract=this.ordOrde.numContract;
          if(this.ordOrde.ordStatus.statusId==4){
              this.checked=true;
          }

          //correcto devuelve algo
          return this.service.getListDocument(order);

        }
        //error no devuelve never
        return NEVER;
      })).subscribe(response => {
      //segunda respuesta
      if (response != null
          && response.success
          && response.successResponse != null) {
            this.dataSource = response.successResponse as WalletDocument[];
            if(this.checked){
              this.disabledBack=true;
              this.disabledIntgr=false;
            }
            else{
              this.verificar();
            }


      } else {
        this.commonsService.closeMixer();

      }


    }, walletDocumentsErr => {
      //error
          this.commonsService.closeMixer();
    });

  }


  private downloadFile(item: WalletDocument){

    this.service.getWalletDocument(item)
      .subscribe(response =>{

        if (response != null
          && response.success
          && response.successResponse != null) {

         const img: string = response.successResponse.toString();
         const bytes: string = atob(img);
         const byteNumbers = new Array(bytes.length);
         for (let i = 0; i < bytes.length; i++) {
           byteNumbers[i] = bytes.charCodeAt(i);
         }
         const byteArray = new Uint8Array(byteNumbers);
         const blob: Blob = new Blob([byteArray], { type: 'application/pdf' });
         FileSaver.saveAs(blob, item.documentName+".pdf");

       }
       else {
         this.commonsService.closeMixer();

       }


     });

  }

  private verificar(){
    let j=0;
    for(var i in this.dataSource){

      if(this.dataSource[i].gidOK){
        j++;
      }
    }

    if(j==this.dataSource.length){
      this.disabledBack=true;
      this.disabledIntgr=false;
    }
  }

  processIntegr(){
    if(this.dataSource!=null){

     this.commonsService.openMixer("Validation en cours...");
     this.service.ValidationDocument(this.ordOrde).subscribe(response=>{

       this.dataSource = response.successResponse as WalletDocument;

       // No es necesario hacer esto, validando si response.success es true,
       // ya sabemos si el proceso ha ido bien o no

       //this.verificar();

       this.commonsService.closeMixer();

       if (response.success) {
         this.disabledBack=true;
         this.disabledIntgr=false;
         this.service.showPopupProcessCompleted();
       } else {
         this.service.showPopupProcessFailed();
       }

     }, error1 => {
       this.commonsService.closeMixer();

     });
   }
  }

 goback(){
   this.router.navigate(['listTransaction']);
  }
}
