import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-page-errors',
  templateUrl: './page-errors.component.html',
  styleUrls: ['./page-errors.component.css']
})
export class PageErrorsComponent implements OnInit {

  constructor( private location: Location ) {
  }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

}
