import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyBalanceComponent } from './agency-balance.component';

describe('AgencyBalanceComponent', () => {
  let component: AgencyBalanceComponent;
  let fixture: ComponentFixture<AgencyBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencyBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
