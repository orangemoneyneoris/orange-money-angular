import {Component, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PinPad} from "../../../domain/PinPad";
import {CommonsService} from "../../../services/commons.service";
import {AgencyService} from "../../../services/agency/agency.service";
import {BalanceOrders} from "../../../domain/BalanceOrders";

let required = Validators.required;

@Component({
  selector: 'app-agency-balance',
  templateUrl: './agency-balance.component.html',
  styleUrls: ['./agency-balance.component.css']
})
export class AgencyBalanceComponent implements OnInit {

  balanceForm = new FormGroup({
    pin: new FormControl({value:'', disabled: true}),
    balance: new FormControl({value:''})
  });

  pin = '';
  consulted = false;
  balance = 0;
  balanceOrder: BalanceOrders = new BalanceOrders();
  msisdnAgency = '0614227930';
  pinPad: PinPad = new PinPad();

  constructor(private serviceAgency: AgencyService,
              private commonService: CommonsService) { }

  ngOnInit() {

  }

  setPinPad(pinpad: PinPad) {
    if (pinpad) {
      this.pinPad = pinpad;
    }
  }

  eventClickButton(res){
    //Event to get the button clicked on the pinPad component
    if (res !== null || res !== undefined) {
      this.consulted = false;
      let button = res;

      if (button == "r") {
        this.pin = this.pin.substring(0, this.pin.length - 1);
      } else {
        this.pin = this.pin + button;
      }
    }
  }

  consultBalance(){
    if (this.pin != null && this.pin !== undefined && this.pin !== '' ) {
      //Initialization of the balance order
      //Each time this button is clicked a new order is created
      this.balanceOrder = new BalanceOrders();

      let errorMessage = "";
      this.commonService.openMixer("Chargement en cours...");

      this.balanceOrder.pin = this.pin;
      this.balanceOrder.pinKey = this.pinPad.privateKey;
      this.balanceOrder.msisdn = this.msisdnAgency;

      this.serviceAgency.createBalanceOrder(this.balanceOrder).subscribe(response => {

          if (response != null) {

            if (response.success) {

              this.balanceOrder = response.successResponse as BalanceOrders;

              this.getBalance();

            } else {
              this.commonService.closeMixer();
              errorMessage = response.errorMessage != null ? response.errorMessage : 'Une erreur est survenue lors de la consultation du solde Agence';
              this.serviceAgency.showPopupErrorService(errorMessage);
            }
          } else {
            this.commonService.closeMixer();

            this.serviceAgency.showPopupConnectionError();
          }

          // Error on call
        }, () => {
          this.commonService.closeMixer();

          this.serviceAgency.showPopupConnectionError();
        });


    }else{
      this.serviceAgency.showFormPinEmpty();
    }

  }

  private getBalance(){
    let amount = "";
    let errorMessage = "";

    this.serviceAgency.getBalance(this.balanceOrder).subscribe(response => {
        if (response != null && response.success) {

          amount = response.successResponse.toString();
          if(amount!= null && amount != ""){
            this.commonService.closeMixer();
            this.consulted = true;
            this.balanceForm.get('balance').setValue(amount);

          }else{
            this.commonService.closeMixer();
            errorMessage = 'Le solde Agence est épuisé';
            this.serviceAgency.showPopupErrorService(errorMessage);
          }


        }else{
          this.commonService.closeMixer();
          errorMessage = response.errorMessage != null ? response.errorMessage : 'Error';
          this.serviceAgency.showPopupErrorService(errorMessage);
        }

      }, () => {
        this.commonService.closeMixer();

        this.serviceAgency.showPopupConnectionError();
      });

    //return balance;

  }

}
