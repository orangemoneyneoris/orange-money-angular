import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTransactionCashComponent } from './list-transaction-cash.component';

describe('ListTransactionCashComponent', () => {
  let component: ListTransactionCashComponent;
  let fixture: ComponentFixture<ListTransactionCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTransactionCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTransactionCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
