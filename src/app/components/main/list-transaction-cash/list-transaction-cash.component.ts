import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {PopupObj} from '../../../util/PopupObj';
import {CommonsService} from '../../../services/commons.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ExportexcelService} from '../../../services/exportexcel/exportexcel.service';
import {FormControl, FormGroup} from '@angular/forms';
import {DatesFilterList} from '../../../domain/DatesFilterList';
import {DataExcelCash} from '../../../domain/DataExcelCash';
import {ViewCashTransactions} from '../../../domain/ViewCashTransactions';
import {ListTransactionsCashService} from '../../../services/list-transactions-cash/list-transactions-cash.service';
import {MatDialog} from "@angular/material/dialog";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {PROFILES} from "../../../util/Constants";
import {DataExcelCashRC} from "../../../domain/DataExcelCashRC";

@Component({
  selector: 'app-list-transaction-cash',
  templateUrl: './list-transaction-cash.component.html',
  styleUrls: ['./list-transaction-cash.component.css']
})
export class ListTransactionCashComponent implements OnInit {
  dataSource;
  ListaTotal;
  visible = true;
  valor = '';

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });

  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }

  constructor(private service: ListTransactionsCashService,
              private route: Router,
              private actRoute: ActivatedRoute,
              private commonsService: CommonsService,
              private translateService: TranslateService,
              private excelService: ExportexcelService,
              public dialog: MatDialog,
              private sOrder: SubscriptionOrderService) {}

  displayedColumns: string[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  currentPage = 0;
  private LIMIT_MONTH = 2;
  private dates: DatesFilterList;

  ngOnInit() {
    const groupId = localStorage.getItem('groupId');
    if(this.isReportingCentral == groupId) {
      this.displayedColumns =
        ['entity',
          'updateDate',
          'type',
          'status',
          'amount',
          'tangoID',
          'orderID',
          'compteClient',
          'compteAgence',
          'dealer',
          'detail'
        ];
    }else{
      this.displayedColumns =
        ['updateDate',
          'type',
          'status',
          'amount',
          'tangoID',
          'orderID',
          'compteClient',
          'compteAgence',
          'dealer',
          'detail'
        ];
    }

    this.dates = new DatesFilterList();
    const dateNow = new Date();

    const page = this.actRoute.snapshot.paramMap.get('currentPage');
    this.currentPage = +page;

    dateNow.setMonth(dateNow.getMonth() + 1);
    if (dateNow.getMonth() == 0) {
      this.dates.finishDate = dateNow.getFullYear() + '/' + 12 + '/' + dateNow.getDate();
    } else {
      this.dates.finishDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
    }

    dateNow.setMonth(dateNow.getMonth() - this.LIMIT_MONTH);
    if (dateNow.getMonth() == 0) {
      const year = dateNow.getFullYear() - 1;

      this.dates.startDate = year + '/' + 12 + '/' + dateNow.getDate();
    } else {
      this.dates.startDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
    }

    this.dates.userName = localStorage.getItem('username');

    this.commonsService.openMixer('Consultation en cours...');
    this.getCashTransactionsList();
  }

  openModal(event, title, value) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = title + ':';
    popupObj.popupMessage = value;
    popupObj.enableClose = true;

    this.commonsService.openGeneralPopup(popupObj);
  }

  getdatePickerFormatF(date: string) {
    const formattedDate = {year: '0', month: '0', day: '0'};
    const items = date.split('/', 3);
    if (window.navigator.userAgent.toLowerCase().indexOf('trident') > -1) {
      formattedDate.year = items[2].substr(0, 5);
      if (items[1].substring(1, 2) == '0') {
        formattedDate.month = items[1].substring(2, 3);
      } else {
        formattedDate.month = items[1];
      }
      if (items[0].substring(1, 2) == '0') {
        formattedDate.day = items[0].substring(2, 3);
      } else {
        formattedDate.day = items[0];
      }
    } else {
      formattedDate.year = items[2].substr(0, 4);
      formattedDate.month = items[1];
      formattedDate.day = items[0];
    }
    console.log(formattedDate);
    return formattedDate;
  }


  getDateRange(value) {
    this.commonsService.openMixer('Consultation en cours...');
    this.resetListArray();
    // getting date from calendar
    if (value.fromDate == null) {
      value.fromDate = '';
    }
    if (value.toDate == null) {
      value.toDate = '';
    }
    if (value.fromDate == '' && value.toDate == '') {
      const dateNow = new Date();
      dateNow.setMonth(dateNow.getMonth() + 1);
      if (dateNow.getMonth() == 0) {
        this.dates.finishDate = dateNow.getFullYear() + '/' + 12 + '/' + dateNow.getDate();
      } else {
        this.dates.finishDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
      }

      dateNow.setMonth(dateNow.getMonth() - this.LIMIT_MONTH);
      if (dateNow.getMonth() == 0) {
        const year = dateNow.getFullYear() - 1;

        this.dates.startDate = year + '/' + 12 + '/' + dateNow.getDate();
      } else {
        this.dates.startDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
      }
    } else if (value.fromDate != '' && value.toDate != '') {
      let fecha;
      const startDate = value.fromDate.toLocaleString();
      const finishDate = value.toDate.toLocaleString();
      fecha = this.getdatePickerFormatF(startDate);
      this.dates.startDate = fecha.year + '/' + fecha.month + '/' + fecha.day;

      fecha = this.getdatePickerFormatF(finishDate);
      this.dates.finishDate = fecha.year + '/' + fecha.month + '/' + fecha.day;
    } else if (value.fromDate == '' && value.toDate != '') {
      let fecha;
      const dateNow = new Date(value.toDate);
      dateNow.setMonth(dateNow.getMonth() - 2);

      if (dateNow.getMonth() == 0) {
        const year = dateNow.getFullYear() - 1;

        this.dates.startDate = year + '/' + 12 + '/' + dateNow.getDate();
      } else {
        this.dates.startDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
      }

      const finishDate = value.toDate.toLocaleString();
      fecha = this.getdatePickerFormatF(finishDate);
      this.dates.finishDate = fecha.year + '/' + fecha.month + '/' + fecha.day;
    } else if (value.fromDate != '' && value.toDate == '') {
      let fecha;
      const dateNow = new Date(value.fromDate);
      dateNow.setMonth(dateNow.getMonth() + 4);

      if (dateNow.getMonth() == 0) {
        const year = dateNow.getFullYear() - 1;

        this.dates.finishDate = year + '/' + 12 + '/' + dateNow.getDate();
      } else {
        this.dates.finishDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
      }

      const startDate = value.fromDate.toLocaleString();
      fecha = this.getdatePickerFormatF(startDate);
      this.dates.startDate = fecha.year + '/' + fecha.month + '/' + fecha.day;
    }

    this.dates.userName = localStorage.getItem('username');

    this.getCashTransactionsList();
  }

  applyFilter(filterValue: string) {
    if (filterValue == '') {
      this.dataSource.filter = '';
    }
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length != 0) {
      this.visible = true;
    } else {
      this.visible = false;
    }
  }

  resetListArray() {

    this.dataSource = new MatTableDataSource<ViewCashTransactions>(this.ListaTotal as ViewCashTransactions[]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  resetList() {
    this.commonsService.openMixer('Consultation en cours...');
    this.resetListArray();

    this.filterForm.get('fromDate').setValue('');
    this.filterForm.get('toDate').setValue('');
    this.valor = '';
    const dateNow = new Date();
    dateNow.setMonth(dateNow.getMonth() + 1);
    if (dateNow.getMonth() == 0) {
      this.dates.finishDate = dateNow.getFullYear() + '/' + 12 + '/' + dateNow.getDate();
    } else {
      this.dates.finishDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
    }
    dateNow.setMonth(dateNow.getMonth() - this.LIMIT_MONTH);
    if (dateNow.getMonth() == 0) {
      const year = dateNow.getFullYear() - 1;

      this.dates.startDate = year + '/' + 12 + '/' + dateNow.getDate();
    } else {
      this.dates.startDate = dateNow.getFullYear() + '/' + dateNow.getMonth() + '/' + dateNow.getDate();
    }

    this.dates.userName = localStorage.getItem('username');

    this.getCashTransactionsList();

  }

  getCashTransactionsList() {
    this.service.getOrdOrderList(this.dates)
      .subscribe(response => {

        if (response != null
          && response.success
          && response.successResponse != null) {

          this.ListaTotal = response.successResponse as ViewCashTransactions[];

          this.resetListArray();

        } else {
          if (localStorage.getItem('lastRequest') != null) {
            this.commonsService.closeMixer();
            this.showPopupConnectionError();
          }
        }

        this.commonsService.closeMixer();

        // Error on call
      }, error1 => {
        if (localStorage.getItem('lastRequest') != null) {
          this.commonsService.closeMixer();
          this.showPopupConnectionError();
        }
      });
  }



  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Connection Error';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('listTransations.message.error.list');
    this.commonsService.openGeneralPopup(popupObj);
  }

  executeExport(): void {
    if(this.isReportingCentral == localStorage.getItem('groupId')) {
      let dataExcelList: DataExcelCashRC[] = new Array<DataExcelCashRC>();
      let dataExcelCashTransaction: DataExcelCashRC;
      for (let cashOrders in this.ListaTotal) {
        dataExcelCashTransaction = new DataExcelCashRC();
        dataExcelCashTransaction.entity = this.ListaTotal[cashOrders].entity;
        dataExcelCashTransaction.updateDate = this.ListaTotal[cashOrders].updateDate;
        dataExcelCashTransaction.type = this.ListaTotal[cashOrders].type;
        dataExcelCashTransaction.status = this.ListaTotal[cashOrders].status;
        dataExcelCashTransaction.amount = this.ListaTotal[cashOrders].amount;
        if (this.ListaTotal[cashOrders].tangoID != null && (this.ListaTotal[cashOrders].tangoID.startsWith('CI') || this.ListaTotal[cashOrders].tangoID.startsWith('CO'))) {
          dataExcelCashTransaction.tangoID = this.ListaTotal[cashOrders].tangoID;
        }
        dataExcelCashTransaction.orderID = this.ListaTotal[cashOrders].orderID;
        dataExcelCashTransaction.compteClient = this.ListaTotal[cashOrders].compteClient;
        dataExcelCashTransaction.compteAgence = this.ListaTotal[cashOrders].compteAgence;
        dataExcelCashTransaction.dealer = this.ListaTotal[cashOrders].dealer;

        dataExcelList.push(dataExcelCashTransaction);
      }
      this.excelService.exportAsExcelFile(dataExcelList, 'Cash Transactions', true);
    }else{
      let dataExcelList: DataExcelCash[] = new Array<DataExcelCash>();
      let dataExcelCashTransaction: DataExcelCash;
      for (let cashOrders in this.ListaTotal) {
        dataExcelCashTransaction = new DataExcelCash();
        dataExcelCashTransaction.updateDate = this.ListaTotal[cashOrders].updateDate;
        dataExcelCashTransaction.type = this.ListaTotal[cashOrders].type;
        dataExcelCashTransaction.status = this.ListaTotal[cashOrders].status;
        dataExcelCashTransaction.amount = this.ListaTotal[cashOrders].amount;
        if (this.ListaTotal[cashOrders].tangoID != null && (this.ListaTotal[cashOrders].tangoID.startsWith('CI') || this.ListaTotal[cashOrders].tangoID.startsWith('CO'))) {
          dataExcelCashTransaction.tangoID = this.ListaTotal[cashOrders].tangoID;
        }
        dataExcelCashTransaction.orderID = this.ListaTotal[cashOrders].orderID;
        dataExcelCashTransaction.compteClient = this.ListaTotal[cashOrders].compteClient;
        dataExcelCashTransaction.compteAgence = this.ListaTotal[cashOrders].compteAgence;
        dataExcelCashTransaction.dealer = this.ListaTotal[cashOrders].dealer;

        dataExcelList.push(dataExcelCashTransaction);
      }
      this.excelService.exportAsExcelFile(dataExcelList, 'Cash Transactions', false);
    }
  }

  public get isReportingCentral(): any {
    return PROFILES.MONEY.REPORT_CENTRAL;
  }

}


