import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonsService} from "../../../services/commons.service";
import {TranslateService} from "@ngx-translate/core";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {PopupObj} from "../../../util/PopupObj";
import {ListTransactionsClientService} from "../../../services/list-transactions-client/list-transactions-client.service";
import {OrdOrderRemote} from "../../../domain/OrdOrderRemote";

@Component({
  selector: 'app-list-transaction-table',
  templateUrl: './list-transaction-table.component.html',
  styleUrls: ['./list-transaction-table.component.css']
})
export class ListTransactionTableComponent implements OnInit {

  dataSource;

  constructor(private service: ListTransactionsClientService,
              private route: Router,
              private commonsService: CommonsService,
              private translateService: TranslateService,
              private activatedRoute: ActivatedRoute) {}

  displayedColumns: string[] = ['transactionID', 'amount', 'serviceType', 'mobileNumber', 'entryType', 'statusTransaction', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  edit(transactionID: string, amount: string, serviceType: string, mobileNumber: string, entryType: string, statusTransaction: string){
    this.service.redirectRemote(transactionID, amount, serviceType, mobileNumber, entryType, statusTransaction);
  }

  view(typeId: number, orderId: number){
    this.service.detail(typeId, orderId);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.commonsService.closeMixer();
    this.commonsService.openMixer("Consultation en cours...");

    this.service.getOrdOrderList()
      .subscribe(response => {

        if (response != null
          && response.success
          && response.successResponse!=null) {

          this.dataSource = new MatTableDataSource<OrdOrderRemote>(response.successResponse as OrdOrderRemote[]);

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

        } else {
          if(localStorage.getItem("lastRequest")!=null) {
            this.commonsService.closeMixer();

            this.showPopupConnectionError();
          }
        }

        this.commonsService.closeMixer();

        // Error on call
      }, error1 => {
        if(localStorage.getItem("lastRequest")!=null) {
          this.commonsService.closeMixer();

          this.showPopupConnectionError();
        }
      });


  }


  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Connection Error';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('listTransations.message.error.list');
    this.commonsService.openGeneralPopup(popupObj);
  }

}

