import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTransactionTableComponent } from './list-transaction-table.component';

describe('ListTransactionTableComponent', () => {
  let component: ListTransactionTableComponent;
  let fixture: ComponentFixture<ListTransactionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTransactionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTransactionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
