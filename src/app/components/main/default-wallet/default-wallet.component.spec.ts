import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultWalletComponent } from './default-wallet.component';

describe('DefaultWalletComponent', () => {
  let component: DefaultWalletComponent;
  let fixture: ComponentFixture<DefaultWalletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultWalletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
