import {Component, OnInit} from '@angular/core';
import {Client} from "../../../domain/Client";
import {Wallet} from "../../../domain/Wallet";
import {DefaultOrders} from "../../../domain/DefaultOrders";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultWalletService} from "../../../services/DefaultWallet/default-wallet.service";
import {CommonsService} from "../../../services/commons.service";
import {PopupObj} from "../../../util/PopupObj";
import {catchError, switchMap, takeWhile} from "rxjs/operators";
import {IdentificationType} from "../../../domain/IdentificationType";
import {CombosService} from "../../../services/combos.service";
import {CashInService} from "../../../services/cash-in/cash-in.service";
import {JSONResponse} from "../../../domain/JSONResponse";
import {TranslateService} from "@ngx-translate/core";
import {ClientsService} from "../../../services/clients.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NEVER} from "rxjs";
import {startWith0607} from "../../../util/custom-validators";
import {NEW_SUBSCRIPTION} from "../../../util/Constants";

let required = Validators.required;

@Component({
  selector: 'app-default-wallet',
  templateUrl: './default-wallet.component.html',
  styleUrls: ['./default-wallet.component.css']
})
export class DefaultWalletComponent implements OnInit {

  LEVELN1 = NEW_SUBSCRIPTION.LEVELN1;

  client: Client = new Client();
  wallet: Wallet = new Wallet();
  refId: string = '';

  defaultOrder: DefaultOrders = new DefaultOrders();
  name="";
  nationality="";
  idNumber="";
  surname="";
  idType="";
  phone="";
  expDate;

  defaultForm: FormGroup;
  identityTypes: IdentificationType[] = [];

  private alive: boolean = true;

  showOtpModal: boolean = false;
  orderId: number;
  orderClosed = false;
  clientExist = false;
  searched: boolean = false;

  constructor(private defaultService: DefaultWalletService,
              private commonService: CommonsService,
              private activatedRoute: ActivatedRoute,
              private combosService: CombosService,
              private cashInService: CashInService,
              private translateService: TranslateService,
              private clientService: ClientsService,
              private route: Router) {
  }

  ngOnInit() {
    this.initForm();

    let clientBack = window.history.state.client as Client;
    let walletBack = window.history.state.wallet as Wallet;
    if (clientBack) {
      this.client = clientBack;
      this.disableFields(true);
      this.clientExist = true;
    }
    if (walletBack)
      this.wallet = walletBack;

    let ordrId = parseInt(this.activatedRoute.snapshot.paramMap.get('ordrId'));
    if (ordrId && ordrId != null) {
      this.orderId = ordrId;
      this.loadIdentificationTypeList();
      this.loadOrderFromDatabase(ordrId);
    } else {
      this.loadIdentificationTypeList();
      this.fillInputs();
    }
  }

  searchClient () {
    const id: string = this.defaultForm.get('idNumber').value;
    const idType: string = this.defaultForm.get('idType').value;
    const msisdn: string = this.defaultForm.get('phone').value;

    if(id && id !== '' && msisdn && msisdn !== '') {
      this.getClient(id, idType, msisdn);
    }
  }

  fillInputs(){
    this.name = this.client.name;
    this.surname = this.client.surname;
    this.nationality = this.client.nationality!=null ? this.client.nationality.name: "";

    this.idNumber = this.client.identificationId;
    this.defaultForm.get('idNumber').setValue(this.client.identificationId);
    if(this.client!=null && this.client.accountLevel!=this.LEVELN1){
      this.defaultForm.get('idType').setValue(this.client.identityType!=null?this.client.identityType.identificationTypeID:"");
    }

    this.phone = this.client.msisdn;
    this.defaultForm.get('phone').setValue(this.client.msisdn);

    if (this.wallet &&  this.wallet != null
      && this.wallet.expirationDate && this.wallet.expirationDate != null) {
      this.expDate = this.wallet.expirationDate;
    }else{
      this.expDate = this.client.expirationDate;
    }
  }

  loadOrderFromDatabase(ordrId){

      this.commonService.openMixer("Chargement des informations, veuillez patienter....");

      this.defaultService.getDefaultWalletOrder(ordrId)
        .subscribe(defaultOrder => {
          if (defaultOrder != null) {
            let order = defaultOrder as DefaultOrders;
            this.defaultOrder = defaultOrder;

            if(order.wallet && order.wallet != null)
              this.wallet = order.wallet;

            if(order.client && order.client != null)
              this.client = order.client;

            if(order.ordOrder.ordStatus.statusId === 3)
              this.orderClosed = true;

            this.fillInputs();
            this.searchClient();

            this.commonService.closeMixer();
          }else{
            this.commonService.closeMixer();

            this.showPopupErrorService("Error");
          }

          // Error on call
        }, () => {
          this.commonService.closeMixer();

          this.showPopupErrorService("Error");
        });
  }

  validate() {
    /*if (this.defaultForm.valid) {*/
      this.defaultOrder.client = this.client;
      this.defaultOrder.wallet = this.wallet;
      let errorMessage = '';
      this.commonService.openMixer('Chargement en cours...');

      this.defaultService.createDefaultWalletOrder(this.defaultOrder).pipe(
        catchError((responseErr) => {
          this.commonService.closeMixer();

          this.showPopupErrorService('Error');

          return NEVER;
        }),
        switchMap(defaultOrderResponse => {
          if (defaultOrderResponse.success) {
            this.defaultOrder = defaultOrderResponse.successResponse as DefaultOrders;
            this.wallet = this.defaultOrder.wallet;
            this.orderId = this.defaultOrder.ordOrder.ordrId;
            this.client = this.defaultOrder.client;
            //this.commonService.closeMixer();

            return this.defaultService.executeIntegration(this.defaultOrder);

          } else {
            this.commonService.closeMixer();
            errorMessage = defaultOrderResponse.errorMessage != null ? defaultOrderResponse.errorMessage : 'Validate default Wallet Error';
            this.showPopupErrorService(errorMessage);

            return NEVER;
          }
        }),
        catchError((responseErr) => {
          this.commonService.closeMixer();

          this.showPopupErrorService('Error');

          return NEVER;
        }),
      ).subscribe((integrationResponse) => {
        this.handleIntegrationResponse(integrationResponse);

      } , defaultWalletErr => {
        this.commonService.closeMixer();

        this.showPopupErrorService('Error');
      });

    /*} else {
      this.setFormAsTouched(true);
    }*/
  }

  saveOrder(){
    this.defaultOrder.client = this.client;
    this.defaultOrder.wallet = this.wallet;

    let errorMessage = "";
    this.commonService.openMixer("Chargement en cours...");

    this.defaultService.createDefaultWalletOrder(this.defaultOrder).subscribe(response => {

      if (response != null) {

        if (response.success) {
          this.defaultOrder = response.successResponse as DefaultOrders;
          this.route.navigate(['/listTransaction']);
          this.commonService.closeMixer();

        } else {
          this.commonService.closeMixer();
          errorMessage = response.errorMessage != null ? response.errorMessage : 'Validate erreur wallet par défaut';
          this.showPopupErrorService(errorMessage);
        }
      } else {
        this.commonService.closeMixer();

        this.showPopupErrorService("Error");
      }

      // Error on call
    }, () => {
      this.commonService.closeMixer();

      this.showPopupErrorService("error");
    });

  }

  back() {
    this.route.navigate(['/resercher']);
  }

  closeOtpModal() {
    this.showOtpModal = false;
  }

  showPopupErrorService(message : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('defaultWallet.ErrorService');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = message;
    this.commonService.openGeneralPopup(popupObj);
  }

  getFormControl(controlName: string) {
    return this.defaultForm.get(controlName) as FormControl;
  }

  resetForm() {
    this.defaultForm.reset();

    this.expDate = '';
    this.name = '';
    this.surname = '';
    this.nationality = '';

    this.clientExist = false;

    this.disableFields(false);

  }

  private handleIntegrationResponse(integrationResponse: JSONResponse){

    if(integrationResponse.success){
      this.commonService.closeMixer();
      if(integrationResponse.code == 2) {
        this.showPopupWalletIsAlreadyDefault();
      }else{
        this.showOtpModal = true;
      }
    }else{
      this.commonService.closeMixer();

      this.showPopupErrorService(integrationResponse.errorMessage);
    }
  }

  private loadIdentificationTypeList() {
    this.identityTypes = [];

    this.combosService.getIdentityTypes()
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(identificationTypeList => {
      if (identificationTypeList != null
        && identificationTypeList.success
        && identificationTypeList.successResponse !=null) {
        this.identityTypes = identificationTypeList.successResponse as IdentificationType[];
      }
      // Error on call
    }, error1 => {
      this.cashInService.showPopupConnectionError();
    });
  }

  private getClient(id: string, idType: string, msisdn: string) {
    this.commonService.openMixer("Merci de patienter, la recherche du client est en cours…");

    //  Call to mock search customer
    this.clientService.searchCustomer(msisdn, id, idType).pipe(
      catchError((responseSearchErr) => {
        this.commonService.closeMixer();
        this.showPopupConnectionError();
        this.clientExist = false;

        this.resetFields();

        return NEVER;
      }),
      switchMap(customerSearched => {
        //  Call to mock search wallet
        return this.handleCustomerSearched(customerSearched);
      }),
      catchError((eligibilityResponse) => {
        this.commonService.closeMixer();
        this.showPopupConnectionError();
        this.clientExist = false;

        return NEVER;
      })
    ).subscribe((eligibilityResponse) => {
        this.handleIsEligible(eligibilityResponse);

    } , eligibilityResponseErr => {
      this.commonService.closeMixer();
      this.showPopupConnectionError();
      this.clientExist = false;
    });

  }

  private handleCustomerSearched(customer: JSONResponse) {
    this.searched = true;
    if (customer != null && customer.success) {

      if (customer.errorMessage === 'Id error') {
        let client = customer.successResponse as Client;
        this.defaultOrder.client = client;
        this.client = client;

        this.fillInputs();

        this.commonService.closeMixer();

        this.showPopupErrorID();

        this.searched = false;
        this.clientExist = false;

        console.log('El documento no corresponde con el cliente consultado');

        return NEVER;

      } else {
        let client = customer.successResponse as Client;

        this.defaultOrder.client = client;
        this.client = client;

        this.fillInputs();

        console.log('Cliente encontrado!');

        if(client.statusAccount !== 'Actif'){
          this.commonService.closeMixer();
          this.showPopupWalletSuspended();
          this.clientExist = false;
          return NEVER;
        }else{
          this.clientExist = true;


          if(this.client!=null && this.client.accountLevel!=this.LEVELN1 && client.identityType.identificationTypeID!=null){
            return this.clientService.isCustomerEligibleForMakeDefault(
              client.msisdn, client.identificationId, client.identityType.identificationTypeID.toString());
          }else{
            return this.clientService.isCustomerEligibleForMakeDefault(
              client.msisdn, client.identificationId, null);
          }
        }
      }
    }else if(customer.errorMessage!=undefined && customer.code!=null){

      this.commonService.closePopup();
      this.commonService.closeMixer();

      this.showPopupError(customer.errorMessage);

      return NEVER;

    }else{
      this.commonService.closePopup();
      this.commonService.closeMixer();

      this.showPopupClientNotExist();

      this.clientExist = false;

      console.log('Cliente NO encontrado');

      this.resetFields();

      return NEVER;
    }
  }

  private handleIsEligible(response: JSONResponse) {
    this.commonService.closeMixer();

    if (response && response.success && !!response.successResponse) {
      this.clientExist = true;

      console.log('Cliente elegible');
    } else {
      this.showPopupNotEligible();
      this.clientExist = false;

      console.log('Cliente NO elegible');
    }
  }

  private showPopupNotEligible() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('showPopup.subscription.NotEligible.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('showPopup.subscription.NotEligible.message');
    this.commonService.openGeneralPopup(popupObj);
  }

  private showPopupConnectionError() {
      const popupObj: PopupObj = new PopupObj();
      popupObj.title = this.translateService.instant('search.message.error.searchingClient.title');
      popupObj.popupIconName = 'fas fa-exclamation-triangle';
      popupObj.popupMessage = this.translateService.instant('search.message.error.searchingClient.msg');
      this.commonService.openGeneralPopup(popupObj);
  }

  private showPopupErrorID() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('search.message.form.mandatoryInformationID.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.message.form.mandatoryInformationID.msg');
    this.commonService.openGeneralPopup(popupObj);
  }

  private showPopupError(error : string) {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Orange Money indique';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = error;
    this.commonService.openGeneralPopup(popupObj);
  }

  private showPopupClientNotExist() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant( 'search.label.client.notExits.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('search.label.client.notExits');
    this.commonService.openGeneralPopup(popupObj);
  }

  private showPopupWalletIsAlreadyDefault() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant( 'defaultWallet.WalletIsAlreadyDefault.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant( 'defaultWallet.WalletIsAlreadyDefault.message');
    this.commonService.openGeneralPopup(popupObj);
  }

  private showPopupWalletSuspended() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('showPopup.subscription.WalletSuspended.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('showPopup.subscription.WalletSuspended.message');
    this.commonService.openGeneralPopup(popupObj);
  }

  private initForm() {
    this.defaultForm = new FormGroup({
      idType: new FormControl('', [required]),
      idNumber: new FormControl('', [required]),
      phone: new FormControl('', [required, Validators.pattern('[0-9]*'), Validators.minLength(10), startWith0607()]),
    });
  }

  private disableFields(disable: boolean) {
    for (let key in this.defaultForm.controls){
      if (this.defaultForm.controls.hasOwnProperty(key)) {
        disable ? this.defaultForm.controls[key].disable() : this.defaultForm.controls[key].enable();
      }
    }
  }

  private setFormAsTouched(touched: boolean) {
    for (const key in this.defaultForm.controls) {
      if (this.defaultForm.controls.hasOwnProperty(key)) {
        if (touched) {
          this.defaultForm.controls[key].markAsTouched();
        } else {
          this.defaultForm.controls[key].markAsUntouched();
        }
      }
    }
  }

  private resetFields(){
    this.defaultForm.reset();
  }


}
