import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {NgbModal, NgbModalOptions, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {OTPService} from "../../../services/otp.service";
import {Otp} from "../../../domain/Otp";
import {CommonsService} from "../../../services/commons.service";
import {switchMap} from "rxjs/operators";
import {NEVER, Observable, timer} from "rxjs";
import {JSONResponse} from "../../../domain/JSONResponse";
import {catchError, takeWhile, tap} from "rxjs/internal/operators";
import {OtpRequest} from "../../../domain/OtpRequest";
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-otp-validation',
  templateUrl: './otp-validation.component.html',
  styleUrls: ['./otp-validation.component.css']
})
export class OtpValidationComponent implements OnInit, OnDestroy {

  private borrar: boolean;
  @HostListener('document:keyup', ['$event'])
  handleDeleteKeyboardEvent(event: KeyboardEvent) {
    if (event.key === 'Delete' || event.key === 'Backspace') {
      this.borrar=true;
    }else {
      this.borrar=false;
    }
    this.addCharacter();
  }

  @ViewChild('OTPModal') OTPModal : ElementRef;

  @Input() orderId: number;
  @Input() msisdn: string;
  @Input() walletId: number;
  @Input() refId: string;

  @Input() showValidate: Boolean;

  @Output() closed = new EventEmitter<boolean>();

  orderClosed = false;
  resendLimit = false;

  timerOtp = 0;

  OTPForm = new FormGroup({
    OTP: new FormControl(''),
  });

  private OTPModalRef: NgbModalRef;
  private alive: boolean = true;
  private enableRefresh: boolean = true;

  constructor(private otpService: OTPService,
              private modalService: NgbModal,
              private router: Router,
              private commonsService: CommonsService,
              private translateService: TranslateService,
              private sCommons: CommonsService) { }

  ngOnInit() {
    this.openOTPModal();
    this.initOtp();

    this.initAutoRefresh();

  }

  openOTPModal() {
    const ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      keyboard : false,
    };
    this.OTPModalRef = this.modalService.open(this.OTPModal, ngbModalOptions);

  }

  private initAutoRefresh() {
    this.enableRefresh = true;
    const refreshTime = this.otpService.getRefreshDelay();
    timer(refreshTime, refreshTime).pipe(
      takeWhile(() => this.enableRefresh)
    ).subscribe((val) => {
      console.log('Checking timer OTP ', val);
      this.refresh();
    });

  }

  refresh() {
    this.otpService.checkOTPTimer(this.orderId)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(response => {
      if (response != null && response.success) {
        // otp timer received, enable Resent button
        this.resendLimit = false;
      }
    });

  }


  closeOTPModal() {
    this.OTPModalRef.close();
    this.closed.emit(true); // TODO send required information to parent
  }

  resendOTP() {
    this.commonsService.openMixer('Merci de patienter, la renvoi d\'OTP est en cours…');  //(this.translateService.instant('otp.message.load.resendOtp'));
      this.otpService.resendOtp(this.orderId)
        .pipe(
          takeWhile(() => this.alive),
          catchError(resendResponseErr => {
            this.otpService.showPopupConnectionError('');
            this.commonsService.closeMixer();
            return NEVER;
          }),
          switchMap(resendResponse => {
            if (resendResponse.success) {
              let otp = resendResponse.successResponse as Otp;
              //alert(otp.resendTries + " sur " + otp.resendTriesLeft + " Tentatives de renvoi restantes");
              return this.otpService.getOtpDefaultStatus(this.msisdn);
            } else {
              this.otpService.showPopupError(resendResponse.errorMessage);
              this.resendLimit = true;
              this.commonsService.closeMixer();
              return NEVER;
            }
          }),
          catchError(defaultStatusErr => {
            this.otpService.showPopupConnectionError('');
            this.commonsService.closeMixer();
            return NEVER;
          }),
          switchMap(defaultStatusResponse => {
            const statusCode = defaultStatusResponse.code;
            if (statusCode === 1) { // On going
              if(defaultStatusResponse.interopRefId || defaultStatusResponse.interopRefId != null)
                return this.otpService.interopResendOtp(defaultStatusResponse.interopRefId);
              else {
                return NEVER;
                this.commonsService.closeMixer();
              }
            } else { // Other
              return this.otpService.interOpEnrollment(this.getOtpRequest());
            }
          })
        ).subscribe(interopResponse => {
          if (interopResponse.success) {
            this.refId = interopResponse.interopRefId;
            this.commonsService.closeMixer();
            this.otpService.showPopupResend();
          } else {
            this.commonsService.closeMixer();
            this.otpService.showPopupError(interopResponse.errorMessage);
          }
        }, interopErr => {
          this.commonsService.closeMixer();
          this.otpService.showPopupConnectionError('');
      });

  }

  validateOTP() {
    if(this.OTPForm.get('OTP').value != null && this.OTPForm.get('OTP').value != "") {

      this.commonsService.openMixer('Merci de patienter, la confirmation d\'OTP est en cours…'); //(this.translateService.instant('otp.message.load.confirmOtp'));

      this.otpService.getOtpDefaultStatus(this.msisdn).pipe(
        takeWhile(() => this.alive),
        switchMap(defStatusResponse => {
          if (defStatusResponse.success) {
            this.refId = defStatusResponse.interopRefId;

            console.debug("refId = " + this.refId);

            return this.switchOtpStatus(defStatusResponse.code);
          } else {
            this.otpService.showPopupConnectionError(defStatusResponse.errorMessage);
            this.commonsService.closeMixer();
            return NEVER;
          }
        }))
        .subscribe(status => {
          console.log(status);
          //this.router.navigate(['/listTransaction']);
          this.commonsService.closeMixer();
        }, statusErr => {
          this.otpService.showPopupConnectionError('');
          this.commonsService.closeMixer();
        });
    }else{
      this.otpService.showPopupError(this.translateService.instant('otp.message.field.mandatory'));
      this.commonsService.closeMixer();
    }
  }

  cancelOTP() {
    this.otpService.cancelOtp(this.orderId)
      .pipe(takeWhile(() => this.alive))
      .subscribe(cancelRes => {
        this.closeOTPModal();
      }, cancelErr => {
        this.otpService.showPopupConnectionError('');
      });

  }

  ngOnDestroy(): void {
    this.alive = false;
    this.enableRefresh = false;
    this.closeOTPModal();
  }

  private getOtpRequest() {
    let params = new OtpRequest();
    params.refId = this.refId;
    params.orderId = this.orderId;
    params.msisdn = this.msisdn;
    params.walletId = this.walletId;
    params.otpCode = this.OTPForm.get('OTP').value as string;
    return params;
  }

  private switchOtpStatus(statusValue: number): Observable<JSONResponse> {
    switch (statusValue) {
      case 1: // On going
        return this.otpService.confirmEnrollment(this.getOtpRequest())
          .pipe(
            tap(confirmRes => {
              if (confirmRes.success) {
                this.listenCloseConfirmation();
                this.showValidate = false;
                this.otpService.showPopupOtpCompleted();
                this.orderClosed= true;
                this.router.navigate(['/listTransaction']);
              } else {
                this.otpService.showPopupError(confirmRes.errorMessage);
              }
            })
          );
        break;
      case 2: // Enrollment done
        this.otpAlreadyDone();
        return NEVER;
        break;
      default: // Not found
        this.otpService.showPopupNotFound();
        this.closeOTPModal();
        this.router.navigate(['/listTransaction']);
        break;
    }
  }

  private initOtp(){
    this.otpService.initOtp(this.orderId)
      .pipe(takeWhile(() => this.alive))
      .subscribe(res => {
        console.log(res.successResponse);
      }, err => {
        this.otpService.showPopupConnectionError('');
      });
  }

  private otpAlreadyDone() {
    this.otpService.showPopupOtpAlreadyDone();
    this.closeOTPModal();
    this.router.navigate(['/listTransaction']);
  }

  private listenCloseConfirmation() {
    this.sCommons.popups()
      .pipe(takeWhile(() => this.alive))
      .subscribe(close => {
        this.closeOTPModal();
    });
  }


  addCharacter(){
    let code: string = this.OTPForm.controls['OTP'].value.toString();
    if(code.length==3 && !this.borrar){
      this.OTPForm.controls['OTP'].setValue(code+"-");
    }else if(code.length==3 && this.borrar){
      this.OTPForm.controls['OTP'].setValue(code.substring(0,2));
    }
  }
}
