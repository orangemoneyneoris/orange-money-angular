import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CommonsService} from "../../../services/commons.service";
import {TranslateService} from "@ngx-translate/core";
import {PopupObj} from "../../../util/PopupObj";
import {PinPad} from "../../../domain/PinPad";
import {CashInService} from "../../../services/cash-in/cash-in.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ListTransactionsClientService} from "../../../services/list-transactions-client/list-transactions-client.service";

@Component({
  selector: 'app-list-transaction-client',
  templateUrl: './list-transaction-client.component.html',
  styleUrls: ['./list-transaction-client.component.css']
})
export class ListTransactionClientComponent implements OnInit {

  constructor(private service: ListTransactionsClientService,
              private route: Router,
              private commonsService: CommonsService,
              private translateService: TranslateService,
              private cashInService: CashInService) {}

  pinNumbers = [
    {viewValue: '0', value: '0'},
    {viewValue: '1', value: '1'},
    {viewValue: '2', value: '2'},
    {viewValue: '3', value: '3'},
    {viewValue: '4', value: '4'},
    {viewValue: '5', value: '5'},
    {viewValue: '6', value: '6'},
    {viewValue: '7', value: '7'},
    {viewValue: '8', value: '8'},
    {viewValue: '9', value: '9'},
    {viewValue: 'R', value: 'R'}
  ];

  pin = '';
  pinPosition = '';
  pinCode = '';

  clientForm = new FormGroup({
    msisdn: new FormControl('', [Validators.required]),
    pin: new FormControl({value:'', disabled: true})
  });

  ngOnInit() {
    //this.loadPINnumbers();
  }

  search () {
    if(this.pin != null && this.pin !== undefined
      && this.pin !== '' && this.clientForm.valid) {
      this.route.navigate(['/listTransactionTable/'
      + this.pin + '/' + this.pinPosition + '/' + this.pinCode + '/' + this.clientForm.get('msisdn').value]);
    }else{
      this.showPopupFormError();
    }
  }

  fillPIN(pos, value){
    this.pin = this.pin + value;
    this.pinPosition = this.pinPosition + pos;
  }

  deleteLastDigit(){
    this.pin = this.pin.substring(0, this.pin.length - 1);
    this.pinPosition = this.pinPosition.substring(0, this.pinPosition.length - 1);
  }

  getFormControl(controlName: string) {
    return this.clientForm.get(controlName) as FormControl;
  }

  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Connection Error';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('listTransations.message.error.list');
    this.commonsService.openGeneralPopup(popupObj);
  }

  private showPopup(title: string, icon: string, message: string){
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = title;
    popupObj.popupIconName = icon;
    popupObj.popupMessage = message;
    this.commonsService.openGeneralPopup(popupObj);
  }

  private showPopupFormError() {
    this.showPopup(
      'Errors on the Form',
      'fas fa-exclamation-triangle',
      'There are still errors on the Form to be fixed or fields still ' +
      'to be filled in. Please, provide such information and correct any errors'
    );
  }


  private loadPINnumbers(){


  }

}
