import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTransactionClientComponent } from './list-transaction-client.component';

describe('ListTransactionClientComponent', () => {
  let component: ListTransactionClientComponent;
  let fixture: ComponentFixture<ListTransactionClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTransactionClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTransactionClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
