import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import {PinPad} from "../../../domain/PinPad";
import {CommonsService} from "../../../services/commons.service";
import {AgencyService} from "../../../services/agency/agency.service";
import {MustMatch} from "../../../util/custom-validators";
import {ChangePINOrders} from "../../../domain/ChangePINOrders";

let required = Validators.required;

@Component({
  selector: 'app-change-pin',
  templateUrl: './change-pin.component.html',
  styleUrls: ['./change-pin.component.css']
})
export class ChangePINComponent implements OnInit {

  changePINForm : FormGroup;

  constructor(private serviceAgency: AgencyService,
      private commonService: CommonsService,
      private formBuilder: FormBuilder) { }
      
  pinCurrent = '';
  pinNew = '';
  pinConfirm = '';
  pinCode = '';

  inputFocus = "";

  changePinOrder : ChangePINOrders;
  msisdnAgency = '0614227930';


  pinPad: PinPad = new PinPad();

  ngOnInit() {
    this.changePINForm = this.formBuilder.group({
      currentPin: new FormControl(null, [required]),
      newPin: new FormControl(null,[required]),
      confirmNewPin: new FormControl(null, [required, Validators.minLength(4)] ),
    }, {
      validator: MustMatch('newPin', 'confirmNewPin')
    });
    this.inputFocus="current";

  }

  setPinPad(pinpad: PinPad) {
    if (pinpad) {
      this.pinPad = pinpad;
    }
  }

  eventClickButton(res){
    //Event to get the button clicked on the pinPad component
    if (res !== null || res !== undefined) {
      let button = res;

      if (this.inputFocus == 'current') {
        if (button == "r") {
          this.pinCurrent = this.pinCurrent.substring(0, this.pinCurrent.length - 1);
        } else {
          this.pinCurrent = this.pinCurrent + button;
        }
        this.changePINForm.get('currentPin').setValue(this.pinCurrent);

      } else if (this.inputFocus == 'new') {
        if (button == "r") {
          this.pinNew = this.pinNew.substring(0, this.pinNew.length - 1);
        } else {
          this.pinNew = this.pinNew + button;
        }
        this.changePINForm.get('newPin').setValue(this.pinNew);

      } else if (this.inputFocus == 'confirm') {
        if (button == "r") {
          this.pinConfirm = this.pinConfirm.substring(0, this.pinConfirm.length - 1);
        } else {
          this.pinConfirm = this.pinConfirm + button;
        }
        this.changePINForm.get('confirmNewPin').setValue(this.pinConfirm);
      }
    }
  }

  //Get the input focus to write or delete
  inputSelected(input:string){
    this.inputFocus = input;
  }

  getFormControl(controlName: string) {
    return this.changePINForm.get(controlName) as FormControl;
  }


  changePin(){
      if (this.changePINForm.valid) {
        //Initialization of the balance order
        //Each time this button is clicked a new order is created
        this.changePinOrder = new ChangePINOrders();

        let errorMessage = "";
        this.commonService.openMixer("Chargement en cours...");

        this.changePinOrder.newPin = this.pinNew;
        this.changePinOrder.currentPin = this.pinCurrent;
        this.changePinOrder.confirmPin = this.pinConfirm;
        this.changePinOrder.pinKey = this.pinPad.privateKey;
        this.changePinOrder.msisdn = this.msisdnAgency;

        this.serviceAgency.createChangePinOrder(this.changePinOrder).subscribe(response => {

          if (response != null) {

            if (response.success) {

              this.changePinOrder = response.successResponse as ChangePINOrders;

              this.getChangePin();

            } else {
              this.commonService.closeMixer();
              errorMessage = response.errorMessage != null ? response.errorMessage : 'Erreur est survenue lors de la création du PIN';
              this.serviceAgency.showPopupErrorService(errorMessage);
            }
          } else {
            this.commonService.closeMixer();

            this.serviceAgency.showPopupConnectionError();
          }

          // Error on call
        }, () => {
          this.commonService.closeMixer();

          this.serviceAgency.showPopupConnectionError();
        });


      }else{
        this.serviceAgency.showFormPinEmpty();
      }

    }

  private getChangePin(){

      let errorMessage = "";

      this.serviceAgency.changePin(this.changePinOrder).subscribe(response => {
        if (response != null && response.success) {
          let message = 'Le PIN a été modifié avec succès';
          this.commonService.closeMixer();
          this.serviceAgency.showPopupSuccessService(message);

        }else{
          this.commonService.closeMixer();
          errorMessage = response.errorMessage != null ? response.errorMessage : 'Modifier l\'erreur PIN';
          this.serviceAgency.showPopupErrorService(errorMessage);
        }

      }, () => {
        this.commonService.closeMixer();

        this.serviceAgency.showPopupConnectionError();
      });

    }

}
