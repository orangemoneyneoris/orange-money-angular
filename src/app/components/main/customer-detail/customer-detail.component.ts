import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CustomerDetailService} from "../../../services/Customer-detail/customer-detail.service";
import {dateIsValid, under18} from "../../../util/custom-validators";
import {PopupObj} from "../../../util/PopupObj";
import {CommonsService} from "../../../services/commons.service";
import {Client} from "../../../domain/Client";
import {CustomerAddress} from "../../../domain/CustomerAddress";
import {Wallet} from "../../../domain/Wallet";

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {

  subscriptionForm = new FormGroup({
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]),
    nationality: new FormControl({value: '', disabled:true}, [Validators.required]),
    idType: new FormControl('', [Validators.required]),
    id: new FormControl('', [Validators.required]),
    expirationDate: new FormControl({value: '', disabled: true}, [Validators.required, dateIsValid()]),
    gender: new FormControl({value: '', disabled:true}, [Validators.required]),
    name: new FormControl({value: '', disabled:true}, [Validators.required]),
    surname: new FormControl({value: '', disabled:true}, [Validators.required]),
    birthDate: new FormControl({value: '', disabled:true}, [Validators.required, under18(), dateIsValid()]),
    accountLevel: new FormControl({value:'', disabled: true}, [Validators.required]),
    email: new FormControl({value: '', disabled:true}, [Validators.email]),
    sponsorCode: new FormControl({value: '', disabled:true}),
    profession: new FormControl({value: '', disabled:true}, [Validators.required]),
    accountOpeningReasons: new FormControl({value: '', disabled: true}, [Validators.required]),
    country: new FormControl({value: '', disabled: true}),
    province: new FormControl({value: '', disabled: true}),
    ville: new FormControl({value: '', disabled: true}),
    street: new FormControl({value: '', disabled: true}),
    codePostal: new FormControl({value: '', disabled: true}),
    address: new FormControl({value: '', disabled: true})
  });

  phoneNumber;
  nationality;
  idType;
  id;
  expDate;
  gender;
  name;
  surname;
  bDate;
  accountLevel;
  country;
  province;
  ville;
  street;
  codePostal;
  address;
  email;
  sponsorCode;
  profession;
  accountOpening;

  numRib;
  adressText;

  wallet: Wallet;
  client: Client;
  currentPage: string;

   constructor(private service: CustomerDetailService,
              private actRoute: ActivatedRoute,
              private commonsService: CommonsService,
              private route: Router) {}

  ngOnInit() {
    this.commonsService.openMixer("Consultation en cours...");

    let ordrId = this.actRoute.snapshot.paramMap.get('ordrId');
    let typeId = this.actRoute.snapshot.paramMap.get('typeId');
    this.currentPage = this.actRoute.snapshot.paramMap.get('currentPage');

    this.fillCustomerInfo(ordrId, typeId);
  }

  fillCustomerInfo(ordrId, typeId){

    this.service.getCustomerDetailInfo(ordrId, typeId)
      .subscribe(order => {

        if (order != null) {

          let customer = order.client;
           this.client=customer;
           this.wallet=order.wallet;
          this.name = customer.name;
          this.phoneNumber = customer.msisdn;
          this.nationality = customer.nationality != null ? customer.nationality.name : '';
          this.idType = customer.identityType != null ? customer.identityType.name : '';
          this.id = customer.identificationId;
          this.gender = customer.gender != null ? customer.gender.name : '';
          this.surname = customer.surname;
          this.bDate = customer.birthDate;
          this.email = customer.email;
          this.profession = customer.profession != null ? customer.profession.name : '';
          this.numRib = order.ribCode;


          this.getClientAddress(customer.address);

          this.fillWalletinfo(order.wallet);

          this.resetControls();

          this.commonsService.closeMixer();

        }else{
          this.commonsService.closeMixer();
          this.showPopupConnectionError();
        }

        // Error on call
      }, () => {
        this.commonsService.closeMixer();
        this.showPopupConnectionError();
      });
  }

  private resetControls() {
    for (let key in this.subscriptionForm.controls){
      if (this.subscriptionForm.controls.hasOwnProperty(key)) {
        this.subscriptionForm.controls[key].disable();
      }
    }
  }

  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = 'Connection Error';
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = 'Impossible de se connecter au serveur en essayant de récupérer les informations du client';
    this.commonsService.openGeneralPopup(popupObj);
  }

  private getClientAddress (address: CustomerAddress){

    if(address != null){
      if(address.entCountry != null){
        this.country = address.entCountry.name;
      }

      if(address.entVille != null){
        this.ville = address.entVille;
      }

      if(address.entCodePostal != null){
        this.codePostal = address.entCodePostal;
      }

      if(address.address != null){
        this.address = address.address;
      }
    }
  }

  private fillClientInfo(client: Client){
    this.name = client.name;
    this.phoneNumber = client.msisdn;
    this.nationality = client.nationality.name;
    this.idType = client.identityType.name;
    this.id = client.identificationId;
    this.gender = client.gender.name;
    this.surname = client.surname;
    this.bDate = client.birthDate;
    this.email = client.email;
    this.profession = client.profession.name;
  }

  private fillWalletinfo(wallet: Wallet){
    if(wallet != null) {
      this.expDate = wallet.expirationDate;
      this.accountLevel = wallet.accountLevel;
      this.sponsorCode = wallet.sponsorCode;
    }
  }

  get accountStatus(){
    if (this.wallet !== undefined && this.wallet.statusAccount !== undefined) {
      return "Actif";
    }

  }

  get enrolStatus(){
    if (this.wallet !== undefined && this.wallet.enrolementWallet !== undefined) {
      return this.wallet.enrolementWallet;
    }
  }


  get post(){
    if (this.client !== undefined && this.client.address !== undefined) {
      let address = (this.client.address.entVille != null ? this.client.address.entVille :"");
      address = address + " "+ (this.client.address.entCodePostal != null ? this.client.address.entCodePostal :"");
      address = address + " "+(this.client.address.entCountry != null ? this.client.address.entCountry.name :"");
      return address;
    }
  }

  backbtn(){
    this.route.navigate(['/listTransaction/' + this.currentPage]);
  }

}
