import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NEVER, Subscription, timer} from "rxjs";
import {NgbModal, NgbModalOptions, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {IdentificationType} from "../../../domain/IdentificationType";
import {environment} from "../../../../environments/environment";
import {CashOrders} from "../../../domain/CashOrders";
import {Client} from "../../../domain/Client";
import {CommonsService} from "../../../services/commons.service";
import {SubscriptionOrderService} from "../../../services/subscription-order.service";
import {ActivatedRoute, Router} from "@angular/router";
import {JasperService} from "../../../services/jasper.service";
import {PinPad} from "../../../domain/PinPad";
import {CashOutService} from "../../../services/cash-out/cash-out.service";
import {catchError, switchMap, takeWhile} from "rxjs/operators";
import {JSONResponse} from "../../../domain/JSONResponse";
import {CASHOUT} from "../../../util/Constants";
import {TranslateService} from "@ngx-translate/core";
import {startWith0607} from "../../../util/custom-validators";

@Component({
  selector: 'app-cash-out',
  templateUrl: './cash-out.component.html',
  styleUrls: ['./cash-out.component.css']
})

export class CashOutComponent implements OnInit, OnDestroy {

  @ViewChild('mixerCash') mixerModalCash: ElementRef;

  cashoutForm: FormGroup;
  identityTypes: IdentificationType[] = [];

  // TODO remove, only for testing
  isLocalEnv = environment.local;

  pin = '';
  pinPad: PinPad = new PinPad();
  pinFocus = true;
  verifyOk = false;

  enableCloseBtn: boolean = false;
  orderIsClosed: boolean = false;
  enableVerifyBtn: boolean = false;
  notRemoteOrder: boolean = true;
  orderCancel: boolean =false;

  private mixerSubscriptionCash: Subscription;
  private mixerModalRefCash: NgbModalRef;
  private enableRefresh: boolean = false;
  private cashOrder: CashOrders = new CashOrders();
  private client: Client;

  private alive: boolean = true;


  constructor(private cashOutService: CashOutService,
              private commonService: CommonsService,
              private subscriptionOrderService: SubscriptionOrderService,
              private activatedRoute: ActivatedRoute,
              private route: Router,
              private modalService: NgbModal,
              private sJasper: JasperService,
              private translateService: TranslateService) { }


  ngOnInit() {
    this.initForm();
    this.subscribe2Mixers();
    this.enableCloseBtn = false;
    this.verifyOk = false;

    let amount = this.activatedRoute.snapshot.paramMap.get('amount');
    if(amount != null){
      this.initRemoteScreenParameters();
    }else {

      let ordrId = this.activatedRoute.snapshot.paramMap.get('ordrId');
      if (ordrId != null) {
        this.loadOrderFromDatabase(ordrId);
      } else {
        this.fillInFormWithSessionCustomer();
      }
    }

  }

  getFormControl(controlName: string) {
    return this.cashoutForm.get(controlName) as FormControl;
  }

  setPinPad(pinpad: PinPad) {
    if (pinpad) {
      this.pinPad = pinpad;
    }
  }

  eventClickButton(res){
    //Event to get the button clicked on the pinPad component
    if (res !== null || res !== undefined) {
      let button = res;

      if (button == "r") {
        this.pin = this.pin.substring(0, this.pin.length - 1);
      } else {
        this.pin = this.pin + button;
      }

      this.cashoutForm.get('pin').setValue(this.pin);
    }

  }

  pinInputSelected(display:boolean){
    this.pinFocus = display;
  }

  refresh(showPopup: boolean = true) {
    this.cashOutService.checkCallBack(this.cashOrder)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(response => {
        if (response != null && response.success && response.successResponse) {
          this.handleCheckCallbackResponse(response);
        }else if(response.code && response.code ==400){
          this.commonService.closeMixerCash();
          this.cashOutService.showPopupPosIntegrationFail();
          this.enableRefresh = false;
          this.orderIsClosed = false;
        } else if (showPopup){
          // callback pending
          this.cashOutService.showPopupCashOutInProgress();
        }
      }, () => {
        if (showPopup) {
          this.cashOutService.showPopupConnectionError();
        }
      });
  }

  save(){
    this.cashOutService.checkCallBack(this.cashOrder)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(response => {
        if (response != null && response.success && response.successResponse) {
          this.handleCheckCallbackResponse(response);
        } else {
          this.navigateToOrderList();
        }
      }, checkCallBackErr => {
        this.navigateToOrderList();
    });
  }

  verify() {
    this.enableVerifyBtn = false;
    this.cashOutService.cashOutVerify(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      if (res.success) {
        if (res.errorMessage) {
          // Verify Limit reached
          this.cashOutService.showPopupVerifyLimitReached();
          this.enableCloseButton();
        } else {
          // Cashout OK
          this.verifyOk = true;
          this.commonService.closeMixerCash();
          this.cashOutService.showPopupCashOUTComplete();
          this.enableCloseButton();
        }
      } else {
        if (res.errorMessage === 'WAITTING') {
          this.cashOutService.showPopupCashOutInProgress();
        } else {
          if (res.successResponse) {
            this.cashOutService.showPopupVerifyFailed(res.successResponse);
          } else {
            this.cashOutService.showPopupCashOutFailed();
          }
        }
        this.enableVerifyTimer();
      }
    }, verifyErr => {
      this.cashOutService.showPopupCashOutFailed();
    });
  }

  close(){
    //update order status --> Annulé
    this.cashOutService.annulCashOrder(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      if(res){
        this.commonService.closeMixerCash();
        this.navigateToOrderList();
      }else{
        this.commonService.closeMixerCash();
        this.cashOutService.showPopupConnectionError();
      }
    });

  }

  cancel(){
    //update order status --> Rejeté
    this.cashOutService.rejectCashOrder(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      if(res){
        this.commonService.closeMixerCash();
        this.navigateToOrderList();
      }else{
        this.commonService.closeMixerCash();
        this.cashOutService.showPopupConnectionError();
      }
    });
  }

  generateContract() {
    this.commonService.openMixer(this.translateService.instant('cashout.popup.pendingContract'));

    this.sJasper.getCashOutPDF(this.cashOrder).pipe(
      takeWhile(() => this.alive)
    ).subscribe( pdfResponse => {
      this.subscriptionOrderService.contractGenerated = true;
      this.commonService.closeMixer();
      this.serveFile2User(pdfResponse);
    }, err => {
      this.commonService.closeMixer();
      this.subscriptionOrderService.contractGenerated = false;
      this.cashOutService.showPopupContractError();
    });

  }

  execute()  {
    this.pinFocus = true;
    this.disableFields(false);
    if (this.isFormValid()) {
      this.commonService.openMixer(this.translateService.instant('cashout.popup.pending'));
      this.initCashOrder();

      this.cashOutService.createOrder(this.cashOrder).pipe(
        takeWhile( () => this.alive),
        catchError((createOrderErr) => {
          this.commonService.closeMixer();
          this.cashOutService.showPopupConnectionError();
          return NEVER;
        }),
        switchMap(createOrderRes => {
          return this.handleCreateOrder(createOrderRes);
        })
      ).subscribe((cashOutInitRes) => {
        this.handleCashoutInit(cashOutInitRes);
      } ,cashOutInitErr => {
        this.initCashPopup();
      });
    } else{
      if(this.cashOrder.ordOrder != null
        && (this.cashOrder.ordOrder.ordSubStatus.statusId === CASHOUT.NON_EDITABLE_MODE.POS_KO)) {
        this.disableFields(true);
        this.cashoutForm.controls['pin'].enable();
      }
      this.setFormAsTouched(true);

    }
  }

  resetForm() {
    this.cashoutForm.reset();
    this.cashOutService.removeSessionOrder();
    this.disableFields(false);
    this.pin = '';
    this.pinFocus = true;
    this.orderIsClosed = false;
    this.cashOrder = new CashOrders();
    this.enableCloseBtn = false;
  }

  back() {
    if(!this.notRemoteOrder) {
      this.route.navigate(['/listTransactionTable']);
    } else {
      if (this.cashOrder != null && this.cashOrder !== undefined
        && this.cashOrder.ordOrder != null) {
        this.route.navigate(['/listTransaction']);
      } else {
        this.route.navigate(['/resercher']);
      }
    }
  }

  ngOnDestroy(): void {
    this.enableRefresh = false;
    this.alive = false;
    this.cashOutService.removeSessionOrder();
    this.subscriptionOrderService.removeSessionOrder();
    if(localStorage.getItem("lastRequest")!=null) {
      this.commonService.closeMixer();
      this.commonService.closeMixerCash();
      this.commonService.closePopup();
      this.commonService.closeCashPopup();
    }
  }

  // TODO remove, only for testing
  callbackConfirmation() {
    this.cashOutService.callBackConfirmation(this.cashOrder.ordOrder.ordrId)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(res => {
        alert('callbackconfirmation ' + res.callBackCode + ' ' + res.callBackMessage);
    });
  }

  // TODO remove mock, only for testing
  fillForm(){
    this.cashoutForm.get('phone').setValue('0666666666');
    this.cashoutForm.get('amount').setValue('100');
    this.cashoutForm.get('pin').setValue('1994');

    this.pin = '1994';
  }

  private initAutoRefresh() {
    this.enableRefresh = true;
    const refreshTime = this.cashOutService.getRefreshDelay();
    timer(refreshTime, refreshTime).pipe(
      takeWhile(() => this.enableRefresh)
    ).subscribe((val) => {
      console.log('Checking callback ', val);
      this.refresh(false);
    });

  }

  private initForm() {
    this.cashoutForm = new FormGroup({
      pin: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), startWith0607()]),
      amount: new FormControl('', [Validators.required, Validators.pattern('^[1-9]\\d*(\\.\\d+)?$')]),
    });
  }

  private initRemoteScreenParameters(){
    let mobileNumber = this.activatedRoute.snapshot.paramMap.get('mobileNumber');
    let amount = this.activatedRoute.snapshot.paramMap.get('amount');

    this.cashoutForm.get('amount').setValue(amount);
    this.cashoutForm.get('phone').setValue(mobileNumber);

    this.orderIsClosed = true;
    this.notRemoteOrder = false;

    //this.cashoutForm.disable();
  }

  private openMixer(MixerModal){
    const ngbModalOptions: NgbModalOptions = {
      centered: true,
      //To avoid user closing the Modal
      keyboard: false,
      backdrop : 'static'
    };

    this.mixerModalRefCash = this.modalService.open(MixerModal, ngbModalOptions);
  }

  private enableVerifyTimer() {
    this.cashOutService.getVerifyDelay()
      .subscribe(response => {

        let timeout = response;

        setTimeout(() => {
          this.enableVerifyBtn = true;
        }, timeout);

      }, () => {
        const timeout = 60000;
        setTimeout(() => {
          this.enableVerifyBtn = true;
        }, timeout);
      });

  }

  private handleCheckCallbackResponse(callbackResponse: JSONResponse) {
    this.cashOrder = callbackResponse.successResponse as CashOrders;
    this.cashOutService.setSessionOrder(this.cashOrder);
    this.enableRefresh = false;
    this.orderIsClosed = true;

    if (this.cashOrder.externalIDCB === CASHOUT.CALLBACK_STATUS.OK) {
      this.commonService.closeMixerCash();
      this.cashOutService.showPopupCashOUTComplete();
    } else if (this.cashOrder.externalIDCB === CASHOUT.CALLBACK_STATUS.TIMEOUT) {
      this.enableCloseButton();
      this.cashOutService.showPopupCallbackTimeout();
    } else {
      this.enableCloseButton();
      this.cashOutService.showPopupCashOutFailed();
    }
  }

  private enableCloseButton() {
    this.enableCloseBtn = true;
    this.orderIsClosed = true;
    this.enableRefresh = false;
    this.enableVerifyBtn = false;
  }

  private navigateToOrderList() {
    this.commonService.closeMixerCash();
    this.route.navigate(['listTransaction']);
  }

  private loadOrderFromDatabase(ordrId) {
    this.commonService.openMixer(this.translateService.instant('cashout.popup.pending'));

    this.cashOutService.getCustomerDetailInfo(ordrId)
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(order => {
        if (order != null) {
          this.cashOrder = order;
          this.client = this.cashOrder.client;
          this.cashOutService.setSessionOrder(this.cashOrder);

          if(this.client && this.client.msisdn)
            this.cashoutForm.get('phone').setValue(this.client.msisdn);

          if (this.cashOrder.amount) {
            this.cashoutForm.get('amount').setValue(this.cashOrder.amount);
          }
          //this.fillInFormWithSessionCustomer();

          if(this.cashOrder.ordOrder != null
            && (this.cashOrder.ordOrder.ordStatus.statusId === 3 || this.cashOrder.ordOrder.ordStatus.statusId === 5 || this.cashOrder.ordOrder.ordStatus.statusId === 4)) {
                  this.orderIsClosed = true;
               if(this.cashOrder.ordOrder.ordStatus.statusId === 5 || this.cashOrder.ordOrder.ordStatus.statusId === 4){
                 this.orderCancel = true;
               }
               else{
                 this.orderCancel = false;

               }

            this.disableFields(true);
          }
          if(this.cashOrder.ordOrder != null
            && (this.cashOrder.ordOrder.ordSubStatus.statusId === CASHOUT.NON_EDITABLE_MODE.POS_KO
              || this.cashOrder.ordOrder.ordSubStatus.statusId === CASHOUT.NON_EDITABLE_MODE.CB)) {
            this.disableFields(true);
            this.cashoutForm.controls['pin'].enable();
          }

          this.commonService.closeMixer();
        } else{
          this.commonService.closeMixer();
          this.cashOutService.showPopupConnectionError();
        }
      }, () => {
        this.commonService.closeMixer();
        this.cashOutService.showPopupConnectionError();
      });
  }

  private fillInFormWithSessionCustomer() {
    let sessionCustomer = this.subscriptionOrderService.getSessionOrder() != null ?
      this.subscriptionOrderService.getSessionOrder().client : window.history.state.client as Client;

    if (!sessionCustomer) {
      this.client = this.cashOutService.getSessionOrder() != null ? this.cashOutService.getSessionOrder().client : null;
    } else {
      this.client = sessionCustomer;
    }

    if(this.client) {
      this.cashoutForm.get('phone').setValue(this.client.msisdn);

      if (this.cashOrder.amount) {
        this.cashoutForm.get('amount').setValue(this.cashOrder.amount);
      }

      this.cashOrder.client = this.client;
      this.disableFields(false);
    }
  }

  private disableFields(disable: boolean) {
    for (let key in this.cashoutForm.controls){
      if (this.cashoutForm.controls.hasOwnProperty(key)) {
        disable ? this.cashoutForm.controls[key].disable() : this.cashoutForm.controls[key].enable();
      }
    }
    disable ? this.cashoutForm.controls['pin'].disable() : this.cashoutForm.controls['pin'].enable();
  }

  private subscribe2Mixers() {
    this.mixerSubscriptionCash = this.commonService.mixersCash()
      .pipe(
        takeWhile(() => this.alive)
      ).subscribe(message => {
        if (message !== undefined) {

          if (this.mixerModalRefCash !== undefined) {
            this.mixerModalRefCash.close();
            this.mixerModalRefCash = undefined;
          }

          this.openMixer(this.mixerModalCash);

        } else {
          if (this.mixerModalRefCash !== undefined) {
            this.mixerModalRefCash.close();
            this.mixerModalRefCash = undefined;
          }
        }
    });
  }

  private serveFile2User(response) {
    let newBlob = new Blob([response.body], {type: "application/pdf"});

    // IE compatibility
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(newBlob);
      return;
    }

    const data = window.URL.createObjectURL(newBlob);
    let filename = response.headers.get("filename");
    let link = this.createLinkAndClick(data, filename);
    this.removeLink(data, link);
  }


  private removeLink(data, link) {
    setTimeout(function () {
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
  }

  private createLinkAndClick(data, filename) {
    let link = document.createElement('a');
    link.href = data;
    link.download = filename;
    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
    return link;
  }

  private isFormValid() {
    return this.pin && this.pin !== '' && this.cashoutForm.valid;
  }

  private initCashOrder() {
    if(!this.cashOrder.client){
      let client = new Client();
      this.cashOrder.client = client;
    }
    this.cashOrder.client.msisdn = this.cashoutForm.controls['phone'].value;
    this.cashOrder.amount = this.cashoutForm.controls['amount'].value;
    this.cashOrder.msisdn = this.cashOutService.getUserId();
    this.cashOrder.pin = this.pin;
    this.cashOrder.pinCode = this.pinPad.privateKey;
  }

  private handleCashoutInit(initResponse: JSONResponse) {
    console.log(initResponse)
    if (initResponse && initResponse.code) {
      switch(initResponse.code) {
        case 200:
          this.cashOrder = initResponse.successResponse as CashOrders;
          this.cashOutService.setSessionOrder(this.cashOrder);
          if(this.cashOrder.ordOrder.ordStatus.statusId ===3){
            //Pos Integration relaunch OK
            this.commonService.closeMixer();
            this.cashOutService.showPopupCashOUTComplete();
            this.orderIsClosed = true;
          }else {
            // cashoutinit OK -> wait for callback notification
            this.initCashPopup();
          }
          break;
        case 500:
          // cashoutinit KO unknown error -> wait for callback notification
          this.initCashPopup();
          break;
        case 400:
          //POS integration fail
          this.cashOutService.showPopupPosIntegrationFail();
          this.commonService.closeMixer();
          this.enableRefresh = false;
          this.disableFields(true);
          break;
        default:
          // cashoutinit KO known error -> enable edit form
          this.commonService.closeMixer();
          this.cashOutService.showPopupKnowError(initResponse.errorMessage);
          this.disableFields(false);
          break;
      }
    } else {
      // cashoutinit KO unknown error -> wait for callback notification
      this.initCashPopup();
    }
  }

  // initCashOut OK, Timeout or unknown error
  private initCashPopup() {
    this.commonService.closeMixer();
    this.commonService.openMixerCash(this.translateService.instant('cashout.modal.title'));
    this.initAutoRefresh();
    this.enableVerifyTimer();
  }

  private handleCreateOrder(orderResponse: JSONResponse) {
    if (orderResponse != null && orderResponse.success) {
      this.cashOrder = orderResponse.successResponse as CashOrders;
      this.cashOutService.setSessionOrder(this.cashOrder);
      this.disableFields(true);
      return this.cashOutService.cashOutInit(this.cashOrder);
    } else {
      this.commonService.closePopup();
      this.commonService.closeMixer();
      this.cashOutService.showPopupKnowError(orderResponse.errorMessage);
      return NEVER;
    }
  }

  private setFormAsTouched(touched: boolean) {
    for (const key in this.cashoutForm.controls) {
      if (this.cashoutForm.controls.hasOwnProperty(key)) {
        if (touched) {
          this.cashoutForm.controls[key].markAsTouched();
        } else {
          this.cashoutForm.controls[key].markAsUntouched();
        }
      }
    }
  }

}
