import {Component, OnInit, ViewChild} from '@angular/core';
import {ListTransactionsClientService} from "../../../services/list-transactions-client/list-transactions-client.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CommonsService} from "../../../services/commons.service";
import {TranslateService} from "@ngx-translate/core";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {PopupObj} from "../../../util/PopupObj";
import {TransactionHistory} from "../../../domain/TransactionHistory";
import {OrdOrderList} from "../../../domain/OrdOrderList";

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css']
})
export class TransactionHistoryComponent implements OnInit {

  dataSource;

  constructor(private service: ListTransactionsClientService,
              private route: Router,
              private activatedRoute: ActivatedRoute,
              private commonsService: CommonsService,
              private translateService: TranslateService) {}

  displayedColumns: string[] = ['updatedBy', 'ordStatus', 'ordSubStatus', 'ordSubStatus2', 'updatedDate'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.commonsService.openMixer('Consultation en cours...');

    let ordrId = this.activatedRoute.snapshot.paramMap.get('ordrId');

    this.service.getTransactionHitoryList(Number(ordrId))
      .subscribe(response => {

        if (response != null
          && response.success
          && response.successResponse != null) {

          this.dataSource = new MatTableDataSource<OrdOrderList>(response.successResponse as OrdOrderList[]);

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

        } else {
          this.commonsService.closeMixer();

          this.showPopupConnectionError();
        }

        this.commonsService.closeMixer();

        // Error on call
      }, error1 => {
        this.commonsService.closeMixer();

        this.showPopupConnectionError();
      });


  }


  private showPopupConnectionError() {
    const popupObj: PopupObj = new PopupObj();
    popupObj.title = this.translateService.instant('listTransations.message.error.list.title');
    popupObj.popupIconName = 'fas fa-exclamation-triangle';
    popupObj.popupMessage = this.translateService.instant('listTransations.message.error.list');
    this.commonsService.openGeneralPopup(popupObj);
  }


}
