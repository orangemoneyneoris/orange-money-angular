import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-validation-errors',
  templateUrl: './validation-errors.component.html',
  styleUrls: ['./validation-errors.component.css']
})
export class ValidationErrorsComponent implements OnInit {

  @Input() control: FormControl;

  @Input() focusOut: Boolean;

  constructor() {}

  ngOnInit() {
  }

  controlIsInvalid() {
    if(this.focusOut!=null){
      if(this.focusOut){
        return this.control.invalid && (this.control.dirty || this.control.touched);
      }
    }else{
      return this.control.invalid && (this.control.dirty || this.control.touched);
    }
  }

}
