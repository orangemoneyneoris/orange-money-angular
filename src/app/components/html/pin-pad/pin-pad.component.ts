import {Component, OnInit, Output, NgModule, HostListener, EventEmitter} from '@angular/core';
import {PinPad} from "../../../domain/PinPad";
import {AgencyService} from "../../../services/agency/agency.service";
import {CommonsService} from "../../../services/commons.service";

@Component({
  selector: 'app-pin-pad',
  templateUrl: './pin-pad.component.html',
  styleUrls: ['./pin-pad.component.css']
})
export class PinPadComponent implements OnInit {

  constructor(private serviceAgency: AgencyService,
              private commonService: CommonsService) { }

  @Output() emitEvent:EventEmitter<PinPad> = new EventEmitter<PinPad>();
  @Output() clickButton:EventEmitter<string> = new EventEmitter<string>();

  em = "";
  pinPad: PinPad = new PinPad();
  pinPosition = [];
  buttonCliked = "";
  servicePinPadOk = false;

  ngOnInit() {
    this.loadPINnumbers();
  }

  public returnPinPad(): PinPad{
      let fResponse = this.pinPad;
      this.emitEvent.emit(fResponse);
      return fResponse;
  }

  public returnButton(): string{
    let fResponse = this.buttonCliked;
    this.clickButton.emit(fResponse);
    return fResponse;
  }

  pinNumbers = [
    {viewValue: '-', value: '0'},
    {viewValue: '-', value: '1'},
    {viewValue: '-', value: '2'},
    {viewValue: '-', value: '3'},
    {viewValue: '-', value: '4'},
    {viewValue: '-', value: '5'},
    {viewValue: '-', value: '6'},
    {viewValue: '-', value: '7'},
    {viewValue: '-', value: '8'},
    {viewValue: '-', value: '9'},
    {viewValue: '-', value: '10'},
    {viewValue: '-', value: '11'},
    {viewValue: 'R', value: 'R'}
  ];

  private loadPINnumbers(){

    this.serviceAgency.getPinPad()
      .subscribe(response => {

        if (response != null && response.success) {
          this.pinPad = response.successResponse as PinPad;
          if(this.pinPad.seq !=null) {
            this.servicePinPadOk=true;
            let seq = this.pinPad.seq;
            //let pinPositions = pinPad.positions;
            this.em = this.pinPad.privateKey;

            //this.pinCode = pinPad.privateKey;

            this.pinNumbers = [
              {viewValue: seq[0].toString(), value: '0'},
              {viewValue: seq[1].toString(), value: '1'},
              {viewValue: seq[2].toString(), value: '2'},
              {viewValue: seq[3].toString(), value: '3'},
              {viewValue: seq[4].toString(), value: '4'},
              {viewValue: seq[5].toString(), value: '5'},
              {viewValue: seq[6].toString(), value: '6'},
              {viewValue: seq[7].toString(), value: '7'},
              {viewValue: seq[8].toString(), value: '8'},
              {viewValue: seq[9].toString(), value: '9'},
              {viewValue: seq[10].toString(), value: '10'},
              {viewValue: seq[11].toString(), value: '11'}
            ];

            this.returnPinPad();
          }else{
            this.servicePinPadOk=false;
            this.commonService.closeMixerCash();
            this.serviceAgency.showPopupErrorService("Réponse incorrecte, le champ seq reçu est vide");
          }

        }else{
          this.servicePinPadOk=false;
          if(localStorage.getItem("lastRequest")!=null) {
            this.commonService.closeMixerCash();
            this.serviceAgency.showPopupErrorService("Impossible de saisir le PIN Agent suite à un problème technique");
          }
        }
      }, () => {
        this.servicePinPadOk=false;
        this.commonService.closeMixerCash();
        this.serviceAgency.showPopupConnectionError();
      });
  }


  fillPIN(position){
    if(this.servicePinPadOk) {
      this.pinPosition.push(position);
      this.pinPad.pinPosition = this.pinPosition;
      this.buttonCliked = "";
      this.buttonCliked = position;
      this.returnButton();
    }

  }

  deleteLastDigit(){
    if(this.servicePinPadOk) {
      this.pinPosition.pop();
      this.pinPad.pinPosition = this.pinPosition;
      this.buttonCliked = "";
      this.buttonCliked = "r";
      this.returnButton();
    }
  }

}
