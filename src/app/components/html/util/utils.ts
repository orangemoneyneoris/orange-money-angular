export interface ComboOption {
  value: string;
  viewValue: string;
}
