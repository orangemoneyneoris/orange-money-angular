import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-orange-title',
  templateUrl: './orange-title.component.html',
  styleUrls: ['./orange-title.component.css']
})
export class OrangeTitleComponent implements OnInit {
  @Input() title;
  @Input() iconClassName;

  constructor() { }

  ngOnInit() {
  }

}
