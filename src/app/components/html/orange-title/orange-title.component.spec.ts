import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrangeTitleComponent } from './orange-title.component';

describe('OrangeTitleComponent', () => {
  let component: OrangeTitleComponent;
  let fixture: ComponentFixture<OrangeTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrangeTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrangeTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
