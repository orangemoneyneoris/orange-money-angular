import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchClientComponent} from './components/main/search-client/search-client.component';
import {NewSubscriptionComponent} from './components/main/new-subscription/new-subscription.component';
import {WalletPreviewComponent} from './components/main/new-subscription/wallet-preview/wallet-preview.component';
import {WalletUpgradePreviewComponent} from './components/main/upgrade-subscription/wallet-upgrade-preview/wallet-upgrade-preview.component';
import {ListTasnsactionComponent} from './components/main/list-transaction/list-tasnsaction.component';
import {CustomerDetailComponent} from './components/main/customer-detail/customer-detail.component';
import {CashOutComponent} from './components/main/cash-out/cash-out.component';
import {CashInComponent} from './components/main/cash-in/cash-in.component';
import { AgencyBalanceComponent } from './components/main/agency-balance/agency-balance.component';
import {ListTransactionTableComponent} from './components/main/list-transaction-table/list-transaction-table.component';
import {ChangePINComponent} from './components/main/change-pin/change-pin.component';
import {VistaDocumentsComponent } from './components/main/vista-documents/vista-documents.component';
import {OtpValidationComponent} from './components/main/otp-validation/otp-validation.component';
import {DefaultWalletComponent} from './components/main/default-wallet/default-wallet.component';
import {UpgradeSubscriptionComponent} from './components/main/upgrade-subscription/upgrade-subscription.component';
import {TransactionHistoryComponent} from './components/main/transaction-history/transaction-history.component';
import {LoginComponent} from './components/main/login/login.component';
import {PageErrorsComponent} from './components/main/page-errors/notAllowed/page-errors.component';
import {NgxPermissionsGuard} from 'ngx-permissions';
import {NotFoundComponent} from './components/main/page-errors/not-found/not-found.component';

import {PROFILES} from './util/Constants';
import {ListTransactionCashComponent} from './components/main/list-transaction-cash/list-transaction-cash.component';
import {ExpiredDateComponent} from './components/main/expired-date/expired-date.component';
import { PosRedirectComponent } from './pos-redirect/pos-redirect.component';



// Be afraid that this urls should not collide with any Spring controller endpoint defined url
const routes: Routes = [
    {path: '', component: LoginComponent, canActivate: [NgxPermissionsGuard],
    data: {
    permissions: {
      except: ['ALLOW_ADFS']
    }
  }},
    {path: 'login', component: LoginComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
      permissions: {
        except: ['ALLOW_ADFS'],
        redirectTo: '/errorNotAllowed'
      }
    }
    },
    {path: 'login/:orderID', component: LoginComponent,
     canActivate: [NgxPermissionsGuard],
      data: {
      permissions: {
        except: ['ALLOW_ADFS'],
        redirectTo: '/errorNotAllowed'
      }
    }
  },
  {path: 'login/:orderID/:msisdnPOS', component: LoginComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
    permissions: {
      except: ['ALLOW_ADFS'],
        redirectTo: '/errorNotAllowed'
      }
    }
    },
    { path: 'posRedirect/:orderID', component: PosRedirectComponent },
    { path: 'posRedirect/:orderID/:msisdnPOS', component: PosRedirectComponent },

    {path: 'resercher', component: SearchClientComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'subscription', component: NewSubscriptionComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'subscription/:step', component: NewSubscriptionComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'wallet-preview', component: WalletPreviewComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'wallet-upgrade-preview', component: WalletUpgradePreviewComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'listTransaction', component: ListTasnsactionComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions:
          [
            PROFILES.POS.VENDEUR,
            PROFILES.POS.VENDEUR_OS,
            PROFILES.POS.CHEF_AGENCE_OS,
            PROFILES.MONEY.VENDEUR,
            PROFILES.MONEY.VENDEUR_OS,
            PROFILES.MONEY.CHEF_AGENCE_OS,
            PROFILES.MONEY.SUPERVISEUR,
            PROFILES.MONEY.HEAD_AGENCE,
          ],
          redirectTo: '/errorNotAllowed'
        }
      },
  {path: 'listTransaction/:currentPage', component: ListTasnsactionComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions:
        [
          PROFILES.POS.VENDEUR,
          PROFILES.POS.VENDEUR_OS,
          PROFILES.POS.CHEF_AGENCE_OS,
          PROFILES.MONEY.VENDEUR,
          PROFILES.MONEY.VENDEUR_OS,
          PROFILES.MONEY.CHEF_AGENCE_OS,
          PROFILES.MONEY.SUPERVISEUR,
          PROFILES.MONEY.HEAD_AGENCE,
        ],
      redirectTo: '/errorNotAllowed'
    }
  },
    {path: 'subscription/:orderId', component: NewSubscriptionComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
   {path: 'detail/:ordrId', component: CustomerDetailComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'cashin', component: CashInComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'cashin/:ordrId', component: CashInComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'cashin/:transactionID/:amount/:serviceType/:mobileNumber/:entryType/:statusTransaction', component: CashInComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'cashout', component: CashOutComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'cashout/:ordrId', component: CashOutComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'cashout/:transactionID/:amount/:serviceType/:mobileNumber/:entryType/:statusTransaction', component: CashOutComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'detail/:ordrId/:typeId/:currentPage', component: CustomerDetailComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: [
            PROFILES.POS.VENDEUR,
            PROFILES.POS.VENDEUR_OS,
            PROFILES.POS.CHEF_AGENCE_OS,
            PROFILES.MONEY.VENDEUR,
            PROFILES.MONEY.VENDEUR_OS,
            PROFILES.MONEY.CHEF_AGENCE_OS,
            PROFILES.MONEY.SUPERVISEUR,
            PROFILES.MONEY.HEAD_AGENCE,
          ],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'agencyBalance', component: AgencyBalanceComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'listTransactionTable' , component: ListTransactionTableComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions:
          [
            PROFILES.POS.VENDEUR,
            PROFILES.POS.VENDEUR_OS,
            PROFILES.POS.CHEF_AGENCE_OS,
            PROFILES.MONEY.VENDEUR,
            PROFILES.MONEY.VENDEUR_OS,
            PROFILES.MONEY.CHEF_AGENCE_OS,
            PROFILES.MONEY.SUPERVISEUR,
            PROFILES.MONEY.HEAD_AGENCE,
          ],
          redirectTo: '/errorNotAllowed'

      }
      },
    {path: 'changePIN', component: ChangePINComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'otpValidation', component: OtpValidationComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'defaultWallet', component: DefaultWalletComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'defaultWallet/:ordrId', component: DefaultWalletComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'viewDocs/:ordrId', component: VistaDocumentsComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'upgrade-subscription', component: UpgradeSubscriptionComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'expired-date', component: ExpiredDateComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: [PROFILES.MONEY.SUPERVISEUR],
        redirectTo: '/errorNotAllowed'
      }
    }},
  {path: 'expired-date/:ordrId', component: ExpiredDateComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: [PROFILES.MONEY.SUPERVISEUR],
        redirectTo: '/errorNotAllowed'
      }
    }},
    {path: 'upgrade-subscription/:ordrId', component: UpgradeSubscriptionComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          except: [PROFILES.MONEY.SUPERVISEUR],
          redirectTo: '/errorNotAllowed'
        }
      }},
    {path: 'listTransactionHistory/:ordrId', component: TransactionHistoryComponent,
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions:
          [
            PROFILES.POS.VENDEUR,
            PROFILES.POS.VENDEUR_OS,
            PROFILES.POS.CHEF_AGENCE_OS,
            PROFILES.MONEY.VENDEUR,
            PROFILES.MONEY.VENDEUR_OS,
            PROFILES.MONEY.CHEF_AGENCE_OS,
            PROFILES.MONEY.SUPERVISEUR,
            PROFILES.MONEY.HEAD_AGENCE,
          ],
          redirectTo: '/errorNotAllowed'

      }},
    {path: 'listTransactionCash', component: ListTransactionCashComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: [PROFILES.MONEY.SUPERVISEUR],
        redirectTo: '/errorNotAllowed'
      }
    }},
    {path: 'errorNotAllowed',  component: PageErrorsComponent},
    {path: 'errorNotFound',  component: NotFoundComponent},
    {path: '404', component: NotFoundComponent },
    {path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true,initialNavigation:false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
