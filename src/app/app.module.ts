import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {AppComponent, SafePipe} from './app.component';
import { HomeComponent } from './components/main/home/home.component';
import { SearchClientComponent } from './components/main/search-client/search-client.component';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import { CustomerCardComponent } from './components/main/customer-card/customer-card.component';
import { NewSubscriptionComponent } from './components/main/new-subscription/new-subscription.component';
import { FormsModule} from '@angular/forms';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DatepickerComponent } from './components/html/datepicker/datepicker.component';
import { NewspStep1Component } from './components/main/new-subscription/newsp-step1/newsp-step1.component';
import { NewspStep2Component } from './components/main/new-subscription/newsp-step2/newsp-step2.component';
import { OrangeTitleComponent } from './components/html/orange-title/orange-title.component';
import {NgbDatepickerI18n, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ValidationErrorsComponent } from './components/html/validation-errors/validation-errors.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { WalletPreviewComponent } from './components/main/new-subscription/wallet-preview/wallet-preview.component';
import {ListTasnsactionComponent} from './components/main/list-transaction/list-tasnsaction.component';
import { CashInComponent } from './components/main/cash-in/cash-in.component';
import { CashOutComponent } from './components/main/cash-out/cash-out.component';
import {MatSelectModule} from '@angular/material/select';
import { NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {DateFormat} from 'src/app/services/commons/dateFormat.service';
import { AppDateAdapter, APP_DATE_FORMATS} from './util/data.adapter';

import {
  MatAutocompleteModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule, MAT_DATE_FORMATS, DateAdapter,

} from '@angular/material';
import { CustomerDetailComponent } from './components/main/customer-detail/customer-detail.component';
import { AgencyBalanceComponent } from './components/main/agency-balance/agency-balance.component';
import { OtpValidationComponent } from './components/main/otp-validation/otp-validation.component';
import { ListTransactionClientComponent } from './components/main/list-transaction-client/list-transaction-client.component';
import {ListTransactionTableComponent} from './components/main/list-transaction-table/list-transaction-table.component';
import { ChangePINComponent } from './components/main/change-pin/change-pin.component';
import { PinPadComponent } from './components/html/pin-pad/pin-pad.component';
import {RestInterceptor} from './services/rest/jwt.interceptor';
import { DefaultWalletComponent } from './components/main/default-wallet/default-wallet.component';
import { VistaDocumentsComponent } from './components/main/vista-documents/vista-documents.component';
import { UpgradeSubscriptionComponent } from './components/main/upgrade-subscription/upgrade-subscription.component';
import { UpgradespStep1Component } from './components/main/upgrade-subscription/upgradesp-step1/upgradesp-step1.component';
import { UpgradespStep2Component } from './components/main/upgrade-subscription/upgradesp-step2/upgradesp-step2.component';
import { WalletUpgradePreviewComponent } from './components/main/upgrade-subscription/wallet-upgrade-preview/wallet-upgrade-preview.component';
import { TransactionHistoryComponent } from './components/main/transaction-history/transaction-history.component';
import { LoginComponent } from './components/main/login/login.component';
import { MenuComponent } from './components/main/menu/menu.component';
import {NgxPermissionsModule, NgxPermissionsService} from 'ngx-permissions';
import { PageErrorsComponent } from './components/main/page-errors/notAllowed/page-errors.component';
import { NotFoundComponent } from './components/main/page-errors/not-found/not-found.component';
import {LoginService} from './services/login/login.service';
import {CustomDatepickerI18n} from 'src/app/services/commons/datepicker-i18n.service';
import { ListTransactionCashComponent } from './components/main/list-transaction-cash/list-transaction-cash.component';
import { ExpiredDateComponent } from './components/main/expired-date/expired-date.component';
import { OAuthModule } from 'angular-oauth2-oidc';
import { CookieService } from 'ngx-cookie-service';
import { PosRedirectComponent } from './pos-redirect/pos-redirect.component';

@NgModule({
  declarations: [
    AppComponent,
    SafePipe,
    HomeComponent,
    SearchClientComponent,
    CustomerCardComponent,
    NewSubscriptionComponent,
    DatepickerComponent,
    NewspStep1Component,
    NewspStep2Component,
    OrangeTitleComponent,
    ValidationErrorsComponent,
    WalletPreviewComponent,
    ListTasnsactionComponent,
    CustomerDetailComponent,
    ValidationErrorsComponent,
    WalletPreviewComponent,
    CashInComponent,
    CashOutComponent,
    AgencyBalanceComponent,
    ListTransactionClientComponent,
    ListTransactionTableComponent,
    OtpValidationComponent,
    ChangePINComponent,
    PinPadComponent,
    DefaultWalletComponent,
    VistaDocumentsComponent,
    UpgradeSubscriptionComponent,
    UpgradespStep1Component,
    UpgradespStep2Component,
    WalletUpgradePreviewComponent,
    TransactionHistoryComponent,
    LoginComponent,
    MenuComponent,
    PageErrorsComponent,
    NotFoundComponent,
    ListTransactionCashComponent,
    ExpiredDateComponent,
    PosRedirectComponent
  ],
  imports: [
    OAuthModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatSelectModule,
    NgbModule,
    HttpClientModule,
    NgxPermissionsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [ HttpClient ]
      }
    })
  ],
  providers: [ CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: RestInterceptor, multi: true },
    {provide: NgbDateParserFormatter, useClass: DateFormat},
    {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: (ds: LoginService ) => function() {
    //     return ds.loadPermissions( )},
    //   deps: [LoginService],
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
