import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'app-pos-redirect',
  templateUrl: './pos-redirect.component.html',
  styleUrls: ['./pos-redirect.component.css']
})
export class PosRedirectComponent implements OnInit {

  constructor(

    private router: Router,

    private activatedRoute: ActivatedRoute,
    private permissionsService: NgxPermissionsService) {

    }


  ngOnInit() {

    const ordrId = this.activatedRoute.snapshot.paramMap.get('orderID');
    const msisdnPOS = this.activatedRoute.snapshot.paramMap.get('msisdnPOS');

    console.log('The URL ordrId is: ' + ordrId);
    console.log('The URL msisdnPOS is: ' + msisdnPOS);

    if (!ordrId !== null) {
      localStorage.setItem('ordrIdPOS', ordrId);
      localStorage.setItem('msisdnPOS', msisdnPOS);

      this.router.navigate(['/subscription']);
    }


  }

}
