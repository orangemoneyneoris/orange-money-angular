import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosRedirectComponent } from './pos-redirect.component';

describe('PosRedirectComponent', () => {
  let component: PosRedirectComponent;
  let fixture: ComponentFixture<PosRedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosRedirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
