import { Component, ElementRef, OnDestroy, Pipe, PipeTransform, ViewChild, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { CommonsService } from './services/commons.service';
import { Subscription } from 'rxjs';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PopupObj } from './util/PopupObj';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { Compiler } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { PROFILES } from './util/Constants';
import jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
 // noinspection AngularMissingOrInvalidDeclarationInModule

 import * as $ from 'jquery';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit,AfterViewInit {

  constructor(private modalService: NgbModal,
              private commonsService: CommonsService,
              private translate: TranslateService,
              private compiler: Compiler,
              private activatedRoute: ActivatedRoute,

              private permissionsService: NgxPermissionsService,
              private oauthService: OAuthService,private cookieService : CookieService,
              private router: Router) {

    if (!environment.local) {
      translate.setDefaultLang('fr');
      translate.use('fr');
    } else {
      translate.setDefaultLang('en');
      translate.use('en');
    }

    this.compiler.clearCache();
    this.permissionsService.flushPermissions();

    this.subscribe2Popups();
    this.subscribe2Mixers();


    /*const perm = ["POS", "BO"];

    this.permissionsService.loadPermissions(perm);*/

    //  TODO: cargar los permisos dinamicamente
    /*this.http.get('url').subscribe((permissions) => {
      this.permissionsService.loadPermissions(permissions);
    });*/

  }

  closeMenu: Boolean;
  overMenu: Boolean = false;

  @ViewChild('popupModal') popupModal: ElementRef;
  @ViewChild('mixer') mixerModal: ElementRef;
  popupSubscription: Subscription;
  mixerSubscription: Subscription;
  eventSubscription: Subscription;

  popupObj: PopupObj;
  mixerMessage: String;
  private popupModalRef: NgbModalRef;
  private mixerModalRef: NgbModalRef;
  @HostListener('click') onClick() {
    if (!this.overMenu) {
      this.closeMenu = true;
    } else {
      this.closeMenu = false;
    }
  }

  private subscribe2Mixers() {
    this.mixerSubscription = this.commonsService.mixers().subscribe(message => {
      if (message !== undefined) {

        if (this.mixerModalRef !== undefined) {
          console.warn('You should not open a new mixer without closing previous one');
          this.mixerModalRef.close();
          this.mixerModalRef = undefined;
        }

        this.mixerMessage = message;
        this.openMixer(this.mixerModal);

      } else {
        if (this.mixerModalRef !== undefined) {
          this.mixerModalRef.close();
          this.mixerModalRef = undefined;
        }
      }
    });
  }

  private subscribe2Popups() {
    this.popupSubscription = this.commonsService.popups().subscribe(popupObj => {
      if (popupObj !== undefined) {

        if (this.popupModalRef !== undefined) {
          console.warn('You should not open a new popup without closing previous one');
          this.popupModalRef.close();
          this.popupModalRef = undefined;
        }

        this.popupObj = popupObj;
        this.openOTPModal(this.popupModal);

      } else {
        if (this.popupModalRef !== undefined) {
          this.popupModalRef.close();
          this.popupModalRef = undefined;
        }
      }
    });
  }

  openOTPModal(OTPModal) {

    const ngbModalOptions: NgbModalOptions = {
      // centered: true,
      // To avoid user closing the Modal
      keyboard: false,
      backdrop: 'static'
    };

    this.popupModalRef = this.modalService.open(OTPModal, ngbModalOptions);
  }

  openMixer(MixerModal) {

    const ngbModalOptions: NgbModalOptions = {
      centered: true,

      // To avoid user closing the Modal
      keyboard: false,
      backdrop: 'static'
    };

    this.mixerModalRef = this.modalService.open(MixerModal, ngbModalOptions);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.popupSubscription.unsubscribe();
    this.mixerSubscription.unsubscribe();
    this.eventSubscription.unsubscribe();

  }


  ngAfterViewInit(){

  }
  ngOnInit() {

    console.log('originnnn' + window.location.origin + '/');
    let url = window.location.href.split('login');
    let currentLocation = window.location.origin + window.location.pathname;

    if (!currentLocation.includes(environment.DOMAIN_ORANGE_ADFS)) {

      console.log('normal login  ', currentLocation);
      if (url.length > 1) {
        this.router.navigate(['/login' + url[1]]);
      } else {
        this.router.navigate(['/login']);
      }
    } else {

      if (currentLocation.endsWith('/')) {
        currentLocation = currentLocation.substring(0, currentLocation.length - 1);
      }
      console.log('Adfs login  ', currentLocation);
      this.oauthService.configure({
        tokenEndpoint: environment.TOKEN_ENDPOINT_ADFS,
        loginUrl: environment.AUTHORIZE_ENDPOINT_ADFS,
        redirectUri: currentLocation,
        clearHashAfterLogin: false,
        clientId: environment.CLIENT_ID,
        dummyClientSecret: environment.CLIENT_SECRET,
        responseType: 'code',
         // sessionCheckIFrameUrl:environment.AUTHORIZE_ENDPOINT,
        showDebugInformation: true,
        disablePKCE: true,

         useHttpBasicAuth: false,

        scope: '',

        oidc: false
      });
      this.oauthService.setStorage(sessionStorage);
      // //  // this.oauthService.tryLogin({});

      this.eventSubscription = this.oauthService.events.subscribe(e => {
        // tslint:disable-next-line:no-console
        console.debug('oauth/oidc event', e);
      });

      //  this.oauthService.setupAutomaticSilentRefresh();

      console.log("rouing url ",this.router.url);
      console.log("location hash url ",  window.location.hash);




      console.log("logout cookie found LoggedIn :", this.cookieService.get("out"));

       if( this.cookieService.get("out")=="Y"){
            // this.cookieService.delete("out");
             console.log("user is logged out From ADFS");
             this.cookieService.set('out', 'null', null, '/OrangeMoney', 'meditel.int',false,'Lax');

             this.oauthService.logOut();
      }else{
        console.log("user still logged in ADFS ", this.cookieService.get("out"));

      }


      this.oauthService.tryLoginCodeFlow().then(() => {
        if (!this.oauthService.hasValidAccessToken()) {
          console.log('no valid token : ');
          console.log(this.router.routerState.snapshot.url);
          this.oauthService.initCodeFlow(window.location.hash);
        } else {

          console.log('state', this.oauthService.state);
          console.log('valid token:' + this.oauthService.getAccessToken());
          const  user: any = jwt_decode(this.oauthService.getAccessToken());
          console.log(user);

          const permissions = user.authorities;
          if (permissions != null) {
            this.permissionsService.loadPermissions(permissions);
          }


          localStorage.setItem('username', user.username);
          localStorage.setItem('usernameInfo', user.usernameInfo);
          localStorage.setItem('group', user.group);
          localStorage.setItem('groupId', user.groupId);
          localStorage.setItem('entEntityId', user.entEntityId);
          localStorage.setItem('isDefaultWalletEnabled', user.isDefaultWalletEnabled);
          localStorage.setItem('dealer', user.dealer);

          if (this.oauthService.state && this.oauthService.state !== 'undefined'
            && this.oauthService.state !== 'null' && this.oauthService.state !== '/') {

              console.log(encodeURI(this.oauthService.state));
              console.log(decodeURI(this.oauthService.state));
              console.log('navigation to ', this.oauthService.state);
              this.router.navigateByUrl(this.oauthService.state.replace('#', ''));


          } else  if (permissions && permissions.includes( PROFILES.MONEY.SUPERVISEUR)) {
            this.router.navigate(['/listTransaction']);
          } else  if (permissions && permissions.includes(PROFILES.MONEY.REPORT_CENTRAL)) {
              this.router.navigate(['/listTransactionCash']);
            } else {
              const ordrId = this.activatedRoute.snapshot.paramMap.get('orderID');
              const msisdnPOS = this.activatedRoute.snapshot.paramMap.get('msisdnPOS');
              if (ordrId != null || msisdnPOS != null) {
                this.router.navigate(['/subscription']);
              } else if( window.location.hash && window.location.hash !== 'undefined'
              && window.location.hash !== 'null' && window.location.hash !== '/'){

                    console.log('navigating to : ' + window.location.hash.replace('#', ''));
                    this.router.navigateByUrl(window.location.hash.replace('#', ''));

              } else {
                console.log('The user in component login is: ' + localStorage.getItem('username'));
                console.log('The token in component login is: ' + localStorage.getItem('jwt_token'));
                console.log('The lastRequest in component login is: ' + localStorage.getItem('lastRequest'));
                this.router.navigate(['/resercher']);
              }
            }
        }
      });

    }

    // document.body.style.height="100vh";
    // window.addEventListener('message', this.handleDocHeightMsg, false);
    // console.debug(this.permissionsService.getPermissions());

  }



  close() {
    this.commonsService.closePopup();
  }

  closeRedirect() {
    this.commonsService.closePopup();
    this.router.navigate(['/listTransaction']);
  }

  closemenu() {
    this.overMenu = false;
  }

  openmenu() {
    this.overMenu = true;
  }


}
