import {IdentificationType} from './IdentificationType';
import {Profession} from './Profession';
import {Nationality} from './Nationality';
import {Gender} from './Gender';
import {CustomerAddress} from './CustomerAddress';
import {OrdOrder} from "./OrdOrder";
import {OpeningReason} from "./OpeningReason";
import {WalletDocument} from "./WalletDocument";
import {Salary} from "./Salary";

export class Client {
  clientID: string;
  identificationId: string;
  identityType: IdentificationType = new IdentificationType();
  msisdn: string;
  name: string;
  surname: string;
  address: CustomerAddress = new CustomerAddress();
  profession: Profession = new Profession();
  nationality: Nationality = new Nationality();
  salary: Salary= new Salary();
  gender: Gender = new Gender();
  email: string;
  birthDate: Date;
  numRib: string;
  subscriptionDate: string;

  walletID: number;
  ordOrder: OrdOrder;
  expirationDate: Date;
  accountLevel: string;
  accountBalance: string = "0";
  defaultAccount: boolean;
  sponsorCode: string;
  walletOpeningReasons: OpeningReason[] = new Array<OpeningReason>();
  walletDocuments: WalletDocument[] = new Array<WalletDocument>();
  enrolementWallet: string;
  statusAccount: string;

  get _birthDate(): string {
    let date = null;
    if(this.birthDate) {
      const year = this.birthDate.getFullYear();
      const month = this.birthDate.getMonth() + 1;
      const day = this.birthDate.getDay();
      date = year + '-' + month + '-' + day;
    }
    return date;
  }

  set _birthDate(value: string) {
   this.birthDate = new Date (Date.UTC(value['year'], value['month'] - 1, value['day']));


  }


}
