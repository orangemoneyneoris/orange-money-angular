export class TransactionHistory {
  ordStatus: string;
  ordSubStatus: string;
  ordSubStatus2: string;
  updatedBy: string;
  updatedDate: string;
}
