export class IdentificationType {
  identificationTypeID: number;
  name: string;
  desc: string;
}
