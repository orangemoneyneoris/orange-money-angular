import {EntCountry} from "./EntCountry";

export class CustomerAddress {
  id: number;
  entCountry: EntCountry = new EntCountry();
  entVille: string = "";
  entCodePostal: string = "";
  address: string;
}
