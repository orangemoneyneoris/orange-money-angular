import {OrdOrder} from "./OrdOrder";

  export class ChangePINOrders {
  id: number;
  ordOrder: OrdOrder;
  msisdn: string;
  newPin: string;
  currentPin: string;
  confirmPin: string;
  pinKey: string;

}
