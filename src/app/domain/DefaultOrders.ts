import {OrdOrder} from "./OrdOrder";
import {Wallet} from "./Wallet";
import {Client} from "./Client";

  export class DefaultOrders {
  id: number;
  client: Client;
  wallet: Wallet;
  ordOrder: OrdOrder;
}
