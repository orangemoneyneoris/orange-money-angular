export class JSONResponse {
  success: boolean;
  errorMessage: string;
  code: number;
  successResponse: object;
  interopRefId: string;
  tokenInvalid: boolean;
  permissions: string[];
}
