import {Client} from "./Client";
import {Wallet} from "./Wallet";
import {OrdOrder} from "./OrdOrder";

export class SubscriptionOrder{

  id: number;
  client: Client;
  wallet: Wallet;
  ordOrder: OrdOrder = new OrdOrder();
  refId: string;
  ribCode: string;
  correlatorID: string;
}
