export class OpeningReason{
  openingReasonID: number;
  name: string;
  desc: string;
}
