import {OrdOrder} from "./OrdOrder";
import {WalletDocument} from "./WalletDocument";
import {OpeningReason} from "./OpeningReason";

export class Wallet {
  walletID: number;
  ordOrder: OrdOrder;
  expirationDate: Date;
  accountLevel: string;
  accountBalance: string = "0";
  defaultAccount: boolean;
  sponsorCode: string;
  walletOpeningReasons: OpeningReason[] = new Array<OpeningReason>();
  walletDocuments: WalletDocument[] = new Array<WalletDocument>();
  enrolementWallet: string;
  statusAccount: string;

  get _expirationDate(): string {
    let date = null;
    if (this.expirationDate) {
      const year = this.expirationDate.getFullYear();
      const month = this.expirationDate.getMonth() + 1;
      const day = this.expirationDate.getDate();
      date = year + '-' + month + '-' + day;
    }
    return date;
  }

  set _expirationDate(value: string) {
    this.expirationDate = new Date(Date.UTC(value['year'], value['month'] - 1, value['day']));
  }


}
