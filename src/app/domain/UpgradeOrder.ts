import {Client} from "./Client";
import {Wallet} from "./Wallet";
import {OrdOrder} from "./OrdOrder";

export class UpgradeOrder{

  id: number;
  client: Client;
  wallet: Wallet;
  ordOrder: OrdOrder = new OrdOrder();
  lastLevelWallet: string;
  refId: string;
  ribCode: string;
  correlatorID: string;
}
