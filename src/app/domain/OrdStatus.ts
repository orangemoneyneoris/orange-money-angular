export class OrdStatus {
  statusId: number;
  name: string;
  description: string;
}
