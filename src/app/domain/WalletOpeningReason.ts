import {OpeningReason} from "./OpeningReason";

export class WalletOpeningReason{
  reasonID: number;
  openingReason: OpeningReason;
}
