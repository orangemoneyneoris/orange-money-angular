export class Pin {
  pin: string;
  value: string;
  key: string;

  constructor(pin: string, value: string, key: string) {
    this.pin = pin;
    this.value = value;
    this.key = key;
  }
}
