import {EntEntity} from './EntEntity';
import {OrdType} from './OrdType';
import {OrdCategory} from "./OrdCategory";
import {OrdSubCategory} from "./OrdSubCategory";
import {OrdStatus} from "./OrdStatus";
import {OrdSubStatus} from "./OrdSubStatus";
import {OrdSubStatus2} from "./OrdSubStatus2";

export class OrdOrder {
  ordrId: number;
  entEntity: EntEntity;
  msisdn: string;
  ordType: OrdType;
  ordCategory: OrdCategory;
  ordSubCategory: OrdSubCategory;
  ordStatus: OrdStatus;
  ordSubStatus: OrdSubStatus;
  ordSubStatus2: OrdSubStatus2;
  createdBy: string;
  createdDate: string;
  updatedBy: string;
  updatedDate: string;
  description: string;
  closedBy: string;
  closedDate: string;
  numContract: string;
  externalID: string;
  posOrderID: string;
  crmMajOk: boolean;
}
