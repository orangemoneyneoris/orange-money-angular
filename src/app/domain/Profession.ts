export class Profession {
  professionID: number;
  name: string;
  desc: string;
}
