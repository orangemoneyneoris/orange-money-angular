import {Client} from "./Client";
import {OrdOrder} from "./OrdOrder";
import {Wallet} from "./Wallet";

export class CashOrders {
  id: number;
  ordOrder: OrdOrder;
  client: Client;
  wallet: Wallet;
  amount: number;
  msisdn: string;
  pin: string;
  pinPosition: string;
  pinCode: string;
  externalID: string;
  externalIDCB: string;
  initDate: Date;
  verifyTries: number = 0;

}
