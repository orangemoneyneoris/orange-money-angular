
export class ADuser {
  username: string;
  group: string;
  groupId: string;
  token: string;
  tokenInvalid: boolean;
  usernameInfo: string;
  entEntityId: string;
  isDefaultWalletEnabled: string;
  dealer: string;
}
