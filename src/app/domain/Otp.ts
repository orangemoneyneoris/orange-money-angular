export class Otp {

  orderId: number;

  resendDate: Date;

  resendTries: number;

  resendTriesLeft: number;

}
