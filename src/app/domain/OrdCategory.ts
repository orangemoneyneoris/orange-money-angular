export class OrdCategory {
  categoryId: number;
  name: string;
  description: string;
}
