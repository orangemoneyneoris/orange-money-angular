import {OrdOrder} from "./OrdOrder";
import {ReasonsExpiredClient} from "./ReasonsExpiredClient";

export class ClientUpdateDocument{
    clientUpdateID: number;
    order: OrdOrder;
    documentType: string;
    documentName: string;
    document: File;
    gidOK:boolean;
    retries:number;
    reasonExpiredClient: ReasonsExpiredClient;

}
