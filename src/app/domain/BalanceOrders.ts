import {OrdOrder} from "./OrdOrder";

  export class BalanceOrders {
  id: number;
  ordOrder: OrdOrder;
  balanceAmount: number;
  msisdn: string;
  pin: string;
  pinKey: string;
}
