export class OrdOrderRemote {
  transactionID: string;
  amount: string;
  serviceType: string;
  mobileNumber: string;
  entryType: string;
  statusTransaction: string;
}
