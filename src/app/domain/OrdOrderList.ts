export class OrdOrderList {
  ordrId: number;
  entEntity: string;
  msisdn: string;
  ordType: string;
  ordCategory: string;
  ordSubCategory: string;
  ordStatus: string;
  ordSubStatus: string;
  ordSubStatus2: string;
  createdBy: string;
  createdDate: string;
  updatedBy: string;
  updatedDate: string;
  description: string;
  closedBy: string;
  closedDate: string;
  numContract: string;
}
