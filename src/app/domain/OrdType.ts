export class OrdType {
  typeId: number;
  name: string;
  description: string;
}
