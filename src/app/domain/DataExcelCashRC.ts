export class DataExcelCashRC{
  entity: string;
  updateDate: string;
  type: string;
  status: string;
  amount: string;
  tangoID: string;
  orderID: string;
  compteClient: string;
  compteAgence: string;
  dealer: string;
}
