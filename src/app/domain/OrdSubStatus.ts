export class OrdSubStatus {
  statusId: number;
  name: string;
  description: string;
}
