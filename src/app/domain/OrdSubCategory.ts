export class OrdSubCategory {
  subCategoryId: number;
  name: string;
  description: string;
}
