export class OtpRequest {

  otpCode: string;

  orderId: number;

  refId: string;

  walletId: number;

  msisdn: string;

}
