export const environment = {
  production: true,
   local: false,
   portalHome: 'https://portaltest.meditel.int:4443/OrangeShopPortal',
   portalMenu:'https://portaltest.meditel.int:4443/OrangeShopPortal/OrangeShopPortalMenu',
  searchCustomerByPhoneUrl: 'http://localhost:8081/client/searchCustomer',
  isCustomerEligibleEndpoint: 'http://localhost:8081/client/isCustomerEligible',
  isCustomerEligibleMakeDefaultEndpoint: 'http://localhost:8081/client/isCustomerEligibleMakeDefault',
  generateContractUrl: 'http://localhost:8081/report/generateContract/newSubscription',
  validateOTPServiceEndpoint: 'http://localhost:8081/subscription/validateOTP',
  searchWalletUrl: 'http://localhost:8081/client/searchWalletByClientID',
  isRetailedWalletEndpoint: 'http://localhost:8081/client/isRetailedWallet',
  getAllCountriesEndpoint: 'http://localhost:8081/client/getAllCountries',
  getCitiesByProvinceEndpoint: 'http://localhost:8081/client/getCitiesByProvince',
  getPostalCodesFromCityEndpoint: 'http://localhost:8081/client/getPostalCodesFromCity',
  getStreetsFromCityEndpoint: 'http://localhost:8081/client/getStreetsFromCity',
  getidentityTypesEndpoint: 'http://localhost:8081/client/getIdentificationTypeList',
  getOpeningReasonsEndpoint: 'http://localhost:8081/client/getOpeningReasonsList',
  getProfessionsEndpoint: 'http://localhost:8081/client/getProfessionsList',
  getNationalitiesEndpoint: 'http://localhost:8081/client/getNationalitiesList',
  saveNewWalletEndPoint: 'http://localhost:8081/subscription/saveSubscriptionOrder',
  getSavedOrderEndpoint: 'http://localhost:8081/subscription/getSavedOrder',
  sendDocumentEndPoint: 'http://localhost:8081/subscription/saveDocuments',
  initOtpStatus: 'http://localhost:8081/subscription/initOtpStatus',
  confirmOtp: 'http://localhost:8081/subscription/confirmOtp',
  getOtpDefaultStatus: 'http://localhost:8081/subscription/getOtpDefaultStatus',
  interopEnrollment: 'http://localhost:8081/subscription/interopEnrollment',
  resendOtp: 'http://localhost:8081/subscription/initOtpResend',
  interopResendOtp: 'http://localhost:8081/subscription/interopResendOtp',
  cancelOtp: 'http://localhost:8081/subscription/cancelOtp',
  saveOrderEndPoint: 'http://localhost:8081/subscription/saveOrder',
  deleteDocumentEndPoint: 'http://localhost:8081/subscription/deleteDocuments',
  defaultWalletStatusUrl: 'http://localhost:8081/subscription/getDefaultWalletStatus',
  getSalaryEndpoint: 'http://localhost:8081/client/getSalaryList',
  getRibCodeEndpoint: 'http://localhost:8081/client/getRibCode',
  getRibCodeSubscriptionEndpoint: 'http://localhost:8081/client/getRibCodeFromSubscription',
  createRibCodeEndpoint: 'http://localhost:8081/client/createRibCode',

  //  Cash-IN
  GET_CUSTOMER_DETAIL_INFO_URL: 'http://localhost:8081/client/getCustomerDetailInfo',
  CASH_IN_GET_CASH_ORDER: 'http://localhost:8081/cash/getCashOrderFromOrder',
  CASH_IN_EXECUTE_OPERATION: 'http://localhost:8081/cash/executeCashin',
  CASH_IN_REFRESH_OPERATION: 'http://localhost:8081/cash/checkCashInCallBack',
  GENERATED_CASH_IN_CONTRACT_URL: 'http://localhost:8081/report/generateContractCashin',
  CASH_IN_VERIFY: 'http://localhost:8081/cash/verifyCashIn',
  CASH_IN_CREATE_ORDER_OPERATION: 'http://localhost:8081/cash/createCashinCashOrder',
  CASHIN_CALLBACK: 'http://localhost:8081/callback/cashCallBackConfirmation',
  GET_VERIFY_TIME: 'http://localhost:8081/cash/getVerifyTimeOut',
  REGISTER_CASH_IN_POS: 'http://localhost:8081/cash/registerCashInPOS',

  // Cash-OUT
  CASH_OUT_EXECUTE_OPERATION: 'http://localhost:8081/cash/cashOutInit',
  CASH_OUT_REFRESH_OPERATION: 'http://localhost:8081/cash/checkCashOutCallBack',
  GENERATED_CASH_OUT_CONTRACT_URL: 'http://localhost:8081/report/generateContractCashout',
  CASH_OUT_VERIFY: 'http://localhost:8081/cash/verifyCashOut',
  ANNUL_CASH_ORDER: 'http://localhost:8081/cash/annulCashOrder',
  REJECT_CASH_ORDER: 'http://localhost:8081/cash/rejectCashOrder',
  CASH_OUT_CREATE_ORDER_OPERATION: 'http://localhost:8081/cash/createCashoutCashOrder',
  REGISTER_CASH_OUT_POS: 'http://localhost:8081/cash/registerCashOutPOS',
  CASHOUT_CALLBACK: 'http://localhost:8081/callback/cashCallBackConfirmation',
  CASHOUT_GET_TAXES: 'http://localhost:8081/callback/cashGetTaxes',

  //  List transaction
  GET_ORD_ORDER_LIST_URL: 'http://localhost:8081/agency/getOrdOrderLocalList',
  GET_ORD_ORDER_CLIENT_LIST_URL: 'http://localhost:8081/agency/getOrdOrderRemoteList',
  GET_TRANSACTIONS_HISTORY_LIST: 'http://localhost:8081/agency/getTransactionHistoryList',
  GET_ORD_PARENT_ORDER_BY_ID: 'http://localhost:8081/agency/getOrdParentOrderById',

  //  Common
  GET_TRANSACTION_TYPE_ID: 'http://localhost:8081/client/getIdentificationType',

  //  Agency
  AGENCY_GET_PIN_PAD: 'http://localhost:8081/agency/getPinPad',
  BALANCE_CREATE_ORDER: 'http://localhost:8081/agency/createBalanceOrder',
  GET_AGENCY_BALANCE: 'http://localhost:8081/agency/getAgencyBalance',
  CHANGE_PIN_CREATE_ORDER: 'http://localhost:8081/agency/createChangePinOrder',
  CHANGE_PIN_AGENCY : 'http://localhost:8081/agency/changePinAgenty',
  GET_WALLET_DOCUMENT_LIST: 'http://localhost:8081/agency/getWalletDocumentList',
  GET_DOCUMENT_FILE: 'http://localhost:8081/agency/GetDocumentFile',
  GET_DOCUMENT_VALIDATION: 'http://localhost:8081/agency/GetDocumentValidation',
  SEND_DOCUMENTS_SUBSCRIPTION:  'http://localhost:8081/agency/sendDocumentsSubscription',
  SEND_DOCUMENTS_ORDER_UPGRADE:  'http://localhost:8081/agency/sendDocumentsUpgrade',
  GET_DOCUMENT_ORDER_VIEWDOCUMENT:  'http://localhost:8081/agency/GetDevolverOrderViewDoc',
  getReasonsExpiredClient: 'http://localhost:8081/agency/getReasonsExpiredClient',
  saveUpdateClient: 'http://localhost:8081/agency/saveDocuments',
  getClientUpdateOrd: 'http://localhost:8081/agency/getClientUpdateOrd',
  resendDocumentClient: 'http://localhost:8081/agency/resendDocumentClient',

  //  Default Wallet
  CLIENT_DEFAULT_WALLET: 'http://localhost:8081/client/validateDefaultWallet',
  GET_DEFAULT_ORDER: 'http://localhost:8081/client/getDefaulOrderFromOrder',
  EXECUTE_INTEGRATION_OTP: 'http://localhost:8081/client/executeIntegrationOTP',
  ENABLE_CUSTOMER_WALLET: 'http://localhost:8081/client/enableCustomerWallet',

  //  Upgrade Wallet
  SUBSCRIPTION_INIT_UPGRADE_ORDER: 'http://localhost:8081/subscription/saveUpgradeOrder',
  GET_ORDER_BY_ID: 'http://localhost:8081/subscription/getUpgradeOrder',
  GET_WALLET_DOCUMENT_BY_ID: 'http://localhost:8081/subscription/getUpgradeWalletDocument',
  CANCEL_ORDER: 'http://localhost:8081/subscription/cancelOrder',
  saveUpgradeOrderEndPoint: 'http://localhost:8081/subscription/sendContract',
  generateContractUrlUpgrade: 'http://localhost:8081/report/generateContract/upgrade',


  //  Login
  LOGIN: 'http://localhost:8081/commons/login',
  LOAD_PERMISSIONS: 'http://localhost:8081/commons/loadPermissions',

  //  OTP
  CHECK_TIMER_OTP: 'http://localhost:8081/subscription/checkTimerOTP',

  //  Cash transaction
  GET_CASH_LIST_URL: '/report/getCashList',


  // oauth_endpoints

  TOKEN_ENDPOINT : 'http://localhost:8081/commons/oauth/token',
  AUTHORIZE_ENDPOINT : 'http://localhost:8081/commons/oauth/authorize',
 //  TOKEN_ENDPOINT : 'https://10\.128\.250\.40:4443/commons/oauth/token',
 //  AUTHORIZE_ENDPOINT : 'https://10\.128\.250\.40:4443/commons/oauth/authorize',
 TOKEN_ENDPOINT_ADFS : 'https://oshop-orangemoneytest.meditel.int:8443/commons/oauth/token',
 AUTHORIZE_ENDPOINT_ADFS : 'https://oshop-orangemoneytest.meditel.int:8443/commons/oauth/authorize',
 
   CLIENT_ID : 'myclient',
   CLIENT_SECRET : 'secret',
 
   DOMAIN_ORANGE_ADFS: '.meditel.int'
 
 };
 
 
