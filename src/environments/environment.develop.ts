export const environment = {
  production: false,
  local: false,
  portal: 'http://10.128.250.233:7002/OrangeShopPortal/OrangeShopPortalMenuWithoutADFS.htm',
  searchCustomerByPhoneUrl: 'http://10.15.105.231:7001/client/searchCustomer',
  isCustomerEligibleEndpoint: 'http://10.15.105.231:7001/client/isCustomerEligible',
  isCustomerEligibleMakeDefaultEndpoint: 'http://10.15.105.231:7001/client/isCustomerEligibleMakeDefault',
  generateContractUrl: 'http://10.15.105.231:7001/report/generateContract/newSubscription',
  validateOTPServiceEndpoint: 'http://10.15.105.231:7001/subscription/validateOTP',
  searchWalletUrl: 'http://10.15.105.231:7001/client/searchWalletByClientID',
  isRetailedWalletEndpoint: 'http://10.15.105.231:7001/client/isRetailedWallet',
  getAllCountriesEndpoint: 'http://10.15.105.231:7001/client/getAllCountries',
  getCitiesByProvinceEndpoint: 'http://10.15.105.231:7001/client/getCitiesByProvince',
  getPostalCodesFromCityEndpoint: 'http://10.15.105.231:7001/client/getPostalCodesFromCity',
  getStreetsFromCityEndpoint: 'http://10.15.105.231:7001/client/getStreetsFromCity',
  getidentityTypesEndpoint: 'http://10.15.105.231:7001/client/getIdentificationTypeList',
  getOpeningReasonsEndpoint: 'http://10.15.105.231:7001/client/getOpeningReasonsList',
  getProfessionsEndpoint: 'http://10.15.105.231:7001/client/getProfessionsList',
  getNationalitiesEndpoint: 'http://10.15.105.231:7001/client/getNationalitiesList',
  saveNewWalletEndPoint: 'http://10.15.105.231:7001/subscription/saveSubscriptionOrder',
  getSavedOrderEndpoint: 'http://10.15.105.231:7001/subscription/getSavedOrder',
  sendDocumentEndPoint: 'http://10.15.105.231:7001/subscription/saveDocuments',
  initOtpStatus: 'http://10.15.105.231:7001/subscription/initOtpStatus',
  confirmOtp: 'http://10.15.105.231:7001/subscription/confirmOtp',
  getOtpDefaultStatus: 'http://10.15.105.231:7001/subscription/getOtpDefaultStatus',
  interopEnrollment: 'http://10.15.105.231:7001/subscription/interopEnrollment',
  resendOtp: 'http://10.15.105.231:7001/subscription/initOtpResend',
  interopResendOtp: 'http://10.15.105.231:7001/subscription/interopResendOtp',
  cancelOtp: 'http://10.15.105.231:7001/subscription/cancelOtp',
  saveOrderEndPoint: 'http://10.15.105.231:7001/subscription/saveOrder',
  deleteDocumentEndPoint: 'http://10.15.105.231:7001/subscription/deleteDocuments',
  defaultWalletStatusUrl: 'http://10.15.105.231:7001/subscription/getDefaultWalletStatus',
  getSalaryEndpoint: 'http://10.15.105.231:7001/client/getSalaryList',
  getRibCodeEndpoint: 'http://10.15.105.231:7001/client/getRibCode',
  getRibCodeSubscriptionEndpoint: 'http://10.15.105.231:7001/client/getRibCodeFromSubscription',
  createRibCodeEndpoint: 'http://10.15.105.231:7001/client/createRibCode',

  //  Cash-IN
  GET_CUSTOMER_DETAIL_INFO_URL: 'http://10.15.105.231:7001/client/getCustomerDetailInfo',
  CASH_IN_GET_CASH_ORDER: 'http://10.15.105.231:7001/cash/getCashOrderFromOrder',
  CASH_IN_EXECUTE_OPERATION: 'http://10.15.105.231:7001/cash/executeCashin',
  CASH_IN_REFRESH_OPERATION: 'http://10.15.105.231:7001/cash/checkCashInCallBack',
  GENERATED_CASH_IN_CONTRACT_URL: 'http://10.15.105.231:7001/report/generateContractCashin',
  CASH_IN_VERIFY: 'http://10.15.105.231:7001/cash/verifyCashIn',
  CASH_IN_CREATE_ORDER_OPERATION: 'http://10.15.105.231:7001/cash/createCashinCashOrder',
  CASHIN_CALLBACK: 'http://10.15.105.231:7001/callback/cashCallBackConfirmation',
  GET_VERIFY_TIME: 'http://10.15.105.231:7001/cash/getVerifyTimeOut',
  REGISTER_CASH_IN_POS: 'http://10.15.105.231:7001/cash/registerCashInPOS',

  // Cash-OUT
  CASH_OUT_EXECUTE_OPERATION: 'http://10.15.105.231:7001/cash/cashOutInit',
  CASH_OUT_REFRESH_OPERATION: 'http://10.15.105.231:7001/cash/checkCashOutCallBack',
  GENERATED_CASH_OUT_CONTRACT_URL: 'http://10.15.105.231:7001/report/generateContractCashout',
  CASH_OUT_VERIFY: 'http://10.15.105.231:7001/cash/verifyCashOut',
  ANNUL_CASH_ORDER: 'http://10.15.105.231:7001/cash/annulCashOrder',
  REJECT_CASH_ORDER: 'http://10.15.105.231:7001/cash/rejectCashOrder',
  CASH_OUT_CREATE_ORDER_OPERATION: 'http://10.15.105.231:7001/cash/createCashoutCashOrder',
  REGISTER_CASH_OUT_POS: 'http://10.15.105.231:7001/cash/registerCashOutPOS',
  CASHOUT_CALLBACK: 'http://10.15.105.231:7001/callback/cashCallBackConfirmation',

  //  List transaction
  GET_ORD_ORDER_LIST_URL: 'http://10.15.105.231:7001/agency/getOrdOrderLocalList',
  GET_ORD_ORDER_CLIENT_LIST_URL: 'http://10.15.105.231:7001/agency/getOrdOrderRemoteList',
  GET_TRANSACTIONS_HISTORY_LIST: 'http://10.15.105.231:7001/agency/getTransactionHistoryList',
  GET_ORD_PARENT_ORDER_BY_ID: 'http://10.15.105.231:7001/agency/getOrdParentOrderById',

  //  Common
  GET_TRANSACTION_TYPE_ID: 'http://10.15.105.231:7001/client/getIdentificationType',

  //  Agency
  AGENCY_GET_PIN_PAD: 'http://10.15.105.231:7001/agency/getPinPad',
  BALANCE_CREATE_ORDER: 'http://10.15.105.231:7001/agency/createBalanceOrder',
  GET_AGENCY_BALANCE: 'http://10.15.105.231:7001/agency/getAgencyBalance',
  CHANGE_PIN_CREATE_ORDER: 'http://10.15.105.231:7001/agency/createChangePinOrder',
  CHANGE_PIN_AGENCY : 'http://10.15.105.231:7001/agency/changePinAgenty',
  GET_WALLET_DOCUMENT_LIST: 'http://10.15.105.231:7001/agency/getWalletDocumentList',
  GET_DOCUMENT_FILE: 'http://10.15.105.231:7001/agency/GetDocumentFile',
  GET_DOCUMENT_VALIDATION: 'http://10.15.105.231:7001/agency/GetDocumentValidation',
  SEND_DOCUMENTS_SUBSCRIPTION:  'http://10.15.105.231:7001/agency/sendDocumentsSubscription',
  SEND_DOCUMENTS_ORDER_UPGRADE:  'http://10.15.105.231:7001/agency/sendDocumentsUpgrade',
  GET_DOCUMENT_ORDER_VIEWDOCUMENT:  'http://10.15.105.231:7001/agency/GetDevolverOrderViewDoc',
  getReasonsExpiredClient: 'http://10.15.105.231:7001/agency/getReasonsExpiredClient',
  saveUpdateClient: 'http://10.15.105.231:7001/agency/saveDocuments',
  getClientUpdateOrd: 'http://10.15.105.231:7001/agency/getClientUpdateOrd',
  resendDocumentClient: 'http://10.15.105.231:7001/agency/resendDocumentClient',

  //  Default Wallet
  CLIENT_DEFAULT_WALLET: 'http://10.15.105.231:7001/client/validateDefaultWallet',
  GET_DEFAULT_ORDER: 'http://10.15.105.231:7001/client/getDefaulOrderFromOrder',
  EXECUTE_INTEGRATION_OTP: 'http://10.15.105.231:7001/client/executeIntegrationOTP',
  ENABLE_CUSTOMER_WALLET: 'http://10.15.105.231:7001/client/enableCustomerWallet',

  //  Upgrade Wallet
  SUBSCRIPTION_INIT_UPGRADE_ORDER: 'http://10.15.105.231:7001/subscription/saveUpgradeOrder',
  GET_ORDER_BY_ID: 'http://10.15.105.231:7001/subscription/getUpgradeOrder',
  GET_WALLET_DOCUMENT_BY_ID: 'http://10.15.105.231:7001/subscription/getUpgradeWalletDocument',
  CANCEL_ORDER: 'http://10.15.105.231:7001/subscription/cancelOrder',
  saveUpgradeOrderEndPoint: 'http://10.15.105.231:7001/subscription/sendContract',
  generateContractUrlUpgrade: 'http://localhost:8081/report/generateContract/upgrade',


  //  Login
  LOGIN: 'http://10.15.105.231:7001/commons/login',
  LOAD_PERMISSIONS: 'http://10.15.105.231:7001/commons/loadPermissions',

  //  OTP
  CHECK_TIMER_OTP: 'http://10.15.105.231:7001/subscription/checkTimerOTP',

  //  Cash transaction
  GET_CASH_LIST_URL: 'http://10.15.105.231:7001/report/getCashList',
};
