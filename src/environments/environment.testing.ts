export const environment = {
  production: true,
  local: false,
  portalHome: 'https://portaltest.meditel.int:4443/OrangeShopPortal',
  portalMenu:'https://portaltest.meditel.int:4443/OrangeShopPortal/OrangeShopPortalMenu',
  searchCustomerByPhoneUrl: '/client/searchCustomer',
  isCustomerEligibleEndpoint: '/client/isCustomerEligible',
  isCustomerEligibleMakeDefaultEndpoint: '/client/isCustomerEligibleMakeDefault',
  generateContractUrl: '/report/generateContract/newSubscription',
  validateOTPServiceEndpoint: '/subscription/validateOTP',
  searchWalletUrl: '/client/searchWalletByClientID',
  isRetailedWalletEndpoint: '/client/isRetailedWallet',
  getAllCountriesEndpoint: '/client/getAllCountries',
  getCitiesByProvinceEndpoint: '/client/getCitiesByProvince',
  getPostalCodesFromCityEndpoint: '/client/getPostalCodesFromCity',
  getStreetsFromCityEndpoint: '/client/getStreetsFromCity',
  getidentityTypesEndpoint: '/client/getIdentificationTypeList',
  getOpeningReasonsEndpoint: '/client/getOpeningReasonsList',
  getProfessionsEndpoint: '/client/getProfessionsList',
  getNationalitiesEndpoint: '/client/getNationalitiesList',
  saveNewWalletEndPoint: '/subscription/saveSubscriptionOrder',
  getSavedOrderEndpoint: '/subscription/getSavedOrder',
  sendDocumentEndPoint: '/subscription/saveDocuments',
  initOtpStatus: '/subscription/initOtpStatus',
  confirmOtp: '/subscription/confirmOtp',
  getOtpDefaultStatus: '/subscription/getOtpDefaultStatus',
  interopEnrollment: '/subscription/interopEnrollment',
  resendOtp: '/subscription/initOtpResend',
  interopResendOtp: '/subscription/interopResendOtp',
  cancelOtp: '/subscription/cancelOtp',
  saveOrderEndPoint: '/subscription/saveOrder',
  deleteDocumentEndPoint: '/subscription/deleteDocuments',
  defaultWalletStatusUrl: '/subscription/getDefaultWalletStatus',
  getSalaryEndpoint: '/client/getSalaryList',
  getRibCodeEndpoint: '/client/getRibCode',
  getRibCodeSubscriptionEndpoint: '/client/getRibCodeFromSubscription',
  createRibCodeEndpoint: '/client/createRibCode',

  //  Cash-IN
  GET_CUSTOMER_DETAIL_INFO_URL: '/client/getCustomerDetailInfo',
  CASH_IN_GET_CASH_ORDER: '/cash/getCashOrderFromOrder',
  CASH_IN_EXECUTE_OPERATION: '/cash/executeCashin',
  CASH_IN_REFRESH_OPERATION: '/cash/checkCashInCallBack',
  GENERATED_CASH_IN_CONTRACT_URL: '/report/generateContractCashin',
  CASH_IN_VERIFY: '/cash/verifyCashIn',
  CASH_IN_CREATE_ORDER_OPERATION: '/cash/createCashinCashOrder',
  CASHIN_CALLBACK: '/callback/cashCallBackConfirmation',
  GET_VERIFY_TIME: '/cash/getVerifyTimeOut',
  REGISTER_CASH_IN_POS: '/cash/registerCashInPOS',

  // Cash-OUT
  CASH_OUT_EXECUTE_OPERATION: '/cash/cashOutInit',
  CASH_OUT_REFRESH_OPERATION: '/cash/checkCashOutCallBack',
  GENERATED_CASH_OUT_CONTRACT_URL: '/report/generateContractCashout',
  CASH_OUT_VERIFY: '/cash/verifyCashOut',
  ANNUL_CASH_ORDER: '/cash/annulCashOrder',
  REJECT_CASH_ORDER: '/cash/rejectCashOrder',
  CASH_OUT_CREATE_ORDER_OPERATION: '/cash/createCashoutCashOrder',
  REGISTER_CASH_OUT_POS: '/cash/registerCashOutPOS',
  CASHOUT_CALLBACK: '/callback/cashCallBackConfirmation',
  CASHOUT_GET_TAXES: '/callback/cashGetTaxes',

  //  List transaction
  GET_ORD_ORDER_LIST_URL: '/agency/getOrdOrderLocalList',
  GET_ORD_ORDER_CLIENT_LIST_URL: '/agency/getOrdOrderRemoteList',
  GET_TRANSACTIONS_HISTORY_LIST: '/agency/getTransactionHistoryList',
  GET_ORD_PARENT_ORDER_BY_ID: '/agency/getOrdParentOrderById',

  //  Common
  GET_TRANSACTION_TYPE_ID: '/client/getIdentificationType',

  //  Agency
  AGENCY_GET_PIN_PAD: '/agency/getPinPad',
  BALANCE_CREATE_ORDER: '/agency/createBalanceOrder',
  GET_AGENCY_BALANCE: '/agency/getAgencyBalance',
  CHANGE_PIN_CREATE_ORDER: '/agency/createChangePinOrder',
  CHANGE_PIN_AGENCY : '/agency/changePinAgenty',
  GET_WALLET_DOCUMENT_LIST: '/agency/getWalletDocumentList',
  GET_DOCUMENT_FILE: '/agency/GetDocumentFile',
  GET_DOCUMENT_VALIDATION: '/agency/GetDocumentValidation',
  SEND_DOCUMENTS_SUBSCRIPTION:  '/agency/sendDocumentsSubscription',
  SEND_DOCUMENTS_ORDER_UPGRADE:  '/agency/sendDocumentsUpgrade',
  GET_DOCUMENT_ORDER_VIEWDOCUMENT:  '/agency/GetDevolverOrderViewDoc',
  getReasonsExpiredClient: '/agency/getReasonsExpiredClient',
  saveUpdateClient: '/agency/saveDocuments',
  getClientUpdateOrd: '/agency/getClientUpdateOrd',
  resendDocumentClient: '/agency/resendDocumentClient',

  //  Default Wallet
  CLIENT_DEFAULT_WALLET: '/client/validateDefaultWallet',
  GET_DEFAULT_ORDER: '/client/getDefaulOrderFromOrder',
  EXECUTE_INTEGRATION_OTP: '/client/executeIntegrationOTP',
  ENABLE_CUSTOMER_WALLET: '/client/enableCustomerWallet',

  //  Upgrade Wallet
  SUBSCRIPTION_INIT_UPGRADE_ORDER: '/subscription/saveUpgradeOrder',
  GET_ORDER_BY_ID: '/subscription/getUpgradeOrder',
  GET_WALLET_DOCUMENT_BY_ID: '/subscription/getUpgradeWalletDocument',
  CANCEL_ORDER: '/subscription/cancelOrder',
  saveUpgradeOrderEndPoint: '/subscription/sendContract',
  generateContractUrlUpgrade: '/report/generateContract/upgrade',


  //  Login
  LOGIN: '/commons/login',
  LOAD_PERMISSIONS: '/commons/loadPermissions',

  //  OTP
  CHECK_TIMER_OTP: '/subscription/checkTimerOTP',

  //  Cash transaction
  GET_CASH_LIST_URL: '/report/getCashList',


  // oauth_endpoints

 TOKEN_ENDPOINT : '/commons/oauth/token',
 AUTHORIZE_ENDPOINT : '/commons/oauth/authorize',
//  TOKEN_ENDPOINT : '/commons/oauth/token',
//  AUTHORIZE_ENDPOINT : '/commons/oauth/authorize',
TOKEN_ENDPOINT_ADFS : 'https://oshop-orangemoneytest.meditel.int:4443/commons/oauth/token',
AUTHORIZE_ENDPOINT_ADFS : 'https://oshop-orangemoneytest.meditel.int:4443/commons/oauth/authorize',


  CLIENT_ID : 'myclient',
  CLIENT_SECRET : 'secret',

  DOMAIN_ORANGE_ADFS: 'oshop-orangemoneytest.meditel.int'

};


